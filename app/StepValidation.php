<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StepValidation extends Model
{
    protected $fillable = [
        'validation_id', 'step_id', 'status',
    ];

    public function step() {
        return $this->belongsTo(FinanceStepValidation::class, 'step_id');
    }
}
