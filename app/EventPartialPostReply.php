<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventPartialPostReply extends Model
{
    protected $table = 'event_partial_post_replies';
     protected $fillable = [
        'member_id','event_partial_post_id','reply_body'
    ];

     public function member()
    {
        return $this->belongsto(Member::class);
    }

     public function eventpost()
    {
        return $this->belongsto(EventPartialPost::class);
    }

}
