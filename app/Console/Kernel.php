<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use App\Notifications\SystemNotification;

use App\MticsEvent;
use App\Meeting;
use App\Borrower;
use App\Member;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            MticsEvent::whereDate('end_date', '<' ,date('Y-m-d'))
                ->where('status', 'on-going')
                ->update(['status' => 'done']);
        })->daily();

        $schedule->call(function () {
            Meeting::whereDate('when', '<', date('Y-m-d'))
                ->where('status', 'scheduled')
                ->update(['status' => 'done']);
        })->daily();

        $schedule->call(function () {
            $borrowers = Borrower::select('member_id')->whereDate('created_at', '<', date('Y-m-d'))
                    ->where('borrower_status', 'Borrowed')
                    ->get();

            foreach ($borrowers as $borrower) {
                $data = array(
                    'message' => "Please return the item that you borrowed immediately.",
                    'redirect' => array('role' => 'external',
                    'event_id' => 1),
                    'origin' => 'finance',
                );

                $member = Member::findOrFail($borrower->member_id);
                $member->notify(new SystemNotification($data));
            }

        })->everyMinute();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
