<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventoryCategory extends Model
{
    protected $table = 'inventory_categories';
	protected $fillable = [
        'inv_cat_name','inv_cat_displayname'
    ];


      public function Inventory()
    {
        return $this->hasMany(Inventory::class, 'inventory_category_id');
    }
}
