<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    public function member(){
        return $this->belongsTo('App\Member', 'member_id');
    }

    public function option(){
        return $this->belongsTo('App\Option', 'option_id');
    }
}
