<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MeetingAbsence extends Model
{
    protected $table = 'meeting_absences';
     protected $fillable = [
       'meeting_id','member_id','admin_id','role_id'
    ];
}
