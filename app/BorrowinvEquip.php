<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BorrowinvEquip extends Model
{
     protected $table = 'borrowinv_equips';
	protected $fillable = [
        'borrower_inventory_id','equip_inventory_id'
    ];

     public function borrowerinventory()
    {
        return $this->hasMany('App\BorrowerInventory');
    }

    public function equip(){
        return $this->belongsTo('App\EquipInventory', 'equip_inventory_id');
    }
}
