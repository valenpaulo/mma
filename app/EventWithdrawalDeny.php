<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventWithdrawalDeny extends Model
{
    protected $table = 'event_withdrawal_denies';
	protected $fillable = [
   		'event_withdrawal_id','faculty_id','denied_reason'
    ];
}
