<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PenaltyImage extends Model
{
    protected $table = 'penalty_images';
	protected $fillable = [
    'penalty_id','admin_id','image_name'
    ];

     public function penalty()
    {
        return $this->belongsto(Penalty::class);
    }

     public function admin()
    {
        return $this->belongsto(Member::class,'admin_id');
    }
}
