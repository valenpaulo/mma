<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MticsWithdrawalReport extends Model
{
    protected $table = 'mtics_withdrawal_reports';
	protected $fillable = [
   		'mtics_withdrawal_id','faculty_id','withdrawal_title','withdrawal_desc'
    ];

    public function faculty() {
        return $this->belongsTo(Member::class, 'faculty_id');
    }

}
