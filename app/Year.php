<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Year extends Model
{
    protected $table = 'years';
	protected $fillable = [
        'year_num','year_code','year_desc','mticsfund_amt'
    ];

     public static function getId($model, $table, $value) 
   {
    return $model::where($table, $value)->first()->id;
    }

}
