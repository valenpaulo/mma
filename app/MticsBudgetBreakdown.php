<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MticsBudgetBreakdown extends Model
{
    protected $table = 'mtics_budget_breakdowns';
	protected $fillable = [
       'mtics_budget_id','mtics_budget_exp_cat_id','budget_bd_desc','budget_bd_amt'
    ];

    public function budget_expenses()
    {
    	return $this->hasMany(MticsBudgetExpense::class,'mtics_budget_breakdown_id');
    }

    public function budget_expenses_cat()
    {
    	return $this->belongsTo(MticsBudgetExpCat::class,'mtics_budget_exp_cat_id');
    }


    public function budget()
    {
        return $this->belongsTo(MticsBudget::class,'mtics_budget_id');
    }
}
