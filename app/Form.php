<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    //
    protected $fillable = [
        'title', 'description',
    ];


    public function member(){
        return $this->belongsTo('App\Member', 'member_id');
    }

    public function question(){
        return $this->hasMany('App\Question');
    }

    public function respondent(){
        return $this->hasMany('App\Respondent');
    }
}
