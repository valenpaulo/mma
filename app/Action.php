<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    protected $table = 'actions';
	protected $fillable = [
        'action_name','action_display_name'
    ];

    public static function getId($model, $table, $value)
   {
    return $model::where($table, $value)->first()->id;
    }

     public function action_member()
    {
        return $this->hasMany('App\ActionMember');
    }
}
