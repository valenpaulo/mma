<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $table = 'tasks';
	protected $fillable = [
        'member_id','admin_id','event_id','due_date','task_desc','task_status','task_name','role_id'
    ];

     public function task_deny()
    {
        return $this->hasMany(TaskDeny::class);
    }

     public function mticspurchaselist()
    {
        return $this->hasMany(MticsPurchaseList::class);
    }

    public function event()
    {
        return $this->belongsTo('App\MticsEvent', 'event_id');
    }

    public function disapprovedLetter()
    {
        return $this->hasMany('App\DisapprovedLetter');
    }

    public function eventpurchaselist()
    {
        return $this->hasMany(EventPurchaseList::class);
    }

    public function moneyrequest_all()
    {
        return $this->hasMany(MoneyRequest::class);

    }

    public function requestmoney()
    {
        return $this->hasOne(MoneyRequest::class);

    }

    public function moneyrequest()
    {
        return $this->hasMany(MoneyRequest::class)->where('mon_req_status','pending');

    }

     public function moneyrequest_ongoing()
    {
        return $this->hasMany(MoneyRequest::class)->where('mon_req_status','for validation')->orwhere('mon_req_status','ongoing');

    }


    public function member()
    {
        return $this->belongsTo(Member::class, 'member_id');
    }

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }

    public function admin()
    {
        return $this->belongsTo(Member::class, 'admin_id');
    }

    public function docu()
    {
        return $this->hasMany(Document::class);
    }
}
