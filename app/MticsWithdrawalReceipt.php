<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MticsWithdrawalReceipt extends Model
{
    protected $table = 'mtics_withdrawal_receipts';
	protected $fillable = [
   		'mtics_withdrawal_id','faculty_id','receipt_image'
    ];

    public function receipt() {
        return $this->hasOne(EventWithdrawalReceipt::class);
    }

}
