<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventPayment extends Model
{
    protected $table = 'event_payments';
	protected $fillable = [
        'payment_id','event_id','payevent_amt','paymevent_status', 'member_id', 'admin_id'
    ];

    public function member() {
        return $this->belongsTo('App\Member', 'member_id');
    }


    public function admin() {
        return $this->belongsTo('App\Member', 'admin_id');
    }

    public function event() {
        return $this->belongsTo('App\MticsEvent', 'event_id');
    }
}
