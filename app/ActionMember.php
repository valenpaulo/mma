<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActionMember extends Model
{
     protected $table = 'action_members';
	protected $fillable = [
        'action_id','member_id'
    ];

     public static function getId($model, $table, $value)
   {
    return $model::where($table, $value)->first()->id;
    }

    public function member()
    {
        return $this->belongsTo('App\Member', 'member_id');
    }

    public function action()
    {
        return $this->belongsTo('App\Action', 'action_id');
    }
}
