<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventPartialPost extends Model
{
     protected $table = 'event_partial_posts';
     protected $fillable = [
        'event_id','admin_id','admin_role_id','title','body','slug','status'
    ];

    public function admin(){
        return $this->belongsTo('App\Member', 'admin_id');
    }

    public function event(){
        return $this->belongsTo('App\MticsEvent', 'event_id');
    }

    public function role(){
        return $this->belongsTo('App\Role', 'admin_role_id');
    }

    public function reply()
    {
        return $this->hasMany(EventPartialPostReply::class)->orderBy('id','DESC');
    }
}
