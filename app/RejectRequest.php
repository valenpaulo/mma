<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RejectRequest extends Model
{
    protected $table = 'reject_requests';
	protected $fillable = [
        'rej_req_member_id','rej_admin_member_id','rej_reason'
    ];
}
