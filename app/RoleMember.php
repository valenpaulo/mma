<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleMember extends Model
{
    protected $table = 'role_members';
	protected $fillable = [
        'role_id','member_id'
    ];

public static function getId($model, $table, $value)
   {
    return $model::where($table, $value)->first()->id;
    }

    public function role()
    {
        return $this->belongsTo(Role::class,'role_id');
    }

    public function member()
    {
        return $this->belongsTo(Member::class,'member_id');
    }
}
