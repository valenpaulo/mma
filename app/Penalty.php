<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penalty extends Model
{
    protected $table = 'penalties';
     protected $fillable = [
        'task_id','member_id','validator_id','reason','penalty_type','penalty_status','fee','event_id'
    ];

     public function task()
    {
        return $this->belongsTo(Task::class, 'task_id');
    }

    public function member(){
        return $this->belongsTo('App\Member', 'member_id');
    }

     public function image()
    {
        return $this->hasmany(PenaltyImage::class, 'penalty_id');
    }
}
