<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;
use App\Member;
use App\Section;
use App\MticsEvent;
use App\EventPartialPost;
use App\Role;
use App\RoleMember;
use App\Filterword;
use App\Task;
use App\AuditorFinancialApprove;

use App\Events\SystemEvent;
use Illuminate\Support\Facades\Event;


class InformationController extends Controller
{


    public function view(Request $request) {
        if(!Gate::allows('info-only'))
        {
        return redirect('/');
        }

        if ($request->q) {
            $section = Section::where('section_code', 'LIKE',"%{$request->q}%")->orWhere('section_code','LIKE',"%{$request->q}%")->first();
            if (count($section) != 0) {
                $members = Member::where('section_id', $section->id)->get();

            } else {

                $members = Member::where('first_name','LIKE',"%{$request->q}%")->orWhere('last_name','LIKE',"%{$request->q}%")->orWhere('mobile_no','LIKE',"%{$request->q}%")->get();
            }

            return view('officers/information', compact('members'));
        }

        $members = Member::all();
        return view('officers/information', compact('members'));
    }

    public function send() {
        \Nexmo::message()->send([
            'to'   => '639063441255',
            'from' => '639055442539',
            'text' => 'MTICS LARAVEL SMS Sample.'
        ]);

        echo 'sent!';
    }

    public function itexmo(Request $request){

        $ch = curl_init();
        $itexmo = array('1' => $request->mobile, '2' => $request->message, '3' => 'TR-LARAV442539_B94KL');
        curl_setopt($ch, CURLOPT_URL,"https://www.itexmo.com/php_api/api.php");
        curl_setopt($ch, CURLOPT_POST, 1);
         curl_setopt($ch, CURLOPT_POSTFIELDS,
                  http_build_query($itexmo));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_exec ($ch);
        curl_close ($ch);
        return redirect()->back();
    }



//MANAGE EVENT //MANAGE EVENT //MANAGE EVENT //MANAGE EVENT //MANAGE EVENT //MANAGE EVENT //MANAGE EVENT

    function wordFilter($text)
      {
        $filtered_text = $text;
        $filterwords = Filterword::all();

        foreach ($filterwords as $filterword)
        {
            $profanity_word = $filterword->Filter_word;

            $wordfirst = $profanity_word[0];
            $wordlength = strlen($profanity_word);
            $char = '*';
            for($x=3;$x<=$wordlength;$x++)
            {
                $char = $char.'*';
            }
            $replace_word = $wordfirst.$char;
            $filtered_text = str_ireplace($profanity_word, $replace_word, $filtered_text);
        }

        return $filtered_text;
      }

    ///////////////////////////////////

    public function eventView($id){
        if(!Gate::allows('info-only'))
        {
        return redirect('/');
        }
        $event = MticsEvent::find($id);

        $info_tasks = Task::where('event_id', $id)->where(function($q) {
                $q->where('task_status', 'pending')->orWhere('task_status', 'on-going')->orWhere('task_status', 'for validation')->orWhere('task_status', 'disapproved');
            })->whereHas('role', function($q){
                $q->where('name', 'info');
            })->get();

        $eventpartials = EventPartialPost::where('event_id',$id)->where('status', '!=', 'archived')->get();

        return view('officers/event/info', compact('event','eventpartials','info_tasks'));
    }

    public function post_event($id){

        if(AuditorFinancialApprove::where('event_id',$id)->get()->isEmpty()){
            return redirect()->back()->withErrors('Liquidation for this Event has not been verified by Auditor, Please speak with your Executive Officer for Audit');
        }


        $event = MticsEvent::find($id);
        $roles = RoleMember::where('member_id',Auth::id())->get();
        foreach ($roles as $role) {
            $role_member = Role::where('id',$role->role_id)->where('assistant','false')->first();
        }

        if($event->status !== 'done'){
            return redirect()->back()->withErrors('Event status is '. $event->status);
        }

        if($event->post_status == 'posted'){
            return redirect()->back()->withErrors('Full detailed for this event has been published');
        }

        $slug = preg_replace('/\s+/', '-', $event->event_title);
        $event->post_status = 'posted';
        $event->post_admin_id = Auth::id();
        $event->post_admin_role_id = $role_member->id;
        $event->slug = $slug;
        $event->save();

        return redirect()->back();
    }


    public function post_partial_event(Request $request,$id){

         $this->validate($request, [
              'title' => 'required',
              'body' => 'required',
              ]);

        $event = MticsEvent::find($id);

        $info_id = Role::where('name','info')->first()->id;
        $info_asst_id = Role::where('name','asst_info')->first()->id;
        $role_id = '';
        if(!RoleMember::where('role_id',$info_id)->where('member_id',Auth::id())->get()->isEmpty()){
            $role_id = $info_id;
        }
        elseif(!RoleMember::where('role_id',$info_asst_id)->where('member_id',Auth::id())->get()->isEmpty()){
            $role_id = $info_asst_id;
        }
        else{
            return redirect()->back()->withErrors('invalid role');
        }



        if($event->status == 'done' and $event->post_status == 'pending'){
            return redirect()->back()->withErrors('Event status is '. $event->status.', Please post the full detailed of the event');
        }
        elseif($event->status == 'done' and $event->post_status == 'posted'){
            return redirect()->back()->withErrors('Full detailed for this event has been published');
        }



        $filterbody = $this->wordFilter($request->body);
        $filtertitle = $this->wordFilter($request->title);
        $slug = preg_replace('/\s+/', '-', $request->title);

        if(strpos($filtertitle, '*') == 1)
        {
            return redirect()->back()->withErrors("This Title contains Profanity, Please make sure your title doesn't contain profanity");
        }


        $eventpartial = New EventPartialPost;
        $eventpartial->event_id = $id;
        $eventpartial->admin_id =Auth::id();
        $eventpartial->admin_role_id = $role_id;
        $eventpartial->title = $filtertitle;
        $eventpartial->body = $filterbody;
        $eventpartial->slug = $slug;
        $eventpartial->save();

        Event::fire(new SystemEvent(auth::id(), 'Event Post Added.'));

        return redirect()->back()->with('success', 'Post Added!');
    }

    public function post_partial_edit_view($id,$partial_id,$slug){
         if(!Gate::allows('info-only'))
        {
        return redirect('/');
        }
        $eventpartial = EventPartialPost::findOrFail($partial_id);
        return view('officers/event/event_partial_post_editview',compact('eventpartial'));
    }

    public function post_partial_edit(Request $request,$id,$partial_id,$slug){
        $eventpartial = EventPartialPost::findOrFail($partial_id);

        $this->validate($request, [
              'title' => 'required',
              'body' => 'required',
              ]);

        $info_id = Role::where('name','info')->first()->id;
        $info_asst_id = Role::where('name','asst_info')->first()->id;
        $role_id = '';
        if(!RoleMember::where('role_id',$info_id)->where('member_id',Auth::id())->get()->isEmpty()){
            $role_id = $info_id;
        }
        elseif(!RoleMember::where('role_id',$info_asst_id)->where('member_id',Auth::id())->get()->isEmpty()){
            $role_id = $info_asst_id;
        }
        else{
            return redirect()->back()->withErrors('invalid role');
        }


        $filterbody = $this->wordFilter($request->body);
        $filtertitle = $this->wordFilter($request->title);
        $slug = preg_replace('/\s+/', '-', $request->title);
         if(strpos($filtertitle, '*') == 1)
        {
            return redirect()->back()->withErrors("This Title contains Profanity, Please make sure your title doesn't contain profanity");
        }

        $eventpartial->event_id = $id;
        $eventpartial->admin_id =Auth::id();
        $eventpartial->admin_role_id = $role_id;
        $eventpartial->title = $filtertitle;
        $eventpartial->body = $filterbody;
        $eventpartial->slug = $slug;
        $eventpartial->save();

        Event::fire(new SystemEvent(auth::id(), 'Event Post Edited.'));

        return redirect('admin/info/manage-event/'.$id)->with('success', 'Post Edited!');
    }


    public function post_partial_delete($id,$partial_id,$slug){
        $eventpartial = EventPartialPost::findOrFail($partial_id);
        $eventpartial->status = 'archived';
        $eventpartial->save();

        Event::fire(new SystemEvent(auth::id(), 'Event Post Deleted.'));

        return redirect()->back()->with('success', 'Post Deleted!');
    }




}
