<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;
use App\Notifications\SystemNotification;
use Illuminate\Support\Facades\Event;
use App\Events\SystemEvent;
use App\FileCategory;
use App\Document;
use App\Role;
use App\Task;
use App\MticsEvent;
use App\Member;
use App\DisapprovedLetter;
use App\EventGallery;
use App\EventAbsence;
use App\EventMember;
use App\Section;
use App\Course;
use App\Year;
use App\RoleMember;
use App\ActionMember;
use App\Action;
use App\Meeting;
use App\MeetingAbsence;
use App\EventPresentAttendee;
use App\Penalty;
use App\Officerterm;


class DocumentationController extends Controller
{
    public function view() {

        if(!Gate::allows('docu-only'))
        {
        return redirect('/');
        }


        $file_categories = FileCategory::all();

        $term = Officerterm::where('status','on-going')->first();
        $documents = Document::where('updated_at','>=',date($term->created_at))->orderBy('id', 'desc')->get();

        return view('officers/document', compact('file_categories', 'documents'));
    }

    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'import_file' => 'required|mimes:docx,doc,pdf,zip',
        ], [
             'import_file.required' => 'Please make sure to upload a file.',
        ]);

        $file_category = FileCategory::where('id', $request->file_category)->first();

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }


        $file = $request->file('import_file');
        if($request->hasFile('import_file'))
        {
            if($request->year !== null)
            {
                 $this->validate($request, [
                'year' => 'required|size:4',
                ]);
                $year = $request->year;
            }
            else
            {
                $year = date("Y");
            }
            $string = $file->getClientOriginalName();
            $remove_hyphen_string = str_replace("‐","_",$string);


             if(url('/') == 'http://mtics-ma.tuptaguig.com'){
            $file->move('public/documents/'.$file_category->category_name.'/'.$year.'/', $year.'-'.$remove_hyphen_string);
                }
                else{

            $file->move('documents/'.$file_category->category_name.'/'.$year.'/', $year.'-'.$remove_hyphen_string);
                }
            $filename =  $file_category->category_name.'/'.$year.'/'.$year.'-'.$remove_hyphen_string;



            Document::create([
                'filename' => $filename,
                'category_id' => $request->file_category
,            ]);

        }

    Event::fire(new SystemEvent(auth::id(), 'Added a New Document.'));

    return redirect()->back()->with('success', 'Document Added!');;
    }

    public function download($id) {
        $doc = Document::where('id', $id)->first();
        $file= public_path(). "/documents/". $doc->filename;
        $filename = explode("-", $doc->filename)[1];
        return response()->download($file, $filename);
    }

    public function delete($id) {
        $doc = Document::where('id', $id)->first();
        if(url('/') == 'http://mtics-ma.tuptaguig.com'){
            File::delete('public/documents/' . $doc->filename);
            }
          else{
            File::delete('documents/' . $doc->filename);
            }
        $doc->delete();

        Event::fire(new SystemEvent(auth::id(), 'Document Deleted.'));

        return redirect()->back()->with('success', 'Document Deleted!');;
    }

    public function eventView($id) {

        if(!Gate::allows('docu-only'))
        {
        return redirect('/');
        }
        // event
        $event = MticsEvent::findOrFail($id);

        // documents
        $file_categories_letter = FileCategory::where('category_name', 'letter')->first();
        $documents = Document::where('category_id', $file_categories_letter->id)->whereHas('task', function($q) use ($id) {
            $q->where('event_id', $id)->where('task_status', 'done');
        })->get();


        // internal tasks
        $internal_id = Role::where('name', 'internal')->first()->id;

        $validate_documents = Document::where('category_id', $file_categories_letter->id)->whereHas('task', function($q) use ($id, $internal_id) {
            $q->where('event_id', $id)->where('task_status', 'for validation')->where('role_id', $internal_id);
        })->get();

        $validate_tasks = Task::where('role_id', $internal_id)->where(function($q) {
            $q->where('task_status', 'for validation')->orWhere('task_status', 'disapproved');
        })->where('event_id', $id)->get();


        $tasks = Task::where(function ($q) {
            $q->where('task_status', 'pending')->orwhere('task_status', 'on-going')->orwhere('task_status', 'for validation')->orwhere('task_status', 'disapproved')->orwhere('task_status', 'done');
        })->where('member_id', Auth::user()->id)->where('event_id', $id)->get();

        // docu
        $task_docus = Document::whereHas('task', function($q) use ($id) {
            $q->where('event_id', $id)->where('task_status', 'done');
        })->get();

        $ready_for_validation_documents = Document::whereHas('task', function($q) use ($id) {
            $q->where('event_id', $id)->where('task_status', 'on-going')->where('member_id', Auth::user()->id);
        })->get();

        $tobe_validated_documents = Document::whereHas('task', function($q) use ($id) {
            $q->where('event_id', $id)->where('task_status', 'for validation')->where('member_id', Auth::user()->id);
        })->get();

        $disapproved_documents = Document::whereHas('task', function($q) use ($id) {
            $q->where('event_id', $id)->where('task_status', 'disapproved')->where('member_id', Auth::user()->id);
        })->get();

        // documentation tasks

        $file_categories = FileCategory::all();


        return view('officers/event/documentation', compact('validate_tasks', 'tasks', 'event', 'documents', 'validate_documents','file_categories', 'task_docus', 'ready_for_validation_documents', 'tobe_validated_documents', 'disapproved_documents'));
    }

    public function approve($id) {
        $task = Task::findOrFail($id);

        $date = date('Y-m-d');
        $due_date = $task->due_date;
        $duedate_with3days = date('Y-m-d', strtotime($due_date. ' + 3 days'));


        if($duedate_with3days < $date){
        $task->task_status = 'failed';


        }
        else{
        $task->task_status = 'done';
        }

        if($task->task_status == 'failed'){
            $penaltylastrow = Penalty::where('member_id',$task->member_id)->orderby('id','desc')->first();
            $penalty = New Penalty;
            $penalty->task_id = $task->id;
            $penalty->member_id = $task->member_id;
            $penalty->validator_id = Auth::id();
            $penalty->reason = 'overdue task';

            if($penaltylastrow == null){
            $penalty->penalty_type = 'verbal';
            }
            elseif($penaltylastrow->penalty_type == 'fee'){
            $penalty->penalty_type = 'verbal';
            }
            elseif($penaltylastrow->penalty_type == 'verbal'){
            $penalty->penalty_type = 'written';
            }
            elseif($penaltylastrow->penalty_type == 'written'){
            $penalty->penalty_type = 'fee';
            $penalty->fee = 10;
            }

            $penalty->save();

        }

        $task->save();




        $data = array(
            'message' => $task->task_name . ' task has been approved',
            'redirect' => array('role' => 'internal', 'event_id' => $task->event_id),
            'origin' => 'docu',
            );

        $member = Member::findOrFail($task->member_id);
        $member->notify(new SystemNotification($data));

        $data = array(
            'message' => $task->task_name . ' task has been approved',
            'redirect' => array('role' => 'president', 'event_id' => $task->event_id),
            'origin' => 'docu',
            );

        $member = Member::findOrFail($task->admin_id);
        $member->notify(new SystemNotification($data));

        Event::fire(new SystemEvent(auth::id(), 'Letter is Approved.'));

        return redirect()->back()->with('success', 'Approved!');
    }

      public function disapprove(Request $request, $id) {
        $task = Task::findOrFail($id);
        $task->task_status = 'disapproved';
        $task->save();

        DisapprovedLetter::create([
                'task_id' => $id,
                'validator_id' => Auth::user()->id,
                'reason' => $request->reason,
            ]);


        $data = array(
            'message' => $task->task_name . ' task has been disapproved',
            'redirect' => array('role' => 'internal', 'event_id' => $task->event_id),
            'origin' => 'docu',
            );

        $member = Member::findOrFail($task->member_id);
        $member->notify(new SystemNotification($data));

        Event::fire(new SystemEvent(auth::id(), 'Letter is Disapproved.'));

        return redirect()->back()->with('success', 'Disapproved!');
    }


     public function event_store(Request $request,$id) {

        $validator = Validator::make($request->all(), [
            'import_file' => 'required|mimes:docx,doc,pdf,zip',
        ], [
             'import_file.required' => 'Please make sure to upload a file.',
        ]);

        $file_category = FileCategory::where('id', $request->file_category)->first();

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }


        $file = $request->file('import_file');
        if($request->hasFile('import_file'))
        {
            if($request->year !== null)
            {
                 $this->validate($request, [
                'year' => 'required|size:4',
                ]);
                $year = $request->year;
            }
            else
            {
                $year = date("Y");
            }


            $event_name = Task::findOrFail($id)->event->event_title;

            $category= FileCategory::where('category_name','Letter')->first();

            if(isset($request->internal))
            {
                $category_name = $category->category_name;
            }
            else
            {
                $category_name= $file_category->category_name;
            }

             if(url('/') == 'http://mtics-ma.tuptaguig.com'){
            $file->move('public/documents/'.$category_name.'/'.$year.'/'.$event_name.'/', $year.'-'.$file->getClientOriginalName());
                }
                else{
            $file->move('documents/'.$category_name.'/'.$year.'/'.$event_name.'/', $year.'-'.$file->getClientOriginalName());
                }
            $filename =  $category_name.'/'.$year.'/'.$event_name.'/'.$year.'-'.$file->getClientOriginalName();

            Document::create([
                'filename' => $filename,
                'category_id' => isset($request->internal) ? $category->id : $request->file_category,
                'task_id' => $id,
            ]);
        }
    Event::fire(new SystemEvent(auth::id(), 'Added a New Document.'));

    return redirect()->back()->with('success', 'Document Added!');
    }

    public function validation($id){
        $task = Task::findOrFail($id);
        $task->task_status = 'for validation';
        $task->save();

        $filename = Document::where('task_id', $task->id)->first()->filename;

        $data = array(
            'message' => 'Validate Document: '. explode("/", $filename)[3],
            'redirect' => array('role' => 'president', 'event_id' => $task->event_id),
            'origin' => 'Document',
            );

        $role_id = Role::where('name', 'president')->first()->id;
        $member = Member::whereHas('role', function($q) use ($role_id) {
            $q->where('role_id', $role_id);
        })->first();
        $member->notify(new SystemNotification($data));

        Event::fire(new SystemEvent(auth::id(), 'Sent a Request for Letter Validation.'));

        return redirect()->back()->with('success', 'Sent Request!');

    }



    public function gallery_view($id){

        if(!Gate::allows('docu-only'))
        {
        return redirect('/');
        }

        $eventphotos = EventGallery::where('event_id',$id)->get();
        $event = MticsEvent::find($id);
        return view('officers/event/event_gallery', compact('eventphotos','event'));
    }

    public function upload_photo(Request $request,$id){

        $event = MticsEvent::find($id);

        $this->validate($request, [
        'upload.*' => 'image|mimes:jpeg,jpg,png|max:5000', //image only
        ]);
        if($request->hasFile('upload')){
            //CREATE DIRECTORY FOLDER FOR EVENT

            $folderpath = 'images/event_gallery/'.$event->event_title;
            $path = public_path($folderpath);
            if(!file_exists($path)){
                File::makeDirectory($path);
            }

            if($request->hasFile('upload')){
                foreach ($request->file('upload') as $upload) {

                    if($upload->getmimeType() !== 'image/jpeg' and $upload->getmimeType()!== 'image/png')
                    {
                      continue;
                    }
                    else
                    {
                        $date = date('Y-m-d');
                        $time = round(microtime(true) * 1000);
                        $fileName = $upload->getClientOriginalName();
                        $image_name = 'event_gallery/'.$event->event_title.'/'.$id.'_'.$time.'_'.$date.'.jpg';
                        //move image to images/event_gallery/'title of event'

                         if(url('/') == 'http://mtics-ma.tuptaguig.com'){
                            $upload->move('public/images/event_gallery/'.$event->event_title.'/', $id.'_'.$time.'_'.$date.'.jpg');
                            }
                            else{
                            $upload->move('images/event_gallery/'.$event->event_title.'/', $id.'_'.$time.'_'.$date.'.jpg');
                            }

                        $event_gallery = new EventGallery;
                        $event_gallery->event_id = $id;
                        $event_gallery->member_id = Auth::id();
                        $event_gallery->image_name = $image_name;
                        $event_gallery->save();

                    }
                }
            }
        }

        $docu_id = Role::where('name','docu')->first()->id;
        $asst_docu_id = Role::where('name','asst_docu')->first()->id;
        $isAdmin = false;

        if(!RoleMember::where('role_id',$docu_id)->where('member_id',Auth::id())->get()->isEmpty())
            {
                $isAdmin = true;
            }
        elseif(!RoleMember::where('role_id',$asst_docu_id)->where('member_id',Auth::id())->get()->isEmpty())
            {
                $isAdmin = true;
            }
        else{$isAdmin=false;}

        if($isAdmin == true)
        {
        return redirect('admin/docu/manage-event/'.$id.'/gallery');
        }
        else
        {
        return redirect('member/'.$id.'/'.$event->slug);
        }
    }

    public function archived_photo(Request $request,$id,$photo_id){
        $event_gallery = EventGallery::find($photo_id);
        $event_gallery->image_status = 'archived';
        $event_gallery->save();
        return redirect('admin/docu/manage-event/'.$id.'/gallery');
    }


    public function delete_photo(Request $request,$id,$photo_id){
        $event_gallery = EventGallery::find($photo_id);
        if(url('/') == 'http://mtics-ma.tuptaguig.com'){
            File::delete('public/images/' . $event_gallery->image_name);
            }
          else{
            File::delete('images/' . $event_gallery->image_name);
            }
        $event_gallery->delete();


        $event = MticsEvent::find($id);
        $docu_id = Role::where('name','docu')->first()->id;
        $asst_docu_id = Role::where('name','asst_docu')->first()->id;
        $isAdmin = false;

        if(!RoleMember::where('role_id',$docu_id)->where('member_id',Auth::id())->get()->isEmpty())
            {
                $isAdmin = true;
            }
        elseif(!RoleMember::where('role_id',$asst_docu_id)->where('member_id',Auth::id())->get()->isEmpty())
            {
                $isAdmin = true;
            }
        else{$isAdmin=false;}

        if($isAdmin == true)
        {
        return redirect('admin/docu/manage-event/'.$id.'/gallery');
        }
        else
        {
        return redirect('member/'.$id.'/'.$event->slug);
        }
    }

    public function upload_theme(Request $request,$id)
    {

        $event = MticsEvent::find($id);

        $this->validate($request, [
            'theme' => 'image|mimes:jpeg,jpg,png|max:10000', //image only
          ]);



        $file = $request->file('theme');
        $date = date('Y-m-d');
        $time = time();
        //move to public/images/event_gallery/$event_title
        if($request->hasFile('theme')){

            if($event->theme_image !== null )
            {

                 if(url('/') == 'http://mtics-ma.tuptaguig.com'){
                File::delete('public/images/' .$event->theme_image);
                }
                else{
                File::delete('images/' .$event->theme_image);
                }
            }


        if(url('/') == 'http://mtics-ma.tuptaguig.com'){
        $file->move('public/images/event_gallery/'.$event->event_title.'/', '_theme_'.$id.'_'.$time.'_'.$date.'.jpg');
        }
        else{
        $file->move('images/event_gallery/'.$event->event_title.'/', '_theme_'.$id.'_'.$time.'_'.$date.'.jpg');
        }

        $image_name = 'event_gallery/'.$event->event_title.'/'.'_theme_'.$id.'_'.$time.'_'.$date.'.jpg';
        $event->theme_image = $image_name;
        $event->save();

        }
        return redirect('admin/docu/manage-event/'.$id);
    }




//MANAGE ATTENDANCE //MANAGE ATTENDANCE //MANAGE ATTENDANCE //MANAGE ATTENDANCE

    public function attendance_view($id){
        //attendance disable
        return redirect('/admin');

        $event = MticsEvent::find($id);
        $event_participants = EventMember::with('year')->with('course')->where('event_id',$id)->get();
        if($event_participants->isEmpty()){

            return redirect()->back()->withErrors('Participants for this event is not set, please speak with your president');
        }

        $members = Member::wherehas('section_active_currentstudent')->with('section')->where('status','active')->get();
        $absences = EventAbsence::where('event_id',$id)->get();

        return view('admin/addFeature/manage_event_attendance',compact('event','event_participants','members','absences'));

    }

    public function attendance_setabsent(Request $request,$id){


        if(!isset($request->member_id)){
            return redirect()->back()->withErrors('Please select student');
        }


        $finalrole = $this->CheckAttendanceRole();

        if($finalrole == ""){
            return redirect()->back()->withErrors('Invalid admin, you cannot process this request');
        }


        foreach ($request->member_id as $key => $value) {
            if(EventAbsence::where('event_id',$id)->where('member_id',$value)->get()->isEmpty()){

                    EventAbsence::create([
                        'event_id' => $id,
                        'member_id' => $value,
                        'admin_id' => Auth::id(),
                        'role_id' => $finalrole->id,
                    ]);

            }
            else{

             $member = Member::find($value);
             $errors[] = $member->first_name.' '.$member->last_name.' is already set as absent';
             continue;
            }
        }

         if (isset($errors)){
                    return redirect('admin/docu/manage-event/'.$id.'/attendance')->withErrors($errors);
                }

        Event::fire(new SystemEvent(auth::id(), 'Marked as present.'));

        return redirect('admin/docu/manage-event/'.$id.'/attendance')->with('success', 'Marked As Absent!');

    }

    public function attendance_setpresent(Request $request,$id,$absent_id){
        $absence = EventAbsence::find($absent_id);
        $absence->delete();
        return redirect('admin/docu/manage-event/'.$id.'/attendance')->with('success', 'Marked As Present!');
    }

    public function presentAttendee($event_id, $member_id) {
        $finalrole = $this->CheckAttendanceRole();
        EventPresentAttendee::create([
                'event_id' => $event_id,
                'member_id' => $member_id,
                'admin_id' => Auth::user()->id,
                'role_id' => $finalrole->id,
            ]);
    }

//MANAGE MEETING //MANAGE MEETING //MANAGE MEETING //MANAGE MEETING //MANAGE MEETING //MANAGE

    public function meeting_view($id){
        if(!Gate::allows('docu-only'))
        {
        return redirect('/');
        }
        $event = MticsEvent::find($id);
        $meetings = Meeting::where('event_id',$id)->get();
       return view('officers/event/docu_manage_meeting', compact('meetings','event'));
    }

    public function meeting_attendance_view($id,$meeting_id){
        if(!Gate::allows('docu-only'))
        {
        return redirect('/');
        }
       $meeting = Meeting::find($meeting_id);
       $event = MticsEvent::find($id);
       $officers = RoleMember::wherehas('role',function($q) {$q->where('name','!=','faculty')->where('name','!=','super_admin')->where('name','!=','mtics_adviser');})->groupby('role_members.member_id')->select('role_members.member_id')->with('member')->get();


       $absences = MeetingAbsence::where('meeting_id',$meeting_id)->get();

       return view('officers/event/manage_meeting_attendance', compact('meeting','officers','event','absences','admins'));
    }


    public function meeting_attendance_setabsent(Request $request,$id,$meeting_id){


        if(!isset($request->officer_id)){
            return redirect()->back()->withErrors('Please select student');
        }

        $finalrole = $this->CheckAttendanceRole();


        if($finalrole == ""){
            return redirect()->back()->withErrors('Invalid admin, you cannot process this request');
        }


        foreach ($request->officer_id as $key => $value) {
            if(MeetingAbsence::where('meeting_id',$meeting_id)->where('member_id',$value)->get()->isEmpty()){

                    MeetingAbsence::create([
                        'meeting_id' => $meeting_id,
                        'member_id' => $value,
                        'admin_id' => Auth::id(),
                        'role_id' => $finalrole->id,
                    ]);

            }
            else{

             $member = Member::find($value);
             $errors[] = $member->first_name.' '.$member->last_name.' is already set as absent';
             continue;
            }
        }

         if (isset($errors)){
                    return redirect('admin/docu/manage-event/'.$id.'/meeting/'.$meeting_id.'/attendance')->withErrors($errors);
                }
        return redirect('admin/docu/manage-event/'.$id.'/meeting/'.$meeting_id.'/attendance');

    }


     public function meeting_attendance_setpresent(Request $request,$id,$meeting_id,$absent_id){

        $absence = MeetingAbsence::find($absent_id);
        $absence->delete();

        Event::fire(new SystemEvent(auth::id(), 'Marked as abset.'));

        return redirect('admin/docu/manage-event/'.$id.'/meeting/'.$meeting_id.'/attendance');
    }

    private function CheckAttendanceRole(){

        $action = Action::where('action_name','manage_attendance')->first();
        $action_member = ActionMember::where('action_id',$action->id)->get();
        $docu = Role::where('name','docu')->first();
        $asst_docu = Role::where('name','asst_docu')->first();
        $finalrole = '';
        $role_members = RoleMember::where('member_id',Auth::id())->get();
        if(RoleMember::where('member_id',Auth::id())->where('role_id',$docu->id)->get()->isEmpty()){
            if(RoleMember::where('member_id',Auth::id())->where('role_id',$asst_docu->id)->get()->isEmpty()){
                if(ActionMember::where('member_id',Auth::id())->where('action_id',$action->id)->get()->isEmpty()){

                    foreach ($role_members as $role_member) {
                        $role = Role::where('id',$role_member->role_id)->first();
                        if($role->name !== 'asst_internal' or $role->name !== 'asst_external' or $role->name !== 'asst_finance' or $role->name !== 'asst_auditor' or $role->name !== 'asst_info' or $role->name !== 'asst_activity' or $role->name !== 'asst_logistic'){

                            $finalrole = Role::where('id',$role_member->role_id)->first();
                        }
                        else{
                            continue;
                        }
                    }
                }
            }
            else{
                $finalrole = $asst_docu;
            }
        }
        else{
            $finalrole = $docu;
        }

        return $finalrole;

    }

    function mticsBylaws() {
        return view('officers/bylaws');
    }

    function mticsBylawsUpdate(Request $request) {
        $validator = Validator::make($request->all(), [
            'import_file' => 'required|mimes:pdf',
        ], [
             'import_file.required' => 'Please make sure to upload a pdf file.',
        ]);

        $file_category = FileCategory::where('id', $request->file_category)->first();

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $file = $request->file('import_file');
        if($request->hasFile('import_file'))
        {
            if(url('/') == 'http://mtics-ma.tuptaguig.com'){
            $file->move('public/documents/bylaws/', 'mtics_bylaws.pdf');
            }
            else{
            $file->move('documents/bylaws/', 'mtics_bylaws.pdf');
            }
        }

        return redirect()->back()->with("success", "MTICS By-laws Updated!");
    }
}
