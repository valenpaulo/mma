<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\File;
use App\MticsEvent;
use App\EventOffer;
use App\EventOfferMember;
use App\Task;
use App\RoleMember;

use App\Events\SystemEvent;
use Illuminate\Support\Facades\Event;

class EventOffersController extends Controller
{


//ACTIVITIES VIEW //ACTIVITIES VIEW //ACTIVITIES VIEW //ACTIVITIES VIEW //ACTIVITIES VIEW


  public function eventoffers_view($id) {
      if(!Gate::allows('activity-only'))
        {
        return redirect('/');
        }
    	$event = MticsEvent::find($id);
      $event_offers = EventOffer::where('event_id',$id)->get();

      $activity_tasks = Task::where('event_id', $id)->where(function($q) {
          $q->where('task_status', 'pending')->orWhere('task_status', 'on-going')->orWhere('task_status', 'for validation')->orWhere('task_status', 'disapproved');
      })->whereHas('role', function($q){
          $q->where('name', 'activity');
      })->get();

		return view('officers/event/activities', compact('event','event_offers','activity_tasks'));
	}

    public function eventoffers_add(Request $request,$id) {

        $this->validate($request, [
            'offer_image' => 'image|mimes:jpeg,jpg,png|max:2000', //image only
            'offer_name' => 'required',
          ]);

        $event = MticsEvent::find($id);
        $date = date('Y-m-d');
        $time = time();
        $file = $request->file('offer_image');
        //Save to images/event_offers
        if($request->hasFile('offer_image')){
          if(url('/') == 'http://mtics-ma.tuptaguig.com'){
          $file->move('public/images/event_offers', $event->id.'_'.$request->offer_name.'_'.$time.'_'.$date.'_eventoffers.jpg');
            }
            else{
          $file->move('images/event_offers', $event->id.'_'.$request->offer_name.'_'.$time.'_'.$date.'_eventoffers.jpg');
            }

          $imgname =  "event_offers/".$event->id.'_'.$request->offer_name.'_'.$time.'_'.$date.'_eventoffers.jpg';

          $event_offer = new EventOffer;
          $event_offer->activities_id = Auth::id();
          $event_offer->event_id = $event->id;
          $event_offer->offer_name = $request->offer_name;
          $event_offer->offer_desc = $request->offer_desc;
          $event_offer->offer_image = $imgname;
          $event_offer->save();


         }

          Event::fire(new SystemEvent(auth::id(), 'Event Offer Added.'));

         return redirect('admin/activities/event-offers/'.$id)->with('success', 'Event Offer Added!');

    }

    public function eventoffers_edit(Request $request,$id,$eventoffer_id) {


      $this->validate($request, [
            'offer_name' => 'required',
          ]);

      $event_offer = EventOffer::find($eventoffer_id);

      if($event_offer->offer_status !== 'pending')
      {
        return redirect()->back()->withErrors('This Event offer is '.$event_offer->offer_status.', make changes are prohibited');
      }


        $event = MticsEvent::find($id);
        //Save to images/event_offers
        if($request->hasFile('offer_image')){
          $date = date('Y-m-d');
          $time = time();
          $file = $request->file('offer_image');

           if(url('/') == 'http://mtics-ma.tuptaguig.com'){
          $file->move('public/images/event_offers', $event->id.'_'.$request->offer_name.'_'.$time.'_'.$date.'_eventoffers.jpg');
            }
            else{
          $file->move('images/event_offers', $event->id.'_'.$request->offer_name.'_'.$time.'_'.$date.'_eventoffers.jpg');
            }
          $imgname =  "event_offers/".$event->id.'_'.$request->offer_name.'_'.$time.'_'.$date.'_eventoffers.jpg';

          $event_offer->offer_image = $imgname;
         }

          $event_offer->activities_id = Auth::id();
          $event_offer->event_id = $event->id;
          $event_offer->offer_name = $request->offer_name;
          $event_offer->offer_desc = $request->offer_desc;
          $event_offer->save();

          Event::fire(new SystemEvent(auth::id(), 'Event Offer Edited.'));

         return redirect('admin/activities/event-offers/'.$id)->with('success', 'Event Offer Edited!');

    }


    //FINANCE VIEW //FINANCE VIEW //FINANCE VIEW //FINANCE VIEW //FINANCE VIEW

    public function finance_eventoffers_edit(Request $request,$id,$eventoffer_id) {

    	$this->validate($request, [
            'offer_amount' => 'required|integer',
          ]);

    	if(!EventOfferMember::where('event_offer_id',$eventoffer_id)->get()->isEmpty())
    		{
		        return redirect()->back()->withErrors('Someone has been ordered this Event offers, make changes are prohibited');
    		}

    	$event_offer = EventOffer::find($eventoffer_id);
    	$event = MticsEvent::find($id);

    	$event_offer->offer_amt = $request->offer_amount;
    	$event_offer->finance_id = Auth::id();
    	$event_offer->offer_status = 'on-going';
    	$event_offer->save();

     return redirect('admin/finance/event-fund/'.$id.'/event-offers');
    }



    //EVENT OFFERS PAYMENT ORDER //EVENT OFFERS PAYMENT ORDER //EVENT OFFERS PAYMENT ORDER //EVENT OFFERS

    public function event_offers_payment(Request $request) {
        $this->validate($request, [
            'member_id' => 'required|integer',
            'eventoffers_id' => 'required|integer',
          ]);

        if($request->quantity == null){
          $quantity = 1;
        }
        else{
          $quantity = $request->quantity;
        }


        $role = RoleMember::where('member_id',Auth::id())->wherehas('role',function($q){$q->where('assistant','false');})->first();

        if($role == null){
          $role = RoleMember::where('member_id',Auth::id())->first();
        }

        $item = EventOffer::find($request->eventoffers_id);


        $order = new EventOfferMember;
        $order->event_offer_id = $request->eventoffers_id;
        $order->member_id = $request->member_id;
        $order->admin_id = Auth::id();
        $order->role_id = $role->role_id;
        $order->quantity = $quantity;

        $amount = $item->offer_amt * $quantity;
        $order->total = $amount;
        $order->desc = $request->desc;
        $order->save();


        return redirect()->back();

    }



    public function claimed(Request $request,$id) {
      $order = EventOfferMember::find($id);
      $order->item_status = 'claimed';
      $order->save();

      Event::fire(new SystemEvent(auth::id(), 'Processed Item Claim.'));

      return redirect()->back()->with('success', 'Item Claimed!');
    }
}
