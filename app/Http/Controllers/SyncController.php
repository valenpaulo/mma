<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\MticsfundPayment;
use App\EventPayment;
use App\Mticsfund;
use App\Member;
use App\Section;
use App\MticsEvent;
use App\Inventory;
use App\Borrower;
use App\BorrowerInventory;
use App\EquipInventory;
use App\BorrowinvEquip;


class SyncController extends Controller
{
    public function store(Request $request) {

        $member = Member::where('id_num', $request->student_id)->first();

        $receiver = Member::where('username', $request->received)->first();

        $type = (int) $request->for;

        if($request->for == 0) {

            $section = Section::findOrFail($member->section_id);
            $mticsfund_id = Mticsfund::where('year_id', $member->year_id)->where('course_id', $section->course_id)->first()->id;

            MticsfundPayment::create([
                    'member_id' => $member->id,
                    'mticsfund_id' => $mticsfund_id,
                    'paymticsfund_amt' => (int) $request->amount,
                    'paymticsfund_status' => 'paid',
                    'admin_id' => $receiver->id,
                ]);
        }

        else {

            $checkPayment = EventPayment::where('member_id', $member->id)->where('event_id', $type)->first();

            if(is_null($checkPayment)) {
                EventPayment::create([
                        'member_id' => $member->id,
                        'event_id' => $type,
                        'payevent_amt' => (int) $request->amount,
                        'paymevent_status' => 'paid',
                        'admin_id' => $receiver->id,
                    ]);
            }
        }


        return "Sync Succeed!";
    }

    public function syncBorrowedItems(Request $request) {
        $member_id = Member::select("id")->where("id_num", $request->student_id)->first()->id;

        $borrower = Borrower::where("member_id", $member_id)->where("borrower_status", "Borrowed")->first();
        if(is_null($borrower)) {
            $borrower = Borrower::create([
                    'member_id' => $member_id,
                    'borrower_status' => 'Borrowed',
                ]);
        }

        if($request->quantity > 0) {
            $inventory = Inventory::where("inv_name", $request->item_name)->first();

            BorrowerInventory::create([
                    'inventory_id' => $inventory->id,
                    'borrower_id' => $borrower->id,
                    'borrower_quantity' => $request->quantity
                ]);

            $inventory->inv_quantity -= (int) $request->quantity;
            $inventory->save();
        }
        else {
            $inv_specs = EquipInventory::where("serial_num", $request->item_name)->first();

            $borrower_inv = BorrowerInventory::create([
                    'inventory_id' => $inv_specs->inventory->id,
                    'borrower_id' => $borrower->id,
                    'borrower_quantity' => 1,
                    'borrower_desc' => '(-' . $inv_specs->brand_name . ' ' . $inv_specs->serial_num . ')'
                ]);

            BorrowinvEquip::create([
                    'borrower_inventory_id' => $borrower_inv->id,
                    'equip_inventory_id' => $inv_specs->id,
                ]);

            $inv_specs->status = "borrowed";
            $inv_specs->save();
        }

    }

    public function syncReturnedItems(Request $request) {
        $member_id = Member::select("id")->where("id_num", $request->student_id)->first()->id;

        $borrower = Borrower::where("member_id", $member_id)->where("borrower_status", "Borrowed")->first();

        if ($borrower) {

            $borrower->borrower_status = "Returned";
            $borrower->save();
        }


        if($request->quantity > 0) {
            $inventory = Inventory::where("inv_name", $request->item_name)->first();

            $inventory->inv_quantity += (int) $request->quantity;
            $inventory->save();
        }
        else {
            $inv_specs = EquipInventory::where("serial_num", $request->item_name)->first();

            $inv_specs->status = "working";
            $inv_specs->save();
        }
    }

    public function getEvent() {
        $eventArray = array();

        $events = MticsEvent::select('event_title', 'id', 'event_amount')->where('status', 'on-going')->where('sync_status', "0")->where('event_amount', '!=', NULL)->get();

        foreach ($events as $key => $event) {
            $eventArray[] = array(
                'event' => $event->event_title,
                'amount' => $event->event_amount,
                'id' => $event->id,
                );
        }

        return response()->json($eventArray);
    }

    public function getEventReply(Request $request) {
        $eventId = (int) $request->event_id;

        $event = MticsEvent::findOrFail($request->event_id);
        $event->sync_status = '1';
        $event->save();

        return "done!";
    }


    public function getInventory() {
        $inventoryArray = array();

        $inventories = Inventory::select('id', 'inv_name', 'inv_quantity', 'inventory_category_id')->where('inv_quantity', '>', 0)->where('sync_status', "0")->get();

        foreach ($inventories as $key => $inv) {

            if ($inv->Inventory_Category->inv_cat_name == "equip") {

                $specs = array();
                foreach ($inv->equipment as $equip) {
                    $specs[] = array(
                        "brand" => $equip->brand_name,
                        "serial" => $equip->serial_num
                        );
                }
            }

            else {
                $specs = NULL;
            }

            $inventoryArray[] = array(
                'id' => $inv->id,
                'name' => $inv->inv_name,
                'quantity' => $inv->inv_quantity,
                'specs' => $specs
                );
        }

        return response()->json($inventoryArray);
    }

    public function getInventoryReply(Request $request) {
        $invId = (int) $request->inv_id;

        $inv = Inventory::findOrFail($request->inv_id);
        $inv->sync_status = '1';
        $inv->save();

        return "done!";
    }

    public function signIn(Request $request) {
        $member = Member::where('username', $request->username)->first();

        if($member) {
            if (password_verify($request->password, $member->password)) {
                return "online";

            } else {
                return "password";
            }

        } else {
            return "username";
        }
    }
}
