<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FireBaseController extends Controller
{
    public function sendNotification($body) {
        // Set POST variables
        $url = 'https://fcm.googleapis.com/fcm/send';

        $headers = array(
            'Authorization: key=AAAAvXG6qvw:APA91bGHb6-utXIwh4dJpFPpK6QdHmS1bsrDTjU8RtKLNz00_WZSvAoG4D5pJhaDLERUhN2b3SMXcd4c76-3IWfE4OIQYwio-AGoHtnZjwHejzBACBUK3chrrpY3AD__KUsuWoAU1y65',
            'Content-Type: application/json'
        );

        $fields = array(
            'to' => '/topics/global',
            'topic' => "global",
            'notification' => array('body' => $body, 'title' => 'MTICS Notification'),
        );
        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }

        // Close connection
        curl_close($ch);
        return $result;
    }
}
