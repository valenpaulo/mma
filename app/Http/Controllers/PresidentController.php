<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use App\Role;
use App\RoleMember;
use App\Task;
use App\InventoryCategory;
use App\MticsPurchaseList;
use App\EventPurchaseList;
use App\EventPurchaseEquipList;
use App\Member;
use App\MticsEvent;
use App\EventAmountBreakdownDeny;
use App\EventPurchaseReport;
use App\Inventory;
use App\MticsPurchaseReport;

use App\Events\SystemEvent;
use App\Notifications\SystemNotification;
use App\Notifications\SystemBroadcastNotification;

use Illuminate\Support\Facades\Event;
use App\MticsPurchaseEquipList;
use App\MticsDepositReport;
use App\MticsDeposit;
use App\EventDeposit;
use App\EventDepositReport;
use App\EventWithdrawal;
use App\EventWithdrawalReport;
use App\MticsWithdrawal;
use App\MticsWithdrawalReport;
use App\MticsBudget;
use App\MoneyRequest;
use App\Penalty;
use App\Officerterm;
use App\Action;
use App\ActionMember;
use App\ReimburseRequest;




class PresidentController extends Controller
{
    public function view()
    {
    	echo 'PRESIDENT VIEW';
    }


//TASK //TASK //TASK //TASK //TASK //TASK //TASK //TASK
    public function task_view()
    {

        $roles = Role::where('name','!=','faculty')->where('name','!=','super_admin')
                        ->where('name','!=','asst_internal')
                        ->where('name','!=','asst_external')
                        ->where('name','!=','asst_docu')
                        ->where('name','!=','asst_finance')
                        ->where('name','!=','asst_auditor')
                        ->where('name','!=','asst_info')
                        ->where('name','!=','asst_activity')
                        ->where('name','!=','asst_logistic')
                        ->where('name','!=','president')
                        ->where('name','!=','vice_president')
                        ->get();
        $item_categories = InventoryCategory::all();

        return view('officers/manage_task',compact('roles','item_categories'));
    }

    public function task_store(Request $request, $id)
    {
         //dd($request);
         $this->validate($request, [
        'role_id' => 'required|max:255',
        'due_date' => 'required|max:255',
        'task_desc' => 'required|min:8',
        'task_name' => 'required',
        ]);


         $role_members = RoleMember::where('role_id',$request->role_id)->get();
         foreach ($role_members as $role_member) {
             $member_id = $role_member->member_id;
         }

         $task = New Task;
         $task->member_id = $member_id;
         $task->admin_id = Auth::id();
         $task->due_date = $request->due_date;
         $task->task_desc = $request->task_desc;
         $task->task_name = $request->task_name;
         $task->role_id = $request->role_id;

         if($id) {
             $task->event_id = $id;
         }
         $task->save();
        return redirect()->back();

    }

//TASK //TASK //TASK //TASK //TASK //TASK //TASK //TASK


//MANAGE OFFICERS //MANAGE OFFICERS //MANAGE OFFICERS
    public function officers_change(Request $request,$id)
    {

        $officers = Role::with('role_member')
                ->where('assistant','false')->get();
        $approved_request_account = Action::where('action_name','acctreq_approval')->first();
        $pres_id = Role::where('name','president')->first()->id;
        $vpres_id = Role::where('name','vice_president')->first()->id;
        $info_id = Role::where('name','info')->first()->id;
        $asstinfo_id = Role::where('name','asst_info')->first()->id;


        if($request->member_id == null){

            $role_member = RoleMember::where('role_id', $id)->first();

            if($role_member == null){
               return redirect()->back()->withErrors('Please Select Member');
            }

                $data = array(
                    'message' => $role_member->role->display_name . " role has been removed from you.",
                    'redirect' => 'none',
                    'origin' => 'privilege_removed',
                    );

                $member = Member::findOrFail($role_member->member_id);
                $member->notify(new SystemNotification($data));

            if($id == $pres_id or $id == $vpres_id or $id == $info_id or $id == $asstinfo_id){
                ActionMember::where('action_id',$approved_request_account->id)->where('member_id',$role_member->member_id)->delete();

            }
                $role_member->delete();

            if(RoleMember::where('member_id',$member->id)->get()->isEmpty()){
                ActionMember::where('member_id',$member->id)->delete();
            }

            return redirect()->back();

        }

        foreach ($officers as $officer) {

            $role_member = RoleMember::where('role_id', $officer->id)->first();
            if($officer->id == $id)
            {

                if($role_member !== null)
                {
                    $data = array(
                        'message' => $role_member->role->display_name . " role has been removed from you.",
                        'redirect' => 'none',
                        'origin' => 'privilege_removed',
                        );

                    $member = Member::findOrFail($role_member->member_id);
                    $member->notify(new SystemNotification($data));
                    $role_member->delete();
                }


                    if($id == $pres_id or $id == $vpres_id or $id == $info_id or $id == $asstinfo_id){


                        if(ActionMember::get()->isEmpty())
                            {
                                goto skip;
                            }

                        if($role_member !== null){
                          ActionMember::where('action_id',$approved_request_account->id)->where('member_id',$role_member->member_id)->delete();
                        }


                    }
                    skip:
                    if($role_member !== null)
                    {
                        $role_member->delete();
                        if(RoleMember::where('member_id',$member->id)->get()->isEmpty()){
                              ActionMember::where('member_id',$member->id)->delete();
                        }
                    }



            }

            if($role_member !== null)
                {
                    $data = array(
                        'message' => $role_member->role->display_name . " role has been removed from you.",
                        'redirect' => 'none',
                        'origin' => 'privilege_removed',
                        );

                    $member = Member::findOrFail($role_member->member_id);
                    $member->notify(new SystemNotification($data));
                    RoleMember::where('role_id', $officer->id)->where('member_id',$request->member_id)->delete();
                }


        }


        RoleMember::create([
            'role_id' => $id,
            'member_id' => $request->member_id,
        ]);

        if($id == $pres_id or $id == $vpres_id or $id == $info_id or $id == $asstinfo_id){
            ActionMember::create([
            'action_id' => $approved_request_account->id,
            'member_id' => $request->member_id,
        ]);
        }


        $role = Role::findOrFail($id);

        $data = array(
            'message' => $role->display_name . " role has been assigned to you.",
            'redirect' => $role->name,
            'origin' => 'privilege',
            );

        $member = Member::findOrFail($request->member_id);
        $member->notify(new SystemNotification($data));

        Event::fire(new SystemEvent(auth::id(), 'Assigned ' . $role->display_name . ' role to ' . $member->username . '.'));

        return redirect('admin/manage-privilege')->with('success', 'Role Updated!');
    }


//MANAGE OFFICERS //MANAGE OFFICERS //MANAGE OFFICERS




//APPROVAL OF EVENT BREAKDOWN //APPROVAL OF EVENT BREAKDOWN //APPROVAL OF EVENT BREAKDOWN //APPROVAL OF EVENT BREAKDOWN

    public function event_breakdown_view()
    {
        if(!Gate::allows('president-only'))
        {
        return redirect('/');
        }
        $term = Officerterm::where('status','on-going')->first();
        $events = MticsEvent::where('amount_confirm','!=',null)->where('updated_at','>=',date($term->created_at))->orderBy('id', 'desc')->get();

       // $events = MticsEvent::where('amount_confirm','!=',null)->get();
        return view('officers/event/event_breakdown_president',compact('events'));
    }

     public function event_breakdown_approved($id)
    {
        $events = MticsEvent::find($id);
        if($events->amount_confirm !== 'pending' )
        {
          return redirect()->back()->withErrors('Request status is '.$events->amount_confirm.', make changes are prohibited ');
        }

        $role_pres = Role::where('name','president')->first();
        $role_vice = Role::where('name','vice_president')->first();

        if(!RoleMember::where('member_id',Auth::id())->where('role_id',$role_pres->id)->get()->isEmpty()){
                MticsEvent::where('id', $id)->update(array('confirm_admin_role_id' => $role_pres->id));
            }
            elseif(!RoleMember::where('member_id',Auth::id())->where('role_id',$role_vice->id)->get()->isEmpty()){
                MticsEvent::where('id', $id)->update(array('confirm_admin_role_id' => $role_vice->id));
            }
            else
            {
            return redirect()->back()->withErrors('invalid role');
            }


            MticsEvent::where('id', $id)->update(array('confirm_admin_id' => Auth::id()));
            MticsEvent::where('id', $id)->update(array('amount_confirm' => 'approved'));


        $data = array(
        'message' => "Event Breakdown Request has been approved",
        'redirect' => array('role' => 'finance', 'event_id' => $id),
        'origin' => 'breakdown-request-president',
        );

        $role_id = Role::where('name', 'finance')->first()->id;
        $member = Member::whereHas('role', function($q) use ($role_id) {
        $q->where('role_id', $role_id);
        })->first();
        $member->notify(new SystemNotification($data));

        Event::fire(new SystemEvent(auth::id(), 'Approved Event Breakdown Request.'));


        return redirect()->back()->with('success', 'Request Approved!');

    }

     public function event_breakdown_denied(Request $request,$id)
    {
        $events = MticsEvent::find($id);
        if($events->amount_confirm !== 'pending' )
        {
          return redirect()->back()->withErrors('Request status is '.$events->amount_confirm.', make changes are prohibited ');
        }

        $this->validate($request, [
        'denied_reason' => 'required',
        ]);

        $role_pres = Role::where('name','president')->first();
        $role_vice = Role::where('name','vice_president')->first();

        if(!RoleMember::where('member_id',Auth::id())->where('role_id',$role_pres->id)->get()->isEmpty()){
                $role_id = $role_pres->id;
            }
            elseif(!RoleMember::where('member_id',Auth::id())->where('role_id',$role_vice->id)->get()->isEmpty()){
                $role_id = $role_vice->id;
            }
            else
            {
            return redirect()->back()->withErrors('invalid role');
            }
         EventAmountBreakdownDeny::create([
            'event_id' => $id,
            'confirm_admin_id' => Auth::id(),
            'confirm_admin_role_id' => $role_id,
            'denied_reason' => $request->denied_reason,
        ]);

    MticsEvent::where('id', $id)->update(array('confirm_admin_id' => Auth::id()));
    MticsEvent::where('id', $id)->update(array('amount_confirm' => 'denied'));

        return redirect('admin/president/requested-event-breakdown');
    }



//purchased-item-reported //purchased-item-reported //purchased-item-reported

    public function purchase_item_reported_view(){

        $term = Officerterm::where('status','on-going')->first();

        $eventreported = EventPurchaseReport::where('report_category','critical')->where('solution_fr_critical',null)->where('updated_at','>=',date($term->created_at))->get();
        $mticsreported = MticsPurchaseReport::where('report_category','critical')->where('solution_fr_critical',null)->where('updated_at','>=',date($term->created_at))->get();
        $inventory_categories = InventoryCategory::all();

        $reportcount = MticsPurchaseReport::where('report_category','critical')->where('solution_fr_critical', null)->where('updated_at','>=',date($term->created_at))->count();

        return view('officers/adviser_purchased_items_reported', compact('eventreported','mticsreported','inventory_categories', 'reportcount'));
    }

    public function purchase_item_reported_mtics_add(Request $request, $id){
        $this->validate($request, [
        'report_solution' => 'required',
        ]);

        $mticsreported = MticsPurchaseReport::find($id);
        $mticspurchaselist = MticsPurchaseList::find($mticsreported->mtics_purchase_list_id);

        if($mticspurchaselist->mtics_inv_status !== 'pending' and $mticsreported->reportedby !== 'finance'){
            return redirect()->back()->withErrors('This issue has been resolved, make changes are prohibited');
        }

        $mticsreported->solution_fr_critical = $request->report_solution;
        $mticsreported->update();
        $checkifcanmove = false;



        $equip_id = Inventory::getId('App\InventoryCategory','inv_cat_name','equip');
    if($mticsreported->reportedby = 'logistics')
    {


        //CHECK IF THERE IS EQUIPMENT, the category will be automatically 'equip'
        if(!MticsPurchaseEquipList::where('mtics_purchase_list_id',$mticspurchaselist->id)->get()->isEmpty()){
            $category = $equip_id;
        }
        else{
            $category = $request->inv_cat_id;
        }


        if($category == $equip_id)
        {
            $checkifcanmove = true;
        }

        if(!isset($request->dontmove))
        {
            $checkifcanmove = true;
        }

         if($checkifcanmove == true)
        {
            if($mticspurchaselist->mtics_orig_quan > $mticspurchaselist->mtics_act_quan)
            {
                if($mticspurchaselist->mtics_inv_quan !== null)
                {
                    $quan = $mticspurchaselist->mtics_act_quan - $mticspurchaselist->mtics_inv_quan;
                }
                else
                {
                    $quan = $mticspurchaselist->mtics_act_quan;
                }


            }
            else
            {
                if($mticspurchaselist->mtics_inv_quan !== null)
                {
                    $quan = $mticspurchaselist->mtics_act_quan - $mticspurchaselist->mtics_inv_quan;
                }
                else
                {
                    $quan = $mticspurchaselist->mtics_act_quan;
                }
                  MticsPurchaseList::where('id', $mticspurchaselist->id)->update(array('mtics_inv_status' => 'moved'));
            }

                  MticsPurchaseList::where('id', $mticspurchaselist->id)->update(array('mtics_inv_quan' => $mticspurchaselist->mtics_act_quan));
                  MticsPurchaseList::where('id', $mticspurchaselist->id)->update(array('inventory_category_id' => $category));


            if(Inventory::where('inv_name',$mticspurchaselist->mtics_itemname)->where('inventory_category_id',$category)->get()->isEmpty()){

                $inventory = New Inventory;
                $inventory->inv_name = $mticspurchaselist->mtics_itemname;
                $inventory->inv_quantity = $quan;
                $inventory->inventory_category_id = $category;
                $inventory->save();
                $inventory_id = $inventory->id;


            }
            else{

                $first_inventory = Inventory::where('inv_name',$mticspurchaselist->mtics_itemname)->where('inventory_category_id',$category)->first();
                $totalquan = $quan + $first_inventory->inv_quantity;
                $first_inventory->inv_quantity = $totalquan;
                $first_inventory->save();
                $inventory_id = $first_inventory->id;
            }




            if(!MticsPurchaseEquipList::where('mtics_purchase_list_id',$mticspurchaselist->id)->get()->isEmpty())
            {


                $mticspurchaseequiplists = MticsPurchaseEquipList::where('mtics_purchase_list_id',$mticspurchaselist->id)->get();
                foreach ($mticspurchaseequiplists as $mticspurchaseequiplist) {
                    if(EquipInventory::where('inventory_id',$inventory_id)->where('brand_name',$mticspurchaseequiplist->mtics_brandname)->where('serial_num',$mticspurchaseequiplist->mtics_serialnum)->where('specs',$mticspurchaseequiplist->mtics_specs)->get()->isEmpty())
                        {
                            $equip_inventory = New EquipInventory;
                            $equip_inventory->inventory_id = $inventory_id;
                            $equip_inventory->brand_name = $mticspurchaseequiplist->mtics_brandname;
                            $equip_inventory->serial_num = $mticspurchaseequiplist->mtics_serialnum;
                            $equip_inventory->specs = $mticspurchaseequiplist->mtics_specs;
                            $equip_inventory->save();
                        }
                }

            }

        }
        else{
            MticsPurchaseList::where('id', $mticspurchaselist->id)->update(array('mtics_inv_status' => 'unmoved'));
        }
    }



        $this->isTaskCleared_Mtics($mticsreported->mtics_purchase_list_id);

        $data = array(
        'message' => "Issue on purchased item was resolved.",
        'redirect' => $arrayName = array('reported' => $mticsreported->reportedby, 'event' => null),
        'origin' => 'purchased-item-report',
        );

        if ($mticsreported->reportedby == "finance") {
            $role_id = Role::where('name', 'finance')->first()->id;
            $member = Member::whereHas('role', function($q) use ($role_id) {
                $q->where('role_id', $role_id);
            })->first();

            $member->notify(new SystemNotification($data));
        }

        elseif ($mticsreported->reportedby == "logistics") {
            $role_id = Role::where('name', 'logistic')->first()->id;
            $member = Member::whereHas('role', function($q) use ($role_id) {
                $q->where('role_id', $role_id);
            })->first();

            $member->notify(new SystemNotification($data));
        }

        Event::fire(new SystemEvent(auth::id(), 'Purchased Item Issue Resolved.'));

        return redirect()->back()->with('success', 'Purchased Item Issue Resolved!');
    }


    public function purchase_item_reported_event_add(Request $request, $id){
        $this->validate($request, [
        'report_solution' => 'required',
        ]);

        $eventreported = EventPurchaseReport::find($id);
        $eventpurchaselist = EventPurchaseList::find($eventreported->event_purchase_list_id);

        if($eventpurchaselist->event_inv_status !== 'pending' and $eventreported->reportedby !== 'finance'){
            return redirect()->back()->withErrors('This issue has been resolved, make changes are prohibited');
        }

        $eventreported->solution_fr_critical = $request->report_solution;
        $eventreported->update();


        $equip_id = Inventory::getId('App\InventoryCategory','inv_cat_name','equip');
        $checkifcanmove = false;

    if($eventreported->reportedby == 'logistics')
    {
        if(!EventPurchaseEquipList::where('event_purchase_list_id',$eventpurchaselist->id)->get()->isEmpty()){
            $category = $equip_id;

        }
        else{
            $category = $request->inv_cat_id;
        }

         if($category == $equip_id)
        {
            $checkifcanmove = true;
        }

        if(!isset($request->dontmove))
        {
            $checkifcanmove = true;
        }


        if($checkifcanmove == true)
        {
            if(Inventory::where('inv_name',$eventpurchaselist->event_itemname)->where('inventory_category_id',$category)->get()->isEmpty()){

                $inventory = New Inventory;
                $inventory->inv_name = $eventpurchaselist->event_itemname;
                $inventory->inv_quantity = $eventpurchaselist->event_act_quan;
                $inventory->inventory_category_id = $category;
                $inventory->save();
                $inventory_id = $inventory->id;
            }
            else{
                $first_inventory = Inventory::where('inv_name',$eventpurchaselist->event_itemname)->where('inventory_category_id',$category)->first();
                $totalquan = $eventpurchaselist->event_act_quan + $first_inventory->inv_quantity;
                $first_inventory->inv_quantity = $totalquan;
                $first_inventory->save();
                $inventory_id = $first_inventory->id;
            }


            if(!EventPurchaseEquipList::where('event_purchase_list_id',$eventpurchaselist->id)->get()->isEmpty()){
                $eventpurchaseequiplists = EventPurchaseEquipList::where('event_purchase_list_id',$eventpurchaselist->id)->get();

                foreach ($eventpurchaseequiplists as $eventpurchaseequiplist) {
                    $equip_inventory = New EquipInventory;
                    $equip_inventory->inventory_id = $inventory_id;
                    $equip_inventory->brand_name = $eventpurchaseequiplist->event_brandname;
                    $equip_inventory->serial_num = $eventpurchaseequiplist->event_serialnum;
                    $equip_inventory->specs = $eventpurchaseequiplist->event_specs;
                    $equip_inventory->save();
                }


            }

            EventPurchaseList::where('id', $eventpurchaselist->id)->update(array('event_inv_status' => 'moved'));
            EventPurchaseList::where('id', $eventpurchaselist->id)->update(array('inventory_category_id' => $category));


        }
        else
        {
            EventPurchaseList::where('id', $eventpurchaselist->id)->update(array('event_inv_status' => 'unmoved'));

        }
    }


        $this->isTaskCleared_Event($eventreported->event_purchase_list_id);

        $data = array(
        'message' => "Issue on purchased item was resolved.",
        'redirect' => array('reported' => $eventreported->reportedby, 'event' => $eventreported->purchaselist->task->event_id ),
        'origin' => 'purchased-item-report',
        );

        if ($eventreported->reportedby == "finance") {
            $role_id = Role::where('name', 'finance')->first()->id;
            $member = Member::whereHas('role', function($q) use ($role_id) {
                $q->where('role_id', $role_id);
            })->first();

            $member->notify(new SystemNotification($data));
        }

        elseif ($eventreported->reportedby == "logistics") {
            $role_id = Role::where('name', 'logistic')->first()->id;
            $member = Member::whereHas('role', function($q) use ($role_id) {
                $q->where('role_id', $role_id);
            })->first();

            $member->notify(new SystemNotification($data));
        }


        Event::fire(new SystemEvent(auth::id(), 'Purchased Item Issue Resolved.'));

        return redirect()->back()->with('success', 'Purchased Item Issue Resolved!');
    }

    Private function isTaskCleared_Event($purchased_id){
        $purchasedlist = EventPurchaseList::find($purchased_id);
        $task_id = $purchasedlist->task_id;
        $purchaseditems = EventPurchaseList::where('task_id',$task_id)->get();
        $status = '';
        $finance = false;
        $logistics = false;

        foreach($purchaseditems as $purchaseditem){

            $eventreported = EventPurchaseReport::where('event_purchase_list_id',$purchaseditem->id)->where('report_category','critical')->where('solution_fr_critical',null)->get();

            if($eventreported->isEmpty()){
                $status = 'failed';
                $status = 'failed';

                $reports_by = EventPurchaseReport::where('event_purchase_list_id',$purchaseditem->id)->where('report_category','critical')->get();
                if(!$reports_by->isEmpty()){
                    foreach ($reports_by as $report_by) {
                        if($report_by->reportedby == 'finance')
                        {
                         $reportedbyfinance = $report_by;
                        $finance = true;

                        }
                        else
                        {
                         $reportedbylogistics = $report_by;
                        $logistics = true;

                        }
                    }
                }
            }
            else
            {
                $status = 'for validation';
                break;
            }

        }



        Task::where('id', $task_id)->update(array('task_status' => $status));

        $task = Task::find($task_id);
        if($status == 'failed'){
            $penaltylastrow = Penalty::where('member_id',$task->member_id)->orderby('id','desc')->first();

            $penalty = New Penalty;
            $penalty->task_id = $task_id;
            $penalty->member_id = $task->member_id;
            $penalty->validator_id = Auth::id();
            if($finance == true and $logistics == true){$penalty->reason = 'has critical report, reported by finance and logistics';}
            elseif($logistics == true){$penalty->reason = 'has critical report, reported by logistics';}
            elseif($finance == true){$penalty->reason = 'has critical report, reported by finance';}
            if($penaltylastrow == null){
            $penalty->penalty_type = 'verbal';
            $message = "Executive Officer for Logistics reported a purchased item. You've failed the task. Please talk to your president for the said issue.";

            }
            elseif($penaltylastrow->penalty_type == 'fee'){
            $penalty->penalty_type = 'verbal';
            $message = "Executive Officer for Logistics reported a purchased item. You've failed the task. Please talk to your president to resolve the said issue.";

            }
            elseif($penaltylastrow->penalty_type == 'verbal'){
            $penalty->penalty_type = 'written';
            $message = "Executive Officer for Logistics reported a purchased item. You've failed the task. Please submit a letter of explanation to resolve the said issue.";

            }
            elseif($penaltylastrow->penalty_type == 'written'){
            $penalty->penalty_type = 'fee';
            $penalty->fee = 10;
            $message = "Executive Officer for Logistics reported a purchased item. You've failed the task. You have a fine of 10 pesos.";

            }
            $penalty->save();

            $data = array(
            'message' => $message,
            'redirect' => 'admin/external/manage-event/' . $task->event_id . '/task/' . $task->id,
            'origin' => 'purchased-item-validation',
            );

            $role_id = Role::where('name', 'external')->first()->id;
            $member = Member::whereHas('role', function($q) use ($role_id) {
                $q->where('role_id', $role_id);
            })->first();
            $member->notify(new SystemNotification($data));

            $data = array(
            'message' => "Please review the officer's penalty.",
            'redirect' => 'admin/president/manage-event/' . $task->event_id . '/failed-task',
            'origin' => 'purchased-item-validation',
            );

            $role_id = Role::where('name', 'president')->first()->id;
            $member = Member::whereHas('role', function($q) use ($role_id) {
                $q->where('role_id', $role_id);
            })->first();
            $member->notify(new SystemNotification($data));

            $reimbursement = $task->requestmoney->amount - $task->eventpurchaselist->sum('event_total_price');

            $reimburse = ReimburseRequest::where('money_request_id', $task->requestmoney->id)->first();

            if ($reimbursement < 0 and is_null($reimburse)) {

                $data = array(
                'message' => "You can now request for reimbursement.",
                'redirect' => 'admin/external/manage-event/' . $task->event_id . '/task/' . $task->id,
                'origin' => 'purchased-item-validation',
                );

                $role_id = Role::where('name', 'external')->first()->id;
                $member = Member::whereHas('role', function($q) use ($role_id) {
                    $q->where('role_id', $role_id);
                })->first();
                $member->notify(new SystemNotification($data));
            }

            $data = array(
            'message' => "There are purchased items to be validated",
            'redirect' => array('role' => 'finance', 'event_id' => $task->event_id),
            'origin' => 'logistic',
            );

            $role_id = Role::where('name', 'finance')->first()->id;
            $member = Member::whereHas('role', function($q) use ($role_id) {
                $q->where('role_id', $role_id);
            })->first();
            $member->notify(new SystemNotification($data));
        }


    }


    Private function isTaskCleared_Mtics($purchased_id){
        $purchasedlist = MticsPurchaseList::find($purchased_id);
        $task_id = $purchasedlist->task_id;
        $task = Task::findOrFail($task_id);
        $purchaseditems = MticsPurchaseList::where('task_id',$task_id)->get();
        $status = '';
        $reportedbyfinance = '';
        $reportedbylogistics = '';

        foreach($purchaseditems as $purchaseditem){
            $mticsreported = MticsPurchaseReport::where('mtics_purchase_list_id',$purchaseditem->id)->where('report_category','critical')->where('solution_fr_critical',null)->get();

            if($mticsreported->isEmpty()){
                $status = 'failed';

                $reports_by = MticsPurchaseReport::where('mtics_purchase_list_id',$purchaseditem->id)->where('report_category','critical')->get();
                if(!$reports_by->isEmpty()){
                    foreach ($reports_by as $report_by) {
                        if($report_by->reportedby == 'finance'){
                         $reportedbyfinance = $report_by;
                        }
                        else
                        {
                         $reportedbylogistics = $report_by;
                        }
                    }
                }
            }
            else
            {
                $status = 'for validation';
                break;
            }

        }


        Task::where('id', $task_id)->update(array('task_status' => $status));

        if ($status == 'failed') {
            $reimbursement = $task->requestmoney->amount - $task->mticspurchaselist->sum('mtics_total_price');

            $reimburse = ReimburseRequest::where('money_request_id', $task->requestmoney->id)->first();


            if ($reimbursement < 0 and is_null($reimburse)) {

                $data = array(
                'message' => "You can now request for reimbursement.",
                'redirect' => 'admin/external/task/' . $task->id,
                'origin' => 'logistics-validation',
                );

                $role_id = Role::where('name', 'external')->first()->id;
                $member = Member::whereHas('role', function($q) use ($role_id) {
                $q->where('role_id', $role_id);
                })->first();
                $member->notify(new SystemNotification($data));

            }

            $data = array(
            'message' => "Item has been validated.",
            'redirect' => 'admin/finance/requested-money/',
            'origin' => 'purchased-item-validation',
            );

            $role_id = Role::where('name', 'finance')->first()->id;
            $member = Member::whereHas('role', function($q) use ($role_id) {
                $q->where('role_id', $role_id);
            })->first();
            $member->notify(new SystemNotification($data));

        }
    }


//REPORTED SOLUTION //REPORTED SOLUTION //REPORTED SOLUTION //REPORTED SOLUTION
//bank-transaction-request-reported

    public function bank_reported_solution_view(){
        //deposit reported
        $mticsDepositReported = MticsDeposit::whereHas('report')->with('report')->get();
        $eventDepositReported = EventDeposit::whereHas('report')->with('report')->get();

        //withdrawal reported
        $mticsWithdrawReported = MticsWithdrawal::whereHas('report')->with('report')->get();
        $eventWithdrawReported = EventWithdrawal::whereHas('report')->with('report')->get();

        //dd($mticsWithdrawReported);

        return view('officers/president_banktrans_reported',compact('mticsDepositReported','eventDepositReported','mticsWithdrawReported','eventWithdrawReported'));

    }

    public function bank_mtics_deposit_reported_solution(Request $request,$id){
        $this->validate($request, [
        'report_solution' => 'required',
        ]);

        $mticsDepositReported = MticsDeposit::find($id);
        if($request->amount !== null){

            $totalamount = $request->amount + $mticsDepositReported->deposit_amt;

            //for MTICS collection
            if($mticsDepositReported->deposit_source == 'MTICS Payment'){
                $formula = new FormulaMoneyController();
                $mticsfund_money = $formula->CheckMticsFundPayment();
                if($totalamount > $mticsfund_money){
                    return redirect()->back()->withErrors('The total amount deposit will be greater than on hand MTICS payment collection');
                }
            }

            //for Event Collection
            if($mticsDepositReported->deposit_source == 'Event Collection'){
                $eventpayment = new FormulaMoneyController();
                $eventbucket = $eventpayment->CheckEventFundPayment($mticsDepositReported->event_id);
                if($totalamount > $eventbucket){
                    return redirect()->back()->withErrors('The total amount deposit will be greater than on hand Event payment collection');
                }
            }

            //Event Transfer
            if($mticsDepositReported->deposit_source == 'Event'){
                $bank = new BankTransactionController();
                $eventbucket = $bank->CheckEventFundBucketPerEvent($id);
                if($totalamount > $eventbucket){
                    return redirect()->back()->withErrors('The total amount deposit will be greater than Event fund');
                }
            }

            $mticsDepositReported->deposit_amt = $totalamount;

        }

            $reported = MticsDepositReport::where('mtics_deposit_id',$mticsDepositReported->id)->first();
            $reported->solution = $request->report_solution;
            $reported->update();
            $mticsDepositReported->deposit_status = 'cleared';
            $mticsDepositReported->update();

            $data = array(
                    'message' => "Issue on deposit transaction was resolved.",
                    'redirect' => "bank-transaction-report",
                    'origin' => 'president',
                    );


            $role_id = Role::where('name', 'mtics_adviser')->first()->id;
            $member = Member::whereHas('role', function($q) use ($role_id) {
                  $q->where('role_id', $role_id);
              })->first();
            $member->notify(new SystemNotification($data));

            Event::fire(new SystemEvent(auth::id(), 'Deposit Issue was resolved.'));

        return redirect()->back()->with('success', 'Issue Resolved!');
    }

    public function bank_mtics_withdraw_reported_solution(Request $request,$id){
        $this->validate($request, [
        'report_solution' => 'required',
        ]);

        $mticsWithdrawReported = MticsWithdrawal::find($id);
        if($request->amount !== null){
            $totalamount = $request->amount + $mticsWithdrawReported->withdrawal_amt;
            //Monthly budget amount check
            if($mticsWithdrawReported->mtics_budget_id !== null){
                $budget = MticsBudget::find($mticsWithdrawReported->mtics_budget_id);
                if($totalamount > $budget->budget_amt){
                    return redirect()->back()->withErrors('The total amount withdraw will be greater than Monthly Budget');
                }
            }
            //external task's money request
            if($mticsWithdrawReported->external_task_id !== null){
                $external_task = MoneyRequest::where('task_id',$mticsWithdrawReported->external_task_id)->first();
                if($totalamount > $external_task->amount){
                    return redirect()->back()->withErrors("The total amount withdraw will be greater than task's money request");
                }
            }

            $bank = new BankTransactionController();

            $mticsfund_bucket = $bank->CheckMticsFundBucket();

            if(($mticsfund_bucket - $totalamount) < 1)
            {
                return redirect()->back()->withErrors('You only have P'.$mticsfund_bucket.'. MTICS FUND will be 0 or negative, cannot request a withdraw');
            }

            $mticsWithdrawReported->withdrawal_amt = $totalamount;
        }

            $reported = MticsWithdrawalReport::where('mtics_withdrawal_id',$mticsWithdrawReported->id)->first();
            $reported->solution = $request->report_solution;
            $reported->update();
            $mticsWithdrawReported->withdrawal_status = 'cleared';
            $mticsWithdrawReported->update();

            if($mticsWithdrawReported->mtics_budget_id !== null)
            {
                if(!MticsBudget::where('budget_status','on-going')->get()->isEmpty())
                    {
                        $mticsbudget_ongoing = MticsBudget::where('budget_status','on-going')->first();
                        $budget = new FormulaMoneyController;
                        $mticsfund_amt = $budget->CheckMticsFundBudget();
                        MticsBudget::where('id', $mticswithdrawal->mtics_budget_id)->update(array('excess' => $mticsfund_amt));
                        MticsBudget::where('id', $mticsbudget_ongoing->id)->update(array('budget_status' => 'cleared'));
                    }

                MticsBudget::where('id', $mticsWithdrawReported->mtics_budget_id)->update(array('budget_status' => 'on-going'));
            }

            $data = array(
                    'message' => "Issue on withdraw transaction was resolved.",
                    'redirect' => "bank-transaction-report",
                    'origin' => 'president',
                    );


            $role_id = Role::where('name', 'mtics_adviser')->first()->id;
            $member = Member::whereHas('role', function($q) use ($role_id) {
                  $q->where('role_id', $role_id);
              })->first();
            $member->notify(new SystemNotification($data));

            Event::fire(new SystemEvent(auth::id(), 'Withdraw Issue was resolved.'));

            return redirect()->back()->with('success', 'Issue Resolved!');
    }

    //EVENT

    public function bank_event_deposit_reported_solution(Request $request,$id){
        $this->validate($request, [
        'report_solution' => 'required',
        ]);

        $eventDepositReported = EventDeposit::find($id);
        if($request->amount !== null){

            $totalamount = $request->amount + $eventDepositReported->deposit_amt;

            //for EVENT collection
            if($eventDepositReported->deposit_source == 'Event Payment'){
                $eventpayment = new FormulaMoneyController();
                $eventbucket = $eventpayment->CheckEventFundPayment($eventDepositReported->event_id);
                if($totalamount > $eventbucket){
                    return redirect()->back()->withErrors('The total amount deposit will be greater than on hand Event payment collection');
                }
            }

        $eventDepositReported->deposit_amt = $totalamount;

        }

        $reported = EventDepositReport::where('event_deposit_id',$eventDepositReported->id)->first();
        $reported->solution = $request->report_solution;
        $reported->update();
        $eventDepositReported->deposit_status = 'cleared';
        $eventDepositReported->update();

        $data = array(
                    'message' => "Issue on deposit transaction was resolved.",
                    'redirect' => "bank-transaction-report",
                    'origin' => 'president',
                    );


            $role_id = Role::where('name', 'mtics_adviser')->first()->id;
            $member = Member::whereHas('role', function($q) use ($role_id) {
                  $q->where('role_id', $role_id);
              })->first();
            $member->notify(new SystemNotification($data));

            Event::fire(new SystemEvent(auth::id(), 'Deposit Issue Resolved.'));

        return redirect()->back()->with('success', 'Issue Resolved!');
    }

    public function bank_event_withdraw_reported_solution(Request $request,$id){
        $this->validate($request, [
        'report_solution' => 'required',
        ]);

        $eventWithdrawReported = EventWithdrawal::where('id',$id)->with('event')->first();
        if($request->amount !== null){
            $totalamount = $request->amount + $eventWithdrawReported->withdrawal_amt;


        //external task's money request
            if($eventWithdrawReported->external_task_id !== null){
                $external_task = MoneyRequest::where('task_id',$eventWithdrawReported->external_task_id)->first();
                if($totalamount > $external_task->amount){
                    return redirect()->back()->withErrors("The total amount withdraw will be greater than task's money request");
                }
            }

            $bank = new BankTransactionController();
            $eventbucket = $bank->CheckEventFundBucketPerEvent($eventWithdrawReported->event_id);
            if(($eventbucket - $totalamount) < 1)
                {

                    return redirect()->back()->withErrors('You only have P'.$eventbucket.'. Event FUND '.$eventWithdrawReported->event->event_title.' will be 0 or negative, cannot request a withdraw');
                }

            $eventWithdrawReported->withdrawal_amt = $totalamount;
        }

            $reported = EventWithdrawalReport::where('event_withdrawal_id',$eventWithdrawReported->id)->first();
            $reported->solution = $request->report_solution;
            $reported->update();
            $eventWithdrawReported->withdrawal_status = 'cleared';
            $eventWithdrawReported->update();

            $data = array(
                    'message' => "Issue on withdraw transaction was resolved.",
                    'redirect' => "bank-transaction-report",
                    'origin' => 'president',
                    );


            $role_id = Role::where('name', 'mtics_adviser')->first()->id;
            $member = Member::whereHas('role', function($q) use ($role_id) {
                  $q->where('role_id', $role_id);
              })->first();
            $member->notify(new SystemNotification($data));

            Event::fire(new SystemEvent(auth::id(), 'Withdraw Issue Resolved.'));

            return redirect()->back()->with('success', 'Issue Resolved!');

    }


}
