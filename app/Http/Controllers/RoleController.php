<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use App\Role;
use App\Member;
use App\RoleMember;
use App\Action;
use App\ActionMember;
use App\Section;
use App\Notifications\SystemNotification;
use Illuminate\Support\Facades\Auth;

use App\Events\SystemEvent;
use Illuminate\Support\Facades\Event;


class RoleController extends Controller
{
    public function view()
    {

        if(!Gate::allows('superadmin-only'))
        {
        return redirect('/');
        }

    	$adminmembers = DB::table('role_members')
            ->join('members', 'members.id', '=', 'role_members.member_id')
            ->join('roles', 'roles.id', '=', 'role_members.role_id')
            ->select('members.*','roles.*', 'role_members.role_id')
            ->get();

        $actionMembers = ActionMember::all();
        $action_member = array();
        $member_with_action = array();

        if (!is_null($actionMembers)) {
            foreach ($actionMembers as $with_action) {

                if(!in_array($with_action->member->username, $member_with_action)){
                    $member_with_action[] = $with_action->member->username;
                    $action_member[$with_action->member->username][] = $with_action->action_id;
                } else {
                    $action_member[$with_action->member->username][] = $with_action->action_id;

                }

            }
        }

        $role_member = array();
        $member_with_role = array();
        foreach ($adminmembers as $admin) {

            if(!in_array($admin->username, $member_with_role)){
                $member_with_role[] = $admin->username;
                $role_member[$admin->username][] = $admin->role_id;
            } else {
                $role_member[$admin->username][] = $admin->role_id;

            }
        }

       // $roles = Role::all();
        $members=Member::where('status', 'active')->get();
        // $members=Member::where('status', 'active')->get();

        if(!Gate::allows('superadminrole-only'))
        {
        $roles = Role::where('name','!=','internal')
                        ->where('name','!=','external')
                        ->where('name','!=','docu')
                        ->where('name','!=','finance')
                        ->where('name','!=','auditor')
                        ->where('name','!=','info')
                        ->where('name','!=','activity')
                        ->where('name','!=','logistic')
                        ->where('name','!=','president')
                        ->where('name','!=','mtics_adviser')
                        ->where('name','!=','vice_president')
                        ->where('name','!=','super_admin')->get();
        }
        else
        {
            $roles = Role::where('name','!=','internal')
                        ->where('name','!=','external')
                        ->where('name','!=','docu')
                        ->where('name','!=','finance')
                        ->where('name','!=','auditor')
                        ->where('name','!=','info')
                        ->where('name','!=','activity')
                        ->where('name','!=','logistic')
                        ->where('name','!=','president')
                        ->where('name','!=','mtics_adviser')
                        ->where('name','!=','vice_president')->get();
        }


        $officers = Role::with('role_member')
                ->where('name','!=','faculty')
                ->where('name','!=','mtics_adviser')
                ->where('name','!=','super_admin')
                ->where('name','!=','asst_internal')
                ->where('name','!=','asst_external')
                ->where('name','!=','asst_docu')
                ->where('name','!=','asst_finance')
                ->where('name','!=','asst_auditor')
                ->where('name','!=','asst_info')
                ->where('name','!=','asst_activity')
                ->where('name','!=','asst_logistic')->get();

        $sectionfaculty = Section::where('section_code','Faculty')->first();
        $memberNoRole = Member::where('status','active')->where('section_id','!=',$sectionfaculty->id)->get();

        $actions = Action::where('action_name','!=','manage_attendance')->get();
        $role_mtics_adviser = Role::where('name','mtics_adviser')->first();
        $faculty = Role::where('name','faculty')->first();
        $mtics_adviser = RoleMember::where('role_id',$role_mtics_adviser->id)->get();

    	return view('admin/addFeature/manage_privilege',compact('members','roles','adminmembers', 'member_with_role', 'role_member', 'actions', 'actionMembers', 'action_member', 'member_with_action','mtics_adviser', 'faculty', 'officers', 'memberNoRole'));
    }

     public function edit(Request $request,$id)
    {
        $member_role = RoleMember::where('member_id', $id)->whereHas('role', function($q) {
            $q->where('assistant', 'true')->orWhere('name', 'faculty')->orWhere('name', 'super_admin');
        })->get();
        $count = count($member_role);
        //ACTION REQUEST ACCOUNT APPROVAL
        $faculty_id = Role::getId('App\Role','name','faculty');
        $president_id = Role::getId('App\Role','name','president');
        $vicepresident_id = Role::getId('App\Role','name','vice_president');
        $info_id = Role::getId('App\Role','name','info');
        $asstinfo_id = Role::getId('App\Role','name','asst_info');
        $acctreq_approval_id = Action::getId('App\Action','action_name','acctreq_approval');

        if ($count == 0) {
            if (!isset($request->roles)){

                return redirect('admin/manage-privilege');
            } else {
                foreach ($request->roles as $selected) {
                    RoleMember::create([
                        'role_id' => $selected,
                        'member_id' => $id,
                    ]);

                    //faculty,Pres, vice, info, asst_info for Request Account Approval
                     if($president_id == $selected or $vicepresident_id == $selected or $info_id == $selected or $asstinfo_id == $selected or $faculty_id == $selected)
                        {
                          if(ActionMember::where('action_id', $acctreq_approval_id)->where('member_id', $id)->get()->isEmpty())
                            {
                             ActionMember::create([
                            'action_id' => $acctreq_approval_id,
                            'member_id' => $id,]);
                            }
                        }

                    $role = Role::findOrFail($selected);
                    $data = array(
                        'message' => $role->display_name . " role has been assigned to you",
                        'redirect' => $role->name,
                        'origin' => 'privilege',
                        );

                    $member = Member::findOrFail($id);
                    $member->notify(new SystemNotification($data));

                    Event::fire(new SystemEvent(auth::id(), 'Assigned ' . $role->display_name . ' role to ' . $member->username . '.'));

                }
            }
        } else {

            if (!isset($request->roles)){
                RoleMember::where('member_id', $id)->whereHas('role', function($q) {
                    $q->where('assistant', 'true')->orWhere('name', 'faculty')->orWhere('name', 'super_admin')->orWhere('name', 'mtics_adviser');
                })->delete();

                
                if(RoleMember::where('member_id', $id)->get()->isEmpty()){
                ActionMember::where('member_id', $id)->delete();

                }
                elseif(RoleMember::where('member_id',$id)->whereHas('role', function($q) {

                    $q->Where('name', 'president')->orWhere('name', 'vice_president')->orWhere('name', 'info');
                })->get()->isEmpty()){
                ActionMember::where('member_id', $id)->where('action', $acctreq_approval_id)->delete();

                }


                $data = array(
                        'message' => "Your admin privilege has been removed",
                        'redirect' => 'none',
                        'origin' => 'privilege_removed',
                        );

                $member = Member::findOrFail($id);
                $member->notify(new SystemNotification($data));

                Event::fire(new SystemEvent(auth::id(), 'Removed role of ' . $member->username . '.'));


            } else {
                // first update the existing roles

                foreach ($member_role as $role) {
                    if (!in_array($role->role_id, $request->roles)){
                        RoleMember::where('role_id', $role->role_id)->where('member_id', $id)->delete();

                        $role_name = Role::findOrFail($role->role_id);
                        $data = array(
                            'message' => "Your " . $role_name->display_name . " role has been removed",
                            'redirect' => 'none',
                            'origin' => 'privilege_removed',
                            );

                        $member = Member::findOrFail($id);
                        $member->notify(new SystemNotification($data));

                        Event::fire(new SystemEvent(auth::id(), 'Removed ' . $role_name->display_name . ' role of ' . $member->username . '.'));


                        //DELETE REQUEST ACCOUNT APPROVAL
                           if(RoleMember::where('role_id', $president_id)->where('member_id', $id)->get()->isEmpty() and RoleMember::where('role_id', $vicepresident_id)->where('member_id', $id)->get()->isEmpty() and RoleMember::where('role_id', $info_id)->where('member_id', $id)->get()->isEmpty() and RoleMember::where('role_id', $asstinfo_id)->where('member_id', $id)->get()->isEmpty() and RoleMember::where('role_id', $faculty_id)->where('member_id', $id)->get()->isEmpty())
                            {
                            ActionMember::where('action_id', $acctreq_approval_id)->where('member_id', $id)->delete();
                            }

                        }
                    $tmp[] = $role->role_id;
                }

                // second add the new role
                foreach ($request->roles as $selected) {
                    if (!in_array($selected, $tmp)) {
                         RoleMember::create([
                            'role_id' => $selected,
                            'member_id' => $id,
                        ]);

                        //faculty,Pres, vice, info, asst_info for Request Account Approval
                        if($president_id == $selected or $vicepresident_id == $selected or $info_id == $selected or $asstinfo_id == $selected or $faculty_id == $selected)
                        {
                             if(ActionMember::where('action_id', $acctreq_approval_id)->where('member_id', $id)->get()->isEmpty())
                            {
                             ActionMember::create([
                            'action_id' => $acctreq_approval_id,
                            'member_id' => $id,]);
                            }
                        }

                        $role = Role::findOrFail($selected);
                        $data = array(
                            'message' => $role->display_name . " role has been assigned to you",
                            'redirect' => $role->name,
                            'origin' => 'privilege',
                            );

                        $member = Member::findOrFail($id);
                        $member->notify(new SystemNotification($data));

                        Event::fire(new SystemEvent(auth::id(), 'Assigned ' . $role->display_name . ' role to ' . $member->username . '.'));

                    }

                }
            }
        }

        return redirect('admin/manage-privilege')->with('success', 'Role Upated!');
    }

    public function edit_action(Request $request, $id)
    {

        if(RoleMember::where('member_id',$id)->get()->isEmpty())
            {
                 $errors[] = "You are not allowed to have an 'Action Privilege', Set a Role Privilege First";
                return redirect('admin/manage-privilege')->withErrors($errors);
            }

        $member = Member::findOrFail($id);

        $action_members = ActionMember::where('member_id', $id)->get();
        $count = count($action_members);

        if ($count == 0) {
            if (!isset($request->actions)){
                return redirect('admin/manage-privilege');
            } else {
                foreach ($request->actions as $selected) {
                    ActionMember::create([
                        'action_id' => $selected,
                        'member_id' => $id,
                    ]);

                    $action = Action::findOrFail($selected);
                    $data = array(
                        'message' => $action->action_display_name . " action has been assigned to you",
                        'redirect' => $action->action_name,
                        'origin' => 'privilege',
                        );

                    $member->notify(new SystemNotification($data));

                    Event::fire(new SystemEvent(auth::id(), 'Assigned ' . $action->action_display_name . ' action to ' . $member->username . '.'));

                }
            }

        } else {
            if (!isset($request->actions)){
                ActionMember::where('member_id', $id)->delete();

                $data = array(
                        'message' => "Your actions has been removed",
                        'redirect' => 'none',
                        'origin' => 'privilege_removed',
                        );

                $member->notify(new SystemNotification($data));

                Event::fire(new SystemEvent(auth::id(), 'Removed action of ' . $member->username . '.'));

            } else {
                // first update the existing action
                foreach ($action_members as $action) {
                    if (!in_array($action->action_id, $request->actions)){
                        ActionMember::where('action_id', $action->action_id)->where('member_id', $id)->delete();

                        $action = Action::findOrFail($action->action_id);
                        $data = array(
                            'message' => "Your " . $action->action_display_name . " action has been removed",
                            'redirect' => 'none',
                            'origin' => 'privilege_removed',
                            );

                        $member->notify(new SystemNotification($data));

                        Event::fire(new SystemEvent(auth::id(), 'Removed ' . $action->action_display_name . ' action of ' . $member->username . '.'));

                        }
                        $tmp[] = $action->action_id;
                }

                // second add the new action

                foreach ($request->actions as $selected) {
                    if (!in_array($selected, $tmp)) {
                         ActionMember::create([
                            'action_id' => $selected,
                            'member_id' => $id,
                        ]);

                        $action = Action::findOrFail($selected);
                        $data = array(
                            'message' => $action->action_display_name . " action has been assigned to you",
                            'redirect' => $action->action_name,
                            'origin' => 'privilege',
                            );

                        $member->notify(new SystemNotification($data));

                        Event::fire(new SystemEvent(auth::id(), 'Assigned ' . $action->action_display_name . ' action to ' . $member->username . '.'));
                    }
                }
            }
        }
        return redirect('admin/manage-privilege')->with('success', 'Action Updated!');
    }


    public function mtics_adviser_remove($id)
    {
        RoleMember::findOrFail($id)->delete();
        return redirect()->back();

    }



    public function mtics_adviser(Request $request)
    {
        $role = Role::where('name','mtics_adviser')->first();
        
        if(!RoleMember::where('member_id',$request->member_id)->where('role_id',$role->id)->get()->isEmpty()){
            return redirect()->back()->withErrors('already set as MTICS Adviser');
        }

        RoleMember::create([
        'role_id' => $role->id,
        'member_id' => $request->member_id,
        ]);

        $data = array(
            'message' => "Mtics Adviser role has been assigned to you",
            'redirect' => $role->name,
            'origin' => 'privilege',
            );

        $member = Member::findOrFail($request->member_id);
        $member->notify(new SystemNotification($data));

        Event::fire(new SystemEvent(auth::id(), 'Assigned  new mtics adviser.'));

        return redirect('admin/manage-privilege')->with('success', 'MTICS ADVISER Updated');

    }



}
