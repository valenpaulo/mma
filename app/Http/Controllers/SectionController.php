<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Input;
use App\Member;
use App\Section;
use App\Year;
use App\Course;
use App\Officerterm;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;
use App\Events\SystemEvent;



class SectionController extends Controller
{


	public function view()
	{


        if(!Gate::allows('superadmin-only'))
        {
        return redirect('/');
        }
		$years = Year::where('year_code','!=','Faculty')->where('year_code','!=','Alumni')->get();
		$sections = DB::table('sections')
            ->join('courses', 'courses.id', '=', 'sections.course_id')
            ->where('sections.section_status','active')
            ->where('sections.section_code','!=','Alumni')
            ->where('sections.section_code','!=','Faculty')
            ->select('sections.*','courses.course_code')
            ->get();
        $allsections = DB::table('sections')
            ->join('courses', 'courses.id', '=', 'sections.course_id')
            ->where('sections.section_code','!=','Alumni')
            ->where('sections.section_code','!=','Faculty')
            ->select('sections.*','courses.course_code')
            ->get();
        $courses = Course::where('course_code','!=','Faculty')->where('course_code','!=','Alumni')->get();

           // dd($changes);
		return view('admin/addFeature/manage_section', compact('years','sections','courses','allsections'));
	}


    public function template_download()
    {
        if(!Gate::allows('superadmin-only'))
        {
        return redirect('/');
        }


        $file= public_path(). "/documents/excel_template/Sync_Masterlist.xlsx";

        return response()->download($file);
    }



    public function print_masterlist(Request $request)
    {
        //dd($request);
         $this->validate($request, [
            'year' => 'required',
            'section' => 'required',
          ]);

         $students = Member::where('year_id',$request->year)->where('section_id',$request->section)->where('status','active')->orderby('last_name','ASC')->get();
         $officerterm = Officerterm::where('status','on-going')->first();
         $section = Section::find($request->section);
         $year = Year::find($request->year);

         $fromyear = $officerterm->from_year;
         $toyear = $officerterm->to_year;
         return view('reports/masterlist',compact('fromyear','toyear','students','year','section'));
     
    }





	public function checksection(Request $request)
    {
        //IF YEAR IS INVALID WITH SELECTED SECTION
         $section = Section::find($request->section);
         $course = Course::find($section->course_id);
         $year =  Year::find($request->year);

         if($course->course_year_count < $year->year_num)
         {
             $errors[] = $year->year_desc. ' is not valid with your selected section';
            return redirect('admin/manage-sections')->withErrors($errors);
         }



            //GET ALL STUDENT WITHIN SELECTED SECTION
    	$allmembers = Member::where('section_id',$request->section)->where('year_id',$request->year)->where('status','active')->get();
    	foreach ($allmembers as $allmember) {
    		$student[$allmember->id]['first_name']=$allmember->first_name;
    		$student[$allmember->id]['last_name']=$allmember->last_name;
            $student[$allmember->id]['stud_id']=$allmember->id_num;
            $student[$allmember->id]['id']=$allmember->id;

    	}



        if(Input::hasFile('import_file')){
            $path = Input::file('import_file')->getRealPath();
            $data = Excel::load($path, function($reader) {
            })->get();
            if(!empty($data) && $data->count()){
                foreach ($data as $key => $value) {

                    $insert[] = ['first_name' => $value->first_name,
                    			 'last_name' => $value->last_name,
                                 'stud_id' => $value->student_id

                                ];

                }

                //dd($insert);
                if(!empty($insert)){
                    foreach ($insert as $key => $value) {
                        # code...

                       //STUDENT NOT IN DATABASE (NEED TO REGISTER FIRST)
                       if(Member::where('id_num',$value['stud_id'])->get()->isEmpty())
                       {

                        $NotInDB[$value['stud_id']]['first_name'] = $value['first_name'];
                        $NotInDB[$value['stud_id']]['last_name'] = $value['last_name'];
                       }


                       //PROCESS FOR ALUMNI
                       $alumni_id = Year::getId('App\Year','year_code','Alumni');
                       $members_alumni = Member::where('first_name',$value['first_name'])->where('last_name',$value['last_name'])->where('id_num',$value['stud_id'])->where('year_id',$alumni_id)->get();
                           // dd($members_alumni);

                       foreach ($members_alumni as $member_alumni) {

                           if($value['stud_id']==$member_alumni->id_num)
                           {
//HERE

                            /*Member::where('id', $member_alumni->id)->update(array('section_id' => $request->section));
                            Member::where('id', $member_alumni->id)->update(array('year_id' => $request->year));
                            Member::where('id', $member_alumni->id)->update(array('status' => 'active'));
                            */

                            $irreg_alumni[$member_alumni->id]['section_id'] =  $request->section;
                            $irreg_alumni[$member_alumni->id]['year_id'] =  $request->year;

                          //REGULAR IF NOT SAME WITH PREVIOUS COURSE
                          //IRREGULAR IF SAME WITH PREVIOUS COURSE
                           $prevcourse = $member_alumni->grad_course;
                          if($prevcourse == $course->course_code)
                          {
//HERE
                            $irreg_alumni[$member_alumni->id]['type'] =  'Irregular';
                           /*Member::where('id', $member_alumni->id)->update(array('type' => 'Irregular'));*/
                          }
                          else
                          {

                            $irreg_alumni[$member_alumni->id]['type'] =  'Regular';
//HERE
                           /*Member::where('id', $member_alumni->id)->update(array('type' => 'Regular'));*/
                          }


                            $irreg_alumni[$member_alumni->id]['stud_id']=$member_alumni->id_num;
                            $irreg_alumni[$member_alumni->id]['first_name']=$member_alumni->first_name;
                            $irreg_alumni[$member_alumni->id]['last_name']=$member_alumni->last_name;
                            continue;
                           }

                       }


                       //IRREGULAR STUDENT WITHIN HIGHER LEVEL
                       $members = Member::where('first_name',$value['first_name'])->where('last_name',$value['last_name'])->where('id_num',$value['stud_id'])->where('status','active')->get();

    					foreach ($members as $member) {
                            if($request->year != $member->year_id)
                            {
                                if($member->type !== "Irregular")
                                {
                                $yearirreg = Year::find($member->year_id);
                                $sectionirreg = Section::find($member->section_id);

                                $irreg[$member->id]['id_num']=$member->id_num;
                                $irreg[$member->id]['first_name']=$member->first_name;
                                $irreg[$member->id]['last_name']=$member->last_name;
                                $irreg[$member->id]['year']=$yearirreg->year_desc;
                                $irreg[$member->id]['section']=$sectionirreg->section_code;
                                $irreg[$member->id]['type'] = 'Irregular';
                                }
//HERE
                                /* Member::where('id', $member->id)->update(array('type' => 'Irregular'));*/

                                continue;
                            }


    					   //IF THE STUDENT HAS DIFFERENT SECTION
	    					if($request->section !== $member->section_id){



//HERE
            					/*Member::where('id', $member->id)->update(array('section_id' => $request->section));*/

                                $pre_sec_code = Section::find($member->section_id)->section_code;
                                $cur_sec_code = Section::find($request->section)->section_code;
                                $changes[$member->id]['prev']= $pre_sec_code;
            					$changes[$member->id]['cur']=$cur_sec_code;
                                $changes[$member->id]['stud_id']=$member->id_num;
            					$changes[$member->id]['first_name']=$member->first_name;
            					$changes[$member->id]['last_name']=$member->last_name;

	    					}




    					}


                    }


                    if(!isset($student))
                    {   //dd($student);

                        // $sectionselected = Section::find($request->section);
                        // $yearselected = Year::find($request->year);
                        // $errors[] = $sectionselected->section_code ." and ". $yearselected->year_desc. " are incorrect  with your masterlist. Please check your Master list or your Inputs";

                    }
                    //IF THE STUDENT ARE NOT INCLUDED IN THE LIST BUT WITHIN THE SELECTED SECTION
                    else
                    {
                    foreach ($student as $key => $value) {

                        $key = array_search($value['stud_id'], array_column($insert, 'stud_id'));

                        if($key !== false)
                        {} //AYAW GUMANA ANG PUSTPA,, pero oks na yan(DONT REMOVE)
                        else
                        {
                            $NotInList[$value['id']]['first_name'] = $value['first_name'];
                            $NotInList[$value['id']]['last_name'] = $value['last_name'];
                            $NotInList[$value['id']]['stud_id'] = $value['stud_id'];
                        }

                    }
                    }
                }
            }
        }


        //dd($NotInDB);
        if (isset($errors)){
            return view('admin/addFeature/section_changes', compact('changes','NotInList','NotInDB','irreg','irreg_alumni'))->withErrors($errors);
        }

		return view('admin/addFeature/section_changes', compact('changes','NotInList','NotInDB','irreg','irreg_alumni', 'year', 'section', 'course'));

    }



    public function make_changes(Request $request)
    {
           // return view('admin/addFeature/section_summary');

        if(isset($request->changes))
        {
            foreach ($request->changes as $key => $value) {

                $section_id = Section::getId('App\Section','section_code',$value['section']);
                $member_id = Member::getId('App\Member','id_num',$value['stud_id']);

              Member::where('id', $member_id)->update(array('status' => 'active'));
                //echo $section_id." <br> ". $member_id.'<br>';
              Member::where('id', $member_id)->update(array('section_id' => $section_id));
            }

        }

         if(isset($request->irreg))
        {
            foreach ($request->irreg as $key => $value) {

                $member_id = Member::getId('App\Member','id_num',$value['stud_id']);
                Member::where('id', $member_id)->update(array('status' => 'active'));
               Member::where('id', $member_id)->update(array('type' => 'Irregular'));


            }
        }

        if(isset($request->alumni))
        {
            foreach ($request->alumni as $key => $value) {


                $member_id = Member::getId('App\Member','id_num',$value['stud_id']);


                Member::where('id', $member_id)->update(array('section_id' => $value['section']));
                Member::where('id', $member_id)->update(array('year_id' => $value['year']));
                Member::where('id', $member_id)->update(array('status' => 'active'));
                Member::where('id', $member_id)->update(array('type' => $value['type']));



            }
        }
        $NotInList = array();
        if(isset($request->NotInList))
        {
            foreach ($request->NotInList as $key => $value) {
                $member_id = Member::getId('App\Member','id_num',$value['stud_id']);
                Member::where('id', $member_id)->update(array('status' => 'inactive'));
            }
            $NotInList = $request->NotInList;

        }

        $NotInDB = array();
        if(isset($request->NotInDB))
        {
           
            $NotInDB = $request->NotInDB;

        }



        $students = Member::where('year_id',$request->year_id)->where('section_id',$request->section_id)->where('status','active')->orderby('last_name','ASC')->get();
        $section = Section::find($request->section_id);
        $year = Year::find($request->year_id);
        //dd($NotInList);

            return view('admin/addFeature/section_summary', compact('students','NotInList','section','year','NotInDB'));




    }





//COURSE //COURSE //COURSE //COURSE //COURSE //COURSE //COURSE

    public function course_store(Request $request)
    {
        $this->validate($request, [
        'course_code' => 'required|max:255|unique:courses',
        'course_name' => 'required|max:255',
        'course_year_count' => 'required|integer',
        ]);

        if ($request->course_year_count > 5)
        {
            return redirect()->back()->withErrors('Maximum Year is 5 years only');

        }
         Course::create([
            'course_name' => $request->course_name,
            'course_code' => $request->course_code,
            'course_year_count' => $request->course_year_count,
        ]);

         Event::fire(new SystemEvent(auth::id(), 'Added course "' . $request->course_name . '".'));

        return redirect('admin/manage-sections')->with('success', 'Course Added!');
    }

     public function course_edit(Request $request,$id)
    {
        $course = Course::find($id);

        if($course->course_code !== $request->course_code)
        {
            $this->validate($request, [
            'course_code' => 'required|max:255|unique:courses',
            ]);
        }

        $this->validate($request, [
        'course_code' => 'required|max:255',
        'course_name' => 'required|max:255',
        ]);


        $course = Course::find($id);
        $course->course_name = $request->course_name;
        $course->course_code = $request->course_code;
        $course->course_year_count = $request->course_year_count;
        $course->save();

        Event::fire(new SystemEvent(auth::id(), 'Updated "' . $course->course_name . '" course information.'));

        return redirect('admin/manage-sections')->with('success', 'Course Edited!');
    }


    public function course_delete(Request $request,$id)
    {
        if(!Section::where('course_id',$id)->get()->isEmpty())
            {
                return redirect()->back()->withErrors('Course already has section, you cannot delete this course');
            }
            else
            {
                Course::find($id)->delete();
                return redirect()->back();
            }
    }


//COURSE //COURSE //COURSE //COURSE //COURSE //COURSE //COURSE



//SECTION //SECTION //SECTION //SECTION //SECTION //SECTION
 public function section_store(Request $request)
    {
        $this->validate($request, [
        'course' => 'required',
        'section_code' => 'required',
        ]);
        //dd($request);
        Section::create([
            'course_id' => $request->course,
            'section_code' => $request->section_code,
        ]);

        Event::fire(new SystemEvent(auth::id(), 'Added section "' . $request->section_code . '".'));

        return redirect('admin/manage-sections')->with('success', 'Section Added!');


    }


  public function section_edit(Request $request, $id)
    {
        $section = Section::find($id);
        if($section->section_code !== $request->section_code)
        {
            $this->validate($request, [
            'section_code' => 'required|max:255|unique:sections',
            ]);
        }

        $this->validate($request, [
        'course' => 'required',
        'section_code' => 'required|max:255',
        'status' => 'required',
        ]);

        $section = Section::find($id);
        $section->course_id = $request->course;
        $section->section_code = $request->section_code;
        $section->section_status = $request->status;
        $section->save();

        Event::fire(new SystemEvent(auth::id(), 'Updated "' . $section->section_code . '" section information.'));

        return redirect('admin/manage-sections')->with('success', 'Section Edited!');

    }


    public function section_delete(Request $request,$id)
    {
        if(!Member::where('section_id',$id)->get()->isEmpty())
            {
                return redirect()->back()->withErrors('Some Members already using this section, you cannot delete this section');
            }
            else
            {
                Section::find($id)->delete();
                return redirect()->back();
            }
    }


//SECTION //SECTION //SECTION //SECTION //SECTION //SECTION




}
