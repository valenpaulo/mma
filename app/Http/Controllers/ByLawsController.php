<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ByLawsController extends Controller
{
    public function download() {
        $file= public_path(). "/documents/bylaws/mtics_bylaws.pdf";
        $filename = explode("/", 'bylaws/mtics_bylaws.pdf')[1];
        
        return response()->download($file, $filename);
    }
}
