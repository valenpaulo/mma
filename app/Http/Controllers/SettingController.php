<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;


class SettingController extends Controller
{
    public function index()
    {
        # code...
        if(!Gate::allows('admin-only'))
        {
        return redirect('/');
        }
        return view('setting/setting');
    }
}
