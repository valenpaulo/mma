<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\RoleMember;
use App\Role;
use App\Task;
use App\Member;
use App\MticsEvent;
use App\Notifications\SystemNotification;
use App\EventPurchaseList;
use App\EventPurchaseEquipList;
use App\TaskDeny;
use App\Penalty;
use App\PenaltyImage;

use Illuminate\Support\Facades\Event;
use App\Events\SystemEvent;

class TaskController extends Controller
{
    public function store(Request $request, $id)
    {

        $event = MticsEvent::find($id);
        if($event->status !== 'on-going'){
            return redirect()->back()->withErrors("Event status is '".$event->status."', make changes are prohibited");
        }

         $this->validate($request, [
        'role_id' => 'required|max:255',
        'due_date' => 'required|max:255',
        'task_name' => 'required',
        ]);

        //dd($request);
        $member_role = Role::where('id', $request->role_id)->first()->name;

        // if ($member_role == 'external') {
        //     $this->validate($request, [
        //         'itemname.*' => 'required|max:255',
        //         'itemquan.*' => 'required|max:255',
        //         'itemdesc.*' => 'required|max:255',
        //     ]);

        //     foreach ($request->itemname as $key => $item) {
        //         dd($key);
        //     }
        // }

        $role_member = RoleMember::where('role_id',$request->role_id)->first();
        $member_id = $role_member->member_id;
        $role_members = RoleMember::where('role_id',$request->role_id)->get();
        foreach ($role_members as $role_member) {
         $member_id = $role_member->member_id;
        }

        $task = New Task;
        $task->member_id = $member_id;
        $task->admin_id = Auth::id();
        $task->due_date = $request->due_date;
        if($request->task_desc == "" or $request->task_desc == null)
        {
        $task->task_desc = 'Please do this task, '.$request->task_name;
        }
        else
        {
        $task->task_desc = $request->task_desc;
        }
        $task->task_name = $request->task_name;
        $task->role_id = $request->role_id;

        if($id) {
         $task->event_id = $id;
        }
        $task->save();

        if ($member_role == 'external') {
        $this->validate($request, [
            'itemname.*' => 'required|max:255',
            'itemquan.*' => 'required|max:255',
        ]);

        foreach ($request->itemname as $key => $item) {
            $items[$key]['itemname']=$item;
        }
        foreach ($request->itemquan as $key => $quan) {
            $items[$key]['itemquan']=$quan;
        }
        foreach ($request->itemdesc as $key => $desc) {
            $items[$key]['itemdesc']=$desc;
        }

        foreach ($items as $key => $item) {
           $eventpurchaselist = New EventPurchaseList;
           $eventpurchaselist->task_id = $task->id;
           $eventpurchaselist->event_itemname = $item['itemname'];
           $eventpurchaselist->event_orig_quan = $item['itemquan'];
           $eventpurchaselist->event_itemdesc = $item['itemdesc'];
           $eventpurchaselist->save();
        }
        }

        $data = array(
            'message' => 'New Task: ' . $request->task_name,
            'redirect' => array('role' => $role_member->role->name, 'event_id' => $id),
            'origin' => 'task',
            );

        $member = Member::findOrFail($member_id);
        $member->notify(new SystemNotification($data));

        Event::fire(new SystemEvent(auth::id(), 'Added a New Task.'));

        return redirect()->back()->with('success', 'Task Added!');

    }

    public function edit(Request $request, $id)
    {
         //dd($request);
        $this->validate($request, [
        'role_id' => 'required|max:255',
        'due_date' => 'required|max:255',
        'task_desc' => 'required|min:8',
        'task_name' => 'required',
        ]);

        $role_members = RoleMember::where('role_id',$request->role_id)->get();
        foreach ($role_members as $role_member) {
         $member_id = $role_member->member_id;
        }

        $task = Task::findOrFail($id);
        $task->member_id = $member_id;
        $task->admin_id = Auth::id();
        $task->due_date = $request->due_date;
        $task->task_desc = $request->task_desc;
        $task->task_name = $request->task_name;
        $task->role_id = $request->role_id;

        if($request->event_id) {
         $task->event_id = $request->event_id;
        }
        $task->save();
        return redirect()->back();

    }

    public function delete($id) {
        Task::findOrFail($id)->delete();
        return redirect()->back();
    }

    public function start($id) {
        $task = Task::findOrFail($id);
        $task->task_status = 'on-going';
        $task->save();

        $data = array(
            'message' => $task->task_name . ' task has started',
            'redirect' => array('role' => 'president', 'event_id' => $task->event_id),
            'origin' => 'task',
            );

        $member = Member::findOrFail($task->admin_id);
        $member->notify(new SystemNotification($data));

        Event::fire(new SystemEvent(auth::id(), 'Task Started.'));

        return redirect()->back()->with('success', 'Task Started!');
    }


    public function forvalidation($id) {
        $task = Task::findOrFail($id);
        $task->task_status = 'for validation';
        $task->save();
        return redirect()->back();
    }


    public function approved($id) {
        $task = Task::findOrFail($id);

        $date = date('Y-m-d');
        $due_date = $task->due_date;
        $duedate_with3days = date('Y-m-d', strtotime($due_date. ' + 3 days'));


        if($duedate_with3days < $date){
        $task->task_status = 'failed';


        }
        else{
        $task->task_status = 'done';
        }

        if($task->task_status == 'failed'){
            $penaltylastrow = Penalty::where('member_id',$task->member_id)->orderby('id','desc')->first();
            $penalty = New Penalty;
            $penalty->task_id = $task->id;
            $penalty->member_id = $task->member_id;
            $penalty->validator_id = Auth::id();
            $penalty->reason = 'overdue task';

            if($penaltylastrow == null){
            $penalty->penalty_type = 'verbal';
            }
            elseif($penaltylastrow->penalty_type == 'fee'){
            $penalty->penalty_type = 'verbal';
            }
            elseif($penaltylastrow->penalty_type == 'verbal'){
            $penalty->penalty_type = 'written';
            }
            elseif($penaltylastrow->penalty_type == 'written'){
            $penalty->penalty_type = 'fee';
            $penalty->fee = 10;
            }

            $penalty->save();

        }

        $task->save();

        $data = array(
            'message' => $task->task_name . ' task has been approved',
            'redirect' => array('role' => 'docu', 'event_id' => $task->event_id),
            'origin' => 'docu',
            );

        $member = Member::findOrFail($task->member_id);
        $member->notify(new SystemNotification($data));


        Event::fire(new SystemEvent(auth::id(), 'Letter is Approved.'));

        return redirect()->back()->with('success', 'Approved!');
    }

    public function disapprove(Request $request,$id) {


         $this->validate($request, [
        'reason' => 'required',
        ]);

        $taskdeny = new TaskDeny;
        $taskdeny->task_id = $id;
        $taskdeny->validator_id = Auth::id();
        $taskdeny->reason = $request->reason;
        $taskdeny->save();

        $task = Task::findOrFail($id);
        $task->task_status = 'disapproved';
        $task->save();

        $data = array(
            'message' => $task->task_name . ' task has been approved',
            'redirect' => array('role' => 'docu', 'event_id' => $task->event_id),
            'origin' => 'docu',
            );

        $member = Member::findOrFail($task->member_id);
        $member->notify(new SystemNotification($data));


        Event::fire(new SystemEvent(auth::id(), 'Letter is Disapproved.'));

        return redirect()->back()->with('success', 'Disapproved!');
    }


//MANAGE PENALTY FOR TASK //MANAGE PENALTY FOR TASK//MANAGE PENALTY FOR TASK//MANAGE PENALTY FOR TASK//MANAGE

    public function failedtask_view($id){
        $failedtasks = Penalty::whereHas('task', function($q) use($id) {
            $q->where('event_id', $id)->where('task_status','failed');
            })->orwhere('event_id',$id)->get();

        $member_officers = RoleMember::wherehas('role', function($q){$q->where('name','!=','faculty')->where('name','!=','super_admin')->where('name','!=','mtics_adviser');})->groupby('member_id')->selectRaw('member_id')->get();
        $roles =RoleMember::wherehas('role', function($q){$q->where('name','!=','faculty')->where('name','!=','super_admin')->where('name','!=','mtics_adviser');})->get();


        //dd($member_officers);
        return view('officers/event/failedtaskview',compact('failedtasks','id','member_officers','roles'));
    }

    public function failedtask_done(Request $request,$id,$penalty_id){
        $penalty = Penalty::findOrFail($penalty_id);
        $this->validate($request, [
        'upload.*' => 'image|mimes:jpeg,jpg,png|max:5000', //image only
        ]);

        // dd($request->upload);
        if($request->hasFile('upload')){


            if($request->hasFile('upload')){
                foreach ($request->file('upload') as $upload) {

                    if($upload->getmimeType() !== 'image/jpeg' and $upload->getmimeType()!== 'image/png')
                    {
                      continue;
                    }
                    else
                    {
                        $date = date('Y-m-d');
                        $time = round(microtime(true) * 1000);
                        $fileName = $upload->getClientOriginalName();
                        $image_name = 'penalties/'.$penalty->penalty_type.'/'.$penalty_id.'_'.$time.'_'.$date.'.jpg';
                        //move image to images/event_gallery/'title of event'

                         if(url('/') == 'http://mtics-ma.tuptaguig.com'){
                            $upload->move('public/images/penalties/'.$penalty->penalty_type.'/', $penalty_id.'_'.$time.'_'.$date.'.jpg');
                            }
                            else{
                            $upload->move('images/penalties/'.$penalty->penalty_type.'/', $penalty_id.'_'.$time.'_'.$date.'.jpg');
                            }

                        $penalty_upload = new PenaltyImage;
                        $penalty_upload->penalty_id = $penalty_id;
                        $penalty_upload->admin_id = Auth::id();
                        $penalty_upload->image_name = $image_name;
                        $penalty_upload->save();

                    }
                }
            }
        }

        if($penalty->penalty_status == 'done' or $penalty->penalty_status == 'paid')
        {
            return redirect()->back()->withErrors('This penalty is alredy '.$penalty->penalty_status);

        }

        if($penalty->penalty_type == 'fee'){
            return redirect()->back()->withErrors('You cannot set as done this penalty, need to pay the penalty fines');
        }

        $penalty->penalty_status = 'done';
        $penalty->save();

        Event::fire(new SystemEvent(auth::id(), 'Penalty Done.'));

        return redirect()->back()->with('success', 'Done!');
    }


     public function failedtask_edit(Request $request,$id,$penalty_id){
        $penalty = Penalty::findOrFail($penalty_id);

        if($penalty->penalty_status == 'done' or $penalty->penalty_status == 'paid')
        {
            return redirect()->back()->withErrors('This penalty is alredy '.$penalty->penalty_status);

        }

        $this->validate($request, [
        'penalty_type' => 'required',
        'reason' => 'required',
        ]);

        if($request->penalty_type == 'fee'){
            $this->validate($request, [
            'fee_amount' => 'required|integer',
            ]);

        $penalty->fee = $request->fee_amount;
        }
        else{
        $penalty->fee = null;
        }
        $penalty->penalty_type = $request->penalty_type;
        $penalty->reason = $request->reason;
        $penalty->save();
        return redirect()->back();

    }


    public function failedtask_add(Request $request,$id){
         $this->validate($request, [
            'officer_id' => 'required|integer',
            'penalty_type' => 'required',
            'reason' => 'required',
            ]);


        $penalty = new Penalty;
        $penalty->member_id = $request->officer_id;
        $penalty->validator_id = Auth::id();
        $penalty->reason = $request->reason;

        if($request->penalty_type == 'fee'){
            $this->validate($request, [
            'fee_amount' => 'required|integer',
            ]);
        $penalty->fee = $request->fee_amount;
        }

        $penalty->penalty_type = $request->penalty_type;
        $penalty->event_id = $id;
        $penalty->save();

        return redirect()->back();


    }



//PAYMENT FOR PENALTY //PAYMENT FOR PENALTY //PAYMENT FOR PENALTY //PAYMENT FOR PENALTY //PAYMENT FOR PENALTY
    public function penalty_payment(Request $request){

        $penaltysum = Penalty::where('penalty_type','fee')->where('penalty_status','pending')->groupby('member_id')->selectRaw('member_id, sum(fee) as sum')->where('member_id',$request->member_id)->first();

        if($penaltysum->sum < $request->mtics_payment){
            return redirect()->back()->withErrors('The total amount of penalty fines for '. $penaltysum->member->first_name .' '. $penaltysum->member->last_name . ' is '.$penaltysum->sum);
        }

        $penalties = Penalty::where('penalty_type','fee')->where('penalty_status','pending')->where('member_id',$request->member_id)->get();

        $totalpayment = $request->mtics_payment;
       // dd($penalties);
        foreach ($penalties as $penalty) {
           if($penalty->fee <= $totalpayment){
            $penalty->penalty_status = 'paid';
            $penalty->update();

            $data = array(
            'message' => "Officer paid a penalty fee.",
            'redirect' => 'admin/president/manage-event/' . $penalty->task->event_id . '/failed-task',
            'origin' => 'penalty',
            );

            $role_id = Role::where('name', 'president')->first()->id;
            $member = Member::whereHas('role', function($q) use ($role_id) {
                $q->where('role_id', $role_id);
            })->first();
            $member->notify(new SystemNotification($data));

            $totalpayment = $totalpayment - $penalty->fee;
           }
        }

        if($totalpayment > 0){
          return redirect()->back()->withErrors('please return Php '.$totalpayment.'.00, must pay full amount per penalty');
        }

        Event::fire(new SystemEvent(auth::id(), 'Receive a Payment for Penalty.'));

        return redirect()->back()->with('success', 'Receive Payment!');
    }


    public function getPenaltyAmount(Request $request) {
        $penaltysum = Penalty::where('penalty_type','fee')->where('penalty_status','pending')->groupby('member_id')->selectRaw('member_id, sum(fee) as sum')->where('member_id', $request->officer_id)->first();

        return $penaltysum->sum;
    }

}
