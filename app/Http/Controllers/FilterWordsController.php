<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Filterword;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Event;

use App\Events\SystemEvent;

class FilterWordsController extends Controller
{


//VIEW VIEW VIEW VIEW  VIEW VIEW  VIEW VIEW  VIEW VIEW
 public function view()
   {

    if(!Gate::allows('admin-only'))
        {
        return redirect('/');
        }

   	$filterwords = Filterword::all();
   	return view('setting/manage_filterwords', compact('filterwords'));
   }


/*STORE STORE STORE STORE STORE STORE STORE STORE STORE */
public function store(Request $request)
	{
        $this->validate($request, [
        'Filter_word' => 'unique:filterwords|max:255',
        ]);

        Filterword::create([
            'Filter_word' => $request->Filter_word,
        ]);

        Event::fire(new SystemEvent(auth::id(), 'Added new profranity word "' . $request->Filter_word . '".'));

        return redirect('admin/manage-filterwords')->with('success', 'Profanity Word Added!');

	}


public function delete($id)
    {
        $profanity = Filterword::findOrFail($id);
        $profanity->delete();

        Event::fire(new SystemEvent(auth::id(), 'Deleted profranity word "' . $profanity->Filter_word . '".'));

        return redirect('admin/manage-filterwords')->with('success', 'Profanity Word Deleted!');

    }

public function edit(Request $request, $id)
    {
        $profanity = Filterword::findOrFail($id);
        $profanity->Filter_word = $request->Filter_word;
        $profanity->save();

        Event::fire(new SystemEvent(auth::id(), 'Edited profranity word to "' . $request->Filter_word . '".'));

        return redirect('admin/manage-filterwords')->with('success', 'Saved!');

    }

/*BULK FOR FILTER WORDS BULK FOR FILTER WORDS BULK FOR FILTER WORDS BULK FOR FILTER WORDS */
 public function importExcel()
    {
        if(Input::hasFile('import_file')){
            $path = Input::file('import_file')->getRealPath();
            $data = Excel::load($path, function($reader) {
            })->get();
            if(!empty($data) && $data->count()){
                foreach ($data as $key => $value) {

                   if(is_null($value->filter_word))
                        {
                            continue;
                        }
                    elseif(Filterword::where('Filter_word',$value->filter_word)->get()->isEmpty()){
                        $insert[] = ['Filter_word' => $value->filter_word];
                    }

                }
                    //dd($insert);

                if(!empty($insert)){
                    foreach ($insert as $key => $value) {
                        # code...
                        $profanity = Filterword::where('Filter_word', '=', $value['Filter_word'])->first();
                        if ($profanity) {
                           // user doesn't exist
                            $errors[] = $value['Filter_word'].' has already been taken.';
                            continue;
                        }

                        Filterword::create([
                            'Filter_word' => $value['Filter_word'],
                        ]);
                    }

                }
            }
        }
        if (isset($errors)){
            return redirect('admin/manage-filterwords')->withErrors($errors);
        }
        return redirect('admin/manage-filterwords')->with('success', 'Profanity Word Added!');
    }


}
