<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Event;

use App\Events\SystemEvent;

use App\Banner;

class BannerController extends Controller
{
    public function view() {
        $banners = Banner::all();
        return view('setting/banner', compact('banners'));
    }

    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'banner_name' => 'required|max:255',
            'image' => 'required|mimes:jpeg,jpg,png',
        ], [
             'banner_name.required' => 'The banner name field is required.',
             'image.required' => 'Please make sure to choose image to upload.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $image = $request->file('image');
        if($request->hasFile('image'))
        {   
             if(url('/') == 'http://mtics-ma.tuptaguig.com'){
                    $image->move('public/images/banner/', $request->file('image')->getFilename().'-'.$image->getClientOriginalName());
                }
                else{
                 $image->move('images/banner/', $request->file('image')->getFilename().'-'.$image->getClientOriginalName());
                }
            $imagename =  'banner/'.$request->file('image')->getFilename().'-'.$image->getClientOriginalName();

            Banner::create([
                'banner_name' => $request->banner_name,
                'banner_path' => $imagename,
                'member_id'   => Auth::user()->id,
            ]);

        }

        Event::fire(new SystemEvent(auth::id(), 'Added a new banner.'));

        return redirect()->back()->with('success', 'Banner Added!');
    }

    public function delete($id) {
        $banner = Banner::where('id', $id)->first();
          if(url('/') == 'http://mtics-ma.tuptaguig.com'){
            File::delete('public/images/' . $banner->filename);
            }
          else{ 
            File::delete('images/' . $banner->filename);
            }
        $banner->delete();

        Event::fire(new SystemEvent(auth::id(), 'Deleted a banner.'));

        return redirect()->back()->with('success', 'Banner Deleted!');
    }

    public function enable($id) {
        $oldbanner = Banner::where('status', 'active')->first();
        if($oldbanner) {
            $oldbanner->status = 'inactive';
            $oldbanner->save();
        }

        $banner = Banner::where('id', $id)->first();
        $banner->status = 'active';
        $banner->save();

        Event::fire(new SystemEvent(auth::id(), 'Enabled banner.'));

        return redirect()->back()->with('success', 'Enabled!');
    }

     public function disable($id) {
        $banner = Banner::where('id', $id)->first();
        $banner->status = 'inactive';
        $banner->save();

        Event::fire(new SystemEvent(auth::id(), 'Disabled banner.'));

        return redirect()->back()->with('success', 'Disabled!');
    }
}
