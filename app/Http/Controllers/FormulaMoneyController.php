<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MticsBudget;
use App\MticsBudgetBreakdown;
use App\MticsBudgetDeny;
use App\MticsBudgetExpense;
use App\RoleMember;
use App\Role;
use App\Member;
use App\MticsfundPayment;
use App\MticsWithdrawal;
use App\MticsDeposit;
use App\MticsBudgetExpCat;
use App\MoneyRequest;
use App\ReimburseRequest;
use App\EventExpense;
use App\EventWithdrawal;
use App\EventDeposit;
use App\EventPayment;
use App\MticsEvent;
use App\Task;
use App\EventPurchaseEquipList;
use App\EventAmountBreakdown;
use App\Receipt;
use App\Penalty;

class FormulaMoneyController extends Controller
{
	public function checkExternalBudget($task_id) {

	//formula for External Bucket money per specific task
	//Money request - totalexpenses_purchasedlist
    $task = Task::findOrFail($task_id);
    $moneyrequest = MoneyRequest::where('task_id',$task->id)->first();
    if($moneyrequest !== null){
       $sum_equip_price = 0;
       $sum_price = 0;
         if($task->event_id == null)
         {
             foreach ($task->mticspurchaselist as $mticspurchaselist) {
               if(!MticsPurchaseEquipList::where('mtics_purchase_list_id',$mticspurchaselist->id)->get()->isEmpty()){
                $sum_equip_price = $sum_equip_price + MticsPurchaseEquipList::where('mtics_purchase_list_id',$mticspurchaselist->id)->get()->sum('mtics_equip_price');
               }
               else{
                $sum_price = $sum_price + $mticspurchaselist->mtics_total_price;
               }
             }
         }
         else
         {
              foreach ($task->eventpurchaselist as $eventpurchaselist) {
               if(!EventPurchaseEquipList::where('event_purchase_list_id',$eventpurchaselist->id)->get()->isEmpty()){
                $sum_equip_price = $sum_equip_price + EventPurchaseEquipList::where('event_purchase_list_id',$eventpurchaselist->id)->get()->sum('event_equip_price');
               }
               else{
                $sum_price = $sum_price + $eventpurchaselist->event_total_price;

               }
             }
         }


         $total_deduction = $sum_price + $sum_equip_price;
         //COMPUTING TOTAL MONEY BUCKET
         $total_money_bucket = $moneyrequest->amount - $total_deduction;
         if($moneyrequest->mon_req_status == 'cleared' or $moneyrequest->mon_req_status == 'denied' or $moneyrequest->excess_amount !== null or $moneyrequest->mon_req_status == 'for validation' ){
          $total_money_bucket = 0;
         }
      }
      else
      {
        $total_money_bucket = 0;
      }

      return $total_money_bucket;
    }



	//MTICS //MTICS //MTICS //MTICS //MTICS //MTICS //MTICS //MTICS //MTICS //MTICS //MTICS //MTICS
	public function CheckMticsFundBudget()
	{
	//Formula for MTICSFUNDBUDGET cashOnHand
	//totalwithdrawal - budgetexpense - moneyrequest - reimbursement + excessmoney - (penalty payment)
	$mticspayment_withdraw = MticsWithdrawal::where('withdrawal_status','cleared')->get()->sum('withdrawal_amt');
	$mtics_expenses = MticsBudgetExpense::all()->sum('expense_amt');
	$totalmticsbudget = $mticspayment_withdraw - $mtics_expenses;

	$moneyrequests = MoneyRequest::with('task')->where('mon_req_status','ongoing')->orwhere('mon_req_status','cleared')->orwhere('mon_req_status','for validation')->orwhere('excess_amount','!=',null)->get();

	//SUBTRACT REQUEST MONEY , SUBTRACT REIMBURSEMENT, ADD EXCESS MONEY
	$requestedmoney = 0;
	$excess_money = 0;
	$reimburse_money = 0;
	if(!$moneyrequests->isEmpty()){
	foreach ($moneyrequests as $moneyrequest) {


	if($moneyrequest->task->event_id == null){
	$requestedmoney = $requestedmoney + $moneyrequest->amount;

	  if($moneyrequest->excess_amount > 0){
	    $excess_money = $excess_money + $moneyrequest->excess_amount;
	  }
	  if(!ReimburseRequest::where('money_request_id',$moneyrequest->id)->where('reimburse_status','accepted')->get()->isEmpty()){
	    $reimburse = ReimburseRequest::where('money_request_id',$moneyrequest->id)->where('reimburse_status','accepted')->first();
	    $reimburse_money = $reimburse_money + $reimburse->reimburse_amount;
	  }

	}

	}
	}



  //penalty payment
  $penaltypayment = Penalty::where('penalty_type','fee')->where('penalty_status','paid')->get()->sum('fee');
  
	$mticsfund_money = $totalmticsbudget - $requestedmoney + $excess_money - $reimburse_money + $penaltypayment;

	return $mticsfund_money;
	}


	public function CheckMticsFundPayment()
  {
    //formula for mticsfundpayment_cashOnHand
    //totalmticsfundpayment - depositedmticsfundpayemnt
      $mticspayment = MticsfundPayment::all()->sum('paymticsfund_amt');
      $mticspayment_deposit = MticsDeposit::where('deposit_source','MTICS Payment')->where('deposit_status','cleared')->get()->sum('deposit_amt');
      $totalmticspayment = $mticspayment - $mticspayment_deposit;
    return $totalmticspayment;
  }

//EVENT //EVENT //EVENT //EVENT //EVENT //EVENT //EVENT //EVENT //EVENT //EVENT //EVENT //EVENT

   public function CheckEventFundBudget($event_id)
  {
  	//Formula for specific event
  	//eventtotalwithdraw - financeexpenses - requestmoney - reimbursement + excessmoney
     $eventpayment_withdraw = EventWithdrawal::where('event_id',$event_id)->where('withdrawal_status','cleared')->get()->sum('withdrawal_amt');
      $event_expenses = EventExpense::where('event_id',$event_id)->sum('expense_amt');
      $totaleventbudget = $eventpayment_withdraw - $event_expenses;

    $moneyrequests = MoneyRequest::wherehas('task', function($q) use($event_id) {
      $q->where('event_id', $event_id);
    })->where(function($q) {
      $q->where('mon_req_status','ongoing')->orwhere('mon_req_status','cleared')->orwhere('mon_req_status','for validation')->orwhere('excess_amount','!=',null);
    })->get();

        //SUBTRACT REQUEST MONEY
        $requestedmoney = 0;
        $excess_money = 0;
        $reimburse_money = 0;
        if(!$moneyrequests->isEmpty()){
          foreach ($moneyrequests as $moneyrequest) {

            //filtering of selected event
            if($moneyrequest->task->event_id == $event_id){
            $requestedmoney = $requestedmoney + $moneyrequest->amount;
            $eventfund_money = $totaleventbudget - $requestedmoney;

        //dd($totaleventbudget);

              if($moneyrequest->excess_amount > 0){
                $excess_money = $excess_money + $moneyrequest->excess_amount;
              }
              if(!ReimburseRequest::where('money_request_id',$moneyrequest->id)->where('reimburse_status','accepted')->get()->isEmpty()){
                $reimburse = ReimburseRequest::where('money_request_id',$moneyrequest->id)->where('reimburse_status','accepted')->first();
                $reimburse_money = $reimburse_money + $reimburse->reimburse_amount;
              }
            }
            else
            {
              $eventfund_money = $totaleventbudget;
            }
          }
        }

        else{
          $eventfund_money = $totaleventbudget;
        }



        $eventfund_money = $eventfund_money + $excess_money - $reimburse_money;
        return $eventfund_money;
  }


  public function CheckEventFundPayment($event_id)
  { //formula for Event fund payment - deposited event fund payment
    $moneyrequests = MoneyRequest::whereHas('task', function($q) use($event_id) {
        $q->where('event_id', $event_id);
    })->where(function($q) {
        $q->where('mon_req_status','ongoing')->orwhere('mon_req_status','cleared')->orwhere('mon_req_status','for validation')->orwhere('excess_amount','!=',null);
    })->get();

      $eventpayment = EventPayment::where('event_id',$event_id)->sum('payevent_amt');

      $eventpayment_deposit = EventDeposit::where('event_id',$event_id)->where('deposit_source','Event Payment')->where('deposit_status','cleared')->get()->sum('deposit_amt');
      $mtics_eventpayment_deposit = MticsDeposit::where('deposit_source','Event Collection')->where('event_id',$event_id)->get()->sum('deposit_amt');

      $totaleventpayment = $eventpayment - $eventpayment_deposit - $mtics_eventpayment_deposit;

    return $totaleventpayment;
  }


}
