<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\File;
use App\Notifications\SystemNotification;
use DateTime;
use App\MticsBudget;
use App\MticsBudgetBreakdown;
use App\MticsBudgetDeny;
use App\MticsBudgetExpense;
use App\RoleMember;
use App\Role;
use App\Member;
use App\MticsfundPayment;
use App\MticsWithdrawal;
use App\MticsDeposit;
use App\MticsBudgetExpCat;
use App\MoneyRequest;
use App\ReimburseRequest;
use App\EventExpense;
use App\EventWithdrawal;
use App\EventDeposit;
use App\EventPayment;
use App\MticsEvent;
use App\EventAmountBreakdown;
use App\Receipt;
use App\AuditorFinancialApprove;
use App\Officerterm;


use Illuminate\Support\Facades\Event;
use App\Events\SystemEvent;


class MticsBudgetController extends Controller
{

  //PRESIDENT //PRESIDENT //PRESIDENT //PRESIDENT
  public function president_budget_view()
  {
     if(!Gate::allows('president-only'))
        {
        return redirect('/');
        }
    $mticsbudgetsRequest = MticsBudget::with('budget_breakdown')->where('budget_status', '!=', 'pending')->orderBy('id', 'desc')->get();

    //dd($mticsbudgetsRequest);
    $budget_expcats = MticsBudgetExpCat::wherehas('breakdowns')->get();
    return view('officers/manage_mtics_budget_president',compact('mticsbudgetsRequest','budget_expcats'));
  }

  public function president_budget_approved($id)
  {

    $mticsbudget = MticsBudget::find($id);

    if($mticsbudget->budget_status !== 'for validation' )
    {
      return redirect()->back()->withErrors('Request status is '.$mticsbudget->budget_status.', make changes are prohibited ');
  	}

  	$role_pres = Role::where('name','president')->first();
  	$role_vice = Role::where('name','vice_president')->first();

  	if(!RoleMember::where('member_id',Auth::id())->where('role_id',$role_pres->id)->get()->isEmpty()){
		    MticsBudget::where('id', $id)->update(array('confirm_admin_role_id' => $role_pres->id));
  		}
  		elseif(!RoleMember::where('member_id',Auth::id())->where('role_id',$role_vice->id)->get()->isEmpty()){
		    MticsBudget::where('id', $id)->update(array('confirm_admin_role_id' => $role_vice->id));
  		}
  		else
  		{
  		return redirect()->back()->withErrors('invalid role');
  		}

	MticsBudget::where('id', $id)->update(array('confirm_admin_id' => Auth::id()));
    MticsBudget::where('id', $id)->update(array('budget_status' => 'approved'));

    $data = array(
    'message' => "Budget Request has been approved",
    'redirect' => array('role' => 'finance'),
    'origin' => 'mticsbudgetPresident',
    );

    $role_id = Role::where('name', 'finance')->first()->id;
    $member = Member::whereHas('role', function($q) use ($role_id) {
        $q->where('role_id', $role_id);
    })->first();
    $member->notify(new SystemNotification($data));

    Event::fire(new SystemEvent(auth::id(), 'Approved Monthly Budget Request.'));

    return redirect('admin/president/requested-mtics-budget')->with('success', 'Request Approved!');

  }


   public function president_budget_denied(Request $request,$id)
  {

    $mticsbudget = MticsBudget::find($id);
     if($mticsbudget->budget_status !== 'for validation' )
    {
      return redirect()->back()->withErrors('Request status is '.$mticsbudget->budget_status.', make changes are prohibited ');
    }

  	$this->validate($request, [
    'denied_reason' => 'required',
    ]);

    $role_pres = Role::where('name','president')->first();
  	$role_vice = Role::where('name','vice_president')->first();

  	if(!RoleMember::where('member_id',Auth::id())->where('role_id',$role_pres->id)->get()->isEmpty()){
		    $role_id = $role_pres->id;
  		}
  		elseif(!RoleMember::where('member_id',Auth::id())->where('role_id',$role_vice->id)->get()->isEmpty()){
		    $role_id = $role_vice->id;
  		}
  		else
  		{
  		return redirect()->back()->withErrors('invalid role');
  		}

  	 MticsBudgetDeny::create([
            'mtics_budget_id' => $id,
            'confirm_admin_id' => Auth::id(),
            'confirm_admin_role_id' => $role_id,
            'denied_reason' => $request->denied_reason,
        ]);

	MticsBudget::where('id', $id)->update(array('confirm_admin_id' => Auth::id()));
    MticsBudget::where('id', $id)->update(array('budget_status' => 'denied'));

    $data = array(
    'message' => "Budget Request has been denied",
    'redirect' => array('role' => 'finance'),
    'origin' => 'mticsbudgetPresident',
    );

    $role_id = Role::where('name', 'finance')->first()->id;
    $member = Member::whereHas('role', function($q) use ($role_id) {
        $q->where('role_id', $role_id);
    })->first();
    $member->notify(new SystemNotification($data));

    return redirect('admin/president/requested-mtics-budget');

  }

	// FINANCE FINANCE FINANCE
  public function finance_budget_view()
  {
    if(!Gate::allows('finance-only'))
    {
    return redirect('/');
    }

    $term = Officerterm::where('status','on-going')->first();
  	$mticsbudgets = MticsBudget::with('budget_breakdown')->where('updated_at','>=',date($term->created_at))->get();
    $budget_expcats = MticsBudgetExpCat::wherehas('breakdowns')->get();
    $expcats = MticsBudgetExpCat::all();
    return view('officers/manage_mtics_budget',compact('mticsbudgets','budget_expcats','expcats'));
  }

  public function finance_budget_edit(Request $request,$id)
  {
  	$mticsbudget = MticsBudget::find($id);
  	if($mticsbudget->budget_status != 'pending' and $mticsbudget->budget_status != 'denied')
  	{
      return redirect()->back()->withErrors('Request status is '.$mticsbudget->budget_status.', make changes are prohibited ');
    }


    $this->validate($request, [
    'budget_month' => 'required',
    ]);

    $dateObj = DateTime::createFromFormat('Y-m', $request->budget_month);
    $budgetmonth = $dateObj->format('F Y');
    $budgetname = $budgetmonth.' - 1st Request';

    if(!MticsBudget::where('budget_month',$request->budget_month)->get()->isEmpty())
    {
      $count = (count(MticsBudget::where('budget_month',$request->budget_month)->get()) + 1);

      if($count == 2)
      {
        $budgetname = $budgetmonth.' - 2nd Request';
      }
      elseif($count == 3)
      {
        $budgetname = $budgetmonth.' - 3rd Request';
      }
      else
      {
        $budgetname = $budgetmonth.' - '.$count.'th Request';
      }



    }
      $mticsbudget->budget_month = $request->budget_month;
      $mticsbudget->budget_remarks = $request->budget_remarks;
      $mticsbudget->save();
  //dd($mticsbudget->budget_name);

///// ito lng ung iaaray mong process,,,, pero dapat hnd nauulit ung breakdown category id
    foreach ($request->exp_cat_id as $key => $category) {
      if($request->breakdown_name[$key] !== null)
      {
        if(MticsBudgetExpCat::where('name',$request->breakdown_name[$key])->get()->isEmpty())
          {
            $expcat = New MticsBudgetExpCat;
            $expcat->name = $request->breakdown_name[$key];
            $expcat->save();
            $expcat_id = $expcat->id;
          }
          else
          {
            $expcat = MticsBudgetExpCat::where('name',$request->breakdown_name[$key])->first();
            $expcat_id = $expcat->id;
          }
      }
      else
      {
        $expcat_id = $category;
      }

      if($expcat_id == 'others')
      {
          $errors[] = "If 'Other' category selected, Other field is required. This Category will be remove. Please be mindful";
        continue;
      }

      // edit existing breakdown or add a new one
      $mticsbudget_breakdown = MticsBudgetBreakdown::where('mtics_budget_id', $mticsbudget->id)->where('mtics_budget_exp_cat_id', $expcat_id)->first();

      //echo($key.'-'.$request->breakdown_desc[$key].'<br>');
      if($mticsbudget_breakdown) {

        $mticsbudget_breakdown->budget_bd_desc = $request->breakdown_desc[$key];
        $mticsbudget_breakdown->budget_bd_amt = $request->budget_amt[$key];
        $mticsbudget_breakdown->save();
      }
      else {
        $mticsbudget_breakdown1 = new MticsBudgetBreakdown();
        $mticsbudget_breakdown1->mtics_budget_id = $mticsbudget->id;
        $mticsbudget_breakdown1->mtics_budget_exp_cat_id = $expcat_id;
        $mticsbudget_breakdown1->budget_bd_desc = $request->breakdown_desc[$key];
        $mticsbudget_breakdown1->budget_bd_amt = $request->budget_amt[$key];
        $mticsbudget_breakdown1->save();
        }
      }



    // remove the changed breakdown
    foreach ($mticsbudget->budget_breakdown as $breakdown) {
      if (!in_array($breakdown->mtics_budget_exp_cat_id, $request->exp_cat_id)) {
        $breakdown->delete();
      }
    }

////// end of array process

    //fetching all expenses breakdown.... tapos ung total nilang lahat ^_^
    $totalbudgetamt = MticsBudgetBreakdown::where('mtics_budget_id',$mticsbudget->id)->get()->sum('budget_bd_amt');
    MticsBudget::where('id', $mticsbudget->id)->update(array('budget_amt' => $totalbudgetamt));



     if (isset($errors)){
          return redirect()->back()->withErrors($errors);
      }
  //(minus month syntax) $lastmonth =  date('Y-m', strtotime('-1 months', strtotime($today)));
    Event::fire(new SystemEvent(auth::id(), 'Edited Monthly Budget for the month of ' . $budgetmonth . '.'));

    return redirect('admin/finance/manage-mtics-budget')->with('success', 'Monthly Budget Edited!');
  }

  private function CheckTotalBudget($mticsbudget_id)
  {
	$totalbudget = MticsBudgetBreakdown::where('mtics_budget_id',$mticsbudget_id)->get()->sum('budget_bd_amt');
    MticsBudget::where('id', $mticsbudget_id)->update(array('budget_amt' => $totalbudget));
  }


  public function finance_budget_submit($id)
  {

  	$mticsbudget = MticsBudget::find($id);
  	if($mticsbudget->budget_status !== 'pending' and $mticsbudget->budget_status !== 'denied')
  	{
      return redirect()->back()->withErrors('Request status is '.$mticsbudget->budget_status.', you cannot submit this request');

  		return redirect()->back()->withErrors('You cannot submit this Request');
  	}

    MticsBudget::where('id', $id)->update(array('budget_status' => 'for validation'));

    $data = array(
    'message' => "New Budget Request",
    'redirect' => array('role' => 'president'),
    'origin' => 'mticsbudget',
    );

    $role_id = Role::where('name', 'president')->first()->id;
    $member = Member::whereHas('role', function($q) use ($role_id) {
        $q->where('role_id', $role_id);
    })->first();
    $member->notify(new SystemNotification($data));

    Event::fire(new SystemEvent(auth::id(), 'Submitted Monthly Budget.'));


    return redirect('admin/finance/manage-mtics-budget')->with('success', 'Budget Submitted!');

  }





  public function finance_budget_create(Request $request)
  {
    //dd($request);
    //pag meron ng array na automatics na nagaadd sa breakdown(irerequired un)

    $this->validate($request, [
    'budget_month' => 'required',
    ]);

    $dateObj = DateTime::createFromFormat('Y-m', $request->budget_month);
    $budgetmonth = $dateObj->format('F Y');
    $budgetname = $budgetmonth.' - 1st Request';

    if(!MticsBudget::where('budget_month',$request->budget_month)->get()->isEmpty())
    {
      $count = (count(MticsBudget::where('budget_month',$request->budget_month)->get()) + 1);

      if($count == 2)
      {
        $budgetname = $budgetmonth.' - 2nd Request';
      }
      elseif($count == 3)
      {
        $budgetname = $budgetmonth.' - 3rd Request';
      }
      else
      {
        $budgetname = $budgetmonth.' - '.$count.'th Request';
      }



    }
      $mticsbudget = New MticsBudget;
      $mticsbudget->finance_id = Auth::id();
      $mticsbudget->budget_month = $request->budget_month;
      $mticsbudget->budget_name = $budgetname;
      $mticsbudget->budget_remarks = $request->budget_remarks;
      $mticsbudget->budget_amt = 0; //0 muna ung total amount hehe
      $mticsbudget->save();

  //dd($mticsbudget->budget_name);

///// ito lng ung iaaray mong process,,,, pero dapat hnd nauulit ung breakdown category id

    foreach ($request->exp_cat_id as $key => $category) {

      if($request->breakdown_name[$key] !== null)
      {

        if(MticsBudgetExpCat::where('name',$request->breakdown_name[$key])->get()->isEmpty())
          {
            $expcat = New MticsBudgetExpCat;
            $expcat->name = $request->breakdown_name[$key];
            $expcat->save();
            $expcat_id = $expcat->id;
          }
          else
          {
            $expcat = MticsBudgetExpCat::where('name',$request->breakdown_name[$key])->first();
            $expcat_id = $expcat->id;
          }
      }
      else
      {
        $expcat_id = $category;
      }

      if($expcat_id == 'others')
      {
        MticsBudgetBreakdown::where('mtics_budget_id',$mticsbudget->id)->delete();
        $mticsbudget->delete();
        return redirect()->back()->withErrors("If 'Other' category selected, Other field is required " );
      }

      $mticsbudgetbreakdown1 = New MticsBudgetBreakdown;
      $mticsbudgetbreakdown1->mtics_budget_id = $mticsbudget->id;
      $mticsbudgetbreakdown1->mtics_budget_exp_cat_id = $expcat_id;
      $mticsbudgetbreakdown1->budget_bd_desc = $request->breakdown_desc[$key];
      $mticsbudgetbreakdown1->budget_bd_amt = $request->budget_amt[$key];
      $mticsbudgetbreakdown1->save();

    }

////// end of array process

    //fetching all expenses breakdown.... tapos ung total nilang lahat ^_^
    $totalbudgetamt = MticsBudgetBreakdown::where('mtics_budget_id',$mticsbudget->id)->get()->sum('budget_bd_amt');
    MticsBudget::where('id', $mticsbudget->id)->update(array('budget_amt' => $totalbudgetamt));



  //(minus month syntax) $lastmonth =  date('Y-m', strtotime('-1 months', strtotime($today)));
    Event::fire(new SystemEvent(auth::id(), 'Added Monthly Budget for the month of ' . $budgetmonth . '.'));

    return redirect('admin/finance/manage-mtics-budget')->with('success', 'Monthly Budget Added!');

  }


  //EXPENSES //EXPENSES //EXPENSES //EXPENSES //EXPENSES //EXPENSES //EXPENSES

  public function finance_mtics_expenses_view()
  {
    if(!Gate::allows('finance-only'))
        {
        return redirect('/');
        }
    $roles = Role::where('name','=','internal')
            ->orwhere('name','=','external')
            ->orwhere('name','=','docu')
            ->orwhere('name','=','finance')
            ->orwhere('name','=','auditor')
            ->orwhere('name','=','info')
            ->orwhere('name','=','activity')
            ->orwhere('name','=','logistic')
            ->orwhere('name','=','president')
            ->orwhere('name','=','mtics_adviser')
            ->orwhere('name','=','vice_president')->get();
    $mticsbudgets = MticsBudget::with('budget_breakdown')->where('budget_status','on-going')->get();
    $mtics_bd_cats = MticsBudgetExpCat::all();
    $budget = new FormulaMoneyController;
    $mticsfund_amt = $budget->CheckMticsFundBudget();
    if(!$mticsbudgets->isEmpty()){
    $month = date('m',strtotime($mticsbudgets->first()->budget_month));
    }
    else{
      $month = '';
    }
    $mtics_expenses = MticsBudgetExpense::whereMonth('created_at',$month)->get();      

    if(!MticsBudget::where('budget_status','on-going')->get()->isEmpty())
      {
        $totalExpenses = MticsBudgetExpense::whereMonth('created_at', date('m'))->sum('expense_amt');
      }
      else
      {
        $totalExpenses = 0;
      }


    return view('officers/mtics_expenses',compact('roles','mticsbudgets','mticsfund_amt','mtics_expenses','mtics_bd_cats', 'totalExpenses'));
  }

  public function finance_mtics_expenses_add(Request $request)
  {
    //dd($request);

    if(MticsBudget::where('budget_status','on-going')->get()->isEmpty())
      {
        return redirect()->back()->withErrors('There is no on-going budget');
      }


     $this->validate($request, [
    'mticsbreakdown_id' => 'required',
    'role_id' => 'required',
    'expense_name' => 'required',
    'expense_amt' => 'required',
    ]);

    $role = Role::with('role_member')->find($request->role_id);

    if(!$role->role_member->isEmpty())
    {
      foreach ($role->role_member as $role_member) {
        $member_id = $role_member->member_id;
        $role_id = $role_member->role_id;
      }
    }
    else
    {
      return redirect()->back()->withErrors('This role has not been set');
    }

    $role_finance = Role::where('name','finance')->first();
    $role_asst_finance = Role::where('name','asst_finance')->first();

    if(!RoleMember::where('member_id',Auth::id())->where('role_id',$role_finance->id)->get()->isEmpty())
      {
        $confirm_admin_role_id = $role_finance->id;
      }
    elseif(!RoleMember::where('member_id',Auth::id())->where('role_id',$role_asst_finance->id)->get()->isEmpty())
      {
        $confirm_admin_role_id = $role_finance->id;
      }
    else
      {
        return redirect()->back()->withErrors('You are not Finance nor Assitance of Finance, You cannot transact this process');
      }

        $confirm_admin_id = Auth::id();
        $budget = new FormulaMoneyController;
        $total_mtics_budget_amt = $budget->CheckMticsFundBudget();
        $final_amount = $total_mtics_budget_amt - $request->expense_amt;
        if($final_amount < 0)
        {
          return redirect()->back()->withErrors('Budget is not enough');
        }

        $expenses = New MticsBudgetExpense;
        $expenses->purchaser_admin_id = $member_id;
        $expenses->purchaser_admin_role_id = $role_id;
        $expenses->confirm_admin_id = $confirm_admin_id;
        $expenses->confirm_admin_role = $confirm_admin_role_id;
        $expenses->mtics_budget_breakdown_id = $request->mticsbreakdown_id;
        $expenses->expense_name = $request->expense_name;
        $expenses->expense_desc = $request->expense_desc;
        $expenses->expense_amt = $request->expense_amt;
        $expenses->save();

        return redirect()->back();

  }



  public function finance_mtics_expenses_uploadreceipt(Request $request,$id)
  {


    $this->validate($request, [
            'receipt' => 'image|mimes:jpeg,jpg,png|max:2000', //image only
          ]);

        $expense = MticsBudgetExpense::findOrFail($id);
        $file = $request->file('receipt');
        $date = date('Y-m-d');
        $time = time();
        //move to public/images/receipts/
        if($request->hasFile('receipt')){


          if(url('/') == 'http://mtics-ma.tuptaguig.com'){
          $file->move('public/images/receipts/', $id.'_'.$time.'_'.$date.'_expenses.jpg');}
          else{
          $file->move('images/receipts/', $id.'_'.$time.'_'.$date.'_expenses.jpg');}


          $imgname =  "receipts/".$id.'_'.$time.'_'.$date.'_expenses.jpg';


          if($expense->receipt_id == null )
          {
            $receipt = New Receipt;
          }
          else
            {
              $receipt = Receipt::findOrFail($expense->receipt_id);
                if(url('/') == 'http://mtics-ma.tuptaguig.com'){
                File::delete('public/images/'. $receipt->receipt_image);}
                else{
                File::delete('images/'. $receipt->receipt_image);}
            }

          $receipt->receipt_image = $imgname;
          $receipt->admin_id = Auth::id();
          $receipt->save();
        MticsBudgetExpense::where('id', $id)->update(array('receipt_id' => $receipt->id));

        }
         return redirect()->back();


  }


//EVENT EXPENSES //EVENT EXPENSES //EVENT EXPENSES //EVENT EXPENSES //EVENT EXPENSES

 public function finance_event_expenses_view($id)
  {

    $roles = Role::where('name','=','internal')
            ->orwhere('name','=','external')
            ->orwhere('name','=','docu')
            ->orwhere('name','=','finance')
            ->orwhere('name','=','auditor')
            ->orwhere('name','=','info')
            ->orwhere('name','=','activity')
            ->orwhere('name','=','logistic')
            ->orwhere('name','=','president')
            ->orwhere('name','=','mtics_adviser')
            ->orwhere('name','=','vice_president')->get();
    $event_expenses = EventExpense::where('event_id',$id)->get();
    $eventbudget = New FormulaMoneyController;
    $event_budget = $eventbudget->CheckEventFundBudget($id);
    $breakdowns = EventAmountBreakdown::where('event_id',$id)->get();

    $event = MticsEvent::find($id);
    return view('officers/event_expenses',compact('roles','event_expenses','event','event_budget','breakdowns'));
  }


  public function finance_event_expenses_uploadreceipt(Request $request,$id)
  {



    $this->validate($request, [
            'receipt' => 'image|mimes:jpeg,jpg,png|max:2000', //image only
          ]);

        $expense = EventExpense::findOrFail($id);
        $file = $request->file('receipt');
        $date = date('Y-m-d');
        $time = time();



        //move to public/images/receipts/
        if($request->hasFile('receipt')){


          if(url('/') == 'http://mtics-ma.tuptaguig.com'){
          $file->move('public/images/receipts/'.$expense->event->event_title.'/', $id.'_'.$time.'_'.$date.'_expenses.jpg');}
          else{
          $file->move('images/receipts/'.$expense->event->event_title.'/', $id.'_'.$time.'_'.$date.'_expenses.jpg');}


          $imgname =  'receipts/'.$expense->event->event_title.'/'.$id.'_'.$time.'_'.$date.'_expenses.jpg';


          if($expense->receipt_id == null )
          {
            $receipt = New Receipt;
          }
          else
            {
              $receipt = Receipt::findOrFail($expense->receipt_id);
                if(url('/') == 'http://mtics-ma.tuptaguig.com'){
                File::delete('public/images/'. $receipt->receipt_image);}
                else{
                File::delete('images/'. $receipt->receipt_image);}
            }

          $receipt->receipt_image = $imgname;
          $receipt->admin_id = Auth::id();
          $receipt->save();
        EventExpense::where('id', $id)->update(array('receipt_id' => $receipt->id));

        }
         return redirect()->back();


  }

  public function finance_event_expenses_add(Request $request,$id)
  {

    if(!AuditorFinancialApprove::where('event_id',$id)->get()->isEmpty())
      {
        return redirect()->back()->withErrors('Event Liquation has been approved by Executive Officer for Audit, Make changes are prohibited');
      }


     $this->validate($request, [
    'role_id' => 'required',
    'expense_name' => 'required',
    'expense_amt' => 'required',
    'breakdown_id' => 'required',
    ]);


    $role = Role::with('role_member')->find($request->role_id);

    if(!$role->role_member->isEmpty())
    {
      foreach ($role->role_member as $role_member) {
        $member_id = $role_member->member_id;
        $role_id = $role_member->role_id;
      }
    }
    else
    {
      return redirect()->back()->withErrors('This role has not been set');
    }


    $role_finance = Role::where('name','finance')->first();
    $role_asst_finance = Role::where('name','asst_finance')->first();

    if(!RoleMember::where('member_id',Auth::id())->where('role_id',$role_finance->id)->get()->isEmpty())
      {
        $confirm_admin_role_id = $role_finance->id;
      }
    elseif(!RoleMember::where('member_id',Auth::id())->where('role_id',$role_asst_finance->id)->get()->isEmpty())
      {
        $confirm_admin_role_id = $role_finance->id;
      }
    else
      {
        return redirect()->back()->withErrors('You are not Finance nor Assitance of Finance, You cannot transact this process');
      }

        $confirm_admin_id = Auth::id();
        $eventbudget = New FormulaMoneyController;
        $total_event_budget_amt = $eventbudget->CheckEventFundBudget($id);
        $final_amount = $total_event_budget_amt - $request->expense_amt;
        if($final_amount < 0)
        {
          return redirect()->back()->withErrors('Budget is not enough');
        }

        $expenses = New EventExpense;
        $expenses->purchaser_admin_id = $member_id;
        $expenses->purchaser_admin_role_id = $role_id;
        $expenses->confirm_admin_id = $confirm_admin_id;
        $expenses->confirm_admin_role = $confirm_admin_role_id;
        $expenses->event_id = $id;
        $expenses->event_amount_breakdown_id = $request->breakdown_id;
        $expenses->expense_name = $request->expense_name;
        $expenses->expense_desc = $request->expense_desc;
        $expenses->expense_amt = $request->expense_amt;
        $expenses->save();

    return redirect()->back();


  }




}
