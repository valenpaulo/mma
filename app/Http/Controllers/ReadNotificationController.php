<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;
use Illuminate\Notifications\DatabaseNotification;

class ReadNotificationController extends Controller
{
    public function readAllNotification() {

        foreach (Auth::user()->unreadNotifications as $notification) {
            $notification->markAsRead();
        }

        return redirect()->back();
    }

    public function readTaskNotification($id) {
        $notif = DatabaseNotification::findOrFail($id);
        $notif->markAsRead();

        if ($notif->data['redirect']['role'] == 'vice_president') {
            return redirect('admin/vice-president/manage-event');
        }

        return redirect('admin/'. $notif->data['redirect']['role'] .'/manage-event/' . $notif->data['redirect']['event_id']);
    }

    public function readPrivilegeNotification($id) {
        $notif = DatabaseNotification::findOrFail($id);
        $notif->markAsRead();

        if ($notif->data['redirect'] == 'faculty' or $notif->data['redirect'] == 'super_admin') {
            return redirect('admin/additional-feature');
        }

        return redirect('admin/');
    }

    public function readEventNotification($id) {
        $notif = DatabaseNotification::findOrFail($id);
        $notif->markAsRead();

        return redirect('admin/manage-event');
    }

    public function readMeetingNotification($id) {
        $notif = DatabaseNotification::findOrFail($id);
        $notif->markAsRead();

        return redirect('admin/manage-event');
    }

    public function readFormNotification($id) {
        $notif = DatabaseNotification::findOrFail($id);
        $notif->markAsRead();

        return redirect('/');
    }

    public function readInternalNotification($id) {
        $notif = DatabaseNotification::findOrFail($id);
        $notif->markAsRead();

        return redirect('admin/'. $notif->data['redirect']['role'] .'/manage-event/' . $notif->data['redirect']['event_id']);
    }

    public function readDocuNotification($id) {
        $notif = DatabaseNotification::findOrFail($id);
        $notif->markAsRead();

        if ($notif->data['redirect']['role'] == 'vice_president') {
            return redirect('admin/vice-president/manage-event');
        }

        return redirect('admin/'. $notif->data['redirect']['role'] .'/manage-event/' . $notif->data['redirect']['event_id']);
    }

    public function readExternalNotification($id) {
        $notif = DatabaseNotification::findOrFail($id);
        $notif->markAsRead();

        if ($notif->data['redirect']['role'] == 'vice_president') {
            return redirect('admin/vice-president/manage-event');
        }

        if ($notif->data['redirect']['event_id']) {
            return redirect('admin/'. $notif->data['redirect']['role'] .'/manage-event/' . $notif->data['redirect']['event_id']);
        }

        else {
            return redirect('admin/finance/requested-money');
        }

    }

    public function readFinanceNotification($id) {
        $notif = DatabaseNotification::findOrFail($id);
        $notif->markAsRead();

        if ($notif->data['redirect']['role'] == 'vice_president') {
            return redirect('admin/vice-president/manage-event');
        }

        return redirect('admin/'. $notif->data['redirect']['role'] .'/manage-event/' . $notif->data['redirect']['event_id']);
    }

    public function readLogisticsNotification($id) {
        $notif = DatabaseNotification::findOrFail($id);
        $notif->markAsRead();

        if ($notif->data['redirect']['role'] == 'vice_president') {
            return redirect('admin/vice-president/manage-event');
        }

        return redirect('admin/'. $notif->data['redirect']['role'] .'/manage-event/' . $notif->data['redirect']['event_id']);
    }

    public function readReimbursementNotification($id) {
        $notif = DatabaseNotification::findOrFail($id);
        $notif->markAsRead();

        if ($notif->data['redirect']['role'] == 'vice_president') {
            return redirect('admin/vice-president/manage-event');
        }


      /*  if ($notif->data['redirect']['role'] == 'finance') {
            return redirect('admin/'. $notif->data['redirect']['role'] .'/manage-event' . $notif->data['redirect']['event_id'] . '/validate/' . $notif->data['redirect']['request_id'] . '/next');

*/


        if ($notif->data['redirect']['event_id']) {

            if ($notif->data['redirect']['role'] == 'finance') {
                return redirect('admin/'. $notif->data['redirect']['role'] .'/manage-event/' . $notif->data['redirect']['event_id'] . '/validate/' . $notif->data['redirect']['request_id'] . '/next');
            }

            elseif ($notif->data['redirect']['role'] == 'auditor') {
                return redirect('admin/auditor/manage-event/' . $notif->data['redirect']['event_id'] . '/validate/' . $notif->data['redirect']['request_id']);
            }

        }

        else {

            if ($notif->data['redirect']['role'] == 'finance') {
                return redirect('admin/'. $notif->data['redirect']['role'] . '/requested-money/validate/' . $notif->data['redirect']['request_id'] . '/next');
            }

            elseif ($notif->data['redirect']['role'] == 'auditor') {
                return redirect('admin/auditor');
            }
        }
    }

    public function readReportPostNotification($id) {
        $notif = DatabaseNotification::findOrFail($id);
        $notif->markAsRead();

        return redirect('admin/manage-postsfeed');
    }

    public function readBudgetRequestNotification($id) {
        $notif = DatabaseNotification::findOrFail($id);
        $notif->markAsRead();

        return redirect('admin/president/requested-mtics-budget');
    }

    public function readApprovedBudgetRequestNotification($id) {
        $notif = DatabaseNotification::findOrFail($id);
        $notif->markAsRead();

        return redirect('admin/finance/manage-mtics-budget');
    }

    public function readBankRequestNotification($id) {
        $notif = DatabaseNotification::findOrFail($id);
        $notif->markAsRead();

        return redirect($notif->data['redirect']);
    }

    public function readPurchaseRequestNotification($id) {
        $notif = DatabaseNotification::findOrFail($id);
        $notif->markAsRead();

        return redirect($notif->data['redirect']);
    }

    public function readMoneyRequestNotification($id) {
        $notif = DatabaseNotification::findOrFail($id);
        $notif->markAsRead();

        return redirect($notif->data['redirect']);
    }

    public function readApprovedMoneyRequestNotification($id) {
        $notif = DatabaseNotification::findOrFail($id);
        $notif->markAsRead();

        return redirect($notif->data['redirect']);
    }

    public function readPurchasedItemNotification($id) {
        $notif = DatabaseNotification::findOrFail($id);
        $notif->markAsRead();

        return redirect($notif->data['redirect']);
    }

    public function readPurchasedItemValidationNotification($id) {
        $notif = DatabaseNotification::findOrFail($id);
        $notif->markAsRead();

        return redirect($notif->data['redirect']);
    }

    public function readDepositDisapproveNotification($id) {
        $notif = DatabaseNotification::findOrFail($id);
        $notif->markAsRead();

        return redirect('admin/finance/manage-bank-transaction/mtics');
    }

    public function readReportDepositNotification($id) {
        $notif = DatabaseNotification::findOrFail($id);
        $notif->markAsRead();

        if (Gate::allows('president-only')){
            return redirect('admin/president/bank-transaction-request-reported');
        }

        elseif (Gate::allows('finance-only')) {
            return redirect('admin/finance/manage-bank-transaction/mtics');
        }
    }

    public function readReportWithdrawNotification($id) {
        $notif = DatabaseNotification::findOrFail($id);
        $notif->markAsRead();

        if (Gate::allows('president-only')){
            return redirect('admin/president/bank-transaction-request-reported');
        }

        elseif (Gate::allows('finance-only')) {
            return redirect('admin/finance/manage-bank-transaction/mtics');
        }
    }

    public function readPresidentNotification($id) {
        $notif = DatabaseNotification::findOrFail($id);
        $notif->markAsRead();

        return redirect('admin/adviser/mtics-bank-transaction-request');
    }

    public function readPurchasedItemReportNotification($id) {
        $notif = DatabaseNotification::findOrFail($id);
        $notif->markAsRead();

        if($notif->data['redirect']['reported'] == 'finance') {
            if ($notif->data['redirect']['event']) {
                return redirect('admin/finance/manage-event/' . $notif->data['redirect']['event']);
            } else {
                return redirect('admin/finance/requested-money');
            }
        }
        elseif($notif->data['redirect']['reported'] == 'logistics') {
            if ($notif->data['redirect']['event']) {
                return redirect('admin/logistics/manage-event/' . $notif->data['redirect']['event']);
            } else {
                return redirect('admin/logistics/purchased-items');
            }
        }
    }


    public function readDocumentNotification($id) {
        $notif = DatabaseNotification::findOrFail($id);
        $notif->markAsRead();

        return redirect('admin/president/manage-event/' . $notif['data']['redirect']['event_id']);
    }


    public function readBreakdownRequestNotification($id) {
        $notif = DatabaseNotification::findOrFail($id);
        $notif->markAsRead();

        return redirect('admin/finance/manage-event/' . $notif->data['redirect']['event_id']);
    }

    public function readLogisticsValidationNotification($id) {
        $notif = DatabaseNotification::findOrFail($id);
        $notif->markAsRead();

        return redirect($notif['data']['redirect']);
    }

    public function readBankTransactionApprovedNotification($id) {
        $notif = DatabaseNotification::findOrFail($id);
        $notif->markAsRead();

        return redirect($notif['data']['redirect']);
    }

    public function readPenaltyNotification($id) {
        $notif = DatabaseNotification::findOrFail($id);
        $notif->markAsRead();

        return redirect($notif['data']['redirect']);
    }
}

