<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\File;
use App\Notifications\SystemNotification;
use App\Notifications\SystemBroadcastNotification;
use App\MticsDeposit;
use App\Role;
use App\Member;
use App\MticsDepositReceipt;
use App\MticsDepositDeny;
use App\MticsDepositReport;
use App\MticsWithdrawal;
use App\MticsWithdrawalReceipt;
use App\MticsWithdrawalDeny;
use App\MticsWithdrawalReport;
use App\MoneyRequest;
use App\MticsfundPayment;
use App\ReimburseRequest;
use App\MticsBudget;
use App\MticsBudgetBreakdown;
use App\MticsBudgetDeny;
use App\MticsBudgetExpense;
use App\EventDeposit;
use App\EventDepositReceipt;
use App\EventDepositDeny;
use App\EventDepositReport;
use App\EventPayment;
use App\MticsEvent;
use App\EventWithdrawal;
use App\EventWithdrawalReceipt;
use App\EventWithdrawalDeny;
use App\EventWithdrawalReport;
use App\Task;

use App\Events\SystemEvent;
use Illuminate\Support\Facades\Event;

use App\Officerterm;

class BankTransactionController extends Controller
{
 	public function mtics_view()
    {
        if(!Gate::allows('finance-only'))
        {
        return redirect('/');
        }

        $term = Officerterm::where('status','on-going')->first();
        $events = MticsEvent::where('updated_at','>=',date($term->created_at))->get();

    	$mticsfund_bucket = $this->CheckMticsFundBucket();
        $eventfund_bucket =  $this->CheckEventFundBucket();
        $totalBucket = $mticsfund_bucket + $eventfund_bucket;

        $mticsbudgets = MticsBudget::where('budget_status','approved')->get();
        $moneyrequests = MoneyRequest::with('task','task.event')->where('mon_req_status','pending')->orwhere('mon_req_status','ongoing')->get();
        //dd($moneyrequests);

        // deposit request
        // mtics
        $mticsDepositRequest = MticsDeposit::where(function($q) {
            $q->where('deposit_status', 'pending')->orWhere('deposit_status', 'approved');
        })->where('deposit_source', '!=', 'Event')->get();
        // event
        $eventDepositRequest = EventDeposit::where(function($q) {
            $q->where('deposit_status', 'pending')->orWhere('deposit_status', 'approved');
        })->where('deposit_source', '!=', 'MTICS Fund')->get();

        // deposit transaction
        // mtics
        $mticsDepositTransaction = MticsDeposit::where('deposit_status', 'cleared')->where('deposit_source', '!=', 'Event')->where('updated_at','>=',date($term->created_at))->get();
        // event
        $eventDepositTransaction = EventDeposit::where('deposit_status', 'cleared')->where('updated_at','>=',date($term->created_at))->where('deposit_source', '!=', 'MTICS Fund')->get();

        // withdraw transaction

        // withdraw
        // mtics

        $mticsWithdrawRequest = MticsWithdrawal::where(function($q) {
            $q->where('withdrawal_status', 'pending')->orWhere('withdrawal_status', 'approved');
        })->get();

        $eventWithdrawRequest = EventWithdrawal::where(function($q) {
            $q->where('withdrawal_status', 'pending')->orWhere('withdrawal_status', 'approved');
        })->get();

        // withdraw transaction
        // mtics
        $mticsWithdrawTransaction = MticsWithdrawal::where('withdrawal_status', 'cleared')->where('updated_at','>=',date($term->created_at))->get();

        // event
        $eventWithdrawTransaction = EventWithdrawal::where('withdrawal_status', 'cleared')->where('updated_at','>=',date($term->created_at))->get();

        // transfer transaction
        // mtics
        $mticsTransferTransaction = MticsDeposit::where(function($q) {
            $q->where('deposit_status', 'cleared')->orWhere('deposit_status', 'pending');
        })->where('deposit_source', 'Event')->where('updated_at','>=',date($term->created_at))->get();
        // event
        $eventTransferTransaction = EventDeposit::where(function($q) {
            $q->where('deposit_status', 'cleared')->orWhere('deposit_status', 'pending');
        })->where('updated_at','>=',date($term->created_at))->where('deposit_source', 'MTICS Fund')->get();

        //events
        $events = MticsEvent::with('breakdown')->where('status','on-going')->where('amount_confirm','approved')->get();
        $eventDone = MticsEvent::with('breakdown')->where('amount_confirm','approved')->get();

    	return view('officers/banktrans_mtics',compact('totalBucket','mticsbudgets','moneyrequests', 'mticsFund_inHand', 'mticsDepositRequest', 'eventDepositTransaction', 'mticsWithdrawRequest', 'eventDepositRequest', 'mticsDepositTransaction', 'events', 'withdrawThisMonth', 'mticsWithdrawTransaction', 'eventWithdrawTransaction', 'eventWithdrawRequest', 'eventDone', 'mticsfund_bucket', 'eventfund_bucket', 'mticsTransferTransaction', 'eventTransferTransaction'));

    }

//MTICS REQUEST TO DEPOSIT //MTICS REQUEST TO DEPOSIT //MTICS REQUEST TO DEPOSIT
	public function mtics_deposit(Request $request)
    {
    	$this->validate($request, [
        'deposit_type' => 'required',
        'deposit_amt' => 'required',
        ]);



        if($request->deposit_type == 'others')
        {
            $this->validate($request, [
            'deposit_desc' => 'required',
            ]);
            $deposit_source = 'other';
            $deposit_amt = $request->deposit_amt;
            $event_id = null;
        }

        if($request->deposit_type == 'mtics_payment')
        {
            //COMPUTATION OF TOTAL MTICS PAYMENT - DEPOSITED MTICS PAYMENT
            $mticspayment = MticsfundPayment::all()->sum('paymticsfund_amt');
            $mticspayment_deposit = MticsDeposit::where('deposit_source','MTICS Payment')->where('deposit_status','cleared')->get()->sum('deposit_amt');
            $totalmticspayment = $mticspayment - $mticspayment_deposit;

            //If the inputted mtics payment is not same with the mtics payment bucket, Must need a reason or remarks
            if($request->deposit_amt > $totalmticspayment)
            {
               return redirect()->back()->withErrors('not enough money, MTICS Payment bucket is P'. $totalmticspayment .' only');
            }
            elseif ($request->deposit_amt < $totalmticspayment) {
                $this->validate($request, [
                'remarks' => 'required',
                ]);
            }

            $deposit_source = 'MTICS Payment';
            $event_id = null;
            $deposit_amt = $totalmticspayment;

        }

        if($request->deposit_type == 'event')
        {
            $this->validate($request, [
            'event_id' => 'required',
            ]);
            $eventpayment = new FormulaMoneyController();
            //$eventfund = $this->CheckEventFundBucketPerEvent($request->event_id);
            $eventfund = $eventpayment->CheckEventFundPayment($request->event_id);

            //dd($eventfund);
            if($request->deposit_amt < $eventfund)
            {
                $this->validate($request, [
                'deposit_desc' => 'required',
                ]);
            }
            elseif($request->deposit_amt > $eventfund)
            {
               return redirect()->back()->withErrors('not enough money, Event Collection is P'. $eventfund .' only');

            }

            $deposit_source = 'Event Collection';
            $event_id = $request->event_id;
            $deposit_amt = $request->deposit_amt;

        }



            $mticsdeposit = New MticsDeposit;
            $mticsdeposit->admin_id = Auth::id();
            $mticsdeposit->deposit_desc = $request->deposit_desc;
            $mticsdeposit->event_id = $event_id;
            $mticsdeposit->deposit_amt = $deposit_amt;
            $mticsdeposit->deposit_source = $deposit_source;
            $mticsdeposit->remarks = $request->remarks;
            $mticsdeposit->save();

        $data = array(
        'message' => "New Deposit Request.",
        'redirect' => 'admin/adviser/mtics-bank-transaction-request',
        'origin' => 'depositRequest',
        );

        $role_id = Role::where('name', 'mtics_adviser')->first()->id;
        $member = Member::whereHas('role', function($q) use ($role_id) {
            $q->where('role_id', $role_id);
        })->first();
        $member->notify(new SystemNotification($data));


        Event::fire(new SystemEvent(auth::id(), 'Sent a Deposit Request.'));

        return redirect('admin/finance/manage-bank-transaction/mtics')->with('success', 'Request Sent!');
    }


    public function CheckMticsFundBucket()
    {
        //formula for total Mtics fund in bank (this is the virtual bank amount)
    $mticsfundwithdraw = MticsWithdrawal::where('withdrawal_status','cleared')->get()->sum('withdrawal_amt');

    $mticsfunddeposit = MticsDeposit::where('deposit_status','cleared')->get()->sum('deposit_amt');

    $mticsdeposittoevent = EventDeposit::where('deposit_source','MTICS Fund')->get()->sum('deposit_amt');


    $mticsfundbucket = $mticsfunddeposit - $mticsfundwithdraw - $mticsdeposittoevent;
    return $mticsfundbucket;
    }




    public function mtics_banktrans_view()
    {
         if(!Gate::allows('mticsadviser-only'))
        {
        return redirect('/');
        }

        $mticsfund_bucket = $this->CheckMticsFundBucket();
        $eventfund_bucket =  $this->CheckEventFundBucket();
        $totalBucket = $mticsfund_bucket + $eventfund_bucket;

       // dd($mticsfund_bucket);
        $mticsbudgets = MticsBudget::where('budget_status','approved')->get();
        $moneyrequests = MoneyRequest::with('task')->wherehas('task',function($q){$q->where('event_id',null);})->where('mon_req_status','pending')->get();
        // dd($moneyrequests);

        $formula = new FormulaMoneyController();
        $mticsFund_inHand = $formula->CheckMticsFundPayment();

        // deposit
        $mticsDepositRequest = MticsDeposit::where('deposit_status', 'pending')->get();
        $eventDepositRequest = EventDeposit::where('deposit_status', 'pending')->get();

        // event mtics approved request
        $mticsDepositApprove = MticsDeposit::where('deposit_status', 'approved')->orwhere('deposit_status', 'reported')->get();
        $eventDepositApprove = EventDeposit::where('deposit_status', 'approved')->orwhere('deposit_status', 'reported')->get();

        // event and mtics deposit transaction
        $mticsDepositTransaction = MticsDeposit::where('deposit_status', 'cleared')->get();
        $eventDepositTransaction = EventDeposit::where('deposit_status', 'cleared')->get();

        // mtics
        $mticsWithdrawRequest = MticsWithdrawal::where('withdrawal_status', 'pending')->get();
        $mticsWithdrawApprove = MticsWithdrawal::where('withdrawal_status', 'approved')->orwhere('withdrawal_status', 'reported')->get();

        // event
        $eventWithdrawRequest = EventWithdrawal::where('withdrawal_status', 'pending')->get();
        $eventWithdrawApprove = EventWithdrawal::where('withdrawal_status', 'approved')->orwhere('withdrawal_status', 'reported')->get();

        // event and mtics withdraw transaction
        $mticsWithdrawTransaction = MticsWithdrawal::where('withdrawal_status', 'cleared')->get();

        $eventWithdrawTransaction = EventWithdrawal::where('withdrawal_status', 'cleared')->get();


        return view('officers/adviser_mticsdeposit_view',compact('mticsfund_bucket','eventfund_bucket','totalBucket','mticsbudgets','moneyrequests', 'mticsDepositRequest', 'mticsDepositTransaction', 'mticsDepositApprove', 'mticsWithdrawRequest', 'mticsWithdrawApprove', 'mticsWithdrawTransaction', 'eventDepositRequest', 'eventDepositTransaction', 'eventWithdrawTransaction', 'eventDepositApprove', 'eventWithdrawRequest', 'eventWithdrawApprove'));
    }

    public function mtics_deposit_approved(Request $request,$id)
    {

        $mticsdeposit = MticsDeposit::find($id);
        if($mticsdeposit->deposit_status == 'cleared')
        {
           return redirect()->back()->withErrors('The request has been cleared');
        }

        if($mticsdeposit->deposit_source == 'Event'){
            MticsDeposit::where('id', $id)->update(array('deposit_status' => 'cleared'));
        }
        else{
            MticsDeposit::where('id', $id)->update(array('deposit_status' => 'approved'));
        }

        MticsDeposit::where('id', $id)->update(array('faculty_id' => Auth::id()));

        $data = array(
        'message' => "Your deposit request has been approved.",
        'redirect' => 'admin/finance/manage-bank-transaction/mtics',
        'origin' => 'bank-transaction-approved',
        );

        $role_id = Role::where('name', 'finance')->first()->id;
        $member = Member::whereHas('role', function($q) use ($role_id) {
            $q->where('role_id', $role_id);
        })->first();
        $member->notify(new SystemNotification($data));

        Event::fire(new SystemEvent(auth::id(), 'Approved Deposit Request.'));

        return redirect('admin/adviser/mtics-bank-transaction-request')->with('success', 'Approved Request!');
    }

    public function mtics_deposit_denied(Request $request,$id)
    {
        $mticsdeposit = MticsDeposit::find($id);
        if($mticsdeposit->deposit_status == 'cleared')
        {
           return redirect()->back()->withErrors('The request has been cleared');
        }

        $this->validate($request, [
        'denied_reason' => 'required',
        ]);

        MticsDeposit::where('id', $id)->update(array('deposit_status' => 'denied'));
        MticsDeposit::where('id', $id)->update(array('faculty_id' => Auth::id()));
        MticsDepositDeny::create([
            'mtics_deposit_id' => $id,
            'faculty_id' => Auth::id(),
            'denied_reason' => $request->denied_reason,
        ]);

        $data = array(
        'message' => "Your deposit request has been denied.",
        'redirect' => array('role' => 'finance'),
        'origin' => 'depositDisapprove',
        );

        $role_id = Role::where('name', 'finance')->first()->id;
        $member = Member::whereHas('role', function($q) use ($role_id) {
            $q->where('role_id', $role_id);
        })->first();
        $member->notify(new SystemNotification($data));


        Event::fire(new SystemEvent(auth::id(), 'Denied a Deposit Request.'));

        return redirect('admin/adviser/mtics-bank-transaction-request')->with('success', 'Request Denied!');
    }

    public function mtics_deposit_receipt(Request $request,$id)
    {
        $mticsdeposit = MticsDeposit::find($id);
        if($mticsdeposit->deposit_status == 'cleared')
        {
           return redirect()->back()->withErrors('The request has been cleared');
        }

        if($mticsdeposit->deposit_status == 'reported')
        {
           return redirect()->back()->withErrors('The request has been reported');
        }

        $this->validate($request, [
            'receipt' => 'image|mimes:jpeg,jpg,png|max:2000', //image only
          ]);



        $file = $request->file('receipt');
        $date = date('Y-m-d');
        $time = time();
        //move to public/images/receipts/deposit/mtics
        if($request->hasFile('receipt')){

            if(!MticsDepositReceipt::where('mtics_deposit_id',$id)->get()->isEmpty())
            {

                $mticsdepositreceipt = MticsDepositReceipt::where('mtics_deposit_id',$id)->first();
                if(url('/') == 'http://mtics-ma.tuptaguig.com'){
                File::delete('public/images/' . $mticsdepositreceipt->receipt_image);
                }
                else{
                File::delete('images/' . $mticsdepositreceipt->receipt_image);
                }
                $mticsdepositreceipt->delete();

            }


            if(url('/') == 'http://mtics-ma.tuptaguig.com'){
                $file->move('public/images/receipts/deposit/mtics/', $id.'_'.$time.'_'.$date.'_depositreceipt.jpg');
                }
                else{
                $file->move('images/receipts/deposit/mtics/', $id.'_'.$time.'_'.$date.'_depositreceipt.jpg');
                }
          $imgname =  "receipts/deposit/mtics/".$id.'_'.$time.'_'.$date.'_depositreceipt.jpg';
          MticsDepositReceipt::create([
            'mtics_deposit_id' => $id,
            'faculty_id' => Auth::id(),
            'receipt_image' => $imgname,
            ]);


        }

        Event::fire(new SystemEvent(auth::id(), 'Uploaded receipt for Deposit Transaction.'));

        return redirect('admin/adviser/mtics-bank-transaction-request')->with('success', 'Receipt Uploaded!');
    }

    public function mtics_deposit_report(Request $request,$id)
    {
        $mticsdeposit = MticsDeposit::where('id',$id)->with('receipt')->first();


        if($mticsdeposit->deposit_status == 'cleared')
        {
           return redirect()->back()->withErrors('The request has been cleared');
        }

        if(!MticsDepositReport::where('mtics_deposit_id',$id)->get()->isEmpty()){
           return redirect()->back()->withErrors('The request has been reported');
        }

        if($mticsdeposit->receipt == null)
        {
           return redirect()->back()->withErrors('Please upload the receipt');
        }



        $this->validate($request, [
            'report_name' => 'required',
            'report_reason' => 'required',
          ]);


         MticsDepositReport::create([
            'mtics_deposit_id' => $id,
            'faculty_id' => Auth::id(),
            'deposit_title' => $request->report_name,
            'deposit_desc' => $request->report_reason,
            ]);
        MticsDeposit::where('id', $id)->update(array('deposit_status' => 'reported'));

        $message = "Deposit transaction has been reported.";

        $financeRole_id = Role::where('name', 'finance')->first()->id;
        $presidentRole_id = Role::where('name', 'president')->first()->id;

        $members = Member::whereHas('role', function($q) use ($financeRole_id, $presidentRole_id) {
            $q->where('role_id', $financeRole_id)->orWhere('role_id', $presidentRole_id);
        })->get();

        \Notification::send($members, new SystemBroadcastNotification($message, 'report-deposit'));

        Event::fire(new SystemEvent(auth::id(), 'Reported a Deposit Transaction.'));

        return redirect('admin/adviser/mtics-bank-transaction-request')->with('success', 'Reported!')->with('success', 'Reported!');

    }


    public function mtics_deposit_cleared(Request $request,$id)
    {
        $mticsdeposit = MticsDeposit::where('id',$id)->with('receipt')->first();
        if($mticsdeposit->deposit_status == 'reported')
        {
           return redirect()->back()->withErrors('The request has been reported');
        }

        if($mticsdeposit->receipt == null)
        {
           return redirect()->back()->withErrors('Please upload the receipt');
        }

        MticsDeposit::where('id', $id)->update(array('deposit_status' => 'cleared'));

        $data = array(
        'message' => "Your deposit request has been cleared.",
        'redirect' => array('role' => 'finance'),
        'origin' => 'depositCleared',
        );

        $data = array(
        'message' => "Your deposit request has been cleared.",
        'redirect' => 'admin/finance/manage-bank-transaction/mtics',
        'origin' => 'bank-transaction-approved',
        );

        $role_id = Role::where('name', 'finance')->first()->id;
        $member = Member::whereHas('role', function($q) use ($role_id) {
            $q->where('role_id', $role_id);
        })->first();
        $member->notify(new SystemNotification($data));

        Event::fire(new SystemEvent(auth::id(), 'Deposit Request Cleared.'));

        return redirect('admin/adviser/mtics-bank-transaction-request')->with('success', 'Cleared!');
    }



//MTICS Request to withdraw //MTICS Request to withdraw //MTICS Request to withdraw //MTICS  Request to withdraw

     public function mtics_withdraw(Request $request)
    {

        $mticsfund_bucket = $this->CheckMticsFundBucket();

        if(($mticsfund_bucket - $request->withdraw_amt) < 1)
        {
            return redirect()->back()->withErrors('You only have P'.$mticsfund_bucket.'. MTICS FUND will be 0 or negative, cannot request a withdraw');
        }

        $this->validate($request, [
        'withdraw_type' => 'required',
        ]);

        if($request->withdraw_type == 'withdraw_budget')
        {

            if($request->mticsbudget_id == '-- Select Budget Appropriate for this Month --')
            {
                return redirect()->back()->withErrors('Please Select Monthly Budget');
            }

            if(!MticsWithdrawal::where('mtics_budget_id',$request->mticsbudget_id)->get()->isEmpty())
            {
                return redirect()->back()->withErrors('This MTICS Budget already has withdraw request');
            }

            $mticsbudget = MticsBudget::find($request->mticsbudget_id);
            // $monthtoday = date('Y-m');
            $monthtoday = '2018-05';

            if(($mticsfund_bucket - $mticsbudget->budget_amt) < 0)
            {
                return redirect()->back()->withErrors('You only have P'.$mticsfund_bucket.'. MTICS FUND will be negative, cannot request a withdraw');
            }

            if($monthtoday !== $mticsbudget->budget_month)
            {
                return redirect()->back()->withErrors('Budget request for the month of '. $mticsbudget->budget_name.' have not started yet, Please select the correct monthly budget');
            }


            $reason = 'MTICS Budget for the month of '.$mticsbudget->budget_name;
            $amt = $mticsbudget->budget_amt;
            $mticsbudget_id = $request->mticsbudget_id;
            $external_task_id = null;



        }
        elseif($request->withdraw_type == 'withdraw_external')
        {
            if($request->external_task_id == '-- Select a Task --')
            {
                return redirect()->back()->withErrors('Please Select Monthly Budget');
            }

            $moneyrequest = MoneyRequest::where('task_id',$request->external_task_id)->first();

            $reason = 'External Task';
            $amt = $moneyrequest->amount;
            if($amt < 1) {
            return redirect()->back()->withErrors('Request can not be negative or 0, please input estimated price');
            }

            $mticsbudget_id = null;
            $external_task_id = $request->external_task_id;

        }
        else
        {
           $this->validate($request, [
            'withdraw_amt' => 'required',
            'withdraw_reason' => 'required',
            ]);
            $reason = $request->withdraw_reason;
            $amt = $request->withdraw_amt;
            $mticsbudget_id = null;
            $external_task_id = null;
        }


        $mticswithdrawal = New MticsWithdrawal;
        $mticswithdrawal->admin_id = Auth::id();
        $mticswithdrawal->withdrawal_reason = $reason;
        $mticswithdrawal->mtics_budget_id = $mticsbudget_id;
        $mticswithdrawal->withdrawal_desc = $request->withdraw_desc;
        $mticswithdrawal->withdrawal_amt = $amt;
        $mticswithdrawal->external_task_id = $external_task_id;
        $mticswithdrawal->save();

        $data = array(
        'message' => "New Withdraw Request.",
        'redirect' => 'admin/adviser/mtics-bank-transaction-request',
        'origin' => 'withdrawRequest',
        );

        $role_id = Role::where('name', 'mtics_adviser')->first()->id;
        $member = Member::whereHas('role', function($q) use ($role_id) {
            $q->where('role_id', $role_id);
        })->first();
        $member->notify(new SystemNotification($data));

        Event::fire(new SystemEvent(auth::id(), 'Sent a Withdraw Request.'));

        return redirect('admin/finance/manage-bank-transaction/mtics')->with('success', 'Request Sent!');
    }


    public function mtics_withdraw_approved(Request $request,$id)
    {

        $mticswithdrawal = MticsWithdrawal::find($id);
        if($mticswithdrawal->withdrawal_status == 'cleared')
        {
           return redirect()->back()->withErrors('The request has been cleared');
        }
        MticsWithdrawal::where('id', $id)->update(array('withdrawal_status' => 'approved'));
        MticsWithdrawal::where('id', $id)->update(array('faculty_id' => Auth::id()));

        $data = array(
        'message' => "Your withdraw request has been approved.",
        'redirect' => 'admin/finance/manage-bank-transaction/mtics',
        'origin' => 'bank-transaction-approved',
        );

        $role_id = Role::where('name', 'finance')->first()->id;
        $member = Member::whereHas('role', function($q) use ($role_id) {
            $q->where('role_id', $role_id);
        })->first();
        $member->notify(new SystemNotification($data));

        Event::fire(new SystemEvent(auth::id(), 'Approved Withdraw Request.'));

        return redirect('admin/adviser/mtics-bank-transaction-request')->with('success', 'Approved Request!');

    }


    public function mtics_withdraw_denied(Request $request,$id)
    {

        $mticswithdrawal = MticsWithdrawal::find($id);
        if($mticswithdrawal->withdrawal_status == 'cleared')
        {
           return redirect()->back()->withErrors('The request has been cleared');
        }

        if($mticswithdrawal->mtics_budget_id !== null)
        {
           return redirect()->back()->withErrors('Request is MTICS Monthly Budget, You cannot denied this request');
        }

        $this->validate($request, [
        'denied_reason' => 'required',
        ]);


        MticsWithdrawal::where('id', $id)->update(array('withdrawal_status' => 'denied'));
        MticsWithdrawal::where('id', $id)->update(array('faculty_id' => Auth::id()));
        MticsWithdrawalDeny::create([
            'mtics_withdrawal_id' => $id,
            'faculty_id' => Auth::id(),
            'denied_reason' => $request->denied_reason,
        ]);

        return redirect('admin/adviser/mtics-bank-transaction-request');
    }


    public function mtics_withdraw_receipt(Request $request,$id)
    {

        $mticswithdrawal = MticsWithdrawal::find($id);
        if($mticswithdrawal->withdrawal_status == 'cleared')
        {
           return redirect()->back()->withErrors('The request has been cleared');
        }

         if($mticswithdrawal->withdrawal_status == 'reported')
        {
           return redirect()->back()->withErrors('The request has been reported');
        }



        $this->validate($request, [
            'receipt' => 'image|mimes:jpeg,jpg,png|max:2000', //image only
          ]);

        $file = $request->file('receipt');
        $date = date('Y-m-d');
        $time = time();
        //move to public/images/receipts/withdraw/mtics
        if($request->hasFile('receipt')){

        if(!MticsWithdrawalReceipt::where('mtics_withdrawal_id',$id)->get()->isEmpty())
            {

                $mticswithdrawalreceipt = MticsWithdrawalReceipt::where('mtics_withdrawal_id',$id)->first();

                if(url('/') == 'http://mtics-ma.tuptaguig.com'){
                File::delete('public/images/' . $mticswithdrawalreceipt->receipt_image);
                }
                else{
                File::delete('images/' . $mticswithdrawalreceipt->receipt_image);
                }

                $mticswithdrawalreceipt->delete();

            }

        if(url('/') == 'http://mtics-ma.tuptaguig.com'){
        $file->move('public/images/receipts/withdrawal/mtics/', $id.'_'.$time.'_'.$date.'_withdrawalreceipt.jpg');
        }
        else{
        $file->move('images/receipts/withdrawal/mtics/', $id.'_'.$time.'_'.$date.'_withdrawalreceipt.jpg');
        }

          $imgname =  "receipts/withdrawal/mtics/".$id.'_'.$time.'_'.$date.'_withdrawalreceipt.jpg';
          MticsWithdrawalReceipt::create([
            'mtics_withdrawal_id' => $id,
            'faculty_id' => Auth::id(),
            'receipt_image' => $imgname,
            ]);

        }

        Event::fire(new SystemEvent(auth::id(), 'Uploaded Receipt for Withdraw Request.'));

        return redirect('admin/adviser/mtics-bank-transaction-request')->with('success', 'Receipt Uploaded!');
    }

    private function CheckMticsFundCashinHand()
  {
    //Formula = Total_payment - total_deposit(mticspayment_only) + total_withdraw
    //formula for total cash in cash including payment and withdraw
      $mticspayment = MticsfundPayment::all()->sum('paymticsfund_amt');
      $mticspayment_deposit = MticsDeposit::where('deposit_source','MTICS Payment')->where('deposit_status','cleared')->get()->sum('deposit_amt');
      $mticspayment_withdraw = MticsWithdrawal::where('withdrawal_status','cleared')->get()->sum('withdrawal_amt');

      $totalmticspayment = $mticspayment - $mticspayment_deposit + $mticspayment_withdraw;

    return $totalmticspayment;
  }


    public function mtics_withdraw_report(Request $request,$id)
    {

        $mticswithdrawal = MticsWithdrawal::where('id',$id)->with('receipt')->first();
        if($mticswithdrawal->withdrawal_status == 'cleared')
        {
           return redirect()->back()->withErrors('The request has been cleared');
        }

        if(!MticsWithdrawalReport::where('mtics_withdrawal_id',$id)->get()->isEmpty()){
           return redirect()->back()->withErrors('The request has been reported');
        }


        if($mticswithdrawal->receipt == null)
        {
           return redirect()->back()->withErrors('Please upload the receipt');
        }



        $this->validate($request, [
            'report_name' => 'required',
            'report_reason' => 'required',
          ]);

        $mticswithdrawal = MticsWithdrawal::find($id);
        if($mticswithdrawal->withdrawal_status == 'cleared')
        {
           return redirect()->back()->withErrors('The request has been cleared');
        }

         MticsWithdrawalReport::create([
            'mtics_withdrawal_id' => $id,
            'faculty_id' => Auth::id(),
            'withdrawal_title' => $request->report_name,
            'withdrawal_desc' => $request->report_reason,
            ]);

        MticsWithdrawal::where('id', $id)->update(array('withdrawal_status' => 'reported'));

        $message = "Withdraw transaction has been reported.";

        $financeRole_id = Role::where('name', 'finance')->first()->id;
        $presidentRole_id = Role::where('name', 'president')->first()->id;

        $members = Member::whereHas('role', function($q) use ($financeRole_id, $presidentRole_id) {
            $q->where('role_id', $financeRole_id)->orWhere('role_id', $presidentRole_id);
        })->get();

        \Notification::send($members, new SystemBroadcastNotification($message, 'report-withdraw'));

        return redirect('admin/adviser/mtics-bank-transaction-request')->with('success', 'Reported!');

    }

    public function mtics_withdraw_cleared(Request $request,$id)
    {

        $mticswithdrawal = MticsWithdrawal::where('id',$id)->with('receipt')->first();
        if($mticswithdrawal->withdrawal_status == 'reported')
        {
           return redirect()->back()->withErrors('The request has been reported');
        }

        if($mticswithdrawal->receipt == null)
        {
           return redirect()->back()->withErrors('Please upload the receipt');
        }



            if($mticswithdrawal->mtics_budget_id !== null)
            {
                if(!MticsBudget::where('budget_status','on-going')->get()->isEmpty())
                    {
                        $mticsbudget_ongoing = MticsBudget::where('budget_status','on-going')->first();

                        $budget = new FormulaMoneyController;
                        $mticsfund_amt = $budget->CheckMticsFundBudget();
                        MticsBudget::where('id', $mticswithdrawal->mtics_budget_id)->update(array('excess' => $mticsfund_amt));
                        MticsBudget::where('id', $mticsbudget_ongoing->id)->update(array('budget_status' => 'cleared'));
                    }

                MticsBudget::where('id', $mticswithdrawal->mtics_budget_id)->update(array('budget_status' => 'on-going'));

            }

         MticsWithdrawal::where('id', $id)->update(array('withdrawal_status' => 'cleared'));

         $data = array(
        'message' => "Your withdraw request has been cleared.",
        'redirect' => 'admin/finance/manage-bank-transaction/mtics',
        'origin' => 'bank-transaction-approved',
        );

        $role_id = Role::where('name', 'finance')->first()->id;
        $member = Member::whereHas('role', function($q) use ($role_id) {
            $q->where('role_id', $role_id);
        })->first();
        $member->notify(new SystemNotification($data));

        Event::fire(new SystemEvent(auth::id(), 'Withdraw Request Cleared.'));

        return redirect('admin/adviser/mtics-bank-transaction-request')->with('success', 'Cleared!');
    }


// EVENT BANK TRANSACTION // EVENT BANK TRANSACTION // EVENT BANK TRANSACTION // EVENT BANK TRANSACTION
    private function CheckEventFundBucket()
    {
    //formula for total EVENT fund in bank (this is the virtual bank amount)
    $eventfundwithdraw = EventWithdrawal::where('withdrawal_status','cleared')->get()->sum('withdrawal_amt');
    $eventfunddeposit = EventDeposit::where('deposit_status','cleared')->get()->sum('deposit_amt');
    $eventdeposittomtics = MticsDeposit::where('deposit_source','Event')->get()->sum('deposit_amt');
    $eventfundbucket = $eventfunddeposit - $eventfundwithdraw - $eventdeposittomtics;
        //dd($eventfundwithdraw);

    return $eventfundbucket;
    }

     public function CheckEventFundBucketPerEvent($event_id)
    {

    $eventfunddeposit = EventDeposit::where('event_id',$event_id)->where('deposit_status','cleared')->get()->sum('deposit_amt');
    $eventfundwithdraw = EventWithdrawal::where('event_id',$event_id)->where('withdrawal_status','cleared')->get()->sum('withdrawal_amt');
    $eventdeposittomtics = MticsDeposit::where('deposit_source','Event')->where('event_id',$event_id)->get()->sum('deposit_amt');
    $eventfundbucket = $eventfunddeposit - $eventfundwithdraw - $eventdeposittomtics;
    return $eventfundbucket;
    }



//TRANSFER //TRANSFER //TRANSFER //TRANSFER //TRANSFER //TRANSFER //TRANSFER //TRANSFER
    public function transfer(Request $request){


       $this->validate($request, [
        'deposit_type' => 'required',
        'event_id' => 'required',
        ]);

        if($request->event_id == '-- Select Event --')
        {
            return redirect()->back()->withErrors('Please Select Event');
        }

       if($request->deposit_type == 'transfer_mtics_event'){

            $mticsfund_bucket = $this->CheckMticsFundBucket();
            $this->validate($request, [
            'deposit_desc' => 'required',
            ]);

            if($request->deposit_amt > $mticsfund_bucket)
            {
               return redirect()->back()->withErrors('not enough money, MTICS Fund is P'. $mticsfund_bucket .' only');
            }

            $amt = $request->deposit_amt;
            $deposit_source = 'MTICS Fund';

            $eventdeposit = New EventDeposit;
            $eventdeposit->admin_id = Auth::id();
            $eventdeposit->event_id = $request->event_id;
            $eventdeposit->deposit_desc = $request->deposit_desc;
            $eventdeposit->deposit_amt = $amt;
            $eventdeposit->deposit_source = $deposit_source;
            $eventdeposit->remarks = $request->remarks;
            $eventdeposit->save();


       }

       if($request->deposit_type == 'transfer_event_mtics'){

            $eventpayment = new FormulaMoneyController();
            $eventfund = $this->CheckEventFundBucketPerEvent($request->event_id);

            //dd($eventfund);
            if($request->deposit_amt < $eventfund)
            {
                $this->validate($request, [
                'deposit_desc' => 'required',
                ]);
            }
            elseif($request->deposit_amt > $eventfund)
            {
               return redirect()->back()->withErrors('not enough money, Event fund is P'. $eventfund .' only');

            }

            $deposit_source = 'Event';
            $event_id = $request->event_id;
            $deposit_amt = $request->deposit_amt;

            $mticsdeposit = New MticsDeposit;
            $mticsdeposit->admin_id = Auth::id();
            $mticsdeposit->deposit_desc = $request->deposit_desc;
            $mticsdeposit->event_id = $event_id;
            $mticsdeposit->deposit_amt = $deposit_amt;
            $mticsdeposit->deposit_source = $deposit_source;
            $mticsdeposit->remarks = $request->remarks;
            $mticsdeposit->save();
       }

        $data = array(
        'message' => "New Deposit Request.",
        'redirect' => 'admin/adviser/mtics-bank-transaction-request',
        'origin' => 'depositRequest',
        );

        $role_id = Role::where('name', 'mtics_adviser')->first()->id;
        $member = Member::whereHas('role', function($q) use ($role_id) {
            $q->where('role_id', $role_id);
        })->first();
        $member->notify(new SystemNotification($data));


        Event::fire(new SystemEvent(auth::id(), 'Sent a Transfer Request.'));

        return redirect('admin/finance/manage-bank-transaction/mtics')->with('success', 'Request Sent!');

    }






//EVENT REQUEST TO DEPOSIT //EVENT REQUEST TO DEPOSIT //EVENT REQUEST TO DEPOSIT
     public function event_deposit(Request $request)
    {

        if($request->event_id == '-- Select Event --')
        {
            return redirect()->back()->withErrors('Please Select Event');
        }

       $this->validate($request, [
        'deposit_type' => 'required',
        'deposit_amt' => 'required',
        ]);

        if($request->deposit_type == 'others')
        {
            $this->validate($request, [
            'deposit_desc' => 'required',
            ]);

            $amt = $request->deposit_amt;
            $deposit_source = 'other';
        }

        if($request->deposit_type == 'event')
        {

            $eventpayment = EventPayment::where('event_id',$request->event_id)->sum('payevent_amt');
            $eventpayment_deposit = EventDeposit::where('event_id',$request->event_id)->where('deposit_source','Event Payment')->where('deposit_status','cleared')->get()->sum('deposit_amt');

            $totaleventpayment = $eventpayment - $eventpayment_deposit;

            //If the inputted event payment is not same with the event payment bucket, Must need a reason or remarks
            if($request->deposit_amt > $totaleventpayment)
            {
               return redirect()->back()->withErrors('not enough money, Event Payment bucket is P'. $totaleventpayment .' only');
            }
            elseif ($request->deposit_amt < $totaleventpayment) {
                $this->validate($request, [
                'remarks' => 'required',
                ]);
                $totaleventpayment = $request->deposit_amt;
            }

            $amt = $totaleventpayment;
            $deposit_source = 'Event Payment';

        }
        elseif($request->deposit_type == 'mtics_payment')
        {
            $mticsfund_bucket = $this->CheckMticsFundBucket();
            $this->validate($request, [
            'deposit_desc' => 'required',
            ]);

            if($request->deposit_amt > $mticsfund_bucket)
            {
               return redirect()->back()->withErrors('not enough money, MTICS Fund is P'. $mticsfund_bucket .' only');
            }

            $amt = $request->deposit_amt;
            $deposit_source = 'MTICS Fund';

        }

            $eventdeposit = New EventDeposit;
            $eventdeposit->admin_id = Auth::id();
            $eventdeposit->event_id = $request->event_id;
            $eventdeposit->deposit_desc = $request->deposit_desc;
            $eventdeposit->deposit_amt = $amt;
            $eventdeposit->deposit_source = $deposit_source;
            $eventdeposit->remarks = $request->remarks;
            $eventdeposit->save();

        $data = array(
        'message' => "New Deposit Request.",
        'redirect' => 'admin/adviser/mtics-bank-transaction-request',
        'origin' => 'depositRequest',
        );

        $role_id = Role::where('name', 'mtics_adviser')->first()->id;
        $member = Member::whereHas('role', function($q) use ($role_id) {
            $q->where('role_id', $role_id);
        })->first();
        $member->notify(new SystemNotification($data));


        Event::fire(new SystemEvent(auth::id(), 'Sent a Deposit Request.'));

        return redirect('admin/finance/manage-bank-transaction/mtics')->with('success', 'Request Sent!');
    }


    public function event_banktrans_view()
    {
        $eventdeposits = EventDeposit::all();
        $eventwithdrawals = EventWithdrawal::all();
        return view('officers/adviser_eventdeposit_view',compact('eventdeposits','eventwithdrawals'));
    }

    public function event_deposit_approved(Request $request,$id)
    {

        $eventdeposit = EventDeposit::find($id);
        if($eventdeposit->deposit_status == 'cleared')
        {
           return redirect()->back()->withErrors('The request has been cleared');
        }

        if($eventdeposit->deposit_source == 'MTICS Fund'){
            EventDeposit::where('id', $id)->update(array('deposit_status' => 'cleared'));
        }
        else{
            EventDeposit::where('id', $id)->update(array('deposit_status' => 'approved'));
        }


        EventDeposit::where('id', $id)->update(array('faculty_id' => Auth::id()));

        $data = array(
        'message' => "Your deposit request has been approved.",
        'redirect' => 'admin/finance/manage-bank-transaction/mtics',
        'origin' => 'bank-transaction-approved',
        );

        $role_id = Role::where('name', 'finance')->first()->id;
        $member = Member::whereHas('role', function($q) use ($role_id) {
            $q->where('role_id', $role_id);
        })->first();
        $member->notify(new SystemNotification($data));

        Event::fire(new SystemEvent(auth::id(), 'Approved Deposit Request.'));

        return redirect('admin/adviser/mtics-bank-transaction-request')->with('success', 'Approved Request!');
    }

     public function event_deposit_denied(Request $request,$id)
    {
        $eventdeposit = EventDeposit::find($id);
        if($eventdeposit->deposit_status == 'cleared')
        {
           return redirect()->back()->withErrors('The request has been cleared');
        }

        $this->validate($request, [
        'denied_reason' => 'required',
        ]);

        EventDeposit::where('id', $id)->update(array('deposit_status' => 'denied'));
        EventDeposit::where('id', $id)->update(array('faculty_id' => Auth::id()));
        EventDepositDeny::create([
            'event_deposit_id' => $id,
            'faculty_id' => Auth::id(),
            'denied_reason' => $request->denied_reason,
        ]);

        $data = array(
        'message' => "Your deposit request has been denied.",
        'redirect' => array('role' => 'finance'),
        'origin' => 'depositDisapprove',
        );

        $role_id = Role::where('name', 'finance')->first()->id;
        $member = Member::whereHas('role', function($q) use ($role_id) {
            $q->where('role_id', $role_id);
        })->first();
        $member->notify(new SystemNotification($data));

        return redirect('admin/adviser/mtics-bank-transaction-request');
    }



    public function event_deposit_report(Request $request,$id)
    {
        $eventdeposit = EventDeposit::where('id',$id)->with('receipt')->first();
        if($eventdeposit->deposit_status == 'cleared')
        {
           return redirect()->back()->withErrors('The request has been cleared');
        }

        if(!EventDepositReport::where('event_deposit_id',$id)->get()->isEmpty()){
           return redirect()->back()->withErrors('The request has been reported');
        }

        if($eventdeposit->receipt == null)
        {
           return redirect()->back()->withErrors('Please upload the receipt');
        }


        $this->validate($request, [
            'report_name' => 'required',
            'report_reason' => 'required',
          ]);


         EventDepositReport::create([
            'event_deposit_id' => $id,
            'faculty_id' => Auth::id(),
            'deposit_title' => $request->report_name,
            'deposit_desc' => $request->report_reason,
            ]);
        EventDeposit::where('id', $id)->update(array('deposit_status' => 'reported'));

        $message = "Deposit transaction has been reported.";

        $financeRole_id = Role::where('name', 'finance')->first()->id;
        $presidentRole_id = Role::where('name', 'president')->first()->id;

        $members = Member::whereHas('role', function($q) use ($financeRole_id, $presidentRole_id) {
            $q->where('role_id', $financeRole_id)->orWhere('role_id', $presidentRole_id);
        })->get();

        \Notification::send($members, new SystemBroadcastNotification($message, 'report-deposit'));

        return redirect('admin/adviser/mtics-bank-transaction-request')->with('success', 'Reported!');

    }

    public function event_deposit_receipt(Request $request,$id)
    {
        $eventdeposit = EventDeposit::find($id);
        if($eventdeposit->deposit_status == 'cleared')
        {
           return redirect()->back()->withErrors('The request has been cleared');
        }

        if($eventdeposit->deposit_status == 'reported')
        {
           return redirect()->back()->withErrors('The request has been reported');
        }

        $this->validate($request, [
            'receipt' => 'image|mimes:jpeg,jpg,png|max:2000', //image only
          ]);


        $file = $request->file('receipt');
        $date = date('Y-m-d');
        $time = time();
        //move to public/images/receipts/deposit/event
        if($request->hasFile('receipt')){

            if(!EventDepositReceipt::where('event_deposit_id',$id)->get()->isEmpty())
            {

                $eventdepositreceipt = EventDepositReceipt::where('event_deposit_id',$id)->first();
                if(url('/') == 'http://mtics-ma.tuptaguig.com'){
                File::delete('public/images/' . $eventdepositreceipt->receipt_image);
                }
                else{
                File::delete('images/' . $eventdepositreceipt->receipt_image);
                }
                $eventdepositreceipt->delete();

            }

            if(url('/') == 'http://mtics-ma.tuptaguig.com'){
                $file->move('public/images/receipts/deposit/event/', $eventdeposit->event_id.'_'.$id.'_'.$time.'_'.$date.'_depositreceipt.jpg');
                }
            else{
            $file->move('images/receipts/deposit/event/', $eventdeposit->event_id.'_'.$id.'_'.$time.'_'.$date.'_depositreceipt.jpg');
            }



          $imgname =  "receipts/deposit/event/".$eventdeposit->event_id.'_'.$id.'_'.$time.'_'.$date.'_depositreceipt.jpg';
          EventDepositReceipt::create([
            'event_deposit_id' => $id,
            'faculty_id' => Auth::id(),
            'receipt_image' => $imgname,
            ]);
        //EventDeposit::where('id', $id)->update(array('deposit_status' => 'cleared'));

        }

        Event::fire(new SystemEvent(auth::id(), 'Uploaded Receipt for Deposit Request.'));

        return redirect('admin/adviser/mtics-bank-transaction-request')->with('success', 'Receipt Uploaded!');
    }



    public function event_deposit_cleared(Request $request,$id)
    {

        $eventdeposit = EventDeposit::where('id',$id)->with('receipt')->first();
        if($eventdeposit->deposit_status == 'reported')
        {
           return redirect()->back()->withErrors('The request has been reported');
        }

        if($eventdeposit->receipt == null)
        {
           return redirect()->back()->withErrors('Please upload the receipt');
        }

        EventDeposit::where('id', $id)->update(array('deposit_status' => 'cleared'));

        $data = array(
        'message' => "Your deposit request has been cleared.",
        'redirect' => 'admin/finance/manage-bank-transaction/mtics',
        'origin' => 'bank-transaction-approved',
        );

        $role_id = Role::where('name', 'finance')->first()->id;
        $member = Member::whereHas('role', function($q) use ($role_id) {
            $q->where('role_id', $role_id);
        })->first();
        $member->notify(new SystemNotification($data));

        Event::fire(new SystemEvent(auth::id(), 'Deposit Request Cleared.'));

        return redirect('admin/adviser/mtics-bank-transaction-request')->with('success', 'Cleared!');
    }


//EVENT REQUEST TO WITHDRAW //EVENT REQUEST TO WITHDRAW //EVENT REQUEST TO WITHDRAW

     public function event_withdraw(Request $request)
    {
        //dd($request);
        if($request->external_task_id !== 'NULL') {
            $task = Task::findOrFail($request->external_task_id);
            $event = MticsEvent::findOrFail($task->event_id);

            $moneyrequest = MoneyRequest::where('task_id',$task->id)->first();
            if($moneyrequest->amount < 1){
            return redirect()->back()->withErrors('Request can not be negative or 0, please input estimated price');
            }

        }

        elseif($request->event_id !== 'NULL') {
            $event = MticsEvent::findOrFail($request->event_id);
        }

        else {

            return redirect()->back()->withErrors('Task or Event Field is required');
        }


        $eventfund_bucket = $this->CheckEventFundBucketPerEvent($event->id);

        if(($eventfund_bucket - $request->withdraw_amt) < 0)
        {
            return redirect()->back()->withErrors('You only have P'.$eventfund_bucket.'. Event FUND '.$event->event_title.' will be negative, cannot request a withdraw');
        }




        //dd($request);
        $this->validate($request, [
        'withdraw_type' => 'required',
        ]);

        if($request->withdraw_type == 'withdraw_other')
        {

             $this->validate($request, [
            'withdraw_reason' => 'required',
            'withdraw_amt' => 'required',
            ]);

            $external_task_id = null;
            $event_breakdown_id = null;
            $reason = $request->withdraw_reason;
            $amt = $request->withdraw_amt;

        }
        elseif($request->withdraw_type == 'withdraw_external')
        {
             $this->validate($request, [
            'external_task_id' => 'required',
            ]);

            if(!EventWithdrawal::where('event_id',$event->id)->where('external_task_id',$request->external_task_id)->get()->isEmpty())
            {
                return redirect()->back()->withErrors('This External Task already has withdraw request');
            }

            $moneyrequest = MoneyRequest::where('task_id',$request->external_task_id)->first();
            $external_task_id = $request->external_task_id;
            $event_breakdown_id = null;
            $reason = 'External Task';
            $amt = $moneyrequest->amount;

        }

        $finalamt = $eventfund_bucket - $amt;
        if($finalamt < 0)
        {
            return redirect()->back()->withErrors('Not enough event fund, the total amount of fund for this event is P'.$eventfund_bucket);
        }

        $eventwithdrawal = New EventWithdrawal;
        $eventwithdrawal->admin_id = Auth::id();
        $eventwithdrawal->event_id = $event->id;
        $eventwithdrawal->withdrawal_reason = $reason;
        $eventwithdrawal->event_breakdown_id = $event_breakdown_id;
        $eventwithdrawal->withdrawal_desc = $request->withdraw_desc;
        $eventwithdrawal->withdrawal_amt = $amt;
        $eventwithdrawal->external_task_id = $external_task_id;
        $eventwithdrawal->save();

        $data = array(
        'message' => "New Withdraw Request.",
        'redirect' => 'admin/adviser/mtics-bank-transaction-request',
        'origin' => 'withdrawRequest',
        );

        $role_id = Role::where('name', 'mtics_adviser')->first()->id;
        $member = Member::whereHas('role', function($q) use ($role_id) {
            $q->where('role_id', $role_id);
        })->first();
        $member->notify(new SystemNotification($data));


        Event::fire(new SystemEvent(auth::id(), 'Sent a Withdraw Request.'));

        return redirect('admin/finance/manage-bank-transaction/mtics')->with('success', 'Request Sent!');

    }


     public function event_withdraw_approved(Request $request,$id)
    {
        $eventwithdrawal = EventWithdrawal::find($id);
        if($eventwithdrawal->withdrawal_status == 'cleared')
        {
           return redirect()->back()->withErrors('The request has been cleared');
        }
        EventWithdrawal::where('id', $id)->update(array('withdrawal_status' => 'approved'));
        EventWithdrawal::where('id', $id)->update(array('faculty_id' => Auth::id()));

        $data = array(
        'message' => "Your withdraw request has been approved.",
        'redirect' => 'admin/finance/manage-bank-transaction/mtics',
        'origin' => 'bank-transaction-approved',
        );

        $role_id = Role::where('name', 'finance')->first()->id;
        $member = Member::whereHas('role', function($q) use ($role_id) {
            $q->where('role_id', $role_id);
        })->first();
        $member->notify(new SystemNotification($data));

        Event::fire(new SystemEvent(auth::id(), 'Approved Withdraw Request.'));

        return redirect('admin/adviser/mtics-bank-transaction-request')->with('success', 'Approved Request!');

    }

    public function event_withdraw_denied(Request $request,$id)
    {

        $eventwithdrawal = EventWithdrawal::find($id);
        if($eventwithdrawal->withdrawal_status == 'cleared')
        {
           return redirect()->back()->withErrors('The request has been cleared');
        }

        $this->validate($request, [
        'denied_reason' => 'required',
        ]);


        EventWithdrawal::where('id', $id)->update(array('withdrawal_status' => 'denied'));
        EventWithdrawal::where('id', $id)->update(array('faculty_id' => Auth::id()));
        EventWithdrawalDeny::create([
            'event_withdrawal_id' => $id,
            'faculty_id' => Auth::id(),
            'denied_reason' => $request->denied_reason,
        ]);

        return redirect('admin/adviser/mtics-bank-transaction-request');
    }


    public function event_withdraw_report(Request $request,$id)
    {

        $eventwithdrawal = EventWithdrawal::where('id',$id)->with('receipt')->first();
        if($eventwithdrawal->withdrawal_status == 'cleared')
        {
           return redirect()->back()->withErrors('The request has been cleared');
        }

        if(!EventWithdrawalReport::where('event_withdrawal_id',$id)->get()->isEmpty()){
           return redirect()->back()->withErrors('The request has been reported');
        }


        if($eventwithdrawal->receipt == null)
        {
           return redirect()->back()->withErrors('Please upload the receipt');
        }


        $this->validate($request, [
            'report_name' => 'required',
            'report_reason' => 'required',
          ]);

        $eventwithdrawal = EventWithdrawal::find($id);
        if($eventwithdrawal->withdrawal_status == 'cleared')
        {
           return redirect()->back()->withErrors('The request has been cleared');
        }

         EventWithdrawalReport::create([
            'event_withdrawal_id' => $id,
            'faculty_id' => Auth::id(),
            'withdrawal_title' => $request->report_name,
            'withdrawal_desc' => $request->report_reason,
            ]);

        EventWithdrawal::where('id', $id)->update(array('withdrawal_status' => 'reported'));


        $message = "Withdraw transaction has been reported.";

        $financeRole_id = Role::where('name', 'finance')->first()->id;
        $presidentRole_id = Role::where('name', 'president')->first()->id;

        $members = Member::whereHas('role', function($q) use ($financeRole_id, $presidentRole_id) {
            $q->where('role_id', $financeRole_id)->orWhere('role_id', $presidentRole_id);
        })->get();

        \Notification::send($members, new SystemBroadcastNotification($message, 'report-withdraw'));

        return redirect('admin/adviser/mtics-bank-transaction-request')->with('success', 'Reported!');

    }

     public function event_withdraw_receipt(Request $request,$id)
    {

        $eventwithdrawal = EventWithdrawal::find($id);
        if($eventwithdrawal->withdrawal_status == 'cleared')
        {
           return redirect()->back()->withErrors('The request has been cleared');
        }
        if($eventwithdrawal->withdrawal_status == 'reported')
        {
           return redirect()->back()->withErrors('The request has been reported');
        }


        $this->validate($request, [
            'receipt' => 'image|mimes:jpeg,jpg,png|max:2000', //image only
          ]);

        $file = $request->file('receipt');
        $date = date('Y-m-d');
        $time = time();
        //move to public/images/receipts/withdraw/event
        if($request->hasFile('receipt')){


            if(!EventWithdrawalReceipt::where('event_withdrawal_id',$id)->get()->isEmpty())
            {
                $eventwithdrawalreceipt = EventWithdrawalReceipt::where('event_withdrawal_id',$id)->first();
                if(url('/') == 'http://mtics-ma.tuptaguig.com'){
                File::delete('public/images/' . $eventwithdrawalreceipt->receipt_image);
                }
                else{
                File::delete('images/' . $eventwithdrawalreceipt->receipt_image);
                }
                $eventwithdrawalreceipt->delete();

            }

            if(url('/') == 'http://mtics-ma.tuptaguig.com'){
                $file->move('public/images/receipts/withdrawal/event/', $id.'_'.$time.'_'.$date.'_withdrawalreceipt.jpg');
            }
            else{
                $file->move('images/receipts/withdrawal/event/', $id.'_'.$time.'_'.$date.'_withdrawalreceipt.jpg');
            }
          $imgname =  "receipts/withdrawal/event/".$id.'_'.$time.'_'.$date.'_withdrawalreceipt.jpg';
          EventWithdrawalReceipt::create([
            'event_withdrawal_id' => $id,
            'faculty_id' => Auth::id(),
            'receipt_image' => $imgname,
            ]);
        }

        Event::fire(new SystemEvent(auth::id(), 'Uploaded Receipt for Withdraw Request.'));

        return redirect('admin/adviser/mtics-bank-transaction-request')->with('success', 'Receipt Uploaded!');
    }


    public function event_withdraw_cleared(Request $request,$id)
    {

        $eventwithdrawal = EventWithdrawal::where('id',$id)->with('receipt')->first();
        if($eventwithdrawal->withdrawal_status == 'reported')
        {
           return redirect()->back()->withErrors('The request has been reported');
        }


        if($eventwithdrawal->receipt == null)
        {
           return redirect()->back()->withErrors('Please upload the receipt');
        }

        EventWithdrawal::where('id', $id)->update(array('withdrawal_status' => 'cleared'));

        $data = array(
        'message' => "Your withdraw request has been cleared.",
        'redirect' => 'admin/finance/manage-bank-transaction/mtics',
        'origin' => 'bank-transaction-approved',
        );

        $role_id = Role::where('name', 'finance')->first()->id;
        $member = Member::whereHas('role', function($q) use ($role_id) {
            $q->where('role_id', $role_id);
        })->first();
        $member->notify(new SystemNotification($data));

        Event::fire(new SystemEvent(auth::id(), 'Withdraw Request Cleared.'));

        return redirect('admin/adviser/mtics-bank-transaction-request')->with('success', 'Cleared!');

    }


}
