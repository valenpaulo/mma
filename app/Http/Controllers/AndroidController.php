<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AndroidController extends Controller
{
    public function download() {
        $file= public_path(). "/apk/mma.apk";
        return response()->download($file, 'mma.apk');
    }
}
