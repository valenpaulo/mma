<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Meeting;
use App\Member;
use App\Role;
use App\MeetingImage;


use Illuminate\Support\Facades\Auth;
use App\Notifications\SystemBroadcastNotification;

use App\Events\SystemEvent;
use Illuminate\Support\Facades\Event;


class MeetingController extends Controller
{
    public function store(Request $request, $id) {

        $this->validate($request, [
            'agenda' => 'required|max:255',
            'where' => 'required|max:255',
            'when' => 'required',
        ]);

        $meeting_sched = strftime('%Y-%m-%d %H:%M:%S', strtotime($request->when));

        $meeting = Meeting::create([
            'agenda' => $request->agenda,
            'where' => $request->where,
            'when' => $meeting_sched,
            'event_id' => $id,
        ]);


        $message = 'Upcoming Meeting: ' . $request->agenda;

        $members = Member::whereHas('role', function($q){
            $faculty = Role::where('name', 'faculty')->first()->id;
            $super_admin = Role::where('name', 'super_admin')->first()->id;
            $president = Role::where('name', 'president')->first()->id;

            $q->where('role_id', '!=', $faculty)->where('role_id', '!=', $super_admin)->where('role_id', '!=', $president);
        })->get();

        \Notification::send($members, new SystemBroadcastNotification($message, 'meeting'));

        Event::fire(new SystemEvent(auth::id(), 'Call a meeting.'));

        return redirect()->back()->with('success', 'Added!');
    }

    public function edit(Request $request, $id) {
        $meeting = Meeting::findOrFail($id);
        $flag = 0;

        if ($request->agenda != $meeting->agenda and $request->agenda != ""){
            $meeting->agenda = $request->agenda;
            $flag = 1;
        }

        if ($request->where != $meeting->where){
            $meeting->where = $request->where;
            $flag = 1;
        }

        if ($request->when != $meeting->when and $request->when != ""){
            $meeting->when = $request->when;
            $flag = 1;
        }

        if ($flag == 1) {
            $meeting->update();
        }

        return redirect()->back();
    }

     public function cancel($id) {
        $meeting = Meeting::findOrFail($id);
        $meeting->delete();
        return redirect()->back();
    }

    public function done(Request $request,$id) {
        $meeting = Meeting::findOrFail($id);
    
         $this->validate($request, [
        'upload.*' => 'image|mimes:jpeg,jpg,png|max:5000', //image only
        ]);


         if($request->hasFile('upload')){
            

            if($request->hasFile('upload')){
                foreach ($request->file('upload') as $upload) {

                    if($upload->getmimeType() !== 'image/jpeg' and $upload->getmimeType()!== 'image/png')
                    {
                      continue;
                    }
                    else
                    {
                        $date = date('Y-m-d');
                        $time = round(microtime(true) * 1000);
                        $fileName = $upload->getClientOriginalName();
                        $image_name = 'meetings/'.$id.'_'.$time.'_'.$date.'.jpg';
                        //move image to images/event_gallery/'title of event'
                        
                         if(url('/') == 'http://mtics-ma.tuptaguig.com'){
                            $upload->move('public/images/meetings/', $id.'_'.$time.'_'.$date.'.jpg');
                            }
                            else{ 
                            $upload->move('images/meetings/', $id.'_'.$time.'_'.$date.'.jpg');
                            }

                        $upload = new MeetingImage;
                        $upload->meeting_id = $id;
                        $upload->admin_id = Auth::id();
                        $upload->image_name = $image_name;
                        $upload->save();
                        $meeting->status = 'done';
                        $meeting->save();

                    }
                }
            }
        }



        return redirect()->back();
    }

    public function discussion($id) {
        return view('officers.discussion');
    }
}
