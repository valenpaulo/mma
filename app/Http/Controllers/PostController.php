<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Event;

use App\Notifications\SystemBroadcastNotification;

use App\Events\SystemEvent;

use App\Reply;
use App\Post;
use App\Filterword;
use App\PostCategory;
use App\Member;
use App\ReportPost;
use App\EventPartialPost;
use App\EventPartialPostReply;
use App\MticsEvent;
use App\Role;
use App\EventReply;


class PostController extends Controller
{

	public function __construct()
    {
       Auth::shouldUse('user');
    }


/*    VIEW  VIEW    VIEW  VIEW    VIEW  VIEW    VIEW  VIEW*/
    public function view($id,$slug)
    {
        $post = Post::find($id);
       if($slug !== $post->slug)
        {
            return redirect('member/profile');
        }

        if($post->status == 'archived'){
            return redirect('member/profile');
        }

        $author = Member::findOrFail($post->member_id);
        $category = PostCategory::findOrFail($post->post_category_id);
        // $memberpost = DB::table('posts')
        //     ->join('members', 'members.id', '=', 'posts.member_id')
        //     ->select('members.username', 'members.first_name', 'members.last_name')
        //     ->where('posts.id','=',$id)
        //     ->get();
        $post_categories = PostCategory::all();

        $replies = Reply::where('post_id', $id)->orderBy('id', 'desc')->get();

        return view('viewpost', compact('post','author','category', 'post_categories','replies'));
    }



/*    EDIT   EDIT    EDIT   EDIT    EDIT   EDIT    EDIT   EDIT*/
    public function edit(Request $request,$id)
    {

        $this->validate($request, [
        'image' => 'mimes:jpeg,bmp,png', //image only
        'title' => 'required|max:255',
        'body' => 'required',
        'post_category' => 'required',
        ]);

        $post = Post::findOrFail($id);

        $remember_token = $request->_token;
        $slug = preg_replace('/\s+/', '-', $request->title);

        //upload image to public/images/posts
         $file = $request->file('image');
        if($request->hasFile('image'))
        {
            $originalimage = $post->image;
            if($originalimage !== "posts/default.png" )
            {
                if(url('/') == 'http://mtics-ma.tuptaguig.com'){
                  File::delete('public/images/' . $originalimage);
                }
                else{
                  File::delete('images/' . $originalimage);
                }

            }
                if(url('/') == 'http://mtics-ma.tuptaguig.com'){
                  $file->move('public/images/posts', $id.$file->getClientOriginalName());
                }
                else{
                  $file->move('images/posts', $id.$file->getClientOriginalName());
                }

        $imgname =  "posts/".$id.$request->image->getClientOriginalName();
        $post->update(array('image' => $imgname));
        }

        $filterbody = $this->wordFilter($request->body);
        $filtertitle = $this->wordFilter($request->title);

        $post->update(array('title' => $filtertitle));
        $post->update(array('body' => $filterbody));
        $post->update(array('slug' => $slug));
        $post->update(array('post_category_id' => $request->post_category));
        $post->update(array('remember_token' => $request->_token));

        Event::fire(new SystemEvent(auth::id(), 'Edit "' . $post->title . '" post.'));

        return redirect()->back()->with('success', 'Post Edited.');
    }

    public function editView($id, $slug) {
        $post = Post::findOrFail($id);
        $post_categories = PostCategory::all();
        return view('editPost', compact('post', 'post_categories'));
    }



/*    DELETE   DELETE    DELETE   DELETE    DELETE   DELETE*/
    public function delete(Request $request,$id)
    {

        $post = Post::find($id);
        $post->status = 'archived';
        $post->remember_token = $request->_token;
        $post->save();

        Event::fire(new SystemEvent(auth::id(), 'Deleted "' . $post->title . '" post.'));

        return redirect('member/profile/post')->with('success', 'Post Deleted.');
    }


/*    STORE   STORE    STORE   STORE    STORE   STORE    STORE   STORE*/
    public function store(Request $request)
    {
    	//VALIDATOR
    	$this->validate($request, [
        'title' => 'required|max:255',
        'body' => 'required',
        'post_category' => 'required',
        'image' => 'mimes:jpeg,bmp,png,jpg',
        ]);

        if(PostCategory::where('id',$request->post_category)->get()->isEmpty()){
            return redirect()->back()->withErrors("Please Select Category");
        }

    	$slug = preg_replace('/\s+/', '-', $request->title);
    	$member_id = Auth::id();
        $filterbody = $this->wordFilter($request->body);
        $filtertitle = $this->wordFilter($request->title);

        if(strpos($filtertitle, '*') == 1)
        {

            return redirect()->back()->withErrors("This Title contains Profanity, Please make sure your title doesn't contain profanity");
        }


    	$post = New Post;

    	$post->member_id = $member_id;
    	$post->post_category_id = $request->post_category;
    	$post->title = $filtertitle;
    	$post->body = $filterbody;
    	$post->slug = $slug;
    	$post->remember_token = $request->_token;
		$post->save();


		//UPLOAD PHOTO
    	$file = $request->file('image');
        if($request->hasFile('image'))
        {

            if(url('/') == 'http://mtics-ma.tuptaguig.com'){
                $file->move('public/images/posts', $post->id.$file->getClientOriginalName());
            }
            else{
                $file->move('images/posts', $post->id.$file->getClientOriginalName());

            }
        $imgname =  "posts/".$post->id.$request->image->getClientOriginalName();

        $post->image = $imgname;
    	$post->save();
        }

        Event::fire(new SystemEvent($member_id, 'Added a new post entitled "' . $post->title . '".'));

        $firebase = New FireBaseController();
        $firebase->sendNotification('New Post Published!');

       	return redirect()->back()->with('success', 'Post Published!');

    }

    public function status(Request $request, $id)
    {
        /*$report = ReportPost::where('post_id', $id)->first();
        $report->delete();*/

        $post = Post::findOrFail($id);
        $post->status = $request->status;
        $post->save();
        // Member::where('id', $id)->update(array('status' => 'deactivated'));

        Event::fire(new SystemEvent(auth::id(), 'Published "' . $post->title . '".'));

        return redirect()->back()->with("success", 'Status Changed!');
    }


//REPORT REPORT REPORT REPORT REPORT REPORT REPORT REPORT REPORT REPORT
    public function report(Request $request, $id, $slug)
    {
        $post = Post::find($id);
        $this->validate($request, [
        'reason' => 'required',
        ]);

        if(ReportPost::where('member_id',Auth::id())->where('post_id',$id)->get()->isEmpty())
        {
        ReportPost::create([
            'member_id' => Auth::id(),
            'post_id' => $id,
            'reason' => $request->reason,
        ]);
        }
        else
        {
            return redirect()->back()->withErrors('You already reported this Post');
        }

        Post::where('id', $id)->update(array('status' => 'reported'));

        $members = Member::has('role')->get();
        $message = 'Reported Post: ' . $post->title;

        \Notification::send($members, new SystemBroadcastNotification($message, 'report-post'));

        Event::fire(new SystemEvent(auth::id(), 'Reported "' . $post->title . '".'));


        return redirect()->back()->with("success", 'Status Changed!');

    }


/*    REPLY   REPLY    REPLY   REPLY    REPLY   REPLY    REPLY   REPLY*/
    public function reply(Request $request,$id,$slug)
    {

        $validator = Validator::make($request->all(), [
        'reply_body' => 'required|max:255',
        ]);

        if ($validator->fails()){
            return redirect()->back()->withErrors("Please leave a comment.");
        }

       $filteredreply = $this->wordFilter($request->reply_body);

       $post = Post::findOrFail($id);

       $reply = new Reply;
       $reply->member_id = auth::id();
       $reply->post_id = $id;
       $reply->reply_body = $filteredreply;
       $reply->remember_token = $request->_token;
       $reply->save();

        Event::fire(new SystemEvent(auth::id(), 'Commented on "' . $post->title . '".'));

        return redirect()->back()->with('success', 'Done!');
    }

    public function deleteReply($id)
    {
        $reply = Reply::findOrFail($id);
        $reply->delete();

        Event::fire(new SystemEvent(auth::id(), 'Delete Comment.'));

        return redirect()->back()->with('success', 'Deleted!');
    }



     function wordFilter($text)
      {
        $filtered_text = $text;
        $filterwords = Filterword::all();

        foreach ($filterwords as $filterword)
        {
            $profanity_word = $filterword->Filter_word;

            $wordfirst = $profanity_word[0];
            $wordlength = strlen($profanity_word);
            $char = '*';
            for($x=3;$x<=$wordlength;$x++)
            {
                $char = $char.'*';
            }
            $replace_word = $wordfirst.$char;
            $filtered_text = str_ireplace($profanity_word, $replace_word, $filtered_text);
        }

        return $filtered_text;
      }






//MANAGE-POSTSFEED

      public function postsfeed_view()
      {

        if(!Gate::allows('admin-only'))
        {
        return redirect('/');
        }
        $posts = DB::table('posts')
            ->join('members', 'members.id', '=', 'posts.member_id')
            ->join('post_categories', 'post_categories.id', '=', 'posts.post_category_id')
            ->select('posts.*','members.first_name', 'members.last_name','post_categories.type')
            ->where('posts.status','=','published')
            ->orderby('posts.id', 'desc')
            ->get();

        $reported = DB::table('posts')
            ->join('members', 'members.id', '=', 'posts.member_id')
            ->join('post_categories', 'post_categories.id', '=', 'posts.post_category_id')
            ->select('posts.*','members.first_name', 'members.last_name','post_categories.type')
            ->where('posts.status','=','reported')
            ->orderby('posts.id', 'desc')
            ->get();

        $reports = ReportPost::all();

        return view('admin.adminPanel.manage_postsfeed', compact('posts', 'reported', 'reports'));
      }

      public function filter(Request $request) {

        $filter = $request->filter;

        if ($request->filter == 'reported'){
            $posts = DB::table('posts')
            ->join('members', 'members.id', '=', 'posts.member_id')
            ->join('post_categories', 'post_categories.id', '=', 'posts.post_category_id')
            ->join('report_posts', 'report_posts.post_id', '=', 'posts.id')
            ->select('posts.*','members.first_name', 'members.last_name','post_categories.type')
            ->where('posts.status','=',$request->filter)
            ->orderby('report_posts.id', 'desc')
            ->get();

            $reported = ReportPost::get();
            return view('admin.adminPanel.manage_postsfeed', compact('posts', 'reported', 'filter'));
        }

        $posts = DB::table('posts')
            ->join('members', 'members.id', '=', 'posts.member_id')
            ->join('post_categories', 'post_categories.id', '=', 'posts.post_category_id')
            ->select('posts.*','members.first_name', 'members.last_name','post_categories.type')
            ->where('posts.status','=',$request->filter)
            ->orderby('posts.id', 'desc')
            ->get();

        return view('admin.adminPanel.manage_postsfeed', compact('posts', 'filter'));
      }


       public function postsfeed_delete(Request $request, $id)
      {
        $post = New Post;
        $post = Post::find($id);
        $post->status = 'archived';
        $post->remember_token = $request->_token;
        $post->save();

        Event::fire(new SystemEvent(auth::id(), 'Archived "' . $post->title . '" post.'));

        return redirect()->back()->with('success', 'Done!');
      }


//EVENT POST //EVENT POST //EVENT POST //EVENT POST //EVENT POST //EVENT POST //EVENT POST //EVENT POST

//EVENT PARTIAL VIEW
    public function event_partial_view($id,$slug){
        $eventpartialpost = EventPartialPost::find($id);

        if($slug !== $eventpartialpost->slug)
        {
            return redirect('member/home');
        }

        if($eventpartialpost->status == 'archived'){
            return redirect('member/home');
        }

        return view('officers/event/event_partial_post',compact('eventpartialpost'));
    }

    public function event_partial_reply(Request $request,$id,$slug){
        $eventpartialpost = EventPartialPost::find($id);

        if($slug !== $eventpartialpost->slug)
        {
            return redirect('member/home');
        }

        $validator = Validator::make($request->all(), [
        'reply_body' => 'required|max:255',
        ]);

        if ($validator->fails()){
            return redirect()->back()->withErrors("Please leave a comment.");
        }

       $filteredreply = $this->wordFilter($request->reply_body);


       $reply = new EventPartialPostReply;
       $reply->member_id = auth::id();
       $reply->event_partial_post_id = $id;
       $reply->reply_body = $filteredreply;
       $reply->save();

        return redirect('member/event/'.$id.'/'.$slug);

    }
//EVENT FULL DETAILED VIEW
    public function event_view($id,$slug){
        $event = MticsEvent::find($id);

        if($slug !== $event->slug or $event->slug == null or $event->status !== 'done' or $event->post_status !== 'posted')
        {
            return redirect('member/home');
        }

        return view('officers/event/event_post',compact('event'));
    }


    public function event_reply(Request $request,$id,$slug){
        $event = MticsEvent::find($id);

        if($slug !== $event->slug or $event->slug == null or $event->status !== 'done' or $event->post_status !== 'posted')
        {
            return redirect('member/home');
        }

        $validator = Validator::make($request->all(), [
        'reply_body' => 'required|max:255',
        ]);

        if ($validator->fails()){
            return redirect()->back()->withErrors("Please leave a comment.");
        }

       $filteredreply = $this->wordFilter($request->reply_body);

       $reply = new EventReply;
       $reply->member_id = auth::id();
       $reply->event_id = $id;
       $reply->reply_body = $filteredreply;
       $reply->save();

        return redirect('member/'.$id.'/'.$slug);

    }

}
