<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Member;
use App\Section;
use App\Year;
use App\Role;
use App\ActionMember;
use App\RoleMember;
use App\Action;
use App\Course;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

use App\Events\SystemEvent;
use Illuminate\Support\Facades\Event;

class MemberController extends Controller
{



/* VIEW VIEW VIEW  VIEW VIEW VIEW  VIEW VIEW VIEW  VIEW VIEW VIEW */
    public function view()
    {

        if(!Gate::allows('superadmin-only'))
        {
        return redirect('/');
        }

        $sections = Section::where('section_status', 'active')->get();
        $years = Year::where('year_code','!=','Faculty')->where('year_code','!=','Alumni')->get();

        $alumni_section = Section::where('section_code', 'Alumni')->first();
        $faculty_section = Section::where('section_code', 'Faculty')->first();

        $students = Member::where('status', 'active')->where('section_id', '!=', $alumni_section->id)->where('section_id', '!=', $faculty_section->id)->get();

        $alumni = Member::where('status', 'inactive')->get();

        $faculty = Member::where('status', 'active')->where('section_id', $faculty_section->id)->get();

        $deactivated = Member::where('status', 'deactivated')->get();


    	return view('admin/addFeature/manage_member', compact('students', 'alumni', 'faculty', 'sections', 'deactivated', 'years','secs'));
    }

/*STORE STORE STORE STORE STORE STORE STORE STORE STORE STORE STORE STORE */
    protected function store(Request $request)
    {
        $this->validate($request, [
        'first_name' => 'required|max:255',
        'last_name' => 'required|max:255',
        'password' => 'required|min:8',
        'type' => 'required',
        'username' => 'required|unique:members',
        'email' => 'email|max:255|unique:members|nullable',
        'avatar' => 'mimes:jpeg,bmp,png|nullable',
        'mobile_no' => 'max:11|nullable',
        'guardian_mobile_no' => 'max:11|nullable',
        ]);
        $member = New Member;

     if($request->type == 'Alumni')
        {
             $section_id = Section::getId('App\Section','section_code','Alumni');
             $year_id = Year::getId('App\Year','year_code','Alumni');

        }
        elseif($request->type == 'Faculty')
        {
             $section_id = Section::getId('App\Section','section_code','Faculty');
             $year_id = Year::getId('App\Year','year_code','Faculty');

        }
        else
        {
              $this->validate($request, [
                'year' => 'required',
                'section' => 'required',
              ]);

            $section_id = $request->section;

             $section = Section::find($request->section);
             $course = Course::find($section->course_id);
             $year =  Year::find($request->year);


             if($course->course_year_count < $year->year_num)
             {
                $errors[] = $year->year_desc. ' is not valid with your selected section';
                return redirect('admin/manage-members')->withErrors($errors);
             }
             else
             {
             $year_id = $request->year;
             }
        $member->type = $request->stud_type;

        }

        $request->username = str_replace(' ', '', $request->username);
        $member->email = $request->email;
        $member->username = $request->username;
        $member->first_name = $request->first_name;
        $member->last_name = $request->last_name;
        $member->age = $request->age;
        $member->birthday = $request->birthday;
        $member->address = $request->address;
        $member->work = $request->work;
        $member->section_id = $section_id;
        $member->year_id = $year_id;
        $member->id_num = $request->id_num;
        $member->mobile_no = $request->mobile_no;
        $member->guardian_name = $request->guardian_name;
        $member->guardian_mobile_no = $request->guardian_mobile_no;
        $member->password = bcrypt($request->password);

        if($request->type=='Alumni')
        {$member->status = 'inactive';}
        else
        {$member->status = 'active';}
        $member->save();

        $facultyyear = Year::getId('App\Year','year_code','Faculty');
        if($member->year_id==$facultyyear)
        {
            $this->facultyRoleAction($member->id);
        }

        Event::fire(new SystemEvent(auth::id(), 'Added new member with a username "' . $member->username . '" info.'));

        return redirect('admin/manage-members')->with('success', 'New Member Added!');
    }



/*EDIT EDIT EDIT EDIT EDIT EDIT EDIT EDIT EDIT EDIT EDIT EDIT */

     public function edit(Request $request,$id)
    {
        $member = Member::find($id);

        $current_section_code = Section::find($member->section_id)->section_code;
        $remember_token = $request->_token;

        if($request->password)
        {$this->validate($request, ['password' => 'min:8']);}

        if($request->email)
        {
        if($request->email !== $member->email)
        {$this->validate($request, ['email' => 'required|email|max:255|unique:members']);}
        }

        if($request->username !== $member->username)
        {$this->validate($request, ['username' => 'required|max:255|unique:members']);}

        $request->username = str_replace(' ', '', $request->username);

        $this->validate($request, [
        'first_name' => 'required|max:255',
        'last_name' => 'required|max:255',
        'type' => 'required',
        'avatar' => 'mimes:jpeg,bmp,png,jpg',
        'mobile_no' => 'max:11',
        'guardian_mobile_no' => 'max:11',
        ]);
        //IMAGE UPLOAD
        $file = $request->file('avatar');
        if($request->hasFile('avatar'))
        {
            $originalavatar = $member->avatar;
            if($originalavatar !== "members/default.png" )
            {

                if(url('/') == 'http://mtics-ma.tuptaguig.com'){
                      File::delete('public/images/' . $originalavatar);
                
                }
                else{ 
                      File::delete('images/' . $originalavatar);
                }
                
            }
             if(url('/') == 'http://mtics-ma.tuptaguig.com'){
                    $file->move('public/images/members', $request->username.$file->getClientOriginalName());
                
                }
                else{ 
                    $file->move('images/members', $request->username.$file->getClientOriginalName());

                }
        $imgname =  "members/".$request->username.$request->avatar->getClientOriginalName();
        Member::where('id', $id)->update(array('avatar' => $imgname));
        }

        //CHECK IF PASSWORD HAS BEEN CHANGED

        if($request->password !== null)
        {   //dd($request->password);
            Member::where('id', $id)->update(array('password' => bcrypt($request['password'])));
        }

        //CHECK IF ALUMNI
        /*$alumni_id = Year::getId('App\Year','year_code','Alumni');
        $sec_alumni_id = Section::getId('App\Section','section_code','Alumni');

        if($request->year != $current_section_code)
        {
            if($member->status !== 'pending')
            {
                if($request->year == $alumni_id)
                {
                Member::where('id', $id)->update(array('status' => 'inactive'));
                Member::where('id', $id)->update(array('section_id' => $sec_alumni_id));
                }
                else
                {
                Member::where('id', $id)->update(array('status' => 'active'));
                Member::where('id', $id)->update(array('section_id' => $request->section));
                }
            }
        }*/

             if($request->type == 'Alumni')
        {
             $section_id = Section::getId('App\Section','section_code','Alumni');
             $year_id = Year::getId('App\Year','year_code','Alumni');
            Member::where('id', $id)->update(array('type' => null));
            Member::where('id', $id)->update(array('id_num' => null));


        }
        elseif($request->type == 'Faculty')
        {
             $section_id = Section::getId('App\Section','section_code','Faculty');
             $year_id = Year::getId('App\Year','year_code','Faculty');
            Member::where('id', $id)->update(array('type' => null));
            Member::where('id', $id)->update(array('id_num' => null));

        }
        else
        {
              $this->validate($request, [
                'year' => 'required',
                'section' => 'required',
                'stud_type' => 'required',
              ]);

            if($request->id_num !== $member->id_num)
            {$this->validate($request, ['id_num' => 'required|max:7|unique:members']);}


            $section_id = $request->section;

             $section = Section::find($request->section);
             $course = Course::find($section->course_id);
             $year =  Year::find($request->year);


             if($course->course_year_count < $year->year_num)
             {
                $errors[] = $year->year_desc. ' is not valid with your selected section';
                return redirect('admin/manage-members')->withErrors($errors);
             }
             else
             {
             $year_id = $request->year;
             }
        Member::where('id', $id)->update(array('type' => $request->stud_type));
        Member::where('id', $id)->update(array('id_num' => $request->id_num));

        }




        Member::where('id', $id)->update(array('year_id' => $year_id));
        Member::where('id', $id)->update(array('section_id' => $section_id));
        Member::where('id', $id)->update(array('first_name' => $request->first_name));
        Member::where('id', $id)->update(array('last_name' => $request->last_name));
        Member::where('id', $id)->update(array('username' => $request->username));
        Member::where('id', $id)->update(array('email' => $request->email));
        Member::where('id', $id)->update(array('mobile_no' => $request->mobile_no));
        Member::where('id', $id)->update(array('age' => $request->age));
        Member::where('id', $id)->update(array('birthday' => $request->birthday));
        Member::where('id', $id)->update(array('address' => $request->address));
        Member::where('id', $id)->update(array('work' => $request->work));
        Member::where('id', $id)->update(array('guardian_name' => $request->guardian_name));
        Member::where('id', $id)->update(array('guardian_mobile_no' => $request->guardian_mobile_no));
        Member::where('id', $id)->update(array('remember_token' => $remember_token));


         if($request->type=='Alumni')
         {
            Member::where('id', $id)->update(array('status' => 'inactive'));
         }

        else
        {
            Member::where('id', $id)->update(array('status' => 'active'));
        }

        $facultyyear = Year::getId('App\Year','year_code','Faculty');
        $alumniyear = Year::getId('App\Year','year_code','Alumni');
        if($year_id==$facultyyear)
        {
            $this->facultyRoleAction($id);
        }
        elseif ($year_id == $alumniyear) {
            RoleMember::where('member_id', $id)->delete();
            ActionMember::where('member_id', $id)->delete();
        }
        else
        {
             $faculty_id = Role::getId('App\Role','name','Faculty');
             $action_id = Action::getId('App\Action','action_name','acctreq_approval');
             RoleMember::where('member_id', $id)->where('role_id',$faculty_id)->delete();
             ActionMember::where('member_id', $id)->where('action_id',$action_id)->delete();
        }




        Event::fire(new SystemEvent(auth::id(), 'Updated "' . $member->username . '" info.'));

        return redirect('admin/manage-members')->with('success', 'Member Information Updated!');
    }

    public function status(Request $request, $id)
    {
        $member = Member::findOrFail($id);
        $member->status = $request->status;
        $member->save();
        // Member::where('id', $id)->update(array('status' => 'deactivated'));
        Event::fire(new SystemEvent(auth::id(), 'Updated "' . $member->username . '" status.'));

        return redirect('admin/manage-members')->with('success', 'Member Status Updated!');
    }


    public function template_download()
    {
        if(!Gate::allows('superadmin-only'))
        {
        return redirect('/');
        }


        $file= public_path(). "/documents/excel_template/manage-member_template_import.xlsx";

        return response()->download($file);
    }


/*BULK MEMBERS BULK MEMBERS BULK MEMBERS BULK MEMBERS BULK MEMBERS */
    public function importExcel()
    {
        $alumni_id = Year::getId('App\Year','year_code','Alumni');
        $faculty_id = Year::getId('App\Year','year_code','Faculty');
        if(Input::hasFile('import_file')){
            $path = Input::file('import_file')->getRealPath();
            $data = Excel::load($path, function($reader) {
            })->get();
            if(!empty($data) && $data->count()){
                foreach ($data as $key => $value) {


                    if(!Section::where('section_code', $value->section)->get()->isEmpty())
                    {
                    $section_id = Section::getId('App\Section','section_code',$value->section);
                    }
                    elseif($value->section == null)
                    {
                        if($value->year == $alumni_id or $value->year == 'Alumni' or $value->year == 'alumni' )
                        {
                        $section_id = Section::getId('App\Section','section_code','Alumni');
                        }
                        elseif($value->year == $faculty_id or $value->year == 'Faculty' or $value->year == 'faculty' )
                        {
                            $section_id = Section::getId('App\Section','section_code','Faculty');
                        }
                        else
                        {
                            $errors[] = $value->section.' is not a section.';
                            continue;
                        }

                    }
                    else
                    {
                         $errors[] = $value->section.' is not a section.';
                            continue;
                    }

                    if(!Year::where('year_num', $value->year)->orwhere('year_code', $value->year)->get()->isEmpty())
                    {

                        if(!Year::where('year_num', $value->year)->get()->isEmpty())
                        {
                            $year_id = Year::getId('App\Year','year_num',$value->year);
                        }
                        elseif(!Year::where('year_code', $value->year)->get()->isEmpty())
                        {
                            $year_id = Year::getId('App\Year','year_code',$value->year);
                        }

                    }
                    else
                    {
                        $errors[] = $value->year.' is not a year.';
                            continue;
                    }


                    $insert[] = ['first_name' => $value->first_name,
                                 'last_name' => $value->last_name,
                                 'username' => $value->username,
                                 'password' => $value->password,
                                 'id_num' => $value->student_id,
                                 'section_id' => $section_id,
                                 'type' => $value->type,
                                 'year_id' => $year_id

                                ];
                }

                //dd($insert);
                if(!empty($insert)){
                    foreach ($insert as $key => $value) {
                        # code...
                        $member = Member::where('username', '=', $value['username'])->first();

                        if ($member) {
                           // user doesn't exist
                            $errors[] = $value['username'].' has already been taken.';
                            continue;
                        }

                        $id_num = Member::where('id_num', '=', $value['id_num'])->first();
                        if($id_num)
                        {
                            $errors[] = $value['id_num'].' has already been taken.';
                            continue;
                        }

                        if(strlen($value['password'])<8)
                        {
                            $errors[] = $value['username'].', password minimum is 8 character';
                            continue;
                        }

                        //IRREGULAR AND REGULAR STUDENT
                        if($value['type']=='' or $value['type']==null)
                        {
                            $type = 'Regular';
                        }
                        else
                        {
                            if($value['type']=='irregular' or $value['type']=='Irregular' or $value['type']=='irreg' or $value['type']=='Irreg')
                            {
                            $type = 'Irregular';
                            }
                            else
                            {
                                $errors[] = $value['id_num'].', incorrect type (Regular/Irregular) only';
                            continue;
                            }
                        }

                        //IF THE YEAR IS NOT VALID WITH THE SELECTED SECTION
                         $section = Section::find($value['section_id']);
                         $course = Course::find($section->course_id);
                         $year =  Year::find($value['year_id']);


                         if($course->course_year_count < $year->year_num)
                         {
                            $errors[] = $year->year_desc. ' is not valid with your selected section';
                            continue;
                         }
                         else
                         {
                         $year_id = $value['year_id'];
                         }


                        $value['username'] = str_replace(' ', '', $value['username']);
                        $member = New Member;
                        $member->username = $value['username'];
                        $member->first_name = $value['first_name'];
                        $member->last_name = $value['last_name'];
                        $member->section_id = $value['section_id'];
                        $member->id_num = $value['id_num'];
                        $member->type = $type;
                        $member->year_id = $value['year_id'];
                        $member->password = bcrypt($value['password']);
                        if($alumni_id == $value['year_id'])
                        {
                         $member->status = 'inactive';

                        }
                        else
                        {
                            $member->status = 'active';

                        }
                        $member->save();
                        if($faculty_id == $value['year_id'])
                        {
                            $this->facultyRoleAction($member->id);
                        }




                    }
                }
            }
        }
        if (isset($errors)){
            return redirect('admin/manage-members')->withErrors($errors);
        }
        return redirect('admin/manage-members');
    }



    private function facultyRoleAction($id)
    {

            $acctreq_approval_id = Action::getId('App\Action','action_name','acctreq_approval');
            $faculty_id = Role::getId('App\Role','name','faculty');
        if(RoleMember::where('member_id', $id)->where('role_id',$faculty_id)->get()->isEmpty())
            {
                RoleMember::create([
                    'role_id' => $faculty_id,
                    'member_id' => $id,
                ]);
            }
        if(ActionMember::where('member_id', $id)->where('action_id',$acctreq_approval_id)->get()->isEmpty())
            {
             ActionMember::create([
            'action_id' => $acctreq_approval_id,
            'member_id' => $id]);
            }
    }

    public function checkUsername(Request $request) {
        $username = Member::where('username', $request->username)->first();

        if ($username) {
            return 1;
        }
        else {
            return 0;
        }
    }

    public function checkStudentId(Request $request) {
        $stud_id = Member::where('id_num', $request->student_id)->first();

        if ($stud_id) {
            return 1;
        }
        else {
            return 0;
        }
    }

    public function checkMobileNo(Request $request) {
        $mobile_no = Member::where('mobile_no', $request->mobile_no)->first();

        if ($mobile_no) {
            return 1;
        }
        else {
            return 0;
        }
    }

    public function checkEmail(Request $request) {
        $email = Member::where('email', $request->email)->first();

        if ($email) {
            return 1;
        }
        else {
            return 0;
        }
    }

}

