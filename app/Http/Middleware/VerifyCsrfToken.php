<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/android-data-sync/', '/android-data-sync/get-event-reply', '/android-data-sync/borrowed-item', '/android-data-sync/sign-in', '/android-data-sync/get-inventory-reply', '/android-data-sync/returned-item'
    ];
}
