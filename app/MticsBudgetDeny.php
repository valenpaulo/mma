<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MticsBudgetDeny extends Model
{
    protected $table = 'mtics_budget_denies';
	protected $fillable = [
      'mtics_budget_id','confirm_admin_id','confirm_admin_role_id','denied_reason'
    ];

    public function admin() {
        return $this->belongsTo(Member::class, 'confirm_admin_id');
    }
}
