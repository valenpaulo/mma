<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Respondent extends Model
{
    public function member(){
        return $this->belongsTo('App\Member', 'member_id');
    }

    public function form(){
        return $this->belongsTo('App\Form', 'form_id');
    }
}
