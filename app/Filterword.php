<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Filterword extends Model
{
    protected $table = 'filterwords';
	protected $fillable = [
        'Filter_word'
    ];
}
