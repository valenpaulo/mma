<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventDepositReceipt extends Model
{
     protected $table = 'event_deposit_receipts';
	protected $fillable = [
      'event_deposit_id','faculty_id','receipt_image'
    ];
}
