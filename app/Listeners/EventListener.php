<?php

namespace App\Listeners;

use App\Events\SystemEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\ActivityLog;

class EventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SystemEvent  $event
     * @return void
     */
    public function handle(SystemEvent $event)
    {
        ActivityLog::create([
            "member_id" => $event->member_id,
            "log" => $event->log
            ]);
    }
}
