<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventPurchaseEquipList extends Model
{
    protected $table = 'event_purchase_equip_lists';
	protected $fillable = [
       'event_purchase_list_id','event_brandname','event_serialnum','event_specs','created_at','updated_at','event_equip_price'

    ];
}
