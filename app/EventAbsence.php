<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventAbsence extends Model
{
     protected $table = 'event_absences';
     protected $fillable = [
       'event_id','member_id','admin_id','role_id'
    ];
}
