<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';
	protected $fillable = [
        'name','display_name','officer_name'
    ];
    public static function getId($model, $table, $value)
   {
    return $model::where($table, $value)->first()->id;
    }

     public function role_member()
    {
        return $this->hasMany('App\RoleMember');
    }

      public function member()
    {
        return $this->belongsToMany(Member::class, 'role_members','role_id','member_id');
    }

        public function prev_member()
    {
        return $this->belongsToMany(Member::class, 'prev_officers','role_id','member_id');
    }


      public function prev_officer()
    {
        return $this->hasMany(PrevOfficer::class, 'role_id');
    }

}
