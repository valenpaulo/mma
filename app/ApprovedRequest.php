<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApprovedRequest extends Model
{
     protected $table = 'approved_requests';
	protected $fillable = [
        'req_member_id','admin_member_id'
    ];
}
