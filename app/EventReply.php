<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventReply extends Model
{
    protected $table = 'event_replies';
     protected $fillable = [
        'member_id','event_id','reply_body'
    ];

     public function member()
    {
        return $this->belongsto(Member::class);
    }

     public function event()
    {
        return $this->belongsto(MticsEvent::class);
    }
}
