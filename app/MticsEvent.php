<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MticsEvent extends Model
{
    protected $table = 'events';

    protected $fillable = [
        'event_title','description','start_date', 'end_date','status','event_amount','amount_confirm','confirm_admin_id','confirm_admin_role_id','post_status','post_admin_id','post_admin_role_id','slug', 'organizer','goal','accomplishment'
        ];

    public function meeting()
    {
        return $this->hasMany('App\Meeting');
    }

     public function breakdown()
    {
        return $this->hasMany(EventAmountBreakdown::class, 'event_id');
    }

     public function task()
    {
        return $this->hasMany(Task::class, 'event_id');
    }

     public function gallery()
    {
        return $this->hasMany(EventGallery::class, 'event_id');
    }

     public function deposit()
    {
        return $this->hasMany(EventDeposit::class,'event_id');
    }

    public function reply()
    {
        return $this->hasMany(EventReply::class, 'event_id');
    }

    public function program()
    {
        return $this->hasOne(EventProgram::class, 'event_id');
    }

     public function organizer()
    {
        return $this->belongsTo(Member::class, 'organizer');
    }
}
