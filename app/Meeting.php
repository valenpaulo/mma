<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meeting extends Model
{
    protected $fillable = [
        'agenda','where','when', 'event_id'
    ];

    public function meeting()
    {
        return $this->belongsTo('app\MticsEvent', 'event_id');
    }

     public function image()
    {
        return $this->hasmany(MeetingImage::class, 'meeting_id');
    }


}
