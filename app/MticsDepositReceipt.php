<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MticsDepositReceipt extends Model
{
    protected $table = 'mtics_deposit_receipts';
	protected $fillable = [
      'mtics_deposit_id','faculty_id','receipt_image'
    ];
}
