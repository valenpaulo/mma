<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrevOfficer extends Model
{
    protected $table = 'prev_officers';
	protected $fillable = [
        'officerterm_id','member_id','role_id'
    ];

    
     public function officerterm()
    {
        return $this->belongsTo(Officerterm::class,'officerterm_id');
    }

	 public function member()
    {
        return $this->belongsTo(Member::class,'member_id');
    }
    public function role()
    {
        return $this->belongsTo(Role::class,'role_id');
    }



}
