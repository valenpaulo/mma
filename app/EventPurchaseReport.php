<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventPurchaseReport extends Model
{
    protected $table = 'event_purchase_reports';
	protected $fillable = [
        'admin_id','event_purchase_list_id','report_name','created_at','updated_at','report_reason','external_explanation','report_category','solution_fr_critical','reportedby'
    ];

     public function purchaselist()
    {
        return $this->belongsTo(EventPurchaseList::class, 'event_purchase_list_id');
    }
}
