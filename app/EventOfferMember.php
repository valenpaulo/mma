<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventOfferMember extends Model
{
     protected $table = 'event_offer_members';
	protected $fillable = [
     'event_offer_id','member_id','admin_id','role_id','item_status'
    ];

      public function member()
    {
        return $this->belongsTo(Member::class, 'member_id');
    }

      public function item()
    {
        return $this->belongsTo(EventOffer::class, 'event_offer_id');
    }
}
