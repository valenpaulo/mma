<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mticsfund extends Model
{
    protected $table = 'mticsfunds';
	protected $fillable = [
        'year_id','course_id','mticsfund_amt'
    ];

     public static function getId($model, $table, $value)
    {
        return $model::where($table, $value)->first()->id;
    }

      public function year()
    {
        return $this->belongsTo(Year::class);
    }

      public function course()
    {
        return $this->belongsTo(Course::class);
    }
}
