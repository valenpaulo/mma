<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MticsfundPayment extends Model
{
     protected $table = 'mticsfund_payments';
	protected $fillable = [
        'payment_id','paymticsfund_amt','paymticsfund_status','mticsfund_id', 'member_id', 'admin_id'
    ];

      public static function getId($model, $table, $value)
    {
        return $model::where($table, $value)->first()->id;
    }

    public function member() {
        return $this->belongsTo('App\Member', 'member_id');
    }


    public function admin() {
        return $this->belongsTo('App\Member', 'admin_id');
    }

      public function mticsfund() {
        return $this->belongsTo('App\Mticsfund', 'mticsfund_id');
    }


}
