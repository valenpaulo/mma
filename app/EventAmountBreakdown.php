<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventAmountBreakdown extends Model
{
  	 protected $table = 'event_amount_breakdowns';
     protected $fillable = [
        'event_id','breakdown_name','breakdown_desc', 'breakdown_amt'
    ];

    public function event() {
        return $this->belongsTo(MticsEvent::class, 'event_id');
    }

    public function expenses()
    {
        return $this->hasMany(EventExpense::class);
    }
}
