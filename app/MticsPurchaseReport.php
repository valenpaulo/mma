<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MticsPurchaseReport extends Model
{
     protected $table = 'mtics_purchase_reports';
	protected $fillable = [
        'admin_id','mtics_purchase_list_id','report_name','created_at','updated_at','report_reason','external_explanation','report_category','solution_fr_critical','reportedby'
    ];

     public function purchaselist()
    {
        return $this->belongsTo(MticsPurchaseList::class, 'mtics_purchase_list_id');
    }
}
