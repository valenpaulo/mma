<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    //
    protected $fillable = [
        'question', 'option_format',
    ];

    public function form(){
        return $this->belongsTo('App\Form', 'form_id');
    }

    public function option(){
        return $this->hasMany('App\Option');
    }
}
