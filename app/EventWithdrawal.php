<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventWithdrawal extends Model
{

	protected $table = 'event_withdrawals';
	protected $fillable = [
    'admin_id','faculty_id','event_id','external_task_id','event_breakdown_id','withdrawal_reason','withdrawal_desc','withdrawal_amt','withdrawal_status'
    ];

    public function faculty() {
        return $this->belongsTo(Member::class, 'faculty_id');
    }

    public function event() {
        return $this->belongsTo(MticsEvent::class, 'event_id');
    }

    public function receipt() {
        return $this->hasOne(EventWithdrawalReceipt::class);
    }


     public function deny() {
        return $this->hasOne(EventWithdrawalDeny::class);
    }
     public function report() {
        return $this->hasOne(EventWithdrawalReport::class);
    }
}
