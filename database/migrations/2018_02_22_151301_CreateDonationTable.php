<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDonationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('donate_name');
            $table->string('donate_company');
            $table->string('donate_desc');
            $table->string('donate_title')->nullable();
            $table->longtext('donate_message')->nullable();
            $table->integer('admin_id');
            $table->timestamps();
        });    

         Schema::create('cash_donations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('donation_id');
            $table->integer('finance_id');
            $table->string('cash_amt');
            $table->longtext('remarks')->nullable();
            $table->timestamps();
        }); 

        Schema::create('cash_donation_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('finance_id');
            $table->longtext('request_reason');
            $table->string('request_cash_amt');
            $table->enum('request_status',['pending','accepted','denied'])->default('pending');
            $table->integer('faculty_id')->nullable();
            $table->longtext('denied_reason')->nullable();
            $table->timestamps();
        });   

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
