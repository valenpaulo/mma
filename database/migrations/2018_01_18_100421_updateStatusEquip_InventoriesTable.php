<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateStatusEquipInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

     public function __construct()
    {
    DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
    }
    public function up()
    {
          Schema::table('equip_inventories', function ($table) {
        $table->enum('status',['working','repair','archive','borrowed'])->nullable()->default('working');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
