<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropPayBalanceINpaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function __construct()
    {
    DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
    }
    public function up()
    {
        Schema::table('mticsfund_payments', function ($table) {
           $table->dropColumn('paymticsfund_balance');
        });

        Schema::table('event_payments', function ($table) {
           $table->dropColumn('paymevent_balance');
        });

        Schema::table('penalty_payments', function ($table) {
           $table->dropColumn('paypenalty_balance');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
