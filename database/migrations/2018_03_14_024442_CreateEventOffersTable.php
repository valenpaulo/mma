<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('event_offers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('activities_id');            
            $table->integer('event_id');
            $table->string('offer_name'); 
            $table->longtext('offer_desc')->nullable();
            $table->string('offer_image');
            $table->integer('offer_amt')->nullable();
            $table->integer('finance_id')->nullable();
            $table->enum('offer_status',['pending','on-going','done','cancel'])->default('pending');                 
            $table->timestamps();
        });

        Schema::create('event_offer_members', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('event_offer_id');
            $table->integer('member_id');
            $table->integer('admin_id')->nullable();
            $table->integer('role_id')->nullable();
            $table->enum('item_status',['pending','claimed'])->default('pending');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
