<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventPostPartial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function __construct()
    {
    DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
    }
    public function up()
    {
         Schema::create('event_partial_posts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('event_id');
            $table->integer('admin_id');
            $table->integer('admin_role_id');
            $table->string('title');
            $table->longtext('body');
            $table->string('slug');
            $table->enum('status',['published','archived'])->default('published');
            $table->timestamps();
        });

         Schema::create('event_partial_post_replies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('member_id');
            $table->integer('event_partial_post_id');
            $table->longtext('reply_body');
            $table->timestamps();
        });
        
        Schema::table('events', function ($table) {
            $table->string('slug')->nullable()->after('status');
        });

        Schema::create('event_replies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('member_id');
            $table->integer('event_id');
            $table->longtext('reply_body');
            $table->timestamps();
        });


         Schema::table('events', function ($table) {
            $table->longtext('description')->nullable()->change();
        });

        
        Schema::create('event_programs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('admin_id');
            $table->integer('admin_role_id');
            $table->integer('event_id');
            $table->string('title');
            $table->longtext('body');
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
