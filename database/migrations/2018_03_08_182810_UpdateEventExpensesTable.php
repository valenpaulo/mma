<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateEventExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function __construct()
    {
    DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
    }
    public function up()
    {
         Schema::create('event_expenses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('purchaser_admin_id');
            $table->integer('purchaser_admin_role_id');
            $table->integer('confirm_admin_id')->nullable();
            $table->integer('confirm_admin_role')->nullable();
            $table->integer('event_id');
            $table->string('expense_name');
            $table->longtext('expense_desc')->nullable();
            $table->integer('expense_amt');
            $table->timestamps();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('event_expenses');
    }
}
