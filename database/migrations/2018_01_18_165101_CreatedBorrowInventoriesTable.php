<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatedBorrowInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('borrower_inventories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('inventory_id');
            $table->integer('borrower_id');
            $table->integer('borrower_quantity');
            $table->longtext('borrower_desc')->nullable();                        
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
