<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->increments('id');

            $table->string('username')->unique();
            $table->string('password');

            $table->string('first_name');
            $table->string('last_name');
            $table->string('section_id');
            $table->enum('status',['active','inactive']);

            // personal info
            $table->integer('age');
            $table->date('birthday');
            $table->string('address');
            $table->string('work')->nullable();

            // contact info
            $table->string('mobile_no');
            $table->string('email')->unique();

            // in case of emergency
            $table->string('guardian_name')->nullable();
            $table->string('guardian_mobile_no')->nullable();
            $table->string('avatar')->nullable()->default('members/default.png');

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('members');
    }
}
