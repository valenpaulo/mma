<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventWithdrawTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('event_withdrawals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('admin_id');
            $table->integer('faculty_id')->nullable();
            $table->integer('event_id');
            $table->integer('external_task_id')->nullable();
            $table->integer('event_breakdown_id')->nullable();
            $table->longtext('withdrawal_reason');
            $table->longtext('withdrawal_desc')->nullable();
            $table->integer('withdrawal_amt');
            $table->enum('withdrawal_status',['pending','approved','denied','cleared'])->default('pending');
            $table->timestamps();
        });


        Schema::create('event_withdrawal_receipts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('event_withdrawal_id');
            $table->integer('faculty_id'); 
            $table->string('receipt_image');            
            $table->timestamps();
        });

        Schema::create('event_withdrawal_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('event_withdrawal_id');
            $table->integer('faculty_id'); 
            $table->string('withdrawal_title');          
            $table->longtext('withdrawal_desc');         
            $table->timestamps();
        });

       Schema::create('event_withdrawal_denies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('event_withdrawal_id');
            $table->integer('faculty_id');
            $table->longtext('denied_reason');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
