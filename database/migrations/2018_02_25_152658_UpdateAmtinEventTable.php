<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAmtinEventTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function __construct()
    {
    DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
    }
    public function up()
    {

        Schema::table('mticsfund_payments', function ($table) {
             $table->integer('member_id')->after('id');
             $table->integer('admin_id');
             $table->longtext('paymticsfund_remarks')->after('paymticsfund_status')->nullable();

        });

          Schema::table('event_payments', function ($table) {
             $table->integer('member_id')->after('id');
             $table->integer('admin_id');
             $table->longtext('paymevent_remark')->after('paymevent_status')->nullable();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
