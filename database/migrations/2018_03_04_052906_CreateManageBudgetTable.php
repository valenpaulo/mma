<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManageBudgetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mtics_budgets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('finance_id');
            $table->integer('confirm_admin_id')->nullable();
            $table->integer('confirm_admin_role_id')->nullable();
            $table->string('budget_month');
            $table->integer('budget_amt');
            $table->longtext('budget_remarks')->nullable();
            $table->integer('budget_excess_money')->nullable();
            $table->enum('budget_status',['pending','approved','denied','on-going','cleared','for validation'])->default('pending');
            $table->timestamps();
        });


        Schema::create('mtics_budget_breakdowns', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mtics_budget_id');
            $table->string('budget_bd_name');
            $table->longtext('budget_bd_desc')->nullable();
            $table->integer('budget_bd_amt');
            $table->timestamps();
        });

        Schema::create('mtics_budget_denies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mtics_budget_id');
            $table->integer('confirm_admin_id')->nullable();
            $table->integer('confirm_admin_role_id')->nullable();
            $table->longtext('denied_reason')->nullable();
            $table->timestamps();
        });

        Schema::create('mtics_budget_expenses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('purchaser_admin_id');
            $table->integer('purchaser_admin_role_id');
            $table->integer('confirm_admin_id')->nullable();
            $table->integer('confirm_admin_role')->nullable();
            $table->integer('mtics_budget_breakdown_id');
            $table->string('expense_name');
            $table->string('expense_desc');
            $table->integer('expense_amt');
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('mtics_budgets');
         Schema::drop('mtics_budget_breakdowns');
         Schema::drop('mtics_budget_denies');
         Schema::drop('mtics_budget_expenses');

         
    }
}
