<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManageSectionTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('sections');
        Schema::create('sections', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('course_id');
            $table->string('section_code')->unique();
            $table->timestamps();                        
        });
         Schema::create('years', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('year_num');
            $table->string('year_code')->unique();
            $table->integer('mticsfund_amt')->nullable();
            $table->timestamps();                        
        });
         Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('course_code')->unique();
            $table->string('course_name');
            $table->timestamps();                        
        });

          Schema::table('members', function ($table) {
            $table->integer('year_id');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
