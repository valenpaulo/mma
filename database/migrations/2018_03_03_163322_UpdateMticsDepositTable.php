<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMticsDepositTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function __construct()
    {
    DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
    }
    public function up()
    {
     Schema::table('mtics_deposits', function ($table) {
            $table->enum('deposit_source',['MTICS Payment','MTICS Donation','Event','other'])->after('deposit_amt')->default('other');
            $table->integer('event_id')->after('deposit_status')->nullable();
            $table->longtext('remarks')->nullable();
        });   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
