<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNeedtoPayTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mticsfund_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('payment_id');
            $table->integer('paymticsfund_amt');
            $table->integer('paymticsfund_balance')->nullable();
            $table->enum('paymticsfund_status',['paid','balance'])->nullable()->default('paid');
            $table->timestamps();                        
        });

        Schema::create('event_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('payment_id');
            $table->integer('event_id');
            $table->integer('payevent_amt');
            $table->integer('paymevent_balance')->nullable();
            $table->enum('paymevent_status',['paid','balance'])->nullable()->default('paid');
            $table->timestamps();                        
        });


        Schema::create('penalty_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('payment_id');
            $table->integer('penalty_id');
            $table->integer('paypenalty_amt');
            $table->integer('paypenalty_balance')->nullable();
            $table->enum('paypenalty_status',['paid','balance'])->nullable()->default('paid');
            $table->timestamps();                        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
