<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMticsPurchasedListLogistics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function __construct()
    {
    DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
    }
    public function up()
    {
       Schema::table('mtics_purchase_lists', function ($table) {
             $table->integer('mtics_inv_quan')->after('logistics_id')->nullable(
             );
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mtics_purchase_lists', function ($table) {
             $table->dropColumn('mtics_inv_quan');
        });
    }
}
