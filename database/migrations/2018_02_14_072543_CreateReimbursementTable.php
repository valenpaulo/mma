<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReimbursementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('reimburse_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('money_request_id');
            $table->integer('reimburse_amount');
            $table->string('reimburse_remarks')->nullable();        
            $table->enum('reimburse_status',['pending','accepted','denied'])->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
