<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateEventBreakdownStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function __construct()
    {
    DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
    }
    public function up()
    {
        Schema::table('events', function ($table) {
            $table->dropColumn('amount_confirm');
            $table->dropColumn('faculty_id');
        });   

        Schema::table('events', function ($table) {
            $table->enum('amount_confirm',['pending','approved','denied'])->nullable();
            $table->integer('confirm_admin_id')->nullable();
            $table->integer('confirm_admin_role_id')->nullable();
        });   

         Schema::create('event_amount_breakdown_denies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('event_id');
            $table->integer('confirm_admin_id');
            $table->integer('confirm_admin_role_id');
            $table->longtext('denied_reason'); 
            $table->timestamps();
        });

     

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
