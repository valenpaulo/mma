<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceiptTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('receipts', function (Blueprint $table) {
            $table->increments('id');
            $table->longtext('receipt_image');
            $table->integer('admin_id');
            $table->string('receipt_remarks')->nullable();
            $table->timestamps();
        });

       Schema::create('receipt_mticsitems', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('receipt_id');
            $table->integer('mtics_purchase_list_id'); 
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
