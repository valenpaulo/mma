<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function __construct()
    {
    DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
    }
    public function up()
    {
        Schema::table('money_requests', function ($table) {
             $table->dropColumn('mon_req_status');
         
        });

        Schema::table('money_requests', function ($table) {
            $table->enum('mon_req_status',['pending','ongoing','cleared','denied'])->default('pending');
            $table->integer('member_requested_id')->after('id');
            $table->integer('admin_affirm_id')->after('task_id')->nullable();
            $table->integer('admin_receive_id')->after('excess_amount')->nullable();
            $table->integer('admin_confirm_id')->after('mon_req_status')->nullable();  
            $table->longtext('denied_reason')->after('admin_confirm_id')->nullable();  
        });

    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
