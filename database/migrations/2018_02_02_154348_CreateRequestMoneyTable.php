<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestMoneyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
         /*Schema::drop('money_requests');
         Schema::drop('money_request_tasks');*/
         Schema::create('money_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('amount');
            $table->enum('mon_req_status',['ongoing','cleared'])->default('ongoing');
            $table->string('remarks');        
            $table->timestamps();
        });

         Schema::create('money_request_tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('money_request_id');
            $table->integer('task_id');        
            $table->timestamps();
        });         

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
