<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventBankTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('event_deposits', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('admin_id');
            $table->integer('faculty_id')->nullable();
            $table->integer('event_id');
            $table->longtext('deposit_desc')->nullable();
            $table->integer('deposit_amt');
            $table->enum('deposit_source',['Event Payment','MTICS Fund','other'])->default('other');
            $table->enum('deposit_status',['pending','approved','denied','cleared'])->default('pending');
            $table->longtext('remarks')->nullable();
            $table->timestamps();
        });

          Schema::create('event_deposit_receipts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('event_deposit_id');
            $table->integer('faculty_id'); 
            $table->string('receipt_image');            
            $table->timestamps();
        });


           Schema::create('event_deposit_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('event_deposit_id');
            $table->integer('faculty_id'); 
            $table->string('deposit_title');          
            $table->longtext('deposit_desc');         
            $table->timestamps();
        });

           Schema::create('event_deposit_denies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('event_deposit_id');
            $table->integer('faculty_id');
            $table->longtext('denied_reason');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
