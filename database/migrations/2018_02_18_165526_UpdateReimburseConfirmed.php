<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateReimburseConfirmed extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function __construct()
    {
    DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
    }
    public function up()
    {
        Schema::table('reimburse_requests', function ($table) {
           
        });

      Schema::create('reimburse_confirms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reimburse_request_id');
            $table->integer('admin_id');
            $table->string('position');
            $table->enum('confirm_status',['accepted','denied'])->default('accepted');
            $table->longtext('denied_reason')->nullable();
            $table->timestamps();
        });         

        Schema::table('tasks', function ($table) {
            $table->dropColumn('task_status');
        });

        Schema::table('tasks', function ($table) {
            $table->enum('task_status',['pending','cancel','done','for validation','failed'])->default('pending');            
          
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
