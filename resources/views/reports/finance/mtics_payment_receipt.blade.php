<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/css/w3.css') : asset('css/w3.css') }}">
    <link rel="stylesheet" href="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/css/bootstrap.min.css') : asset('css/bootstrap.min.css') }}">
    <link href="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/css/fullcalendar.css') : asset('css/fullcalendar.css')}}" rel='stylesheet' />
    <script src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/js/jquery-3.2.1.min.js') : asset('js/jquery-3.2.1.min.js')}}"></script>

    <link rel="stylesheet" href="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/css/font-awesome.min.css') : asset('css/font-awesome.min.css') }}">
    <link rel="shortcut icon" href="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/logo/mtics.ico') : asset('images/logo/mtics.ico')}}" />


    <title>Payment Receipt</title>
</head>
<body>

    <div class="container">
        <div class="row">

            <div class="panel panel-info">
              <div class="panel-heading">
                  <h3>
                      MTICS Payment Receipt
                  </h3>
              </div>
              <div class="panel-body">

                <div class="w3-container">
                    <div class="row" align="right">
                        <label>Date: {{$date}}</label>
                    </div>
                </div>

                <div class="w3-display-container">
                    <div align="center" class="w3-opacity-max">
                        <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/logo/mtics.png') : asset('images/logo/mtics.png')}}" style="width: 20%; opacity: 0.4">
                    </div>

                    <div class="w3-display-topleft">
                        <label>Received from:</label><span class="w3-large"> <strong><u> {{$payment->member->first_name}} {{$payment->member->last_name}} ({{$payment->member->section->section_code}})</u></strong></span>
                        <br>
                        <label>For:</label><span class="w3-large"> <strong><u> MTICS Fund</u></strong></span>
                    </div>

                    <div class="w3-display-middle" align="center">
                        <h1><strong>Php {{$payment->paymticsfund_amt}}.00</strong></h1>
                        <label>{{$amt_in_words}} pesos</label>
                    </div>
                </div>

                <hr>

                <div class="w3-container">
                    <div class="row" align="center">
                        <label>Received by:</label><span> <strong> {{$payment->admin->first_name}} {{$payment->admin->last_name}}</strong></span>
                    </div>
                </div>

              </div>
            </div>

        </div>

        <div class="row">

            <div class="panel panel-warning">
              <div class="panel-heading">
                  <h3>
                      MTICS Payment Receipt
                  </h3>
              </div>
              <div class="panel-body">

                <div class="w3-container">
                    <div class="row" align="right">
                        <label>Date: {{$date}}</label>
                    </div>
                </div>

                <div class="w3-display-container">
                    <div align="center" class="w3-opacity-max">
                        <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/logo/mtics.png') : asset('images/logo/mtics.png')}}" style="width: 20%; opacity: 0.4">
                    </div>

                    <div class="w3-display-topleft">
                        <label>Received from:</label><span class="w3-large"> <strong><u> {{$payment->member->first_name}} {{$payment->member->last_name}} ({{$payment->member->section->section_code}})</u></strong></span>
                        <br>
                        <label>For:</label><span class="w3-large"> <strong><u> MTICS Fund</u></strong></span>
                    </div>

                    <div class="w3-display-middle" align="center">
                        <h1><strong>Php {{$payment->paymticsfund_amt}}.00</strong></h1>
                        <label>{{$amt_in_words}} pesos</label>
                    </div>
                </div>

                <hr>

                <div class="w3-container">
                    <div class="row" align="center">
                        <label>Received by:</label><span> <strong> {{$payment->admin->first_name}} {{$payment->admin->last_name}}</strong></span>
                    </div>
                </div>

              </div>
            </div>

        </div>

        <div class="row">

            <div class="panel panel-danger">
              <div class="panel-heading">
                  <h3>
                      MTICS Payment Receipt
                  </h3>
              </div>
              <div class="panel-body">

                <div class="w3-container">
                    <div class="row" align="right">
                        <label>Date: {{$date}}</label>
                    </div>
                </div>

                <div class="w3-display-container">
                    <div align="center" class="w3-opacity-max">
                        <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/logo/mtics.png') : asset('images/logo/mtics.png')}}" style="width: 20%; opacity: 0.4">
                    </div>

                    <div class="w3-display-topleft">
                        <label>Received from:</label><span class="w3-large"> <strong><u> {{$payment->member->first_name}} {{$payment->member->last_name}} ({{$payment->member->section->section_code}})</u></strong></span>
                        <br>
                        <label>For:</label><span class="w3-large"> <strong><u> MTICS Fund</u></strong></span>
                    </div>

                    <div class="w3-display-middle" align="center">
                        <h1><strong>Php {{$payment->paymticsfund_amt}}.00</strong></h1>
                        <label>{{$amt_in_words}} pesos</label>
                    </div>
                </div>

                <hr>

                <div class="w3-container">
                    <div class="row" align="center">
                        <label>Received by:</label><span> <strong> {{$payment->admin->first_name}} {{$payment->admin->last_name}}</strong></span>
                    </div>
                </div>

              </div>
            </div>

        </div>
    </div>

<script>
  document.addEventListener("DOMContentLoaded", function(event) {
    window.print()
  });
</script>

</body>
</html>

