@extends('layout/control_panel')

@section('title')
Financial Summary
@endsection

@section('middle')
<div class="w3-container w3-margin">
	 <div class="row">
    <h3>
      <i class="fa fa-file-text-o fa-fw w3-xxlarge"></i>
      <strong>Financial Statement Verification</strong>
    </h3>
  </div>

  <hr>

	<div class="row w3-margin-top">

		@if($auditapprove !== null)
		<a href="#submit" class="w3-text-black" data-toggle="modal"><i class="fa fa-print fa-fw"></i>Print</a>
		@elseif($auditapprove == null)
		<a href="#verify" class="w3-text-black" data-toggle="modal" title="Auditor needs to verify this Financial Statement"><i class="fa fa-check-circle fa-fw"></i> Verify</a>
		@endif

		<i class="fa fa-ellipsis-v fa-fw"></i>

		<a href="javascript:void(0)" class="w3-text-black" onclick="showSummary()"><i class="fa fa-file-text fa-fw"></i> Summary</a>
	</div>

	<!-- VERIFY -->
	<div class="modal fade" id="verify" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-md" role="document">
	<div class="modal-content modal-content-info">
	<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
	<form action="{{ url('admin/finance/financial-reports/mtics/view-month/verify') }}" method="POST" enctype="multipart/form-data">

	  <h4 class="modal-title w3-text-gray w3-center" id="myModalLabel">Verify</h4>
	  </div>
	  <div class="modal-body modal-body-info">
	  <div class="w3-container">
	    <div class="row">
	    	<p>Are you sure you want to proceed?</p>

	         <div class="form-group">
	            <label>Remarks:</label>
	            <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="remarks" placeholder="Remarks" rows="3"></textarea>
	          </div>

	        <input type="hidden" name="month" value="{{$requestmonth}}">
	    </div>
	  </div>
	  </div>

	  <div class="modal-footer">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<button type="submit" class="btn btn-default w3-green w3-text-white" title="Edit"> Yes</button>
		<button type="button" data-dismiss="modal" class="btn btn-default w3-red w3-text-white" title="Edit"> No</button>
	  </div>

	</form>
	</div>
	</div>
	</div>
	<!-- END VERIFY -->

	<!-- PRINT-->
	<div class="modal fade" id="submit" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-sm" role="document">
	<div class="modal-content modal-content-info">
	<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
	<form action="{{ url('admin/finance/financial-reports/mtics/view-month/print') }}" method="POST" enctype="multipart/form-data">

	  <h4 class="modal-title w3-text-gray w3-center" id="myModalLabel">Print</h4>
	  </div>
	  <div class="modal-body modal-body-info">
	  <div class="w3-container">
	    <div class="row">
	        <p>Do you want to print the summary?</p>
	        <input type="hidden" name="month" value="{{$requestmonth}}">
	    </div>
	  </div>
	  </div>

	  <div class="modal-footer">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<button type="submit" class="btn btn-default w3-green w3-text-white" title="Edit">Yes</button>
		<button type="button" data-dismiss="modal" class="btn btn-default w3-red w3-text-white" title="Edit"> No</button>
	  </div>
	</form>
	</div>
	</div>
	</div>
	<!-- END PRINT -->

  <div class="row w3-margin-top">

    <div class="col-lg-3 col-md-3 col-sm-12"></div>

    <div class="col-lg-3 col-md-3 col-sm-12">
      <div class="w3-card-4 w3-amber w3-text-black w3-padding w3-container">

        <div class="w3-container">
          <div class="row" align="center">
            <h4>
              <i class="fa fa-money w3-xxxlarge fa-fw"></i>
              <strong>MTICS Payment for this Month</strong>
            </h4>
          </div>
          <hr style="border-color: black">
          <div class="row">
            <h4>
              <span>
                <i class="fa fa-ellipsis-v fa-fw"></i>
                <strong>P{{$mticspayments->sum('paymticsfund_amt')}}.00</strong>
              </span>
            </h4>
          </div>
        </div>

      </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-12">
      <div class="w3-card-4 w3-teal w3-text-black w3-padding">

        <div class="w3-container">
          <div class="row" align="center">
            <h4>
              <i class="fa fa-calculator w3-xxxlarge fa-fw"></i>
              <strong>Number of Student Paid:</strong>
            </h4>
          </div>

          <hr style="border-color: black">

          <div class="row">
            <h4>
              <span>
                <i class="fa fa-ellipsis-v fa-fw"></i>
                <strong>{{count($mticspayments)}} Student</strong>
              </span>
            </h4>
          </div>
        </div>

      </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-12"></div>

  </div>

  <hr>

   <div id="summaryReport" style="display: none">
         <div class="row w3-margin-top" align="center">
            <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/logo/mticsBanner.png') : asset('images/logo/mticsBanner.png')}}"  style="width: 85%">
            <h2>Financial Report</h2>
            <p>MTICS Fund (S.Y. {{$officerterm->from_year}} - {{$officerterm->toyear}})</p>
            <p>{{$budgetmonth}}</p>

        </div>

        <div class="row w3-margin-top">
            <h3>I. Bank Transaction</h3>
            <h4>Current MTICS Bank Fund Amount: Php {{$mticsbankamount}}.00</h4>
        </div>

        <hr>

        <div style="padding-left: 5em;">
        <div class="row w3-margin-top">
            <h4>1. Deposit Transaction:</h4>
        </div>

        <div class="row">
            <div class="table-responsive">
              <table class="w3-table">
                <thead>
                  <tr>
                   <th>Source</th>
                   <th class="w3-right">Amount</th>
                  </tr>
                </thead>
                <tbody class="w3-text-gray">
                @foreach($deposits as $deposit)
                 <tr>
                   <td>{{$deposit->deposit_source}}</td>
                   <td class="w3-right">{{$deposit->deposit_amt}}</td>
                  </tr>
                @endforeach
                  <tr>
                    <td>Total Cash Deposit for the Month of {{$budgetmonth}}</td>
                    <td class="w3-right">Php {{$totaldeposit}}.00</td>
                  </tr>
               </tbody>
              </table>
            </div>
        </div>

        <div class="row w3-margin-top">
            <h4>2. Withdraw Transaction:</h4>
        </div>

        <div class="row">
            <div class="table-responsive">
              <table class="w3-table">
                <thead>
                  <tr>
                   <th>Reason</th>
                   <th class="w3-right">Amount</th>
                  </tr>
                </thead>
                <tbody class="w3-text-gray">
                @foreach($withdrawals as $withdrawal)
                 <tr>
                   <td>{{$withdrawal->withdrawal_reason}}</td>
                   <td class="w3-right">{{$withdrawal->withdrawal_amt}}</td>
                  </tr>
                @endforeach
                  <tr>
                    <td>Total Cash Withdraw for the Month of {{$budgetmonth}}</td>
                    <td class="w3-right">Php {{$totalwithdraw}}.00</td>
                  </tr>
               </tbody>
              </table>
            </div>
        </div>
        </div>
        <hr>

        <?php $expensestotal=0; ?>
        <?php $expensestotalall=0; ?>

        <div class="row">
            <h3>II. Expenses</h3>
        </div>

        <hr>

        @foreach($monthly_budgets as $key => $monthly_budget )
        <div class="row w3-margin-top" style="padding-left: 5em">
            <h4>{{++$key}}. Monthly Budget: {{$monthly_budget->budget_name}} (Php {{$monthly_budget->budget_amt}}.00)</h4>
        </div>



        <div class="row" style="padding-left: 5em">
            <div class="table-responsive">
              <table class="w3-table">
                <tbody class="w3-text-gray">
                @foreach($monthly_budget->budget_breakdown as $budget_breakdown)
                @if(!$budget_breakdown->budget_expenses->isEmpty())
                @foreach($budget_breakdown->budget_expenses as $budget_expenses)
                 <tr>
                    <td>- {{$budget_expenses->expense_name}}</td>
                   <td class="w3-right">Php {{$budget_expenses->expense_amt}}.00.00</td>
                 </tr>
                 <?php $expensestotal = $expensestotal + $budget_expenses->expense_amt; ?>
                @endforeach
                @endif
                @endforeach
               </tbody>
              </table>
            </div>
        </div>

        <?php $expensestotalall = $expensestotalall + $expensestotal; $expensestotal = 0; ?>

        <hr>
        @endforeach

        <div class="row w3-margin-top">
            <h4>Purchase Request Expenses</h4>
        </div>

        <div class="row">
        <div class="table-responsive">
          <table class="w3-table">
            <tbody class="w3-text-gray">
            @foreach($externaltasks as $externaltask)
            @foreach($externaltask->mticspurchaselist as $mticspurchaselist)
              <tr>
                <td>- {{$mticspurchaselist->mtics_itemname}} ({{$mticspurchaselist->mtics_act_quan}} pc/s)</td>
                <td class="w3-right">Php {{$mticspurchaselist->mtics_total_price}}.00</td>
              </tr>
             <?php $expensestotal = $expensestotal + $mticspurchaselist->mtics_total_price; ?>
            @endforeach
            <?php $expensestotalall = $expensestotalall + $expensestotal; $expensestotal = 0; ?>
            @endforeach
           </tbody>
          </table>
        </div>
        </div>

        <br><br>
        <div align="right"><b>Total Expenses = P{{$expensestotalall}}.00</b><br><br><br>
        </div>
        @if(!$penalties->isEmpty())
        <div align="right"><b>Penalty Fee = P{{$penalties->sum('fee')}}.00</b><br></div>
        @endif
        <div align="right"><b>Total Withdraw = P{{$totalwithdraw}}.00</b><br></div>
        <div align="right"><b>Previous Month Excess = P{{$excess !== null ? $excess : 0}}.00</b><br></div>
        <div align="right"><b>Total Expenses = -P{{$expensestotalall}}.00</b><br></div>
        <div align="right"><b>-----------------------------</b><br></div>
        <div align="right"><b>Total Cash in hand = P{{$totalwithdraw + $excess - $expensestotalall + $penalties->sum('fee')}}.00 </b><br></div>


    <br>

         <p><strong>Prepared by:</strong></p>
          @foreach($roles as $role)
            @if($officerterm->status !== 'on-going')
              @if(!$role->prev_member->isEmpty())
                @foreach($role->prev_member as $member)
                    @if($role->name == 'president')
                      <br>
                      <p><strong>Approved by:</strong></p>
                    @endif
              <p>{{$member->first_name}} {{$member->last_name}}</p>
              <p><i>{{$role->display_name}}</i></p>
                @endforeach
              @endif
            @else
              @if(!$role->member->isEmpty())
                @foreach($role->member as $member)
                @if($role->name == 'president')
                  <br>
                  <p><strong>Approved by:</strong></p>
                @endif
                @if($member !== 'null')
              <p>{{$member->first_name}} {{$member->last_name}}</p>
              <p><i>{{$role->display_name}}</i></p>
                @endif
                @endforeach
              @endif
            @endif

          @endforeach
          <p></p>




    </div>

    <div id="financialStatement">

    <div class="row w3-margin-top">
        <h4>Deposit Transaction</h4>
    </div>

    <div class="row">
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
               <th class="w3-center">Source</th>
               <th class="w3-center">Amount</th>
               <th class="w3-center">Description</th>
               <th class="w3-center">receipt</th>
               <th class="w3-center">status</th>

              </tr>
            </thead>
            <tbody class="w3-text-gray">
            @foreach($alldeposits as $alldeposit)
             <tr>
               <td><a href="" class="w3-text-gray" data-toggle="modal" data-target="#infodeposit-{{$alldeposit->id}}"><i class="fa fa-exclamation-circle fa-fw"></i> {{$alldeposit->deposit_source}}</a></td>
               <td class="w3-center">{{$alldeposit->deposit_amt}}</td>
               <td class="w3-center">{{$alldeposit->deposit_desc}}</td>
               @if($alldeposit->receipt !== null)
               <td class="w3-center">
                <a href="" class="w3-text-gray" data-toggle="modal" data-target="#receiptdeposit-{{$alldeposit->id}}">
               <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/' . $alldeposit->receipt->receipt_image) : asset('images/' . $alldeposit->receipt->receipt_image)}}" style="width: 100px"></a>

               </td>
               @else
               <td class="w3-center"></td>
               @endif
               <td class="w3-center">{{$alldeposit->deposit_status}}</td>
              </tr>

              <!-- INFO RECEIPT VIEW -->
              <div class="modal fade" id="receiptdeposit-{{$alldeposit->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog modal-dialog-info modal-m" role="document">
              <div class="modal-content modal-content-info">
              <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
              <h4 class="modal-title w3-text-gray w3-center" id="myModalLabel">Deposit Receipt</h4>
              </div>
              <div class="modal-body modal-body-info">
              <div class="w3-container">
                <div class="row">
               @if($alldeposit->receipt !== null)
                 <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/' . $alldeposit->receipt->receipt_image) : asset('images/' . $alldeposit->receipt->receipt_image)}}" style="width: 500px"></a>
                @endif
                </div>
              </div>
              </div>


              </div>
              </div>
              </div>
              <!-- END INFO -->


              <!-- INFO DEPOSIT-->
              <div class="modal fade" id="infodeposit-{{$alldeposit->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog modal-dialog-info modal-m" role="document">
              <div class="modal-content modal-content-info">
              <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
              <h4 class="modal-title w3-text-gray w3-center" id="myModalLabel">'{{$alldeposit->deposit_source}}' Deposit Source Information</h4>
              </div>
              <div class="modal-body modal-body-info">
              <div class="w3-container">
                <div class="row">
                    @if($alldeposit->deny !== null)
                        <b>Denied</b><br>
                        Faculty Name: {{$alldeposit->deny->faculty->first_name}} {{$alldeposit->deny->faculty->last_name}}<br>
                        Denied Reason: {{$alldeposit->deny->denied_reason}}<br>

                        <div class="form-group">
                          <label>Denied by</label>
                          <input type="text" disabled class="form-control" value="{{$alldeposit->deny->faculty->first_name}} {{$alldeposit->deny->faculty->last_name}}">
                        </div>
                         <div class="form-group">
                          <label>Denied Reason</label>
                          <input type="text" disabled class="form-control" value="{{$alldeposit->deny->denied_reason}}">
                        </div>
                    @endif

                    @if($alldeposit->report !== null)
                       <b>Issue Report/s</b>
                        <div class="form-group">
                          <label>Reported by</label>
                          <input type="text" disabled class="form-control" value="{{$alldeposit->report->faculty->first_name}} {{$alldeposit->report->faculty->last_name}}">
                        </div>
                        <div class="form-group">
                          <label>Reported Title</label>
                          <input type="text" disabled class="form-control" value="{{$alldeposit->report->deposit_title}}">
                        </div>
                        <div class="form-group">
                          <label>Reported Reason</label>
                          <input type="text" disabled  class="form-control" value="{{$alldeposit->report->deposit_desc}}">
                        </div>
                        <div class="form-group">
                          <label>Solution</label>
                          <input type="text" disabled class="form-control" value="{{$alldeposit->report->solution}}">
                        </div>
                        <br><br>
                    @endif


                </div>
              </div>
              </div>


              </div>
              </div>
              </div>
              <!-- END INFO -->

            @endforeach
           </tbody>
          </table>
        </div>
    </div>

    <div class="row w3-margin-top">
        <h4>Withdraw Transaction:</h4>
    </div>

    <div class="row">
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
               <th class="w3-center">Source</th>
               <th class="w3-center">Amount</th>
               <th class="w3-center">Description</th>
               <th class="w3-center">receipt</th>
               <th class="w3-center">status</th>

              </tr>
            </thead>
            <tbody class="w3-text-gray">
            @foreach($allwithdrawals as $allwithdrawal)
             <tr>
               <td><a href="" class="w3-text-gray" data-toggle="modal" data-target="#infowithdraw-{{$allwithdrawal->id}}"><i class="fa fa-exclamation-circle fa-fw"></i> {{$allwithdrawal->withdrawal_reason}}</a></td>
               <td class="w3-center">{{$allwithdrawal->withdrawal_amt}}</td>
               <td class="w3-center">{{$allwithdrawal->withdrawal_desc}}</td>
               @if($allwithdrawal->receipt !== null)
               <td class="w3-center">
                <a href="" class="w3-text-gray" data-toggle="modal" data-target="#receiptwithdraw-{{$allwithdrawal->id}}">
               <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/' . $allwithdrawal->receipt->receipt_image) : asset('images/' . $allwithdrawal->receipt->receipt_image)}}" style="width: 100px"></a>
               </td>
               @else
               <td class="w3-center"></td>
               @endif
               <td class="w3-center">{{$allwithdrawal->withdrawal_status}}</td>
              </tr>

              <!-- INFO RECEIPT VIEW -->
              <div class="modal fade" id="receiptwithdraw-{{$allwithdrawal->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog modal-dialog-info modal-m" role="document">
              <div class="modal-content modal-content-info">
              <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
              <h4 class="modal-title w3-text-gray w3-center" id="myModalLabel">Withdraw Receipt</h4>
              </div>
              <div class="modal-body modal-body-info">
              <div class="w3-container">
                <div class="row">
               @if($allwithdrawal->receipt !== null)
                 <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/' . $allwithdrawal->receipt->receipt_image) : asset('images/' . $allwithdrawal->receipt->receipt_image)}}" style="width: 500px">
                @endif
                </div>
              </div>
              </div>


              </div>
              </div>
              </div>
              <!-- END INFO -->
              <!-- INFO WITHDRAW-->
              <div class="modal fade" id="infowithdraw-{{$allwithdrawal->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog modal-dialog-info modal-m" role="document">
              <div class="modal-content modal-content-info">
              <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
              <h4 class="modal-title w3-text-gray w3-center" id="myModalLabel">'{{$allwithdrawal->withdrawal_reason}}' Withdrawal Information</h4>
              </div>
              <div class="modal-body modal-body-info">
              <div class="w3-container">
                <div class="row">
                    @if($allwithdrawal->deny !== null)
                        <b>Denied</b><br>
                        <div class="form-group">
                          <label>Denied by</label>
                          <input type="text" disabled class="form-control" value="{{$allwithdrawal->deny->faculty->first_name}} {{$allwithdrawal->deny->faculty->last_name}}">
                        </div>
                         <div class="form-group">
                          <label>Denied Reason</label>
                          <input type="text" disabled class="form-control" value="{{$allwithdrawal->deny->denied_reason}}">
                        </div>
                    @endif

                    <hr>
                    @if($allwithdrawal->report !== null)
                       <b>Issue Report/s</b>
                        <div class="form-group">
                          <label>Reported by</label>
                          <input type="text" disabled class="form-control" value="{{$allwithdrawal->report->faculty->first_name}} {{$allwithdrawal->report->faculty->last_name}}">
                        </div>
                        <div class="form-group">
                          <label>Reported Title</label>
                          <input type="text" disabled class="form-control" value="{{$allwithdrawal->report->deposit_title}}">
                        </div>
                        <div class="form-group">
                          <label>Reported Reason</label>
                          <input type="text" disabled  class="form-control" value="{{$allwithdrawal->report->deposit_desc}}">
                        </div>
                        <div class="form-group">
                          <label>Solution</label>
                          <input type="text" disabled class="form-control" value="{{$allwithdrawal->report->solution}}">
                        </div>
                        <br><br>


                    @endif


                </div>
              </div>
              </div>


              </div>
              </div>
              </div>
              <!-- END INFO -->
            @endforeach
           </tbody>
          </table>
        </div>
    </div>

    @if(!$penalties->isEmpty())
    <div class="row w3-margin-top">
        <h4>Penalty Fee Details</h4>
    </div>

    <div class="row">
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
               <th class="w3-center">Officer</th>
               <th class="w3-center">Reason</th>
               <th class="w3-center">Fee</th>
               <th class="w3-center">Status</th>

              </tr>
            </thead>
            <tbody class="w3-text-gray">
              @foreach($penalties as $penalty)
              <tr>
                <td class="w3-center">{{$penalty->member->first_name}} {{$penalty->member->last_name}}</td>
                <td class="w3-center">{{$penalty->reason}}</td>
                <td class="w3-center">{{$penalty->fee}}</td>
                <td class="w3-center">{{$penalty->penalty_status}}</td>
              </tr>
              @endforeach
           </tbody>
          </table>
        </div>
    </div>
    @endif

    @foreach($allmonthly_budgets as $allmonthly_budget)
    <div class="row">
        <h4>
        <a href="javascript::void(0)" class="w3-text-gray" data-toggle="modal" data-target="#infobudget-{{$allmonthly_budget->id}}">Requested Monthly Budget: {{$allmonthly_budget->budget_name}} (P{{$allmonthly_budget->budget_amt}}.00)/Status:{{$allmonthly_budget->budget_status}}
        </a>
        </h4>
    </div>



    <div class="row">
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
            <th class="w3-center">Expense</th>
            <th class="w3-center">Amount</th>
            <th class="w3-center">Received by</th>
            <th class="w3-center">Approved By</th>
            <th class="w3-center">Receipt</th>

              </tr>
            </thead>
            <tbody class="w3-text-gray">
            @foreach($allmonthly_budget->budget_breakdown as $budget_breakdown)
            @if(!$budget_breakdown->budget_expenses->isEmpty())
            @foreach($budget_breakdown->budget_expenses as $budget_expenses)

             <tr>

               <td class="w3-center">{{$budget_expenses->expense_name}}</td>
               <td class="w3-center">Php {{$budget_expenses->expense_amt}}.00</td>
               <td class="w3-center">{{$budget_expenses->receiver->first_name}} {{$budget_expenses->receiver->last_name}} - {{$budget_expenses->receiverRole->officer_name}}</td>
               <td class="w3-center">{{$budget_expenses->approvedBy->first_name}} {{$budget_expenses->approvedBy->last_name}} - {{$budget_expenses->approvedByRole->officer_name}}</td>

                <td class="w3-center">
                @if($budget_expenses->receipt !== null)
                  <a href="" class="w3-text-gray" data-toggle="modal" data-target="#infoexpenses-{{$budget_expenses->id}}"><img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$budget_expenses->receipt->receipt_image) : asset('images/'.$budget_expenses->receipt->receipt_image)}}" style="width: 100px"></a>
                @endif
                </td>
             </tr>


            <!-- INFO BUDGET-->
            <div class="modal fade" id="infoexpenses-{{$budget_expenses->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-dialog-info modal-m" role="document">
            <div class="modal-content modal-content-info">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
            <h4 class="modal-title w3-text-gray w3-center" id="myModalLabel">'{{$budget_expenses->expense_name}}' Expense Information</h4>
            </div>
            <div class="modal-body modal-body-info">
            <div class="w3-container">
            <div class="row">

              @if($budget_expenses->receipt !== null)
               <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/' . $budget_expenses->receipt->receipt_image) : asset('images/' . $budget_expenses->receipt->receipt_image)}}" style="width: 500px">
              @endif


            </div>
            </div>
            </div>


            </div>
            </div>
            </div>
            <!-- END INFO -->

            @endforeach
            @endif
            @endforeach
           </tbody>
          </table>
        </div>
    </div>

    <!-- INFO BUDGET-->
    <div class="modal fade" id="infobudget-{{$allmonthly_budget->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-dialog-info modal-m" role="document">
    <div class="modal-content modal-content-info">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
    <h4 class="modal-title w3-text-gray w3-center" id="myModalLabel">'{{$allmonthly_budget->budget_name}}' Budget Information</h4>
    </div>
    <div class="modal-body modal-body-info">
    <div class="w3-container">
    <div class="row">
        @if(!$allmonthly_budget->deny->isEmpty())
            <b>Denied</b><br>
            @foreach($allmonthly_budget->deny as $deny)
            Faculty Name: {{$deny->admin->first_name}} {{$deny->admin->last_name}}<br>
            <div class="form-group">
              <label>Denied by</label>
              <input type="text" class="form-control" disabled value="{{$deny->admin->first_name}} {{$deny->admin->last_name}}}">
            </div>
            <div class="form-group">
              <label>Denied Reason</label>
              <input type="text" class="form-control" disabled value="{{$deny->denied_reason}}">
            </div>

            @endforeach
        @endif

        @foreach($allmonthly_budget->withdraw as $withdraw)
          @if($withdraw->receipt !== null)
            <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/' . $withdraw->receipt->receipt_image) : asset('images/' . $withdraw->receipt->receipt_image)}}" style="width: 500px">
          @endif

        @endforeach

    </div>
    </div>
    </div>


    </div>
    </div>
    </div>
    <!-- END INFO -->

    @endforeach

    <div class="row w3-margin-top">
        <h3>Purchase Request Expenses</h3>
    </div>
    @foreach($allexternaltasks as $externaltask)
      @foreach($externaltask->moneyrequest_all as $moneyrequest)
      <h4> <a href="" class="w3-text-gray" data-toggle="modal" data-target="#infomoneyreq-{{$moneyrequest->id}}"> Money Request: P{{$moneyrequest->amount}}.00</a></h4>
      <!-- INFO Money Request-->
          <div class="modal fade" id="infomoneyreq-{{$moneyrequest->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog modal-dialog-info modal-sm" role="document">
          <div class="modal-content modal-content-info">
          <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
          <h4 class="modal-title w3-text-gray w3-center" id="myModalLabel">'{{$externaltask->task_name}}' Money Request Information</h4>
          </div>
          <div class="modal-body modal-body-info">
          <div class="w3-container">
            <div class="row">

                    <div class="form-group">
                      <label>Requested Money</label>
                      <input type="text" class="form-control" value="{{$moneyrequest->amount}}" disabled>
                    </div>
                    @if($moneyrequest->excess_amount !== null)
                     <div class="form-group">
                      <label>Excess Money</label>
                      <input type="text" class="form-control" value="{{$moneyrequest->excess_amount}}" disabled>
                    </div>
                    @endif

                    @if($moneyrequest->reimburse !== null)
                     <div class="form-group">
                      <label>Reimbursed Money</label>
                      <input type="text" class="form-control" value="{{$moneyrequest->reimburse->reimburse_amount}}" disabled>
                    </div>
                    @endif
                    <div class="form-group">
                      <label>Money Request Status</label>
                      <input type="text" class="form-control" value="{{$moneyrequest->mon_req_status}}" disabled>
                    </div>

            </div>
          </div>
          </div>


          </div>
          </div>
          </div>
          <!-- END INFO -->





    <div class="row">
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
               <th class="w3-center">Expenses</th>
               <th class="w3-center">Amount</th>
               <th class="w3-center">Receipt</th>

              </tr>
            </thead>
            <tbody class="w3-text-gray">



            @foreach($externaltask->mticspurchaselist as $mticspurchaselist)
              <tr>
                <td><a href="" class="w3-text-gray" data-toggle="modal" data-target="#inforeqexpenses-{{$mticspurchaselist->id}}"><i class="fa fa-exclamation-circle fa-fw"></i> {{$mticspurchaselist->mtics_itemname}}({{$mticspurchaselist->mtics_act_quan}} pc/s)</a></td>
                <td class="w3-center">Php {{$mticspurchaselist->mtics_total_price}}.00</td>
                <td class="w3-center">
                  @foreach($mticspurchaselist->receipt as $receipt)

                    @if($receipt !== null)
                    <a href="" class="w3-text-gray" data-toggle="modal" data-target="#receiptreqexpenses-{{$mticspurchaselist->id}}">
                    <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/' . $receipt->receipt_image) : asset('images/' . $receipt->receipt_image)}}" style="width: 100px"></a>
                    @endif
                  @endforeach
                </td>
              </tr>
                      <!-- Receipt Request to buy expenses -->
                      <div class="modal fade" id="receiptreqexpenses-{{$mticspurchaselist->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                      <div class="modal-dialog modal-dialog-info modal-m" role="document">
                      <div class="modal-content modal-content-info">
                      <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                      <h4 class="modal-title w3-text-gray w3-center" id="myModalLabel">'{{$mticspurchaselist->mtics_itemname}}' Expense Receipt</h4>
                      </div>
                      <div class="modal-body modal-body-info">
                      <div class="w3-container">
                        <div class="row">
                           @foreach($mticspurchaselist->receipt as $receipt)
                            @if($receipt !== null)
                            <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/' . $receipt->receipt_image) : asset('images/' . $receipt->receipt_image)}}" style="width: 500px">
                            @endif
                           @endforeach

                        </div>
                      </div>
                      </div>


                      </div>
                      </div>
                      </div>
                      <!-- END INFO -->


                       <!-- INFO Request to buy expenses -->
                      <div class="modal fade" id="inforeqexpenses-{{$mticspurchaselist->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                      <div class="modal-dialog modal-dialog-info modal-m" role="document">
                      <div class="modal-content modal-content-info">
                      <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                      <h4 class="modal-title w3-text-gray w3-center" id="myModalLabel">'{{$mticspurchaselist->mtics_itemname}}' Expense Information</h4>
                      </div>
                      <div class="modal-body modal-body-info">
                      <div class="w3-container">
                        <div class="row">

                           @if(!$mticspurchaselist->mticspurchaseequiplist->isEmpty())

                           @foreach($mticspurchaselist->mticspurchaseequiplist as $mticspurchaseequiplist)

                          <strong>Equipment Information</strong>

                           <div class="form-group">
                            <label>Brand Name</label>
                            <input type="text" disabled class="form-control" value="{{$mticspurchaseequiplist->mtics_brandname}}">
                          </div>

                          <div class="form-group">
                            <label>Serial No. </label>
                            <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="reason" placeholder="Reason" rows="3" disabled>{{$mticspurchaseequiplist->mtics_serialnum}}</textarea>
                          </div>

                           <div class="form-group">
                            <label>Specification </label>
                            <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="reason" placeholder="Reason" rows="3" disabled>{{$mticspurchaseequiplist->mtics_specs}}</textarea>
                          </div>
                           @endforeach
                           @endif
                        </div>
                      </div>
                      </div>


                      </div>
                      </div>
                      </div>
                      <!-- END INFO -->


            @endforeach



            @endforeach
           </tbody>
          </table>
        </div>
    </div>
      @endforeach

    </div>

</div>


<script type="text/javascript">
function showSummary() {
    if (document.getElementById('summaryReport').style.display == 'none'){
        $('#summaryReport').show();
        $('#financialStatement').hide();
    } else {
        $('#financialStatement').show();
        $('#summaryReport').hide();
    }
}
</script>

@endsection
