@extends('layout/control_panel')

@section('title')
Financial Report
@endsection

@section('middle')

<div class="w3-container w3-margin">
  <div class="row" id="mticsBanner">
    <h3>
      <i class="fa fa-files-o fa-fw w3-xxlarge"></i>
      <strong>MTICS Financial Report</strong>
    </h3>
  </div>

  <div class="row" id="eventBanner" style="display: none">
    <h3>
      <i class="fa fa-files-o fa-fw w3-xxlarge"></i>
      <strong>Event Financial Report</strong>
    </h3>
  </div>

  <hr>

  <div class="row w3-margin-top">
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#mticsTab" class="w3-text-black" onclick="changeBanner('mtics')">MTICS Monthly Reports</a></li>

      <li><a data-toggle="tab" href="#eventTab" class="w3-text-black" onclick="changeBanner('event')">Event Financial Reports</a></li>
    </ul>
  </div>

  <div class="tab-content">
    <div id="mticsTab" class="tab-pane fade in active">
      <div class="row w3-margin-top">

        <form action="{{ url('admin/finance/financial-reports/mtics/view-month') }}" method="POST" enctype="multipart/form-data">
          <div class="form-group form-inline">
            <label for="month">Select a Month:</label>
            <input id="month" name='month' type="month" class="form-control">
            <button type="submit" class="btn btn-default w3-teal w3-text-white" title="view"><i class="fa fa-eye"></i> view</button>
          </div>
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
        </form>

      </div>
    </div>

    <div id="eventTab" class="tab-pane fade in">
      <div class="row w3-margin-top">
        <div class="table-responsive">
          <table class="table table-hover table-bordered" id="eventTable">
          <thead>
            <tr>
             <th class="w3-center">Event Title</th>
             <th class="w3-center">Event Fee</th>
             <th class="w3-center">Start Date</th>
             <th class="w3-center">End Date</th>
             <th class="w3-center">Action</th>

            </tr>
          </thead>
          <tbody class="w3-text-gray">
            @foreach($events as $event)
            <tr>
              <td class="w3-center">{{$event->event_title}}</td>
              <td class="w3-center">{{$event->event_amount}}</td>
              <td class="w3-center">{{$event->start_date}}</td>
              <td class="w3-center">{{$event->end_date}}</td>
              <td class="w3-center">
                
                <form action="{{ url('admin/finance/financial-reports/event/'.$event->id.'/view-reports') }}" method="POST" enctype="multipart/form-data">
                     <button type="submit" class="btn btn-default w3-teal w3-text-white"><i class="fa fa-eye fa-fw"></i> View</button>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </form>
              </td>
            </tr>
            @endforeach
          </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

</div>

<script type="text/javascript">
  function changeBanner(tab) {
    if (tab == "mtics") {
      $("#mticsBanner").show()
      $("#eventBanner").hide()
    }

    else if (tab == "event") {
      $("#eventBanner").show()
      $("#mticsBanner").hide()
    }
  }
</script>

<script type="text/javascript">
$(document).ready( function () {
  $('#eventTable').DataTable();
});
</script>
@endsection
