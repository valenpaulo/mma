<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/css/w3.css') : asset('css/w3.css') }}">
    <link rel="stylesheet" href="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/css/bootstrap.min.css') : asset('css/bootstrap.min.css') }}">
    <link href="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/css/fullcalendar.css') : asset('css/fullcalendar.css')}}" rel='stylesheet' />
    <script src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/js/jquery-3.2.1.min.js') : asset('js/jquery-3.2.1.min.js')}}"></script>

    <link rel="stylesheet" href="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/css/font-awesome.min.css') : asset('css/font-awesome.min.css') }}">
    <link rel="shortcut icon" href="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/logo/mtics.ico') : asset('images/logo/mtics.ico')}}" />

    <title>Financial Report</title>
</head>
<body>

<div class="container">
    <div class="row" align="center">
    <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/logo/mticsBanner.png') : asset('images/logo/mticsBanner.png')}}"  style="width: 85%">
       
        <h2>{{$event->event_title}}</h2>
        <p>Financial Report (S.Y. {{$fromyear}} - {{$toyear}})</p>
    </div>
</div>

<div class="container" style="padding-left: 5em; padding-right: 5em;">

    <div class="row w3-margin-top">
      <div class="table-responsive">
        <table class="w3-table">
          <tbody>
           <tr>
              <td><p><strong>Total Collection</strong></p></td>
              <td class="w3-right">Php {{$payment}}.00</td>
           </tr>
         </tbody>
        </table>
      </div>
    </div>

    <div class="row w3-margin-top">
      <div class="table-responsive">
        <table class="w3-table">
          <tbody>
            <?php $fromOtherTotal = 0; ?>
            Other Collectibles
            @foreach($fromOthers as $fromOther)
           <tr>
              <td><p><strong>{{$fromOther->deposit_desc}}</strong></p></td>
              <td class="w3-right">Php {{$fromOther->deposit_amt}}.00</td>
              <?php $fromOtherTotal = $fromOtherTotal + $fromOther->deposit_amt?>
           </tr>
            @endforeach
         </tbody>
        </table>
      </div>
    </div>

    <hr>

    <?php $expensestotal=0; ?>
    <?php $expensestotalall=0; ?>

    @if (count($expenses) > 0)
    <div class="row">
      <h3>Expenses</h3>
    </div>

    <div style="padding-left: 5em;">

    <div class="row">
        <div class="table-responsive">
          <table class="w3-table">
            <tbody class="w3-text-gray">
            @foreach($expenses as $expense)
              <tr>
                <td>- {{$expense->breakdown_name}}</td>
                <td class="w3-right">Php {{$expense->expenses->sum('expense_amt')}}.00</td>
              </tr>
                @foreach($expense->expenses as $details)
                  <tr>
                    <td>- {{$details->expense_name}}</td>
                    <td class="w3-right">Php {{$details->expense_amt}}.00</td>
                  </tr>
                  @endforeach


              <?php $expensestotal = $expensestotal + $expense->expenses->sum('expense_amt'); ?>
            @endforeach
            <?php $expensestotalall = $expensestotalall + $expensestotal;  $expensestotal = 0; ?>
           </tbody>
          </table>
        </div>

    </div>
    </div>

    <hr>
    @endif

    <div class="row">
        <h3>Purchased Items</h3>
    </div>

    <div style="padding-left: 5em;">

    <div class="row">
        <div class="table-responsive">
          <table class="w3-table">
            <tbody class="w3-text-gray">
            @foreach($externaltasks as $externaltask)
            @foreach($externaltask->eventpurchaselist as $eventpurchaselist)
              <tr>
                <td>- {{$eventpurchaselist->event_itemname}} ({{$eventpurchaselist->event_act_quan}} pc/s)</td>
                <td class="w3-right">Php {{$eventpurchaselist->event_total_price}}.00</td>
              </tr>
              <?php $expensestotal = $expensestotal + $eventpurchaselist->event_total_price; ?>
            @endforeach
            <?php $expensestotalall = $expensestotalall + $expensestotal;  $expensestotal = 0; ?>
            @endforeach
           </tbody>
          </table>
        </div>

        <hr>

        <div class="table-responsive">
          <table class="w3-table">
            <tbody>
             <tr>
                <td><p><strong>Total Expenses</strong></p></td>
                <td class="w3-right">Php {{$expensestotalall}}.00</td>
             </tr>
           </tbody>
          </table>
        </div>
    </div>
    </div>

  <hr>

  <div class="row">
    <div class="table-responsive">
      <table class="w3-table">
        <thead>
            <th>Total Collection</th>
            <th>Subsidy by MTICS fund</th>
            <th>Total Expenses</th>
            <th>Remaining Total Budget</th>
            <th>Remaining Event Fund</th>
        </thead>
        <tbody>
         <tr>
          <?php $totalcollectibles = $fromOtherTotal + $payment?>
            <td>Php {{$totalcollectibles}}.00</td>
            <td>Php {{$fromMTICSfund}}.00</td>
            <td>Php {{$expensestotalall}}.00</td>
            <td>Php {{$eventwithdraws->sum('withdrawal_amt') - $expensestotalall}}.00</td>
            <td>Php {{($totalcollectibles + $fromMTICSfund) - $eventwithdraws->sum('withdrawal_amt') + ($eventwithdraws->sum('withdrawal_amt') - $expensestotalall)}}.00</td>
         </tr>
       </tbody>
      </table>
    </div>
  </div>

  <br><br><br>
      <p><strong>Prepared by:</strong></p>
          @foreach($roles as $role)
            @if($officerterm->status !== 'on-going')
              @if(!$role->prev_member->isEmpty())
                @foreach($role->prev_member as $member) 
                    @if($role->name == 'president')
                      <br>
                      <p><strong>Approved by:</strong></p>
                    @endif
              <p>{{$member->first_name}} {{$member->last_name}}</p>
              <p><i>{{$role->display_name}}</i></p>
                @endforeach 
              @endif
            @else
              @if(!$role->member->isEmpty())
                @foreach($role->member as $member) 
                @if($role->name == 'president')
                  <br>
                  <p><strong>Approved by:</strong></p>
                @endif
                @if($member !== 'null')
              <p>{{$member->first_name}} {{$member->last_name}}</p>
              <p><i>{{$role->display_name}}</i></p>
                @endif
                @endforeach 
              @endif
            @endif

          @endforeach
          <p></p>

<script>
  document.addEventListener("DOMContentLoaded", function(event) {
    window.print()
  });
</script>

</body>
</html>

