<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/css/w3.css') : asset('css/w3.css') }}">
    <link rel="stylesheet" href="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/css/bootstrap.min.css') : asset('css/bootstrap.min.css') }}">
    <link href="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/css/fullcalendar.css') : asset('css/fullcalendar.css')}}" rel='stylesheet' />
    <script src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/js/jquery-3.2.1.min.js') : asset('js/jquery-3.2.1.min.js')}}"></script>

    <link rel="stylesheet" href="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/css/font-awesome.min.css') : asset('css/font-awesome.min.css') }}">
    <link rel="shortcut icon" href="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/logo/mtics.ico') : asset('images/logo/mtics.ico')}}" />


    <title>Accomplishment Report</title>
</head>
<body>

<div class="container">
    <div class="row" align="center">
        <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/logo/mticsBanner.png') : asset('images/logo/mticsBanner.png')}}"  style="width: 85%">
        <h3>ACCOMPLISHMENT REPORT</h3>
        <p>{{$fromDate}} - {{$toDate}} </p>
          

      <hr>

<div class="container" style="padding-left: 2em; padding-right: 2em; padding-top: 2em;">

     <div class="row">
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
               <th class="w3-center">Project</th>
               <th class="w3-center">Date</th>
               <th class="w3-center">Description</th>
               <th class="w3-center">Objectives</th>
               <th class="w3-center">Accomplishment</th>

              </tr>
            </thead>
            <tbody class="w3-text-gray">

              @foreach($activities as $activity)
              <tr>
                <td>{{$activity['event']->event_title}}</td>
                <td>{{$activity['date']}}</td>
                <td>{{$activity['event']->description}}</td>
                <td>{{$activity['event']->goal}}</td>
                <td>{{$activity['event']->accomplishment}}</td>
              </tr>
              @endforeach

            </tbody>
          </table>
        </div>
      </div>
</div>


    </div>
</div>

<p><strong>Prepared by:</strong></p>
          @foreach($roles as $role)
            @if($officerterm->status !== 'on-going')
              @if(!$role->prev_member->isEmpty())
                @foreach($role->prev_member as $member)
                    @if($role->name == 'president')
                      <br>
                      <p><strong>Approved by:</strong></p>
                    @endif
              <p>{{$member->first_name}} {{$member->last_name}}</p>
              <p><i>{{$role->display_name}}</i></p>
                @endforeach
              @endif
            @else
              @if(!$role->member->isEmpty())
                @foreach($role->member as $member)
                @if($role->name == 'president')
                  <br>
                  <p><strong>Approved by:</strong></p>
                @endif
                @if($member !== 'null')
              <p>{{$member->first_name}} {{$member->last_name}}</p>
              <p><i>{{$role->display_name}}</i></p>
                @endif
                @endforeach
              @endif
            @endif

          @endforeach
  <p></p>



</div>

<script>
  document.addEventListener("DOMContentLoaded", function(event) {
    var css = '@page { size: landscape; }',
    head = document.head || document.getElementsByTagName('head')[0],
    style = document.createElement('style');

    style.type = 'text/css';
    style.media = 'print';

    if (style.styleSheet){
      style.styleSheet.cssText = css;
    } else {
      style.appendChild(document.createTextNode(css));
    }

    head.appendChild(style);
    window.print()
  });
</script>

</body>
</html>

