<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/css/w3.css') : asset('css/w3.css') }}">

    <link rel="stylesheet" href="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/css/bootstrap.min.css') : asset('css/bootstrap.min.css') }}">

    <link href="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/css/fullcalendar.css') : asset('css/fullcalendar.css')}}" rel='stylesheet' />
    
    <script src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/js/jquery-3.2.1.min.js') : asset('js/jquery-3.2.1.min.js')}}"></script>

    <link rel="stylesheet" href="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/css/font-awesome.min.css') : asset('css/font-awesome.min.css') }}">
    
    <link rel="shortcut icon" href="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/logo/mtics.ico') : asset('images/logo/mtics.ico')}}" />

    <title>Master List</title>
</head>
<body>

<div class="container">
  <div class="row" align="center">
    <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/logo/mticsBanner.png') : asset('images/logo/mticsBanner.png')}}"  style="width: 85%">
    <h2>{{$year->year_num}}-{{$section->section_code}}</h2>
    <p>{{$year->year_code}} Year - {{$section->course->course_code}}</p>
  </div>
</div>

<div class="container" style="padding-left: 5em; padding-right: 5em;">
    
  <hr>
  <div style="padding-left: 5em;">
  

  <div class="row">
          <table class="w3-table">
            <thead>
              <tr>
               <th class="w3-center">No.</th>               
               <th class="w3-center">Student ID</th>               
               <th class="w3-center">Last Name</th>
               <th class="w3-center">First Name</th>
              </tr>
            </thead>
            <tbody class="w3-text-gray">
              <?php $count = 1; ?>
              @foreach($students as $student)
              <tr>
               <td class="w3-center">{{$count++}}</td>
               <td class="w3-center">{{$student->id_num}}</td>
               <td class="w3-center">{{$student->last_name}}</td>
               <td class="w3-center">{{$student->first_name}}</td>
              </tr>
              @endforeach
           </tbody>
          </table>

    <hr>

  </div>
  </div>


</body>
</html>


<script>
  document.addEventListener("DOMContentLoaded", function(event) {
    window.print()
  });
</script>
