@extends('layout/control_panel')

@section('title')
Manage Member
@endsection

@section('middle')


	<a class="w3-text-gray w3-hover-text-green" data-toggle="modal" href="#submit">PRINT</a>
<!-- submit to auditor DEPOSIT-->
  <div class="modal fade" id="submit" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
  <div class="modal-content modal-content-info">
  <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
<form action="{{ url('admin/auditor/audit-reports/mtics/view-month/print') }}" method="POST" enctype="multipart/form-data">

  <h4 class="modal-title w3-text-gray w3-center" id="myModalLabel">Print</h4>
  </div>
  <div class="modal-body modal-body-info">
  <div class="w3-container">
    <div class="row">
    	do you want to print the summary?
		<input type="hidden" name="month" value="{{$requestmonth}}">
    </div>
  </div>
  </div>
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<button type="submit" class="btn btn-default w3-orange w3-text-white" title="Edit"><i class="fa fa-pencil"></i> print</button>
</form>
  </div>
  </div>
  </div>
  <!-- END INFO -->



	<a class="w3-text-gray w3-hover-text-green" data-toggle="modal" href="#summary"> summary</a>


<!-- SUMMARY MODAL -->
 <div class="modal fade" id="summary" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
            <h4 class="modal-title w3-text-gray" id="myModalLabel">Summary</h4>
            </div>
            <div class="modal-body">
              <div class="w3-container">

               <title>Financial Report</title>


				<div class="container">
					<div class="row" align="center">
						<img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/logo/mticsBanner.png') : asset('images/logo/mticsBanner.png')}}"  style="width: 85%">
						<h2>Monthly Audit Report</h2>
						<p>MTICS Fund (S.Y. Year-Year)</p>
					</div>

					<div class="row w3-margin-top">
						<h3>MTICS Bank Status</h3>
						<h4>Current MTICS Bank Fund Amount: Php {{$mticsbankamount}}.00</h4>
						<h4>Total Cash Deposit: Php {{$totaldeposit}}.00</h4>
						<h4>Total Cash Withdraw: Php {{$totalwithdraw}}.00</h4>
					</div>


					<?php $expensestotal=0; ?>
					<?php $expensestotalall=0; ?>

					<div class="row">
						<h3>Expenses</h3>
					</div>
            		<?php $overallBudget = 0; $overallExpenses = 0;  ?>
					@foreach($monthly_budgets as $monthly_budget)
					<div class="row">
						<h4>Requested Monthly Budget: {{$monthly_budget->budget_name}} (P{{$monthly_budget->budget_amt}}.00)</h4>
					</div>


					<div class="row">
						<div class="table-responsive">
					      <table class="table table-bordered">
					        <thead>
					          <tr>
					           <th class="w3-center">Expenses</th>
					           <th class="w3-center">Estimated Budget</th>
					           <th class="w3-center">Actual Budget</th>
					          </tr>
					        </thead>
					        <tbody class="w3-text-gray">
					        <?php $totalbudget=0; $actualbudget = 0; $totalactualbudget = 0;$excessmoney = 0;?>
						    @foreach($monthly_budget->budget_breakdown as $budget_breakdown)
					          <tr>
					          	<!-- ESTIMATED BUDGET -->
						        @if($budget_breakdown->mtics_budget_exp_cat_id == $budget_breakdown->budget_expenses_cat->id)
							        <td class="w3-center">{{$budget_breakdown->budget_expenses_cat->name}}</td>
							    @endif

						        <td class="w3-center">{{$budget_breakdown->budget_bd_amt}}</td>
						        <?php $totalbudget = $totalbudget + $budget_breakdown->budget_bd_amt; ?>

						        <!-- ACTUAL BUDGET -->
							    @if(!$budget_breakdown->budget_expenses->isEmpty())
							    @foreach($budget_breakdown->budget_expenses as $budget_expenses)
							    	<?php $actualbudget = $actualbudget + $budget_expenses->expense_amt ?>
							    @endforeach
						        <td class="w3-center">{{$actualbudget}}</td>
						        <?php $totalactualbudget = $totalactualbudget + $actualbudget; $actualbudget=0;  ?>
							    @else
						        <td class="w3-center">0</td>
							    @endif

					          </tr>
							@endforeach


								<tr>
									<td class="w3-center">Total Monthly Budget</td>
									<td class="w3-center">Php {{$totalbudget}}.00</td>
									<td class="w3-center">Php {{$totalactualbudget}}.00</td>
									<?php $overallBudget = $overallBudget + $totalbudget; ?>
								</tr>

								<tr>
									<td class="w3-center">Total Excess money</td>
									<?php $excessmoney = $totalbudget - $totalactualbudget;?>
									<td class="w3-center">Php {{$excessmoney}}.00</td>
									<?php $excessmoney = 0; $totalactualbudget=0;?>

								</tr>
					       </tbody>
					      </table>
						</div>
					</div>



					<div class="row">
						<div class="table-responsive">
					      <table class="table table-bordered">
					        <thead>
					          <tr>
					           <th class="w3-center">Expenses</th>
					           <th class="w3-center">Amount</th>

					          </tr>
					        </thead>
					        <tbody class="w3-text-gray">
					        <?php $totalexpenses = 0; ?>
						    @foreach($monthly_budget->budget_breakdown as $budget_breakdown)
						    @if(!$budget_breakdown->budget_expenses->isEmpty())
					        @foreach($budget_breakdown->budget_expenses as $budget_expenses)

					         <tr>
					           <td class="w3-center"><a href="" class="w3-text-green" data-toggle="modal" data-target="#infoexpenses-{{$budget_expenses->id}}">{{$budget_expenses->expense_name}}</a></td>
					           <td class="w3-center">Php {{$budget_expenses->expense_amt}}.00</td>
						        <?php $totalexpenses = $totalexpenses + $budget_expenses->expense_amt; ?>
					         </tr>


					        @endforeach
							@endif
						    @endforeach
					        	<tr>
									<td class="w3-center">Total Monthly Expenses</td>
									<td class="w3-center">Php {{$totalexpenses}}.00</td>
									<?php $overallExpenses = $overallExpenses + $totalexpenses; ?>
								</tr>
					       </tbody>
					      </table>
						</div>
					</div>

					@endforeach

					<div class="row">
						<h3>Request to Buy Expenses</h3>
					</div>

					<div class="row">
						<div class="table-responsive">
					      <table class="table table-bordered">
					        <thead>
					          <tr>
					           <th class="w3-center">Expenses</th>
					           <th class="w3-center">Amount</th>
					          </tr>
					        </thead>
					        <tbody class="w3-text-gray">
							@foreach($externaltasks as $externaltask)

							@foreach($externaltask->mticspurchaselist as $mticspurchaselist)
					          <tr>
						        <td class="w3-center">{{$mticspurchaselist->mtics_itemname}}({{$mticspurchaselist->mtics_act_quan}} pc/s)</td>
						        <td class="w3-center">Php {{$mticspurchaselist->mtics_total_price}}.00</td>
					          </tr>
					         <?php $expensestotal = $expensestotal + $mticspurchaselist->mtics_total_price; ?>
					        @endforeach
					        <?php $expensestotalall = $expensestotalall + $expensestotal;  $expensestotal = 0; ?>
							@endforeach
					       </tbody>
					      </table>
						</div>
					</div>

					<br><br><?php $overallExpenses = $overallExpenses + $expensestotalall  ?>
				    <div align="left"><b>Total Expenses = {{$overallExpenses}}</b><br><br><br></div>
				    <div align="left"><b>{{$totalwithdraw}}</b><br></div>
				    <div align="left"><b>-{{$overallExpenses}}</b><br></div>

				-----------------------------<br>
				total cash in hand = P{{$totalwithdraw - $overallExpenses}}.00 <br>

				</div>

              </div>
            </div>
            <div class="modal-footer">
            </div>

            </div>
            </div>
            </div>
            <!-- END MODAL -->
            <h4>Total Cash Deposit: Php {{$totaldeposit}}.00</h4>
			<h4>Total Cash Withdraw: Php {{$totalwithdraw}}.00</h4>

            	<?php $overallBudget = 0; $overallExpenses = 0;  ?>
				@foreach($monthly_budgets as $monthly_budget)
					<div class="row">
						<a href="" class="w3-text-green" data-toggle="modal" data-target="#infobudget-{{$monthly_budget->id}}"><h4>Requested Monthly Budget: {{$monthly_budget->budget_name}} (P{{$monthly_budget->budget_amt}}.00)/Status:{{$monthly_budget->budget_status}}</h4></a>
					</div>

					<div class="row">
						<div class="table-responsive">
					      <table class="table table-bordered">
					        <thead>
					          <tr>
					           <th class="w3-center">Expenses</th>
					           <th class="w3-center">Estimated Budget</th>
					           <th class="w3-center">Actual Budget</th>
					          </tr>
					        </thead>
					        <tbody class="w3-text-gray">
					        <?php $totalbudget=0; $actualbudget = 0; $totalactualbudget = 0;$excessmoney = 0;?>
						    @foreach($monthly_budget->budget_breakdown as $budget_breakdown)
					          <tr>
					          	<!-- ESTIMATED BUDGET -->
						        @if($budget_breakdown->mtics_budget_exp_cat_id == $budget_breakdown->budget_expenses_cat->id)
							        <td class="w3-center">{{$budget_breakdown->budget_expenses_cat->name}}</td>
							    @endif

						        <td class="w3-center">{{$budget_breakdown->budget_bd_amt}}</td>
						        <?php $totalbudget = $totalbudget + $budget_breakdown->budget_bd_amt; ?>

						        <!-- ACTUAL BUDGET -->
							    @if(!$budget_breakdown->budget_expenses->isEmpty())
							    @foreach($budget_breakdown->budget_expenses as $budget_expenses)
							    	<?php $actualbudget = $actualbudget + $budget_expenses->expense_amt ?>
							    @endforeach
						        <td class="w3-center">{{$actualbudget}}</td>
						        <?php $totalactualbudget = $totalactualbudget + $actualbudget; $actualbudget=0;  ?>
							    @else
						        <td class="w3-center">0</td>
							    @endif

					          </tr>
							@endforeach


								<tr>
									<td class="w3-center">Total Monthly Budget</td>
									<td class="w3-center">Php {{$totalbudget}}.00</td>
									<td class="w3-center">Php {{$totalactualbudget}}.00</td>
									<?php $overallBudget = $overallBudget + $totalbudget; ?>
								</tr>

								<tr>
									<td class="w3-center">Total Excess money</td>
									<?php $excessmoney = $totalbudget - $totalactualbudget;?>
									<td class="w3-center">Php {{$excessmoney}}.00</td>
									<?php $excessmoney = 0; $totalactualbudget=0;?>

								</tr>
					       </tbody>
					      </table>
						</div>
					</div>

					<div class="row">
						<div class="table-responsive">
					      <table class="table table-bordered">
					        <thead>
					          <tr>
					           <th class="w3-center">Expenses</th>
					           <th class="w3-center">Amount</th>

					          </tr>
					        </thead>
					        <tbody class="w3-text-gray">
					        <?php $totalexpenses = 0; ?>
						    @foreach($monthly_budget->budget_breakdown as $budget_breakdown)
						    @if(!$budget_breakdown->budget_expenses->isEmpty())
					        @foreach($budget_breakdown->budget_expenses as $budget_expenses)

					         <tr>
					           <td class="w3-center"><a href="" class="w3-text-green" data-toggle="modal" data-target="#infoexpenses-{{$budget_expenses->id}}">{{$budget_expenses->expense_name}}</a></td>
					           <td class="w3-center">Php {{$budget_expenses->expense_amt}}.00</td>
						        <?php $totalexpenses = $totalexpenses + $budget_expenses->expense_amt; ?>
					         </tr>

					         		<!-- INFO BUDGET-->
						              <div class="modal fade" id="infoexpenses-{{$budget_expenses->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
						              <div class="modal-dialog modal-dialog-info modal-m" role="document">
						              <div class="modal-content modal-content-info">
						              <div class="modal-header">
						              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
						              <h4 class="modal-title w3-text-gray w3-center" id="myModalLabel">'{{$budget_expenses->expense_name}}' Expense Information</h4>
						              </div>
						              <div class="modal-body modal-body-info">
						              <div class="w3-container">
						                <div class="row">

							                   	receipt : {{$budget_expenses->receipt}}<br>


						                </div>
						              </div>
						              </div>


						              </div>
						              </div>
						              </div>
						              <!-- END INFO -->




					        @endforeach
							@endif
						    @endforeach
					        	<tr>
									<td class="w3-center">Total Monthly Expenses</td>
									<td class="w3-center">Php {{$totalexpenses}}.00</td>
									<?php $overallExpenses = $overallExpenses + $totalexpenses; ?>
								</tr>
					       </tbody>
					      </table>
						</div>
					</div>


					<!-- INFO BUDGET-->
		              <div class="modal fade" id="infobudget-{{$monthly_budget->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
		              <div class="modal-dialog modal-dialog-info modal-m" role="document">
		              <div class="modal-content modal-content-info">
		              <div class="modal-header">
		              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
		              <h4 class="modal-title w3-text-gray w3-center" id="myModalLabel">'{{$monthly_budget->budget_name}}' Budget Information</h4>
		              </div>
		              <div class="modal-body modal-body-info">
		              <div class="w3-container">
		                <div class="row">
		                   	@if(!$monthly_budget->deny->isEmpty())
			                   	<b>Denied</b><br>
			                   	@foreach($monthly_budget->deny as $deny)
			                   	Faculty Name: {{$deny->admin->first_name}} {{$deny->admin->last_name}}<br>
			                   	Denied Reason: {{$deny->denied_reason}}<br>
			                   	@endforeach
		                   	@endif

		                   	@foreach($monthly_budget->withdraw as $withdraw)

			                   	receipt : {{$withdraw->receipt->receipt_image}}<br>

							@endforeach

		                </div>
		              </div>
		              </div>


		              </div>
		              </div>
		              </div>
		              <!-- END INFO -->

				@endforeach




				<div class="row">
						<h3>Request to Buy Expenses</h3>
					</div>

					<div class="row">
						<div class="table-responsive">
					      <table class="table table-bordered">
					        <thead>
					          <tr>
					           <th class="w3-center">Expenses</th>
					           <th class="w3-center">Amount</th>

					          </tr>
					        </thead>
					        <tbody class="w3-text-gray">
					        	<?php $totalreqexpenses = 0;?>
							@foreach($allexternaltasks as $externaltask)
							<a href="" class="w3-text-green" data-toggle="modal" data-target="#infomoneyreq-{{$externaltask->id}}">VIEW money request summary</a>
							<?php $totalexpenses = 0 ?>
							@foreach($externaltask->mticspurchaselist as $mticspurchaselist)
					          <tr>
						        <td class="w3-center"><a href="" class="w3-text-green" data-toggle="modal" data-target="#inforeqexpenses-{{$mticspurchaselist->id}}">{{$mticspurchaselist->mtics_itemname}}({{$mticspurchaselist->mtics_act_quan}} pc/s)</a></td>
						        <td class="w3-center">Php {{$mticspurchaselist->mtics_total_price}}.00</td>
						        <?php $totalexpenses = $totalexpenses + $mticspurchaselist->mtics_total_price; $totalreqexpenses = $totalreqexpenses + $totalexpenses; $totalexpenses = 0;?>

					          </tr>

							           <!-- INFO Request to buy expenses -->
						              <div class="modal fade" id="inforeqexpenses-{{$mticspurchaselist->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
						              <div class="modal-dialog modal-dialog-info modal-m" role="document">
						              <div class="modal-content modal-content-info">
						              <div class="modal-header">
						              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
						              <h4 class="modal-title w3-text-gray w3-center" id="myModalLabel">'{{$mticspurchaselist->mtics_itemname}}' Expense Information</h4>
						              </div>
						              <div class="modal-body modal-body-info">
						              <div class="w3-container">
						                <div class="row">
						                   @foreach($mticspurchaselist->receipt as $receipt)
						                   Receipt: {{$receipt->receipt_image}}
						                   @endforeach

						                   @if(!$mticspurchaselist->mticspurchaseequiplist->isEmpty())

						                   @foreach($mticspurchaselist->mticspurchaseequiplist as $mticspurchaseequiplist)
						                   brand {{$mticspurchaseequiplist->mtics_brandname}}<br>
						                   serial {{$mticspurchaseequiplist->mtics_serialnum}}<br>
						                   spec {{$mticspurchaseequiplist->mtics_specs}}<br>

						                   @endforeach
						                   @endif
						                </div>
						              </div>
						              </div>


						              </div>
						              </div>
						              </div>
						              <!-- END INFO -->

					        @endforeach

					        <!-- INFO Money Request-->
			              <div class="modal fade" id="infomoneyreq-{{$externaltask->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
			              <div class="modal-dialog modal-dialog-info modal-m" role="document">
			              <div class="modal-content modal-content-info">
			              <div class="modal-header">
			              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
			              <h4 class="modal-title w3-text-gray w3-center" id="myModalLabel">'{{$externaltask->task_name}}' Money Request Information</h4>
			              </div>
			              <div class="modal-body modal-body-info">
			              <div class="w3-container">
			                <div class="row">

			                	@foreach($externaltask->moneyrequest_all as $moneyrequest)
			                		money request amount = {{$moneyrequest->amount}}<br>
			                		@if($moneyrequest->excess_amount !== null)
			                		 excess= {{$moneyrequest->excess_amount}}<br>
			                		@endif

			                		@if($moneyrequest->reimburse !== null)
			                		reimbuse = {{$moneyrequest->reimburse->reimburse_amount}}<br>
			                		@endif
			                		money request status = {{$moneyrequest->mon_req_status}}<br>
			                	@endforeach
			                </div>
			              </div>
			              </div>


			              </div>
			              </div>
			              </div>
			              <!-- END INFO -->

							@endforeach




							<tr>
						        <td class="w3-center">Total Expenses</td>
						        <td class="w3-center">Php {{$totalreqexpenses}}.00</td>

						        <?php $overallExpenses = $overallExpenses + $totalreqexpenses  ; $totalexpenses=0; ?>
					          </tr>
					       </tbody>
					      </table>
						</div>
					</div>


				<h1>Withdraw: {{$totalwithdraw}}</h1>
				<h1>Over all Budget: {{$overallBudget}}</h1>

				<h1>withdraw money + Over all Budget : {{$totalwithdraw - $overallBudget}}</h1> <- other withdraw money
				<h1>Over all Expenses: {{$overallExpenses}}</h1>
				<h1>Total cash in hand: {{$totalwithdraw - $overallExpenses}}</h1>





@endsection
