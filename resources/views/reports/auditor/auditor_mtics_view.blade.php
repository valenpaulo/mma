@extends('layout/control_panel')

@section('title')
Manage Member
@endsection

@section('middle')

<form action="{{ url('admin/auditor/audit-reports/mtics/view-month') }}" method="POST" enctype="multipart/form-data">

<div class="form-group">
  <label>Budget for the month of:</label>
  <input id="month" name='month' type="month" class="form-control">
</div>
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<button type="submit" class="btn btn-default w3-orange w3-text-white" title="Edit"><i class="fa fa-pencil"></i> view</button>
</form>


@endsection