 <title>Financial Report</title>


				<div class="container">
					<div class="row" align="center">
						<img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/logo/mticsBanner.png') : asset('images/logo/mticsBanner.png')}}"  style="width: 85%">
						<h2>Monthly Audit Report</h2>
						<p>MTICS Fund (S.Y. Year-Year)</p>
					</div>

					<div class="row w3-margin-top">
						<h3>MTICS Bank Status</h3>
						<h4>Current MTICS Bank Fund Amount: Php {{$mticsbankamount}}.00</h4>
						<h4>Total Cash Deposit: Php {{$totaldeposit}}.00</h4>
						<h4>Total Cash Withdraw: Php {{$totalwithdraw}}.00</h4>
					</div>


					<?php $expensestotal=0; ?>
					<?php $expensestotalall=0; ?>

					<div class="row">
						<h3>Expenses</h3>
					</div>
            		<?php $overallBudget = 0; $overallExpenses = 0;  ?>
					@foreach($monthly_budgets as $monthly_budget)
					<div class="row">
						<h4>Requested Monthly Budget: {{$monthly_budget->budget_name}} (P{{$monthly_budget->budget_amt}}.00)</h4>
					</div>


					<div class="row">
						<div class="table-responsive">
					      <table class="table table-bordered">
					        <thead>
					          <tr>
					           <th class="w3-center">Expenses</th>
					           <th class="w3-center">Estimated Budget</th>
					           <th class="w3-center">Actual Budget</th>
					          </tr>
					        </thead>
					        <tbody class="w3-text-gray">
					        <?php $totalbudget=0; $actualbudget = 0; $totalactualbudget = 0;$excessmoney = 0;?>
						    @foreach($monthly_budget->budget_breakdown as $budget_breakdown)
					          <tr>
					          	<!-- ESTIMATED BUDGET -->
						        @if($budget_breakdown->mtics_budget_exp_cat_id == $budget_breakdown->budget_expenses_cat->id)
							        <td class="w3-center">{{$budget_breakdown->budget_expenses_cat->name}}</td>
							    @endif

						        <td class="w3-center">{{$budget_breakdown->budget_bd_amt}}</td>
						        <?php $totalbudget = $totalbudget + $budget_breakdown->budget_bd_amt; ?>

						        <!-- ACTUAL BUDGET -->
							    @if(!$budget_breakdown->budget_expenses->isEmpty())
							    @foreach($budget_breakdown->budget_expenses as $budget_expenses)
							    	<?php $actualbudget = $actualbudget + $budget_expenses->expense_amt ?>
							    @endforeach
						        <td class="w3-center">{{$actualbudget}}</td>
						        <?php $totalactualbudget = $totalactualbudget + $actualbudget; $actualbudget=0;  ?>
							    @else
						        <td class="w3-center">0</td>
							    @endif

					          </tr>
							@endforeach


								<tr>
									<td class="w3-center">Total Monthly Budget</td>
									<td class="w3-center">Php {{$totalbudget}}.00</td>
									<td class="w3-center">Php {{$totalactualbudget}}.00</td>
									<?php $overallBudget = $overallBudget + $totalbudget; ?>
								</tr>

								<tr>
									<td class="w3-center">Total Excess money</td>
									<?php $excessmoney = $totalbudget - $totalactualbudget;?>
									<td class="w3-center">Php {{$excessmoney}}.00</td>
									<?php $excessmoney = 0; $totalactualbudget=0;?>

								</tr>
					       </tbody>
					      </table>
						</div>
					</div>



					<div class="row">
						<div class="table-responsive">
					      <table class="table table-bordered">
					        <thead>
					          <tr>
					           <th class="w3-center">Expenses</th>
					           <th class="w3-center">Amount</th>

					          </tr>
					        </thead>
					        <tbody class="w3-text-gray">
					        <?php $totalexpenses = 0; ?>
						    @foreach($monthly_budget->budget_breakdown as $budget_breakdown)
						    @if(!$budget_breakdown->budget_expenses->isEmpty())
					        @foreach($budget_breakdown->budget_expenses as $budget_expenses)

					         <tr>
					           <td class="w3-center"><a href="" class="w3-text-green" data-toggle="modal" data-target="#infoexpenses-{{$budget_expenses->id}}">{{$budget_expenses->expense_name}}</a></td>
					           <td class="w3-center">Php {{$budget_expenses->expense_amt}}.00</td>
						        <?php $totalexpenses = $totalexpenses + $budget_expenses->expense_amt; ?>
					         </tr>


					        @endforeach
							@endif
						    @endforeach
					        	<tr>
									<td class="w3-center">Total Monthly Expenses</td>
									<td class="w3-center">Php {{$totalexpenses}}.00</td>
									<?php $overallExpenses = $overallExpenses + $totalexpenses; ?>
								</tr>
					       </tbody>
					      </table>
						</div>
					</div>

					@endforeach

					<div class="row">
						<h3>Request to Buy Expenses</h3>
					</div>

					<div class="row">
						<div class="table-responsive">
					      <table class="table table-bordered">
					        <thead>
					          <tr>
					           <th class="w3-center">Expenses</th>
					           <th class="w3-center">Amount</th>
					          </tr>
					        </thead>
					        <tbody class="w3-text-gray">
							@foreach($externaltasks as $externaltask)

							@foreach($externaltask->mticspurchaselist as $mticspurchaselist)
					          <tr>
						        <td class="w3-center">{{$mticspurchaselist->mtics_itemname}}({{$mticspurchaselist->mtics_act_quan}} pc/s)</td>
						        <td class="w3-center">Php {{$mticspurchaselist->mtics_total_price}}.00</td>
					          </tr>
					         <?php $expensestotal = $expensestotal + $mticspurchaselist->mtics_total_price; ?>
					        @endforeach
					        <?php $expensestotalall = $expensestotalall + $expensestotal;  $expensestotal = 0; ?>
							@endforeach
					       </tbody>
					      </table>
						</div>
					</div>

					<br><br><?php $overallExpenses = $overallExpenses + $expensestotalall  ?>
				    <div align="left"><b>Total Expenses = {{$overallExpenses}}</b><br><br><br></div>
				    <div align="left"><b>{{$totalwithdraw}}</b><br></div>
				    <div align="left"><b>-{{$overallExpenses}}</b><br></div>

				-----------------------------<br>
				total cash in hand = P{{$totalwithdraw - $overallExpenses}}.00 <br>

				</div>

				<script>
  document.addEventListener("DOMContentLoaded", function(event) {
    window.print()
  });
</script>
