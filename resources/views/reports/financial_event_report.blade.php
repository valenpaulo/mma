@extends('layout/control_panel')

@section('title')
Financial Report
@endsection

@section('middle')
<div class="w3-container w3-margin">
  <div class="row">
    <h3>
      <i class="fa fa-files-o fa-fw w3-xxlarge"></i>
      <strong>MTICS Financial Report</strong>
    </h3>
  </div>

  <hr>

  <div class="row w3-margin-top">
    <div class="table-responsive">
      <table class="table table-hover table-bordered" id="eventTable">
      <thead>
        <tr>
         <th class="w3-center">Event Title</th>
         <th class="w3-center">Event Fee</th>
         <th class="w3-center">Start Date</th>
         <th class="w3-center">End Date</th>
         <th class="w3-center">Action</th>

        </tr>
      </thead>
      <tbody class="w3-text-gray">
        @foreach($events as $event)
        <tr>
          <td class="w3-center">{{$event->event_title}}</td>
          <td class="w3-center">{{$event->event_amount}}</td>
          <td class="w3-center">{{$event->start_date}}</td>
          <td class="w3-center">{{$event->end_date}}</td>
          <td class="w3-center">
            
            <form action="{{ url('admin/finance/financial-reports/event/'.$event->id.'/view-reports/print') }}" method="POST" enctype="multipart/form-data" >
              <button type="submit" class="btn btn-default w3-teal w3-text-white"><i class="fa fa-eye fa-fw"></i> View</button>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </form>
          </td>
        </tr>
        @endforeach
      </tbody>
      </table>
    </div>
  </div>
</div>
@endsection
