@extends('layout/login')

@section('title')
MMA Sign In
@endsection

@section('content')
<div class="w3-container">
    <div class="w3-display-middle w3-card-4 w3-white w3-col m4">
        <div class="w3-container w3-center w3-text-white" style="display: 100%; background-color: #2471A3">
            <h4><b>MTICS</b></h4>
        </div>

        <div class="w3-container w3-margin-top">
            <form action="#" method="post">
              <div class="form-group">
                <label for="email">Username:</label>
                <input type="text" class="form-control" id="uname" name="uname" required>
              </div>
              <div class="form-group">
                <label for="pwd">Password:</label>
                <input type="password" class="form-control" id="pwd" name="pwd" required>
              </div>

              <div class="form-group">
                <p>
                    <a href="{{ route('signup') }}" class="w3-teal">Sign Up</a>
                </p>
              </div>

              <button type="submit" class="btn btn-primary w3-text-white w3-margin-bottom">Sign In</button>
            </form>
            <br>
        </div>

    </div>
</div>
@endsection
