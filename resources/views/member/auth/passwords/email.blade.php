@extends('layout/login')

@section('title')
Reset Password
@endsection

@section('content')
<div class="w3-container">
    <div class="w3-display-middle w3-card-4 w3-white w3-col m4">
        <div class="w3-container w3-margin-top">
            <div class="w3-container w3-text-black w3-center">
            <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/logo/mtics.png') : asset('images/logo/mtics.png')}}" class="w3-margin-bottom w3-margin-top" style="height: 100px; width: 100px">
            <h4>Manila Technician Institute Computer Society</h4>
            </div>
            <br>

            <p class="w3-text-gray" align="center"><strong>Reset Password</strong></p>

            <hr>

            <form action="{{ url('/member/password/email') }}" method="post">

              {{ csrf_field() }}

              <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="username">Email</label>
                <input type="text" class="form-control" id="email" name="email" required value="{{old('email')}}" autofocus>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
              </div>

              <div class="form-group" align="center">
                <button type="submit" class="btn btn-default w3-text-white w3-teal" style="background-color: #2471A3">Send Password Reset Link</button>
              </div>
            </form>
            <br>
        </div>

    </div>
</div>

<script type="text/javascript">
  $(window).on('load', function() {
    if("{{session('status')}}" != ""){
      swal("{{session('statu')}}", "Congratulations!", "success")
    }
  });
</script>
@endsection

