@extends('layout/login')

@section('title')
MTICS SIGN UP
@endsection

@section('content')

<div class="container">
    <div class="row w3-margin-top" align="center">
      <a href="{{route('login')}}">
      <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/logo/mtics.png') : asset('images/logo/mtics.png')}}" style="height: 100px; width: 100px">
      </a>
      <h1> Let's Get Started</h1>
    </div>

    <br>

    <form action="{{ url('/member/register') }}" method="post">
    {{ csrf_field() }}

    <div class="row">
      <div class="col-sm-12 col-md-6 col-lg-6 w3-margin-bottom">
        <div class="w3-card-4 w3-white w3-padding">
          <div class="w3-container">
            <div class="row">
              <div class="form-group">
                <p class="w3-text-gray"><b>Please fill up all fields mark with <span class="w3-text-red">*</span></b></p>
                <hr>
                <p class="w3-text-gray"><b>Personal Information</b></p>
              </div>

              <div class="form-group{{ $errors->has('age') ? ' has-error' : '' }}">
                <label>Age</label>
                <input type="number" min="0" class="form-control" id="age" name="age" placeholder="Optional" value="{{ old('age') }}">

                @if ($errors->has('age'))
                  <span class="help-block w3-text-red">
                   <strong>{{ $errors->first('age') }}</strong>
                  </span>
                @endif
              </div>


              <div class="form-group">
                <label for="bday">Birthday</label>
                <input type="date" class="form-control" id="birthday" name="birthday" value="{{ old('birthday') }}">
              </div>


              <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                <label>Address <span class="w3-text-red">*</span></label>
                <input type="text" class="form-control" id="address" name="address" value="{{ old('address') }}" required>
                @if ($errors->has('address'))
                  <span class="help-block w3-text-red">
                   <strong>{{ $errors->first('address') }}</strong>
                  </span>
                @endif
              </div>

              <div class="form-group">
                <label for="work">Work</label>
                <input type="text" class="form-control" id="work" name="work" placeholder="Optional" value="{{ old('work') }}">
              </div>

              <hr>

              <div class="form-group">
                <p class="w3-text-gray"><b>Contact Information</b></p>
              </div>

              <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} email-form-group">
                <label>Email Address <span class="w3-text-red">*</span></label>
                <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}" required>
                <span class="help-block email w3-text-red" style="display: {{$errors->has('email') ? 'block' : 'none' }}">
                 <strong>{{ $errors->first('email') }}</strong>
                </span>
              </div>

              <div class="form-group{{ $errors->has('mobile_no') ? ' has-error' : '' }} mobile-form-group">
                <label>Mobile No <span class="w3-text-red">*</span></label>
                <input type="number" class="form-control" id="mobile_no" name="mobile_no" value="{{ old('mobile_no') }}" required>
                <span class="help-block mobile_no w3-text-red" style="display: {{$errors->has('mobile_no') ? 'block' : 'none' }}">
                 <strong>{{ $errors->first('mobile_no') }}</strong>
                </span>
              </div>

              <hr>

              <div class="form-group">
                <p class="w3-text-gray"><b>In case of emergency</b></p>
              </div>

              <div class="form-group">
                <label for="guard">Name of the Guardian</label>
                <input type="text" class="form-control" id="guardian_name" name="guardian_name" placeholder="Optional" value="{{ old('guardian_name') }}">
              </div>

              <div class="form-group{{ $errors->has('guardian_mobile_no') ? ' has-error' : '' }}">
                <label>Mobile No</label>
                <input type="number" class="form-control" id="guardian_mobile_no" name="guardian_mobile_no" placeholder="Optional" value="{{ old('guardian_mobile_no') }}">
                @if ($errors->has('guardian_mobile_no'))
                  <span class="help-block w3-text-red">
                   <strong>{{ $errors->first('guardian_mobile_no') }}</strong>
                  </span>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-sm-12 col-md-6 col-lg-6 w3-margin-bottom">
        <div class="w3-card-4 w3-white w3-padding">
          <div class="w3-container">
            <div class="row">
              <br>
              <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                <label>First Name <span class="w3-text-red">*</span></label>
                <input type="text" class="form-control" id="first_name" name="first_name" value="{{ old('first_name') }}" required>
                @if ($errors->has('first_name'))
                  <span class="help-block w3-text-red">
                   <strong>{{ $errors->first('first_name') }}</strong>
                  </span>
                @endif
              </div>


              <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                <label>Last Name <span class="w3-text-red">*</span></label>
                <input type="text" class="form-control" id="last_name" name="last_name" value="{{ old('last_name') }}" required>
                @if ($errors->has('last_name'))
                  <span class="help-block w3-text-red">
                   <strong>{{ $errors->first('last_name') }}</strong>
                  </span>
                @endif
              </div>

              <div class="form-group">
                <label>Member Type <span class="w3-text-red">*</span></label>
                <div class="radio w3-center w3-padding">
                  <label><input type="radio" name="type" value="Student" checked> Student</label>
                  <label><input type="radio" name="type" value="Alumni"> Alumni</label>
                  <label><input type="radio" name="type" value="Faculty"> Faculty</label>
                </div>
              </div>

              <!-- start student div -->
              <div class="student">
              <div class="form-group{{ $errors->has('id_num') ? ' has-error' : '' }} id_num-form-group">
                <label>Student Id <span class="w3-text-red">*</span></label>
                <input type="text" class="form-control" id="id_num" name="id_num" value="{{ old('id_num') }}" required>
                <span class="help-block stud-id w3-text-red" style="display: {{$errors->has('id_num') ? 'block' : 'none' }}">
                 <strong>{{ $errors->first('id_num') }}</strong>
                </span>
              </div>

              <div class="form-group{{ $errors->has('year') ? ' has-error' : '' }}">
                <label>Year</label>
                  <select class="form-control" id="year" name="year" required placeholder="Select your year">
                   @foreach($years as $year)

                    <option value='{{$year->id}}'>{{$year->year_desc}}</option>

                   @endforeach
                  </select>
                  @if ($errors->has('year'))
                  <span class="help-block w3-text-red">
                   <strong>{{ $errors->first('year') }}</strong>
                  </span>
                @endif
              </div>

              <div class="form-group{{ $errors->has('section') ? ' has-error' : '' }}">
                <label>Section</label>
                  <select class="form-control" id="section" name="section" required placeholder="Select your Section">
                   @foreach($sections as $section)

                        <option value='{{$section->id}}'>{{$section->course_code}} {{$section->section_code}}</option>

                   @endforeach
                  </select>
                  @if ($errors->has('section'))
                  <span class="help-block w3-text-red">
                   <strong>{{ $errors->first('section') }}</strong>
                  </span>
                @endif
              </div>

              <div class="form-group{{ $errors->has('section') ? ' has-error' : '' }}">
                <select class="form-control" id="stud_type" name="stud_type" required placeholder="Select your type">
                  <option value='Regular'>Regular</option>
                  <option value='Irregular'>Irregular</option>
                </select>
              </div>

              </div>
              <!-- end student div -->


              <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }} username-form-group">
                <label>Username <span class="w3-text-red">*</span></label>
                <input type="text" class="form-control" id="username" name="username" value="{{ old('username') }}" required>
                <span class="help-block username w3-text-red" style="display: {{$errors->has('username') ? 'block' : 'none' }}">
                 <strong>{{ $errors->first('username') }}</strong>
                </span>
              </div>

              <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label>Password <span class="w3-text-red">*</span></label>
                <input type="password" class="form-control" id="password" name="password" required>
                @if ($errors->has('password'))
                  <span class="help-block w3-text-red">
                   <strong>{{ $errors->first('password') }}</strong>
                  </span>
                @endif
              </div>

               <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <label>Confirm Password <span class="w3-text-red">*</span></label>
                <input type="password" class="form-control" id="password-confirm" name="password_confirmation" required>
                @if ($errors->has('password_confirmation'))
                 <span class="help-block w3-text-red">
                   <strong>{{ $errors->first('password_confirmation') }}</strong>
                 </span>
                @endif
              </div>

              <button type="submit" class="btn btn-default w3-orange w3-text-white w3-margin-bottom">Sign Up</button>

            </div>
          </div>
        </div>
      </div>
    </div>

    </form>

</div>


<script type="text/javascript">
  $(document).ready(function() {
   $('input[type="radio"]').click(function() {
       if($(this).val() == 'Student') {
            $('.student').show();
            $('#id_num').attr('required', "");
       }

       else {
            $('.student').hide();
            $('#id_num').removeAttr('required');
       }
   });
});
</script>

<script type="text/javascript">
  $("#username").blur(function() {
    if ($("#username").val() != "") {
      $.ajax({
        url: '{{url("/check/username")}}',
        data: {'username': $("#username").val()},
        type: 'GET',
        success: function(response) {
          if (response == '1') {
              $("#username").val("");
              $(".username").show();
              $(".username-form-group").attr('class', 'form-group has-error username-form-group');
              $(".username").html("username is already taken")
          }

          else {
              $(".username").hide();
              $(".username-form-group").attr('class', 'form-group username-form-group');
          }
        }
      });
    }
  });
</script>

<script type="text/javascript">
  $("#id_num").blur(function() {
    if ($("#id_num").val() != "") {
      $.ajax({
        url: '{{url("/check/student-id")}}',
        data: {'student_id': $("#id_num").val()},
        type: 'GET',
        success: function(response) {
          if (response == '1') {
              $("#id_num").val("");
              $(".stud-id").show();
              $(".stud-id").html("student id is already taken")
              $(".id_num-form-group").attr('class', 'form-group has-error id_num-form-group');
          }

          else {
              $(".stud-id").hide();
              $(".id_num-form-group").attr('class', 'form-group id_num-form-group-form-group');
          }
        }
      });
    }
  });
</script>

<script type="text/javascript">
  $("#mobile_no").blur(function() {
    if ($("#mobile_no").val() != "") {
      $.ajax({
        url: '{{url("/check/mobile")}}',
        data: {'mobile_no': $("#mobile_no").val()},
        type: 'GET',
        success: function(response) {
          if (response == '1') {
              $("#mobile_no").val("");
              $(".mobile_no").show();
              $(".mobile_no").html("mobile no. is already taken")
              $(".mobile-form-group").attr('class', 'form-group has-error mobile-form-group');

          }

          else {
              $(".mobile_no").hide();
              $(".mobile-form-group").attr('class', 'form-group mobile-form-group');
          }
        }
      });
    }
  });
</script>

<script type="text/javascript">
  $("#email").blur(function() {
    if ($("#email").val() != "") {
      $.ajax({
        url: '{{url("/check/email")}}',
        data: {'email': $("#email").val()},
        type: 'GET',
        success: function(response) {
          if (response == '1') {
              $("#email").val("");
              $(".email").show();
              $(".email").html("email is already taken")
              $(".email-form-group").attr('class', 'form-group has-error email-form-group');
          }

          else {
              $(".email").hide();
              $(".email-form-group").attr('class', 'form-group email-form-group');
          }
        }
      });
    }
  });
</script>
@endsection
