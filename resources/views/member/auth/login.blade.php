@extends('layout/login')

@section('title')
MTICS Sign In
@endsection

@section('content')
<div class="w3-container">
    <div class="w3-display-middle w3-card-4 w3-white w3-col m4">
        <div class="w3-container w3-margin-top">
            <div class="w3-container w3-text-black w3-center">
            <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/logo/mtics.png') : asset('images/logo/mtics.png')}}" class="w3-margin-bottom w3-margin-top" style="height: 100px; width: 100px">
            <h4>Manila Technician Institute Computer Society</h4>
            </div>
            <br>
            <hr>
            <form action="#" method="post">

              {{ csrf_field() }}

              <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                <label for="username">Username</label>
                <input type="text" class="form-control" id="username" name="username" required value="{{old('username')}}" autofocus>
                @if ($errors->has('username'))
                    <span class="help-block">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                @endif
              </div>

              <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password">Password</label>
                <input type="password" class="form-control" id="password" name="password" required>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
                </div>

              <div class="form-group">
                <button type="submit" class="btn btn-default w3-text-white w3-orange" style="background-color: #2471A3">Sign In</button>
                    <a href="{{ url('/member/register') }}" style="color: #2471A3">Sign Up</a>
              </div>

              <div class="form-group">
                  <a class="btn btn-link" href="{{ url('/member/password/reset') }}" style="color: #2471A3">
                    Forgot Your Password?
                  </a>
              </div>
            </form>
            <br>
        </div>

    </div>
</div>
@endsection

