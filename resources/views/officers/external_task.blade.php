@extends('admin/layout/admin_panel')

@section('middle')


<div class="col-md-4">


 <label>task:</label><br>

	 @foreach($tasks as $task)
	 Task Id: {{$task->id}}<br>
	 Due Date: {{$task->due_date}}<br>
	 Event Id: {{$task->event_id}}<br>
	 Task Name: {{$task->task_name}}<br>
	 Description: {{$task->task_desc}}<br>
	 Status: {{$task->task_status}}<br>
	 ...........<br>


		<button class="btn btn-default w3-orange w3-text-white" data-toggle="modal" data-target="#edit-{{$task->id}}" title="Edit"><i class="fa fa-pencil"></i> Edit</button>

		<a href="{{ url('admin/external/task/'.$task->id) }}"><button class="btn btn-default w3-orange w3-text-white" title="Request"><i class="fa fa-pencil"></i> view</button></a>
		<button class="btn btn-default w3-orange w3-text-white" data-toggle="modal" data-target="#request-{{$task->id}}" title="Edit"><i class="fa fa-pencil"></i> Request Money</button>

		<!-- REQUEST MONEY -->
		<div class="modal fade" id="request-{{$task->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog modal-sm" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
		        <h4 class="modal-title w3-text-gray" id="myModalLabel">REQUEST MONEY</h4>
		      </div>
		  <form action="{{ url('admin/external/task/'.$task->id.'/request') }}" method="POST">
		      <div class="modal-body">

		  <b>you are about to request money for this task</b>

		           Are you sure you want to request money for task {{$task->id}}?
		           <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="remarks" placeholder="remarks" rows="3"></textarea>

		        <div class="modal-footer">
		          <input type="hidden" name="_token" value="{{ csrf_token() }}">
		          <button type="submit" class="btn btn-default w3-red"><i class="fa fa-plus-square fa-fw"></i> Yes</button>
		        </div>

		      </div>
		    </form>
		    </div>
		  </div>
		</div>


<br><br><br>


<div class="modal fade" id="edit-{{$task->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
        <h4 class="modal-title w3-text-gray" id="myModalLabel">Add Profanity Word</h4>
      </div>
    <form action="{{ url('admin/external/task/'.$task->id.'/edit') }}" method="POST" >
      <div class="modal-body">

 	@if($task->mticspurchaselist)
		 @foreach ($task->mticspurchaselist as $mticspurchaselist)

	          Item Name: {{$mticspurchaselist->mtics_itemname}}<br>
	          Item Description: {{$mticspurchaselist->mtics_itemdesc}}<br>
	          Item Quantity: {{$mticspurchaselist->mtics_orig_quan}}<br>
	          Item Status: {{$mticspurchaselist->mtics_itemstatus}}<br>
	          Item Estimated Price: {{$mticspurchaselist->mtics_est_price}}<br>

       		  <input type="number" name="est_amt[{{$mticspurchaselist->id}}][price]" id="est_amt" tabindex="1" class="form-control" placeholder="Estimated Amount"><br><br>


		 @endforeach
	 @endif


	 @if($task->eventpurchaselist)
		 @foreach ($task->eventpurchaselist as $eventpurchaselist)

	          Item Name: {{$eventpurchaselist->event_itemname}}<br>
	          Item Description: {{$eventpurchaselist->event_itemdesc}}<br>
	          Item Quantity: {{$eventpurchaselist->event_orig_quan}}<br>
	          Item Status: {{$eventpurchaselist->event_itemstatus}}<br>
	          Item Estimated Price: {{$eventpurchaselist->event_est_price}}<br>

       		  <input type="number" name="est_amt[{{$eventpurchaselist->id}}][price]" id="est_amt" tabindex="1" class="form-control" placeholder="Estimated Amount"><br><br>


		 @endforeach
	 @endif




        <div class="modal-footer">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <button type="submit" class="btn btn-default w3-red"><i class="fa fa-plus-square fa-fw"></i> Add</button>
        </div>

      </div>
    </form>
    </div>
  </div>
</div>



	 @endforeach

</div>



@endsection
