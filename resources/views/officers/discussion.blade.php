@extends('layout/control_panel')

@section('title')
Discussion of the Meeting
@endsection

@section('middle')
<div class="container-fluid w3-margin">
  <div class="row">
    <h3>
      <i class="fa fa-comments fa-fw"></i>
      <strong>Discussion</strong>
    </h3>

  </div>


  <div class="row w3-margin-top w3-margin-bottom">
    <a href="javascript:void(0)" onclick="openCity(event, 'Event');">
      <div class="col-lg-1 col-md-1 col-sm-12 tablink w3-bottombar w3-hover-light-grey w3-padding w3-text-black w3-border-red">
      <label>Finance</label>
      </div>
    </a>

    <a href="javascript:void(0)" onclick="openCity(event, 'Meeting');">
      <div class="col-lg-1 col-md-1 col-sm-12 tablink w3-bottombar w3-hover-light-grey w3-padding w3-text-black">
      <label>External</label>
      </div>
    </a>

    <a href="javascript:void(0)" onclick="openCity(event, 'Meeting');">
      <div class="col-lg-1 col-md-1 col-sm-12 tablink w3-bottombar w3-hover-light-grey w3-padding w3-text-black">
      <label>Logistic</label>
      </div>
    </a>

    <a href="javascript:void(0)" onclick="openCity(event, 'Meeting');">
      <div class="col-lg-1 col-md-1 col-sm-12 tablink w3-bottombar w3-hover-light-grey w3-padding w3-text-black">
      <label>Activities</label>
      </div>
    </a>

    <a href="javascript:void(0)" onclick="openCity(event, 'Meeting');">
      <div class="col-lg-1 col-md-1 col-sm-12 tablink w3-bottombar w3-hover-light-grey w3-padding w3-text-black">
      <label>Information</label>
      </div>
    </a>

    <a href="javascript:void(0)" onclick="openCity(event, 'Meeting');">
      <div class="col-lg-1 col-md-1 col-sm-12 tablink w3-bottombar w3-hover-light-grey w3-padding w3-text-black">
      <label>Internal</label>
      </div>
    </a>

    <a href="javascript:void(0)" onclick="openCity(event, 'Meeting');">
      <div class="col-lg-6 col-md-6 col-sm-12 tablink w3-bottombar w3-hover-light-grey w3-padding w3-text-black">
      <label>Documentation</label>
      </div>
    </a>
  </div>
</div>

@endsection
