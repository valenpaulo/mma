@extends('layout/control_panel')

@section('title')
{{$event->event_title}}
@endsection

@section('middle')
<div class="container-fluid w3-margin">

  <div class="row">
    <h3>
      <i class="fa fa-calendar fa-fw w3-xxlarge"></i>
      <strong>Manage Event</strong>
    </h3>
  </div>

  <hr>

  <div class="row" align="center">
    <h2>
      <a href="#edit" class="w3-hover-opacity w3-text-black" data-toggle="modal">
      {{$event->event_title}}
      </a>
    </h2>
    <p>
      {{strip_tags($event->description)}}
    </p>
  </div>

  <div class="row">
    <ul class="nav nav-tabs">
      <li class="active">
        <a data-toggle="tab" href="#task" class="w3-text-black">Tasks</a>
      </li>

      <li>
        <a data-toggle="tab" href="#meeting" class="w3-text-black">Meeting</a>
      </li>

      <li>
        <a data-toggle="tab" href="#participants" class="w3-text-black">Participants</a>
      </li>
    </ul>
  </div>


  <div class="tab-content">
    <div id="task" class="tab-pane fade in active">
      <div class="row w3-margin-top">
        <p>
          <a href="#add-task" data-toggle="modal" class="w3-text-gray" style="outline: 0"><i class="fa fa-plus-square-o fa-fw"></i> Add Task</a>
          <i class="fa fa-ellipsis-v fa-fw"></i>

          <a class="w3-text-gray" href="{{ url('admin/president/manage-event/'.$event->id.'/failed-task') }}" class="w3-text-gray" style="outline: 0"><i class="fa fa-balance-scale fa-fw"></i> Manage Penalty @if($penalties > 0)<span class="w3-badge w3-red">{{$penalties}}</span>@endif</a>
        </p>
      </div>

      <!-- EDIT MODAL -->
      <div class="modal fade" id="edit" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
        <h4 class="modal-title w3-text-gray" id="myModalLabel">Edit Event</h4>
        </div>
        <form action="{{ url('admin/president/manage-event/edit/'.$event->id) }}" method="POST" enctype="multipart/form-data">

        <div class="modal-body">
          <div class="w3-container">
            <div class="row">

            <div class="form-group">
              <label for="title">Event Title:</label>
              <input type="text" name="event_title" class="form-control" placeholder="Event Title" value="{{$event->event_title}}">
            </div>

            <div class="form-group">
              <label for="description">Description:</label>
              <input type="text" name="description" class="form-control" placeholder="Event Description" value="{{$event->description}}">
            </div>

            <div class="form-group">
              <label for="start_date">Start Date:</label>
              <input type="date" name="start_date" class="form-control">
            </div>

            <div class="form-group">
              <label for="end_date">End Date:</label>
              <input type="date" name="end_date" class="form-control">
            </div>

            </div>
          </div>
        </div>

        <div class="modal-footer">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
        </div>

        </form>
        </div>
        </div>
      </div>
      <!-- END MODAL -->

      <!-- ADD MODAL -->
      <div class="modal fade" id="add-task" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
        <h4 class="modal-title w3-text-gray" id="myModalLabel">Add Task</h4>
        </div>
        <form action="{{ url('admin/president/manage-task/store/'.$event->id) }}" method="POST" enctype="multipart/form-data">

        <div class="modal-body">
          <div class="w3-container">
            <div class="row">

              <div class="form-group">
                <label>Officers:</label>

                <select class="form-control" id="addTask_role" name="role_id" placeholder="Select Role" onchange="showExternalTask()">
                 @foreach($roles as $role)
                  <option value='{{$role->id}}'>{{$role->officer_name}}</option>
                 @endforeach
                </select>
              </div>

              <div class="form-group">
                <label>Due Date: </label>
                <input type="date" class="form-control" id="due_date" name="due_date">
              </div>

              <div class="form-group">
                <label>Task Name: </label>
                <input type="text" name="task_name" id="task_name" tabindex="1" class="form-control" placeholder="Task Name">
              </div>

              <div class="form-group">
                <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="task_desc" placeholder="Description" rows="3"></textarea>
              </div>

              <!-- EXTERNAL TASK -->
              <div id="external_task" style="display: none">

                <h4>
                  <i class="fa fa-shopping-basket fa-fw"></i>
                  Purchase List
                  <i class="fa fa-plus-square fa-fw w3-text-gray w3-hover-text-green" data-toggle="tooltip" id="add_item" title="Add Item"></i>
                </h4>

                <div>

                <div class="form-group">
                  <label>Item Name: </label>
                  <input type="text" name="itemname[]" id="itemname" tabindex="1" class="form-control" placeholder="Item Name">
                </div>

                <div class="form-group">
                  <label>Quantity: </label>
                   <input type="number" name="itemquan[]" id="item_quan" tabindex="1" class="form-control" placeholder="Item Quantity">
                </div>

                <div class="form-group">
                  <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="itemdesc[]" placeholder="Item Description" rows="3"></textarea>
                </div>

                </div>

              </div>
              <!-- END EXTERNAL TASK -->

              <input type="hidden" name="_token" value="{{ csrf_token() }}">

            </div>
          </div>
        </div>

        <div class="modal-footer">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
        </div>

        </form>
        </div>
        </div>
      </div>
      <!-- END MODAL -->

      <div class="row w3-margin-top">
        <ul class="nav nav-tabs">
          <li class="active">
            <a data-toggle="tab" href="#external" class="w3-text-black">External @if($externalCount > 0)<span class="w3-badge w3-red">{{$externalCount}}</span>@endif</a>
          </li>

          <li>
            <a data-toggle="tab" href="#finance" class="w3-text-black">Finance
              @if($finance_count > 0)<span class="w3-badge w3-red">{{$finance_count}}</span>@endif</a>
          </li>

          <li>
            <a data-toggle="tab" href="#logistic" class="w3-text-black">Logistics
             @if($logistic_count > 0)<span class="w3-badge w3-red">{{$logistic_count}}</span>@endif</a>
          </li>

          <li>
            <a data-toggle="tab" href="#auditor" class="w3-text-black">Auditor
             @if($auditor_count > 0)<span class="w3-badge w3-red">{{$auditor_count}}</span>@endif</a>
          </li>

          <li>
            <a data-toggle="tab" href="#internal" class="w3-text-black">Internal
            </a>
          </li>

          <li>
            <a data-toggle="tab" href="#document" class="w3-text-black">Documentation @if(count($tobe_validated_documents) > 0)<span class="w3-badge w3-red">{{count($tobe_validated_documents)}}</span>@endif</a>
          </li>

          <li>
            <a data-toggle="tab" href="#activities" class="w3-text-black">Activities
             @if($activity_count > 0)<span class="w3-badge w3-red">{{$activity_count}}</span>@endif</a>
          </li>

          <li>
            <a data-toggle="tab" href="#info" class="w3-text-black">Information
             @if($info_count > 0)<span class="w3-badge w3-red">{{$info_count}}</span>@endif</a>
          </li>
        </ul>
      </div>

      <div class="tab-content">
        <div id="external" class="tab-pane fade in active">

          <div class="row w3-margin-top">
            <div class="table-responsive">
            <table class="table table-hover table-bordered" id="externalTable">
              <thead>
                <tr>
                 <th class="w3-center">Task Name</th>
                 <th class="w3-center">Description</th>
                 <th class="w3-center">Due Date</th>
                 <th class="w3-center">Status</th>
                 <th class="w3-center">Action</th>
                </tr>
              </thead>
              <tbody class="w3-text-gray">
                @foreach($external_tasks as $task)
                <tr>
                  <td class="w3-center">{{$task->task_name}}</td>
                  <td class="w3-center">{{$task->task_desc}}</td>
                  <td class="w3-center">{{$task->due_date}}</td>
                  <td class="w3-center">{{$task->task_status}}</td>
                  <td class="w3-center">
                    @if ($task->task_status == 'pending')
                    <a class="w3-text-orange" data-toggle="modal" data-target="#edit-external-{{$task->id}}"><i class="fa fa-pencil fa-fw w3-large" data-toggle="tooltip" title="Edit"></i></a>

                    <a class="w3-text-red" data-toggle="modal" data-target="#cancel-external-{{$task->id}}"><i class="fa fa-remove fa-fw w3-large" data-toggle="tooltip" title="Cancel"></i></a>
                    @elseif($task->task_status == 'ongoing')
                    <i class="fa fa-spinner fa-fw w3-large w3-text-gray" data-toggle="tooltip" title="on-going"></i>
                    @elseif($task->task_status == 'done')
                    <i class="fa fa-check-circle-o fa-fw w3-large w3-text-green" data-toggle="tooltip" title="Done"></i>
                    @elseif($task->task_status == 'failed')
                    <i class="fa fa-thumbs-o-down fa-fw w3-large w3-text-red" data-toggle="tooltip" title="Failed"></i>
                    @endif

                  </td>
                </tr>

                <!-- EXTERNAl EDIT MODAL -->
                <div class="modal fade" id="edit-external-{{$task->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-md" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Edit Task</h4>
                  </div>
                  <form action="{{ url('admin/president/manage-task/edit/'.$task->id) }}" method="POST" enctype="multipart/form-data">

                  <div class="modal-body">
                    <div class="w3-container">
                      <div class="row">

                        <div class="form-group">
                          <label>Officers:</label>

                          <select class="form-control" id="role" name="role_id" placeholder="Select Role">
                           @foreach($roles as $role)
                            @if($task->role_id == $role->id)
                            <option value='{{$role->id}}' selected>{{$role->officer_name}}</option>
                            @else
                            <option value='{{$role->id}}'>{{$role->officer_name}}</option>
                            @endif
                           @endforeach
                          </select>
                        </div>

                        <div class="form-group">
                          <label>Due Date: </label>
                          <input type="date" class="form-control" id="due_date" name="due_date" value="{{$task->due_date}}">
                        </div>

                        <div class="form-group">
                          <label>Task Name: </label>
                          <input type="text" name="task_name" id="task_name" tabindex="1" class="form-control" placeholder="Task Name" value="{{$task->task_name}}">
                        </div>

                        <div class="form-group">
                          <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="task_desc" placeholder="Description" rows="3">{{$task->task_desc}}</textarea>
                        </div>

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="event_id" value="{{ $event->id }}">

                      </div>
                    </div>
                  </div>

                  <div class="modal-footer">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
                  </div>

                  </form>
                  </div>
                  </div>
                </div>
                <!-- END MODAL -->

                <!-- EXTERNAL REMOVE MODAL -->
                <div class="modal fade" id="cancel-external-{{$task->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-md" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Edit Task</h4>
                  </div>
                  <form action="{{ url('admin/president/manage-task/delete/'.$task->id) }}" method="POST" enctype="multipart/form-data">

                  <div class="modal-body">
                    <div class="w3-container">
                      <div class="row">

                        <p>Are you sure you want to cancel this task?</p>

                      </div>
                    </div>
                  </div>

                  <div class="modal-footer">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="btn btn-default w3-green">Yes</button>
                    <button type="button" class="btn btn-default w3-red" data-dismiss="modal">No</button>
                  </div>

                  </form>
                  </div>
                  </div>
                </div>
                <!-- END MODAL -->

                @endforeach
              </tbody>
            </table>
            </div>
          </div>

        </div>

        <div id="finance" class="tab-pane fade in">
          <div class="row w3-margin-top">
             <div class="table-responsive">
              <table class="table table-hover table-bordered" id="financeTable">
                <thead>
                  <tr>
                   <th class="w3-center">Task Name</th>
                   <th class="w3-center">Description</th>
                   <th class="w3-center">Due Date</th>
                   <th class="w3-center">Status</th>
                   <th class="w3-center">Action</th>
                  </tr>
                </thead>
                <tbody class="w3-text-gray">
                  @foreach($finance_tasks as $task)
                  <tr>
                    <td class="w3-center">{{$task->task_name}}</td>
                    <td class="w3-center">{{$task->task_desc}}</td>
                    <td class="w3-center">{{$task->due_date}}</td>
                    <td class="w3-center">{{$task->task_status}}</td>
                    <td class="w3-center">
                      @if ($task->task_status == 'pending')
                      <a class="w3-text-orange" data-toggle="modal" data-target="#edit-finance-{{$task->id}}"><i class="fa fa-pencil fa-fw w3-large" data-toggle="tooltip" title="Edit"></i></a>

                      <a class="w3-text-red" data-toggle="modal" data-target="#cancel-finance-{{$task->id}}"><i class="fa fa-remove fa-fw w3-large" data-toggle="tooltip" title="Cancel"></i></a>
                      @elseif($task->task_status == 'for validation')
                      <a class="w3-text-green" href="{{url('admin/president/manage-event/approve/' . $task->id)}}"><i class="fa fa-thumbs-up fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Approve"></i></a>

                      <a class="w3-text-red" href="#disapprove-{{$task->id}}" data-toggle="modal"><i class="fa fa-thumbs-down fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Disapprove"></i></a>
                      @else()
                      <i class="fa fa-pencil fa-fw w3-large" data-toggle="tooltip" title="Disabled"></i>
                      <i class="fa fa-remove fa-fw w3-large" data-toggle="tooltip" title="Disabled"></i>

                      @endif
                    </td>

                  </tr>


                  <!-- DISAPPROVE MODAL -->
                  <div class="modal fade" id="disapprove-{{$task->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-md" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Disapproval</h4>
                  </div>
                  <form action="{{url('/admin/president/manage-event/disapprove/' . $task->id)}}" method='Post' enctype="multipart/form-data">
                  <div class="modal-body">
                    <div class="w3-container">
                      <div class="row">

                        <div class="form-group">
                          <label>Reason:</label>
                          <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="reason" rows="5" placeholder="Reason for Disapproval"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="modal-footer">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <button type="submit" class="btn btn-default w3-green w3-text-white"><i class="fa fa-reply fa-fw"></i> Submit</button>
                  </div>

                  </form>
                  </div>
                  </div>
                  </div>
                <!-- END DISAPPROVE MODAL -->


                  <!-- FINANCE EDIT MODAL -->
                  <div class="modal fade" id="edit-finance-{{$task->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-md" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Edit Task</h4>
                  </div>
                  <form action="{{ url('admin/president/manage-task/edit/'.$task->id) }}" method="POST" enctype="multipart/form-data">

                  <div class="modal-body">
                    <div class="w3-container">
                      <div class="row">

                        <div class="form-group">
                          <label>Officers:</label>

                          <select class="form-control" id="role" name="role_id" placeholder="Select Role">
                           @foreach($roles as $role)
                            @if($task->role_id == $role->id)
                            <option value='{{$role->id}}' selected>{{$role->officer_name}}</option>
                            @else
                            <option value='{{$role->id}}'>{{$role->officer_name}}</option>
                            @endif
                           @endforeach
                          </select>
                        </div>

                        <div class="form-group">
                          <label>Due Date: </label>
                          <input type="date" class="form-control" id="due_date" name="due_date" value="{{$task->due_date}}">
                        </div>

                        <div class="form-group">
                          <label>Task Name: </label>
                          <input type="text" name="task_name" id="task_name" tabindex="1" class="form-control" placeholder="Task Name" value="{{$task->task_name}}">
                        </div>

                        <div class="form-group">
                          <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="task_desc" placeholder="Description" rows="3">{{$task->task_desc}}</textarea>
                        </div>

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="event_id" value="{{ $event->id }}">

                      </div>
                    </div>
                  </div>

                  <div class="modal-footer">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
                  </div>

                  </form>
                  </div>
                  </div>
                  </div>
                  <!-- END MODAL -->

                  <!-- FINANCE REMOVE MODAL -->
                  <div class="modal fade" id="cancel-finance-{{$task->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-md" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Edit Task</h4>
                  </div>
                  <form action="{{ url('admin/president/manage-task/delete/'.$task->id) }}" method="POST" enctype="multipart/form-data">

                  <div class="modal-body">
                    <div class="w3-container">
                      <div class="row">

                        <p>Are you sure you want to cancel this task?</p>

                      </div>
                    </div>
                  </div>

                  <div class="modal-footer">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="btn btn-default w3-green">Yes</button>
                    <button type="button" class="btn btn-default w3-red" data-dismiss="modal">No</button>
                  </div>

                  </form>
                  </div>
                  </div>
                  </div>
                  <!-- END MODAL -->
                  @endforeach
                </tbody>
              </table>
              </div>

          </div>
        </div>

        <div id="info" class="tab-pane fade in">
           <div class="row w3-margin-top">
              <div class="table-responsive">
                <table class="table table-hover table-bordered" id="logisticTable">
                  <thead>
                    <tr>
                     <th class="w3-center">Task Name</th>
                     <th class="w3-center">Description</th>
                     <th class="w3-center">Due Date</th>
                     <th class="w3-center">Status</th>
                     <th class="w3-center">Action</th>
                    </tr>
                  </thead>
                  <tbody class="w3-text-gray">
                    @foreach($info_tasks as $task)
                    <tr>
                      <td class="w3-center">{{$task->task_name}}</td>
                      <td class="w3-center">{{$task->task_desc}}</td>
                      <td class="w3-center">{{$task->due_date}}</td>
                      <td class="w3-center">{{$task->task_status}}</td>
                      <td class="w3-center">
                        @if ($task->task_status == 'pending')
                        <a class="w3-text-orange" data-toggle="modal" data-target="#edit-logistic-{{$task->id}}"><i class="fa fa-pencil fa-fw w3-large" data-toggle="tooltip" title="Edit"></i></a>

                        <a class="w3-text-red" data-toggle="modal" data-target="#cancel-logistic-{{$task->id}}"><i class="fa fa-remove fa-fw w3-large" data-toggle="tooltip" title="Cancel"></i></a>
                        @elseif($task->task_status == 'for validation')
                        <a class="w3-text-green" href="{{url('admin/president/manage-event/approve/' . $task->id)}}"><i class="fa fa-thumbs-up fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Approve"></i></a>

                        <a class="w3-text-red" href="#disapprove-{{$task->id}}" data-toggle="modal"><i class="fa fa-thumbs-down fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Disapprove"></i></a>
                        @else()
                        <i class="fa fa-pencil fa-fw w3-large" data-toggle="tooltip" title="Disabled"></i>
                        <i class="fa fa-remove fa-fw w3-large" data-toggle="tooltip" title="Disabled"></i>
                        @endif
                      </td>
                    </tr>


                      <!-- DISAPPROVE MODAL -->
                    <div class="modal fade" id="disapprove-{{$task->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-md" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                    <h4 class="modal-title w3-text-gray" id="myModalLabel">Disapproval</h4>
                    </div>
                    <form action="{{url('/admin/president/manage-event/disapprove/' . $task->id)}}" method='Post' enctype="multipart/form-data">
                    <div class="modal-body">
                      <div class="w3-container">
                        <div class="row">

                          <div class="form-group">
                            <label>Reason:</label>
                            <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="reason" rows="5" placeholder="Reason for Disapproval"></textarea>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="modal-footer">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="btn btn-default w3-green w3-text-white"><i class="fa fa-reply fa-fw"></i> Submit</button>
                    </div>

                    </form>
                    </div>
                    </div>
                    </div>
                  <!-- END DISAPPROVE MODAL -->



                    <!-- LOGISTIC EDIT MODAL -->
                    <div class="modal fade" id="edit-logistic-{{$task->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-md" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                    <h4 class="modal-title w3-text-gray" id="myModalLabel">Edit Task</h4>
                    </div>
                    <form action="{{ url('admin/president/manage-task/edit/'.$task->id) }}" method="POST" enctype="multipart/form-data">

                    <div class="modal-body">
                      <div class="w3-container">
                        <div class="row">

                          <div class="form-group">
                            <label>Officers:</label>

                            <select class="form-control" id="role" name="role_id" placeholder="Select Role">
                             @foreach($roles as $role)
                              @if($task->role_id == $role->id)
                              <option value='{{$role->id}}' selected>{{$role->officer_name}}</option>
                              @else
                              <option value='{{$role->id}}'>{{$role->officer_name}}</option>
                              @endif
                             @endforeach
                            </select>
                          </div>

                          <div class="form-group">
                            <label>Due Date: </label>
                            <input type="date" class="form-control" id="due_date" name="due_date" value="{{$task->due_date}}">
                          </div>

                          <div class="form-group">
                            <label>Task Name: </label>
                            <input type="text" name="task_name" id="task_name" tabindex="1" class="form-control" placeholder="Task Name" value="{{$task->task_name}}">
                          </div>

                          <div class="form-group">
                            <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="task_desc" placeholder="Description" rows="3">{{$task->task_desc}}</textarea>
                          </div>

                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                          <input type="hidden" name="event_id" value="{{ $event->id }}">

                        </div>
                      </div>
                    </div>

                    <div class="modal-footer">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
                    </div>

                    </form>
                    </div>
                    </div>
                    </div>
                    <!-- END MODAL -->

                    <!-- LOGISTIC REMOVE MODAL -->
                    <div class="modal fade" id="cancel-logistic-{{$task->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-md" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                    <h4 class="modal-title w3-text-gray" id="myModalLabel">Edit Task</h4>
                    </div>
                    <form action="{{ url('admin/president/manage-task/delete/'.$task->id) }}" method="POST" enctype="multipart/form-data">

                    <div class="modal-body">
                      <div class="w3-container">
                        <div class="row">

                          <p>Are you sure you want to cancel this task?</p>

                        </div>
                      </div>
                    </div>

                    <div class="modal-footer">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <button type="submit" class="btn btn-default w3-green">Yes</button>
                      <button type="button" class="btn btn-default w3-red" data-dismiss="modal">No</button>
                    </div>

                    </form>
                    </div>
                    </div>
                    </div>
                    <!-- END MODAL -->
                    @endforeach
                  </tbody>
                </table>
                </div>
          </div>
        </div>


        <div id="logistic" class="tab-pane fade in">
          <div class="row w3-margin-top">
              <div class="table-responsive">
                <table class="table table-hover table-bordered" id="logisticTable">
                  <thead>
                    <tr>
                     <th class="w3-center">Task Name</th>
                     <th class="w3-center">Description</th>
                     <th class="w3-center">Due Date</th>
                     <th class="w3-center">Status</th>
                     <th class="w3-center">Action</th>
                    </tr>
                  </thead>
                  <tbody class="w3-text-gray">
                    @foreach($logistic_tasks as $task)
                    <tr>
                      <td class="w3-center">{{$task->task_name}}</td>
                      <td class="w3-center">{{$task->task_desc}}</td>
                      <td class="w3-center">{{$task->due_date}}</td>
                      <td class="w3-center">{{$task->task_status}}</td>
                      <td class="w3-center">
                        @if ($task->task_status == 'pending')
                        <a class="w3-text-orange" data-toggle="modal" data-target="#edit-logistic-{{$task->id}}"><i class="fa fa-pencil fa-fw w3-large" data-toggle="tooltip" title="Edit"></i></a>

                        <a class="w3-text-red" data-toggle="modal" data-target="#cancel-logistic-{{$task->id}}"><i class="fa fa-remove fa-fw w3-large" data-toggle="tooltip" title="Cancel"></i></a>
                        @elseif($task->task_status == 'for validation')
                        <a class="w3-text-green" href="{{url('admin/president/manage-event/approve/' . $task->id)}}"><i class="fa fa-thumbs-up fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Approve"></i></a>

                        <a class="w3-text-red" href="#disapprove-{{$task->id}}" data-toggle="modal"><i class="fa fa-thumbs-down fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Disapprove"></i></a>
                        @else()
                        <i class="fa fa-pencil fa-fw w3-large" data-toggle="tooltip" title="Disabled"></i>
                        <i class="fa fa-remove fa-fw w3-large" data-toggle="tooltip" title="Disabled"></i>
                        @endif
                      </td>
                    </tr>


                      <!-- DISAPPROVE MODAL -->
                    <div class="modal fade" id="disapprove-{{$task->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-md" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                    <h4 class="modal-title w3-text-gray" id="myModalLabel">Disapproval</h4>
                    </div>
                    <form action="{{url('/admin/president/manage-event/disapprove/' . $task->id)}}" method='Post' enctype="multipart/form-data">
                    <div class="modal-body">
                      <div class="w3-container">
                        <div class="row">

                          <div class="form-group">
                            <label>Reason:</label>
                            <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="reason" rows="5" placeholder="Reason for Disapproval"></textarea>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="modal-footer">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="btn btn-default w3-green w3-text-white"><i class="fa fa-reply fa-fw"></i> Submit</button>
                    </div>

                    </form>
                    </div>
                    </div>
                    </div>
                  <!-- END DISAPPROVE MODAL -->



                    <!-- LOGISTIC EDIT MODAL -->
                    <div class="modal fade" id="edit-logistic-{{$task->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-md" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                    <h4 class="modal-title w3-text-gray" id="myModalLabel">Edit Task</h4>
                    </div>
                    <form action="{{ url('admin/president/manage-task/edit/'.$task->id) }}" method="POST" enctype="multipart/form-data">

                    <div class="modal-body">
                      <div class="w3-container">
                        <div class="row">

                          <div class="form-group">
                            <label>Officers:</label>

                            <select class="form-control" id="role" name="role_id" placeholder="Select Role">
                             @foreach($roles as $role)
                              @if($task->role_id == $role->id)
                              <option value='{{$role->id}}' selected>{{$role->officer_name}}</option>
                              @else
                              <option value='{{$role->id}}'>{{$role->officer_name}}</option>
                              @endif
                             @endforeach
                            </select>
                          </div>

                          <div class="form-group">
                            <label>Due Date: </label>
                            <input type="date" class="form-control" id="due_date" name="due_date" value="{{$task->due_date}}">
                          </div>

                          <div class="form-group">
                            <label>Task Name: </label>
                            <input type="text" name="task_name" id="task_name" tabindex="1" class="form-control" placeholder="Task Name" value="{{$task->task_name}}">
                          </div>

                          <div class="form-group">
                            <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="task_desc" placeholder="Description" rows="3">{{$task->task_desc}}</textarea>
                          </div>

                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                          <input type="hidden" name="event_id" value="{{ $event->id }}">

                        </div>
                      </div>
                    </div>

                    <div class="modal-footer">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
                    </div>

                    </form>
                    </div>
                    </div>
                    </div>
                    <!-- END MODAL -->

                    <!-- LOGISTIC REMOVE MODAL -->
                    <div class="modal fade" id="cancel-logistic-{{$task->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-md" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                    <h4 class="modal-title w3-text-gray" id="myModalLabel">Edit Task</h4>
                    </div>
                    <form action="{{ url('admin/president/manage-task/delete/'.$task->id) }}" method="POST" enctype="multipart/form-data">

                    <div class="modal-body">
                      <div class="w3-container">
                        <div class="row">

                          <p>Are you sure you want to cancel this task?</p>

                        </div>
                      </div>
                    </div>

                    <div class="modal-footer">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <button type="submit" class="btn btn-default w3-green">Yes</button>
                      <button type="button" class="btn btn-default w3-red" data-dismiss="modal">No</button>
                    </div>

                    </form>
                    </div>
                    </div>
                    </div>
                    <!-- END MODAL -->
                    @endforeach
                  </tbody>
                </table>
                </div>
          </div>
        </div>

        <div id="auditor" class="tab-pane fade in">
          <div class="row w3-margin-top">
            <div class="table-responsive">
              <table class="table table-hover table-bordered" id="auditorTable">
                <thead>
                  <tr>
                   <th class="w3-center">Task Name</th>
                   <th class="w3-center">Description</th>
                   <th class="w3-center">Due Date</th>
                   <th class="w3-center">Status</th>
                   <th class="w3-center">Action</th>
                  </tr>
                </thead>
                <tbody class="w3-text-gray">
                  @foreach($auditor_tasks as $task)
                  <tr>
                    <td class="w3-center">{{$task->task_name}}</td>
                    <td class="w3-center">{{$task->task_desc}}</td>
                    <td class="w3-center">{{$task->due_date}}</td>
                    <td class="w3-center">{{$task->task_status}}</td>
                    <td class="w3-center">
                      @if ($task->task_status == 'pending')
                      <a class="w3-text-orange" data-toggle="modal" data-target="#edit-auditor-{{$task->id}}"><i class="fa fa-pencil fa-fw w3-large" data-toggle="tooltip" title="Edit"></i></a>

                      <a class="w3-text-red" data-toggle="modal" data-target="#cancel-auditor-{{$task->id}}"><i class="fa fa-remove fa-fw w3-large" data-toggle="tooltip" title="Cancel"></i></a>
                      @elseif($task->task_status == 'for validation')
                      <a class="w3-text-green" href="{{url('admin/president/manage-event/approve/' . $task->id)}}"><i class="fa fa-thumbs-up fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Approve"></i></a>

                      <a class="w3-text-red" href="#disapprove-{{$task->id}}" data-toggle="modal"><i class="fa fa-thumbs-down fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Disapprove"></i></a>
                      @else()
                      <i class="fa fa-pencil fa-fw w3-large" data-toggle="tooltip" title="Disabled"></i>
                      <i class="fa fa-remove fa-fw w3-large" data-toggle="tooltip" title="Disabled"></i>
                      @endif
                    </td>
                  </tr>


                  <!-- DISAPPROVE MODAL -->
                  <div class="modal fade" id="disapprove-{{$task->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-md" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Disapproval</h4>
                  </div>
                  <form action="{{url('/admin/president/manage-event/disapprove/' . $task->id)}}" method='Post' enctype="multipart/form-data">
                  <div class="modal-body">
                    <div class="w3-container">
                      <div class="row">

                        <div class="form-group">
                          <label>Reason:</label>
                          <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="reason" rows="5" placeholder="Reason for Disapproval"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="modal-footer">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <button type="submit" class="btn btn-default w3-green w3-text-white"><i class="fa fa-reply fa-fw"></i> Submit</button>
                  </div>

                  </form>
                  </div>
                  </div>
                  </div>
                <!-- END DISAPPROVE MODAL -->


                  <!-- AUDITOR EDIT MODAL -->
                  <div class="modal fade" id="edit-auditor-{{$task->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-md" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Edit Task</h4>
                  </div>
                  <form action="{{ url('admin/president/manage-task/edit/'.$task->id) }}" method="POST" enctype="multipart/form-data">

                  <div class="modal-body">
                    <div class="w3-container">
                      <div class="row">

                        <div class="form-group">
                          <label>Officers:</label>

                          <select class="form-control" id="role" name="role_id" placeholder="Select Role">
                           @foreach($roles as $role)
                            @if($task->role_id == $role->id)
                            <option value='{{$role->id}}' selected>{{$role->officer_name}}</option>
                            @else
                            <option value='{{$role->id}}'>{{$role->officer_name}}</option>
                            @endif
                           @endforeach
                          </select>
                        </div>

                        <div class="form-group">
                          <label>Due Date: </label>
                          <input type="date" class="form-control" id="due_date" name="due_date" value="{{$task->due_date}}">
                        </div>

                        <div class="form-group">
                          <label>Task Name: </label>
                          <input type="text" name="task_name" id="task_name" tabindex="1" class="form-control" placeholder="Task Name" value="{{$task->task_name}}">
                        </div>

                        <div class="form-group">
                          <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="task_desc" placeholder="Description" rows="3">{{$task->task_desc}}</textarea>
                        </div>

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="event_id" value="{{ $event->id }}">

                      </div>
                    </div>
                  </div>

                  <div class="modal-footer">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
                  </div>

                  </form>
                  </div>
                  </div>
                  </div>
                  <!-- END MODAL -->

                  <!-- AUDITOR REMOVE MODAL -->
                  <div class="modal fade" id="cancel-auditor-{{$task->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-md" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Edit Task</h4>
                  </div>
                  <form action="{{ url('admin/president/manage-task/delete/'.$task->id) }}" method="POST" enctype="multipart/form-data">

                  <div class="modal-body">
                    <div class="w3-container">
                      <div class="row">

                        <p>Are you sure you want to cancel this task?</p>

                      </div>
                    </div>
                  </div>

                  <div class="modal-footer">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="btn btn-default w3-green">Yes</button>
                    <button type="button" class="btn btn-default w3-red" data-dismiss="modal">No</button>
                  </div>

                  </form>
                  </div>
                  </div>
                  </div>
                  <!-- END MODAL -->
                  @endforeach
                </tbody>
              </table>
              </div>

          </div>
        </div>

        <div id="internal" class="tab-pane fade in">
          <div class="row w3-margin-top">
            <div class="table-responsive">
              <table class="table table-hover table-bordered" id="internalTable">
                <thead>
                  <tr>
                   <th class="w3-center">Task Name</th>
                   <th class="w3-center">Description</th>
                   <th class="w3-center">Due Date</th>
                   <th class="w3-center">Status</th>
                   <th class="w3-center">Action</th>
                  </tr>
                </thead>
                <tbody class="w3-text-gray">
                  @foreach($internal_tasks as $task)
                  <tr>
                    <td class="w3-center">{{$task->task_name}}</td>
                    <td class="w3-center">{{$task->task_desc}}</td>
                    <td class="w3-center">{{$task->due_date}}</td>
                    <td class="w3-center">{{$task->task_status}}</td>
                    <td class="w3-center">
                      @if ($task->task_status == 'pending')
                      <a class="w3-text-orange" data-toggle="modal" data-target="#edit-internal-{{$task->id}}"><i class="fa fa-pencil fa-fw w3-large" data-toggle="tooltip" title="Edit"></i></a>

                      <a class="w3-text-red" data-toggle="modal" data-target="#cancel-internal-{{$task->id}}"><i class="fa fa-remove fa-fw w3-large" data-toggle="tooltip" title="Cancel"></i></a>
                      @elseif($task->task_status == 'done')
                      <i class="fa fa-check-circle-o fa-fw w3-large w3-text-green" data-toggle="tooltip" title="Done"></i>
                      @else()
                      <i class="fa fa-spinner fa-fw w3-large" data-toggle="tooltip" title="On-going"></i>
                      @endif
                    </td>
                  </tr>
                  <!-- INTERNAL EDIT MODAL -->
                  <div class="modal fade" id="edit-internal-{{$task->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-md" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Edit Task</h4>
                  </div>
                  <form action="{{ url('admin/president/manage-task/edit/'.$task->id) }}" method="POST" enctype="multipart/form-data">

                  <div class="modal-body">
                    <div class="w3-container">
                      <div class="row">

                        <div class="form-group">
                          <label>Officers:</label>

                          <select class="form-control" id="role" name="role_id" placeholder="Select Role">
                           @foreach($roles as $role)
                            @if($task->role_id == $role->id)
                            <option value='{{$role->id}}' selected>{{$role->officer_name}}</option>
                            @else
                            <option value='{{$role->id}}'>{{$role->officer_name}}</option>
                            @endif
                           @endforeach
                          </select>
                        </div>

                        <div class="form-group">
                          <label>Due Date: </label>
                          <input type="date" class="form-control" id="due_date" name="due_date" value="{{$task->due_date}}">
                        </div>

                        <div class="form-group">
                          <label>Task Name: </label>
                          <input type="text" name="task_name" id="task_name" tabindex="1" class="form-control" placeholder="Task Name" value="{{$task->task_name}}">
                        </div>

                        <div class="form-group">
                          <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="task_desc" placeholder="Description" rows="3">{{$task->task_desc}}</textarea>
                        </div>

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="event_id" value="{{ $event->id }}">

                      </div>
                    </div>
                  </div>

                  <div class="modal-footer">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
                  </div>

                  </form>
                  </div>
                  </div>
                  </div>
                  <!-- END MODAL -->

                  <!-- INTERNAL REMOVE MODAL -->
                  <div class="modal fade" id="cancel-internal-{{$task->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-md" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Edit Task</h4>
                  </div>
                  <form action="{{ url('admin/president/manage-task/delete/'.$task->id) }}" method="POST" enctype="multipart/form-data">

                  <div class="modal-body">
                    <div class="w3-container">
                      <div class="row">

                        <p>Are you sure you want to cancel this task?</p>

                      </div>
                    </div>
                  </div>

                  <div class="modal-footer">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="btn btn-default w3-green">Yes</button>
                    <button type="button" class="btn btn-default w3-red" data-dismiss="modal">No</button>
                  </div>

                  </form>
                  </div>
                  </div>
                  </div>
                  <!-- END MODAL -->
                  @endforeach
                </tbody>
              </table>
              </div>

              <div class="w3-margin">

                <h4>
                  <i class="fa fa-file-word-o fa-fw"></i>
                  Letters
                </h4>

                <div class="w3-container">
                  @foreach($documents as $document)
                  <div class="col-sm-3 w3-center">

                  <div class="w3-card-4 w3-pale-yellow w3-margin">
                    <div class="w3-container w3-padding">

                    <div align="right">
                      <a class="w3-text-green" href="{{url('admin/internal/download/'.$document->id)}}"><i class="fa fa-cloud-download fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Download"></i></a>

                      <a class="w3-text-red" href="#delete-{{$document->id}}" data-toggle="modal"><i class="fa fa-trash-o fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Delete"></i></a>
                    </div>

                    <hr>

                    <div class="w3-center">
                    <h1>
                     <i class="fa fa-file-word-o  w3-text-black"></i>
                    </h1>
                    </div>

                    <a href="">
                    <div class="w3-center w3-text-black">
                    <a href="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/documents/'.$document->filename) : asset('documents/'.$document->filename)}}" target="_blank" class="w3-text-black">
                      <p>{{explode("/", $document->filename)[3]}}</p>
                    </a>
                    <p>Task: {{$document->task->task_name}}</p>
                    </div>
                    </a>

                    </div>

                  </div>
                </div>

                @endforeach

                </div>

              </div>
          </div>
        </div>

        <div id="document" class="tab-pane fade in">
          <div class="row w3-margin-top">
            <div class="table-responsive">
              <table class="table table-hover table-bordered" id="documentTable">
                <thead>
                  <tr>
                   <th class="w3-center">Task Name</th>
                   <th class="w3-center">Description</th>
                   <th class="w3-center">Due Date</th>
                   <th class="w3-center">Status</th>
                   <th class="w3-center">Action</th>
                  </tr>
                </thead>
                <tbody class="w3-text-gray">
                  @foreach($docu_tasks as $task)
                  <tr>
                    <td class="w3-center">{{$task->task_name}}</td>
                    <td class="w3-center">{{$task->task_desc}}</td>
                    <td class="w3-center">{{$task->due_date}}</td>
                    <td class="w3-center">{{$task->task_status}}</td>
                    <td class="w3-center">
                      @if ($task->task_status == 'pending')
                      <a class="w3-text-orange" data-toggle="modal" data-target="#edit-docu-{{$task->id}}"><i class="fa fa-pencil fa-fw w3-large" data-toggle="tooltip" title="Edit"></i></a>

                      <a class="w3-text-red" data-toggle="modal" data-target="#cancel-docu-{{$task->id}}"><i class="fa fa-remove fa-fw w3-large" data-toggle="tooltip" title="Cancel"></i></a>
                      @elseif($task->task_status == 'for validation')
                      <a class="w3-text-green" href="{{url('admin/president/manage-event/approve/' . $task->id)}}"><i class="fa fa-thumbs-up fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Approve"></i></a>

                      <a class="w3-text-red" href="#disapprove-{{$task->id}}" data-toggle="modal"><i class="fa fa-thumbs-down fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Disapprove"></i></a>
                      @elseif($task->task_status == 'done')
                      <i class="fa fa-check-circle-o fa-fw w3-large w3-text-green" data-toggle="tooltip" title="Done"></i>
                      @else()
                      <i class="fa fa-spinner fa-fw w3-large" data-toggle="tooltip" title="On-going"></i>
                      @endif
                    </td>
                  </tr>

                     <!-- DISAPPROVE MODAL -->
                  <div class="modal fade" id="disapprove-{{$task->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-md" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Disapproval</h4>
                  </div>
                  <form action="{{url('/admin/president/manage-event/disapprove/' . $task->id)}}" method='Post' enctype="multipart/form-data">
                  <div class="modal-body">
                    <div class="w3-container">
                      <div class="row">

                        <div class="form-group">
                          <label>Reason:</label>
                          <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="reason" rows="5" placeholder="Reason for Disapproval"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="modal-footer">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <button type="submit" class="btn btn-default w3-green w3-text-white"><i class="fa fa-reply fa-fw"></i> Submit</button>
                  </div>

                  </form>
                  </div>
                  </div>
                  </div>
                <!-- END DISAPPROVE MODAL -->



                  <!-- DOCU EDIT MODAL -->
                  <div class="modal fade" id="edit-docu-{{$task->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-md" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Edit Task</h4>
                  </div>
                  <form action="{{ url('admin/president/manage-task/edit/'.$task->id) }}" method="POST" enctype="multipart/form-data">

                  <div class="modal-body">
                    <div class="w3-container">
                      <div class="row">

                        <div class="form-group">
                          <label>Officers:</label>

                          <select class="form-control" id="role" name="role_id" placeholder="Select Role">
                           @foreach($roles as $role)
                            @if($task->role_id == $role->id)
                            <option value='{{$role->id}}' selected>{{$role->officer_name}}</option>
                            @else
                            <option value='{{$role->id}}'>{{$role->officer_name}}</option>
                            @endif
                           @endforeach
                          </select>
                        </div>

                        <div class="form-group">
                          <label>Due Date: </label>
                          <input type="date" class="form-control" id="due_date" name="due_date" value="{{$task->due_date}}">
                        </div>

                        <div class="form-group">
                          <label>Task Name: </label>
                          <input type="text" name="task_name" id="task_name" tabindex="1" class="form-control" placeholder="Task Name" value="{{$task->task_name}}">
                        </div>

                        <div class="form-group">
                          <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="task_desc" placeholder="Description" rows="3">{{$task->task_desc}}</textarea>
                        </div>

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="event_id" value="{{ $event->id }}">

                      </div>
                    </div>
                  </div>

                  <div class="modal-footer">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
                  </div>

                  </form>
                  </div>
                  </div>
                  </div>
                  <!-- END MODAL -->

                  <!-- DOCU REMOVE MODAL -->
                  <div class="modal fade" id="cancel-docu-{{$task->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-md" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Edit Task</h4>
                  </div>
                  <form action="{{ url('admin/president/manage-task/delete/'.$task->id) }}" method="POST" enctype="multipart/form-data">

                  <div class="modal-body">
                    <div class="w3-container">
                      <div class="row">

                        <p>Are you sure you want to cancel this task?</p>

                      </div>
                    </div>
                  </div>

                  <div class="modal-footer">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="btn btn-default w3-green">Yes</button>
                    <button type="button" class="btn btn-default w3-red" data-dismiss="modal">No</button>
                  </div>

                  </form>
                  </div>
                  </div>
                  </div>
                  <!-- END MODAL -->
                  @endforeach
                </tbody>
              </table>
              </div>

                @if(count($tobe_validated_documents) > 0)
                <h4>
                  <i class="fa fa-spinner fa-fw"></i>
                  To Be Validated
                </h4>

                <div class="w3-container">
                  @foreach($tobe_validated_documents as $document)
                  <div class="col-sm-3 w3-center">

                  <div class="w3-card-4 w3-pale-yellow w3-margin">
                    <div class="w3-container w3-padding">

                    <div align="right">
                      <a class="w3-text-green" href="{{url('admin/internal/download/'.$document->id)}}"><i class="fa fa-cloud-download fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Download"></i></a>
                    </div>

                    <hr>

                    <div class="w3-center">
                    <h1>
                     <i class="fa fa-file-word-o  w3-text-black"></i>
                    </h1>
                    </div>

                    <a href="">
                    <div class="w3-center w3-text-black">
                    <a href="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/documents/'.$document->filename) : asset('documents/'.$document->filename)}}" target="_blank" class="w3-text-black">
                      <p>{{explode("/", $document->filename)[3]}}</p>
                    </a>
                    <p>Task: {{$document->task->task_name}}</p>
                    </div>
                    </a>

                    </div>

                  </div>
                </div>

                @endforeach

                </div>
                @endif

                <h4>
                  <i class="fa fa-file-word-o fa-fw"></i>
                  Documents
                </h4>

                @if(count($documents) > 0)
                <div class="w3-container">
                  @foreach($documents as $document)
                  <div class="col-sm-3 w3-center">

                  <div class="w3-card-4 w3-pale-yellow w3-margin">
                    <div class="w3-container w3-padding">

                    <div align="right">
                      <a class="w3-text-green" href="{{url('admin/internal/download/'.$document->id)}}"><i class="fa fa-cloud-download fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Download"></i></a>
                    </div>

                    <hr>

                    <div class="w3-center">
                    <h1>
                     <i class="fa fa-file-word-o  w3-text-black"></i>
                    </h1>
                    </div>

                    <a href="">
                    <div class="w3-center w3-text-black">
                    <a href="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/documents/'.$document->filename) : asset('documents/'.$document->filename)}}" target="_blank" class="w3-text-black">
                      <p>{{explode("/", $document->filename)[3]}}</p>
                    </a>
                    <p>Task: {{$document->task->task_name}}</p>
                    </div>
                    </a>

                    </div>

                  </div>
                </div>

                @endforeach

                </div>
                @endif
          </div>
        </div>

        <div id="activities" class="tab-pane fade in">
          <div class="row w3-margin-top">
            <div class="table-responsive">
              <table class="table table-hover table-bordered" id="activityTable">
                <thead>
                  <tr>
                   <th class="w3-center">Task Name</th>
                   <th class="w3-center">Description</th>
                   <th class="w3-center">Due Date</th>
                   <th class="w3-center">Status</th>
                   <th class="w3-center">Action</th>
                  </tr>
                </thead>
                <tbody class="w3-text-gray">
                  @foreach($activity_tasks as $task)
                  <tr>
                    <td class="w3-center">{{$task->task_name}}</td>
                    <td class="w3-center">{{$task->task_desc}}</td>
                    <td class="w3-center">{{$task->due_date}}</td>
                    <td class="w3-center">{{$task->task_status}}</td>
                    <td class="w3-center">
                      @if ($task->task_status == 'pending')
                      <a class="w3-text-orange" data-toggle="modal" data-target="#edit-activity-{{$task->id}}"><i class="fa fa-pencil fa-fw w3-large" data-toggle="tooltip" title="Edit"></i></a>

                      <a class="w3-text-red" data-toggle="modal" data-target="#cancel-activity-{{$task->id}}"><i class="fa fa-remove fa-fw w3-large" data-toggle="tooltip" title="Cancel"></i></a>
                      @elseif($task->task_status == 'for validation')
                      <a class="w3-text-green" href="{{url('admin/president/manage-event/approve/' . $task->id)}}"><i class="fa fa-thumbs-up fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Approve"></i></a>

                      <a class="w3-text-red" href="#disapprove-{{$task->id}}" data-toggle="modal"><i class="fa fa-thumbs-down fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Disapprove"></i></a>
                      @else()
                      <i class="fa fa-pencil fa-fw w3-large" data-toggle="tooltip" title="Disabled"></i>
                      <i class="fa fa-remove fa-fw w3-large" data-toggle="tooltip" title="Disabled"></i>
                      @endif
                    </td>
                  </tr>

                   <!-- DISAPPROVE MODAL -->
                  <div class="modal fade" id="disapprove-{{$task->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-md" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Disapproval</h4>
                  </div>
                  <form action="{{url('/admin/president/manage-event/disapprove/' . $task->id)}}" method='Post' enctype="multipart/form-data">
                  <div class="modal-body">
                    <div class="w3-container">
                      <div class="row">

                        <div class="form-group">
                          <label>Reason:</label>
                          <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="reason" rows="5" placeholder="Reason for Disapproval"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="modal-footer">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <button type="submit" class="btn btn-default w3-green w3-text-white"><i class="fa fa-reply fa-fw"></i> Submit</button>
                  </div>

                  </form>
                  </div>
                  </div>
                  </div>
                <!-- END DISAPPROVE MODAL -->


                  <!-- ACTIVITY EDIT MODAL -->
                  <div class="modal fade" id="edit-activity-{{$task->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-md" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Edit Task</h4>
                  </div>
                  <form action="{{ url('admin/president/manage-task/edit/'.$task->id) }}" method="POST" enctype="multipart/form-data">

                  <div class="modal-body">
                    <div class="w3-container">
                      <div class="row">

                        <div class="form-group">
                          <label>Officers:</label>

                          <select class="form-control" id="role" name="role_id" placeholder="Select Role">
                           @foreach($roles as $role)
                            @if($task->role_id == $role->id)
                            <option value='{{$role->id}}' selected>{{$role->officer_name}}</option>
                            @else
                            <option value='{{$role->id}}'>{{$role->officer_name}}</option>
                            @endif
                           @endforeach
                          </select>
                        </div>

                        <div class="form-group">
                          <label>Due Date: </label>
                          <input type="date" class="form-control" id="due_date" name="due_date" value="{{$task->due_date}}">
                        </div>

                        <div class="form-group">
                          <label>Task Name: </label>
                          <input type="text" name="task_name" id="task_name" tabindex="1" class="form-control" placeholder="Task Name" value="{{$task->task_name}}">
                        </div>

                        <div class="form-group">
                          <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="task_desc" placeholder="Description" rows="3">{{$task->task_desc}}</textarea>
                        </div>

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="event_id" value="{{ $event->id }}">

                      </div>
                    </div>
                  </div>

                  <div class="modal-footer">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
                  </div>

                  </form>
                  </div>
                  </div>
                  </div>
                  <!-- END MODAL -->

                  <!-- ACTIVITY REMOVE MODAL -->
                  <div class="modal fade" id="cancel-activity-{{$task->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-md" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Edit Task</h4>
                  </div>
                  <form action="{{ url('admin/president/manage-task/delete/'.$task->id) }}" method="POST" enctype="multipart/form-data">

                  <div class="modal-body">
                    <div class="w3-container">
                      <div class="row">

                        <p>Are you sure you want to cancel this task?</p>

                      </div>
                    </div>
                  </div>

                  <div class="modal-footer">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="btn btn-default w3-green">Yes</button>
                    <button type="button" class="btn btn-default w3-red" data-dismiss="modal">No</button>
                  </div>

                  </form>
                  </div>
                  </div>
                  </div>
                  <!-- END MODAL -->
                  @endforeach
                </tbody>
              </table>
              </div>

          </div>
        </div>

      </div>
    </div>

    <div id="meeting" class="tab-pane fade in">
      <div class="row w3-margin-top">
        <p>
          <a href="#add-meeting" data-toggle="modal" class="w3-text-gray" style="outline: 0"><i class="fa fa-plus-square-o fa-fw"></i> Add Meeting</a>
        </p>
      </div>

      <!-- ADD MODAL -->
      <div class="modal fade" id="add-meeting" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
        <h4 class="modal-title w3-text-gray" id="myModalLabel">Add Meeting</h4>
        </div>
        <form action="{{ url('admin/president/manage-meeting/store/'.$event->id) }}" method="POST" enctype="multipart/form-data">

        <div class="modal-body">
          <div class="w3-container">
            <div class="row">

            <div class="from-group">
              <label>Agenda</label>
              <input type="text" name="agenda" class="form-control" placeholder="Agenda of the meeting">
            </div>

            <div class="from-group">
              <label>Where</label>
              <input type="text" name="where" class="form-control" placeholder="Meeting Place">
            </div>

            <div class="from-group">
              <label>When</label>
              <input type="datetime-local" name="when" class="form-control" placeholder="Date and Time of the Meeting">
            </div>

            </div>
          </div>
        </div>

        <div class="modal-footer">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <button type="submit" class="btn btn-default w3-green"><i class="fa fa-plus-square fa-fw"></i> Add</button>
        </div>

        </form>
        </div>
        </div>
      </div>
      <!-- END MODAL -->

      <div class="row w3-margin-top">
        <div class="table-responsive">
          <table class="table table-hover table-bordered" id="meetingTable">
          <thead>
            <tr>
             <th class="w3-center">Agenda</th>
             <th class="w3-center">Where</th>
             <th class="w3-center">When</th>
             <th class="w3-center">Status</th>
             <th class="w3-center">Action</th>
            </tr>
          </thead>
          <tbody class="w3-text-gray">
            @foreach($meetings as $meeting)
            <tr>
              <td class="w3-center">{{$meeting->agenda}}</td>
              <td class="w3-center">{{$meeting->where}}</td>
              <td class="w3-center">{{$meeting->when}}</td>
              <td class="w3-center">{{$meeting->status}}</td>
              <td class="w3-center">


                @if($meeting->status == 'done')
                <a href="#view-{{$meeting->id}}" data-toggle="modal" class="w3-text-blue" style="outline: 0"><i class="fa fa-eye fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="View Upload"></i></a>

                @else

                 <a data-toggle="modal" href="#edit-{{$meeting->id}}" style="outline: 0"><i class="fa fa-pencil w3-text-orange fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Edit Meeting"></i></a>

                <a data-toggle="modal" href="#cancel-{{$meeting->id}}" style="outline: 0"><i class="fa fa-trash-o w3-text-red fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Delete Meeting"></i></a>

                <a href="#done-{{$meeting->id}}" data-toggle="modal" class="w3-text-green" style="outline: 0"><i class="fa fa-thumbs-o-up fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Accomplished Meeting"></i></a>

                @endif

              </td>
            </tr>


             <!-- View Meeting MODAL -->
            <div class="modal fade" id="view-{{$meeting->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
            <h4 class="modal-title w3-text-gray" id="myModalLabel">Meeting Images</h4>
            </div>
            <form action="{{  url('admin/president/manage-meeting/done/'.$meeting->id) }}" method="POST" enctype="multipart/form-data">
            <div class="modal-body">
            <div class="w3-container">
            <div class="row">


              <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                

                <!-- Wrapper for slides -->
                <div class="carousel-inner">

                    <?php $count = 0; ?>
                  @foreach($meeting->image as $image)   
                  <?php $count++; ?>
                  @if($count == 1)               
                  <div class="item active">
                    <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/' . $image->image_name) : asset('images/' . $image->image_name)}}" alt="Los Angeles">
                  </div>
                  @else
                  <div class="item">
                    <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/' . $image->image_name) : asset('images/' . $image->image_name)}}" alt="Los Angeles">
                  </div>
                  @endif

                  @endforeach
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                  <span class="glyphicon glyphicon-chevron-left"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                  <span class="glyphicon glyphicon-chevron-right"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>


            </div>
            </div>
            </div>
            <div class="modal-footer">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
           <!--  <button type="submit" class="btn btn-default w3-green"><i class="fa fa-plus-square fa-fw"></i> Yes</button> -->
            </div>

            </form>
            </div>
            </div>
            </div>
            <!--view meeting END MODAL -->



             <!-- DONE Meeting MODAL -->
            <div class="modal fade" id="done-{{$meeting->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
            <h4 class="modal-title w3-text-gray" id="myModalLabel">Accomplished Meeting</h4>
            </div>
            <form action="{{  url('admin/president/manage-meeting/done/'.$meeting->id) }}" method="POST" enctype="multipart/form-data">
            <div class="modal-body">
            <div class="w3-container">
            <div class="row">

                   <p><b> You are about to set as done this Meeting, Do you to continue? </b></p>

                    <div class="form-group">
                        <label id="ew-receipt-image--name-{{$meeting->id}}" for="receipt-{{$meeting->id}}" style="cursor: pointer;" title="Bulk Add"><i class="fa fa-file-text fa-fw w3-text-green w3-large" ></i> Upload Image</label>
                        <input type="file" name="upload[]" id="receipt-{{$meeting->id}}" accept="image/*" style="opacity: 0; position: absolute; z-index: -1;" onchange="showuploadFileName(event, {{$meeting->id}})" multiple />
                    </div>

            </div>
            </div>
            </div>

            <div class="modal-footer">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <button type="submit" class="btn btn-default w3-green"><i class="fa fa-plus-square fa-fw"></i> Yes</button>
            </div>

            </form>
            </div>
            </div>
            </div>
            <!--DONE PENALTY END MODAL -->



            <!-- EDIT MODAL -->
            <div class="modal fade" id="edit-{{$meeting->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog modal-md" role="document">
              <div class="modal-content">
              <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
              <h4 class="modal-title w3-text-gray" id="myModalLabel">Edit Meeting</h4>
              </div>
              <form action="{{ url('admin/president/manage-meeting/edit/'.$meeting->id) }}" method="POST" enctype="multipart/form-data">

              <div class="modal-body">
                <div class="w3-container">
                  <div class="row">

                  <div class="from-group">
                    <label>Agenda</label>
                    <input type="text" name="agenda" class="form-control" placeholder="Agenda of the meeting" value="{{$meeting->agenda}}">
                  </div>

                  <div class="from-group">
                    <label>Where</label>
                    <input type="text" name="where" class="form-control" placeholder="Meeting Place" value="{{$meeting->where}}">
                  </div>

                  <div class="from-group">
                    <label>When</label>
                    <input type="datetime-local" name="when" class="form-control" placeholder="Date and Time of the Meeting">
                  </div>

                  </div>
                </div>
              </div>

              <div class="modal-footer">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
              </div>

              </form>
              </div>
              </div>
            </div>
            <!-- END MODAL -->

            <!-- CANCEL MODAL -->
            <div class="modal fade" id="cancel-{{$meeting->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog modal-md" role="document">
              <div class="modal-content">
              <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
              <h4 class="modal-title w3-text-gray" id="myModalLabel">Delete Meeting</h4>
              </div>
              <form action="{{ url('admin/president/manage-meeting/cancel/'.$meeting->id) }}" method="POST" enctype="multipart/form-data">

              <div class="modal-body">
                <div class="w3-container">
                  <div class="row">
                  <p>Are you sure you want to cancel this meeting?</p>
                  </div>
                </div>
              </div>

              <div class="modal-footer">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <button type="submit" class="btn btn-default w3-red"><i class="fa fa-remove fa-fw"></i> Cancel</button>
              </div>

              </form>
              </div>
              </div>
            </div>
            <!-- END MODAL -->

            @endforeach
          </tbody>
          </table>
          </div>
        </div>
    </div>

    <div id="participants" class="tab-pane fade in">
      <div class="row w3-margin-top">
         <p>
          <a href="#participant-{{$event->id}}" data-toggle="modal" class="w3-text-gray" style="outline: 0"><i class="fa fa-plus-square-o fa-fw"></i> Add Participants</a>
        </p>
      </div>

      <!-- PARTICIPANT ADD MODAL -->
      <div class="modal fade" id="participant-{{$event->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
        <h4 class="modal-title w3-text-gray" id="myModalLabel">Add Event</h4>
        </div>
        <form action="{{ url('admin/president/manage-event/'.$event->id.'/participant') }}" method="POST" enctype="multipart/form-data">

        <div class="modal-body">
          <div class="w3-container">
            <div class="row">

              <div class="form-group">
                 <label><input type="checkbox" value="all" id="all" name="all"> All</label>
              </div>

              <div class="courseSection">

                <div class="form-group">
                 <label>Year:</label>
                  <select class="form-control" id="year" name="year" placeholder="Select Year">
                   @foreach($years as $year)
                    <option value='{{$year->id}}'>{{$year->year_desc}}</option>
                   @endforeach
                  </select>
                </div>

                <div class="form-group w3-center">
                   <label>Course:</label><br>
                    <div class="checkbox ">
                     @foreach($courses as $course)
                      <label><input type="checkbox" value="{{$course->id}}" name="course[]">{{$course->course_code}}</label>
                    @endforeach
                    </div>
                </div>
              </div>

            </div>
          </div>
        </div>

        <div class="modal-footer">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <button type="submit" class="btn btn-default w3-green"><i class="fa fa-plus-square fa-fw"></i> Add</button>
        </div>

        </form>
        </div>
        </div>
      </div>
      <!--PARTICIPANT END MODAL -->

      <div class="row w3-margin-top">
        <div class="table-responsive">
          <table class="table table-hover table-bordered" id="participantTable">
          <thead>
            <tr>
             <th class="w3-center">Year</th>
             <th class="w3-center">Course</th>
             <th class="w3-center">Action</th>
            </tr>
          </thead>
          <tbody class="w3-text-gray">
            @foreach($participants as $participant)
            <tr>
              <td class="w3-center">{{$participant->year->year_desc}}</td>
              <td class="w3-center">{{$participant->course->course_name}}</td>
              <td class="w3-center"><a data-toggle="modal" href="#delete-{{$participant->id}}" style="outline: 0"><i class="fa fa-trash-o w3-text-red fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Delete Meeting"></i></a></td>
            </tr>


             <!-- CANCEL MODAL -->
            <div class="modal fade" id="delete-{{$participant->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog modal-md" role="document">
              <div class="modal-content">
              <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
              <h4 class="modal-title w3-text-gray" id="myModalLabel">Remove Participant</h4>
              </div>
              <form action="{{ url('admin/president/manage-event/'.$participant->id.'/participant/delete') }}" method="POST" enctype="multipart/form-data">
              
              <div class="modal-body">
                <div class="w3-container">
                  <div class="row">
                  <p>Are you sure you want to remove this participant?</p>
                  </div>
                </div>
              </div>

              <div class="modal-footer">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <button type="submit" class="btn btn-default w3-red"><i class="fa fa-remove fa-fw"></i>Yes</button>
              </div>

              </form>
              </div>
              </div>
            </div>
            <!-- END MODAL -->





            @endforeach
          </tbody>
          </table>
          </div>
        </div>
    </div>
  </div>

</div>

<script type="text/javascript">
$(document).ready( function () {
  $('#externalTable').DataTable();
  $('#financeTable').DataTable();
  $('#logisticTable').DataTable();
  $('#auditorTable').DataTable();
  $('#internalTable').DataTable();
  $('#documentTable').DataTable();
  $('#activityTable').DataTable();
  $('#meetingTable').DataTable();
  $('#participantTable').DataTable();
} );
</script>

<script>
$(document).ready(function() {

  $('#all').change(function(){
      if(this.checked)
          $('.courseSection').hide();
      else
          $('.courseSection').show();

  });
});
</script>

<script>
  $(document).ready(function() {
    var wrapper         = $("#external_task"); //Fields wrapper
    var add_button      = $("#add_item"); //Add button ID

    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
          $(wrapper).append('\
            <div>\
            <br>\
            <div class="form-group">\
              <label>Item Name: </label>\
              <input type="text" name="itemname[]" id="itemname" tabindex="1" class="form-control" placeholder="Item Name">\
            </div>\
            <div class="form-group">\
              <label>Quantity: </label>\
               <input type="number" name="itemquan[]" id="item_quan" tabindex="1" class="form-control" placeholder="Item Quantity">\
            </div>\
            <div class="form-group">\
              <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="itemdesc[]" placeholder="Item Description" rows="3"></textarea>\
            </div>\
              <a id="remove_item" href="" class="w3-text-red">\
            <p align="center">\
              <i class="fa fa-remove fa-fw"></i> Remove\
            </p>\
              </a>\
            </div>\
          '); //add input box
    });

    $(wrapper).on("click","#remove_item", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
  });
</script>

<script type="text/javascript">
  function showExternalTask() {
    var role = $('#addTask_role option:selected').text();

    if (role == 'External') {
      $('#external_task').show()
    }
  }
</script>



<script>
  // var input = document.getElementById( 'receipt' );

  // input.addEventListener('change', showFileName1);

  function showuploadFileName( event, id ) {

    // the change event gives us the input it occurred in
    var infoArea = document.getElementById( 'ew-receipt-image-file-name-' + id );
    var input = event.srcElement;

    // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
    var fileName = input.files[0].name;

    // use fileName however fits your app best, i.e. add it into a div
    infoArea.textContent = fileName;
  };
</script>

@endsection
