        Event::fire(new SystemEvent(auth::id(), 'Requested a Deposit Transaction.'));


$data = array(
        'message' => "Your deposit request has been approved.",
        'redirect' => array('role' => 'finance'),
        'origin' => 'depositApprove',
        );

        $role_id = Role::where('name', 'finance')->first()->id;
        $member = Member::whereHas('role', function($q) use ($role_id) {
            $q->where('role_id', $role_id);
        })->first();
        $member->notify(new SystemNotification($data));


id="eventWithdrawTable"
table-hover

mticsDepositTable
eventDepositTransTable
