@extends('layout/control_panel')

@section('title')
MTICS Expenses
@endsection

@section('middle')
<div class="container-fluid w3-margin">
  <div class="row">
    <h3>
      <i class="fa fa-calculator fa-fw w3-xxlarge"></i>
      <strong>Manage Expenses</strong>
    </h3>
  </div>

  <hr>

  <div class="row w3-margin-top">

    <div class="col-lg-3 col-md-3 col-sm-12"></div>

    <div class="col-lg-3 col-md-3 col-sm-12">
      <div class="w3-card-4 w3-amber w3-text-black w3-padding w3-container">

        <div class="w3-container">
          <div class="row" align="center">
            <h4>
              <i class="fa fa-money w3-xxxlarge fa-fw"></i>
              <strong>Budget</strong>
            </h4>
          </div>
          <hr style="border-color: black">
          <div class="row">
            <h4>
              <span>
                <i class="fa fa-ellipsis-v fa-fw"></i>
                <strong>{{$mticsfund_amt}} pesos</strong>
              </span>
            </h4>
          </div>
        </div>

      </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-12">
      <div class="w3-card-4 w3-teal w3-text-black w3-padding">

        <div class="w3-container">
          <div class="row" align="center">
            <h4>
              <i class="fa fa-calculator w3-xxxlarge fa-fw"></i>
              <strong>Expenses</strong>
            </h4>
          </div>

          <hr style="border-color: black">

          <div class="row">
            <h4>
              <span>
                <i class="fa fa-ellipsis-v fa-fw"></i>
                <strong>{{$totalExpenses}} pesos</strong>
              </span>
            </h4>
          </div>
        </div>

      </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-12"></div>

  </div>

  <div class="row w3-margin-top">
    <h5>
      <a href="#addExpenses" class="w3-text-gray" data-toggle="modal" style="outline: 0">
        <i class="fa fa-plus-square-o fa-fw"></i>
        Add Expenses
      </a>
    </h5>
  </div>

  <br>

  <!--ADD EXPENSES MODAL -->
  <div class="modal fade" id="addExpenses" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-md" role="document">
  <div class="modal-content">
  <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
  <h4 class="modal-title w3-text-gray" id="myModalLabel">Add Expenses</h4>
  </div>
  <form action="{{ url('admin/finance/manage-mtics-budget/expenses/store') }}" method="POST" enctype="multipart/form-data">
  <div class="modal-body">
    <div class="w3-container">
      <div class="row">

        <div class="form-group">
          <label>Receiver:</label>
          <select name="role_id" class="form-control">
          @foreach($roles as $role)
              <option value="{{$role->id}}">{{$role->officer_name}}</option>
          @endforeach
          </select>
        </div>

        <div class="form-group">
          @foreach($mticsbudgets as $mticsbudget)
          <label>
            Budget Request: {{$mticsbudget->budget_name}}<br>
          </label>
          <select name="mticsbreakdown_id" class="form-control">
            @foreach($mticsbudget->budget_breakdown as $budget_breakdown)
              @foreach($mtics_bd_cats as $mtics_bd_cat)
              @if($budget_breakdown->mtics_budget_exp_cat_id == $mtics_bd_cat->id)
              <option value="{{$budget_breakdown->id}}">{{$mtics_bd_cat->name}}</option>
              @endif
              @endforeach
            @endforeach
          </select>
          @endforeach
        </div>

        <hr>

        <div class="form-group">
          <label>Expenses Name:</label>
          <input type="text" name="expense_name" id="expense_name" tabindex="1" class="form-control" placeholder="Expense Name">
        </div>

        <div class="form-group">
          <label>Amount:</label>
          <input type="number" name="expense_amt" id="expense_amt" tabindex="1" class="form-control" placeholder="Amount">
        </div>

        <div class="form-group">
          <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="expense_desc" placeholder="Description" rows="3"></textarea>
        </div>


      </div>
    </div>
  </div>

  <div class="modal-footer">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
  </div>

  </form>
  </div>
  </div>
  </div>
  <!-- END MODAL -->

  <div class="row">
    <div class="table-responsive">
        <table class="table table-bordered table-hover" id="expensesTable">
          <thead>
            <tr>
             <th class="w3-center">Expense</th>
             <th class="w3-center">Amount</th>
             <th class="w3-center">Received by</th>
             <th class="w3-center">Approved By</th>
             <th class="w3-center">Receipt</th>
             <th class="w3-center">Action</th>
            </tr>
          </thead>
          <tbody class="w3-text-gray">
          
          @foreach($mtics_expenses as $expenses)
           <tr>
             <td class="w3-center">{{$expenses->expense_name}}</td>
             <td class="w3-center">{{$expenses->expense_amt}}</td>
             <td class="w3-center">{{$expenses->receiver->first_name}} {{$expenses->receiver->last_name}} - {{$expenses->receiverRole->officer_name}}</td>
             <td class="w3-center">{{$expenses->approvedBy->first_name}} {{$expenses->approvedBy->last_name}} - {{$expenses->approvedByRole->officer_name}}</td>

            <td class="w3-center">
            @if($expenses->receipt !== null) 
              <a href="" class="w3-text-gray" data-toggle="modal" data-target="#receiptview-{{$expenses->id}}"><img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$expenses->receipt->receipt_image) : asset('images/'.$expenses->receipt->receipt_image)}}" style="width: 100px"></a>
            @endif
            </td>
            


             <td class="w3-center"><a class="w3-text-red" href="#receipt-{{$expenses->id}}" data-toggle="modal"><i class="fa fa-upload fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Upload Receipt"></i></a></td>

               <!-- RECEIPT MODAL-->
            <div class="modal fade" id="receiptview-{{$expenses->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
            <h4 class="modal-title w3-text-gray" id="myModalLabel">Receipt</h4>
            </div>
            <form action="" method="POST" enctype="multipart/form-data">
            <div class="modal-body">
              <div class="w3-container">
                <div class="row">
                @if($expenses->receipt !== null) 
                  <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$expenses->receipt->receipt_image) : asset('images/'.$expenses->receipt->receipt_image)}}" style="width:500px">
                @endif
                </div>
              </div>
            </div>

            </form>
            </div>
            </div>
            </div>
            <!-- END MODAL -->


             <!-- UPLOAD WITHDRAW RECEIPT MODAL -->
                <div class="modal fade" id="receipt-{{$expenses->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title w3-text-gray" id="myModalLabel">Upload Receipt</h4>
                </div>
                <form action="{{ url('admin/finance/manage-mtics-budget/expenses/'.$expenses->id.'/upload-receipt') }}" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                  <div class="w3-container">
                    <div class="row">

                      <div class="form-group">
                        <label id="ew-receipt-image-file-name-{{$expenses->id}}" for="receipt{{$expenses->id}}" style="cursor: pointer;" title="Bulk Add"><i class="fa fa-file-text fa-fw w3-text-green w3-large" ></i> Upload Receipt</label>
                        <input type="file" name="receipt" id="receipt{{$expenses->id}}" accept="image/*" style="opacity: 0; position: absolute; z-index: -1;" onchange="showEventWithdrawFileName(event, {{$expenses->id}})" />
                      </div>

                    </div>
                  </div>
                </div>

                <div class="modal-footer">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
                </div>

                </form>
                </div>
                </div>
                </div>
                <!-- END MODAL -->



            </tr>
          @endforeach


         </tbody>
        </table>
    </div>
  </div>

</div>

<script type="text/javascript">
$(document).ready( function () {
  $('#expensesTable').DataTable();
} );
</script>


<script>
  // var input = document.getElementById( 'receipt' );

  // input.addEventListener('change', showFileName1);

  function showEventWithdrawFileName( event, id ) {

    // the change event gives us the input it occurred in
    var infoArea = document.getElementById( 'ew-receipt-image-file-name-' + id );
    var input = event.srcElement;

    // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
    var fileName = input.files[0].name;

    // use fileName however fits your app best, i.e. add it into a div
    infoArea.textContent = fileName;
  };
</script>
@endsection
