@extends('layout/control_panel')

@section('title')
Manage MTICS Payment
@endsection

@section('middle')
<div class="w3-container w3-margin">
  <div class="row">
    <h3>
      <i class="fa fa-money fa-fw w3-xxlarge"></i>
      <strong>Manage MTICS Payment</strong>
    </h3>
  </div>

  <hr>

  <div class="row">
    <a href="#payment" data-toggle="modal" class="w3-text-gray" style="outline: 0"><i class="fa fa-plus-square-o fa-fw"></i>Add</a>
  </div>

  <br>

  <div class="row w3-margin-top">
    <div class="table-responsive">
      <table class="table table-hover table-bordered" id="financeTable">
        <thead>
          <tr>
           <th class="w3-center">Year</th>
           <th class="w3-center">Course</th>
           <th class="w3-center">Amount</th>
           <th class="w3-center">Action</th>
          </tr>
        </thead>
        <tbody class="w3-text-gray">
          @foreach($mticsfunds as $fund)
          <tr>
            <td class="w3-center">{{$fund->year->year_desc}}</td>
            <td class="w3-center">{{$fund->course->course_code}}</td>
            <td class="w3-center">{{$fund->mticsfund_amt}}</td>
            <td class="w3-center">
              <a class="w3-text-orange" data-toggle="modal" href="#payment-{{$fund->id}}" style="outline: 0"><i class="fa fa-pencil-square-o fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Edit Payment Information"></i></a>

              <a class="w3-text-red" href="javascript:void(0)" onclick="deleteFinance( {{$fund->id}} )" style="outline: 0"><i class="fa fa-trash-o fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Delete Payment Information"></i></a>

              <form action="{{ url('admin/finance/payment/'.$fund->id.'/mtics-delete') }}" method="POST" enctype="multipart/form-data" id="formFinance-{{$fund->id}}">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              </form>
            </td>
          </tr>

          <!-- EDIT MODAL -->
          <div class="modal fade" id="payment-{{$fund->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-md" role="document">
              <div class="modal-content">
              <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
              <h4 class="modal-title w3-text-gray" id="myModalLabel">Edit Payment</h4>
              </div>
              <form action="{{ url('admin/finance/payment/'.$fund->id.'/mtics-edit') }}" method="POST" enctype="multipart/form-data">
              <div class="modal-body">
                <div class="w3-container">
                  <div class="row">

                    <div class="form-group">
                     <label>Year:</label>
                      <input type="text" class="form-control" value="{{$fund->year->year_code}} year" disabled>
                      <input type="hidden" class="form-control" value="{{$fund->year->id}}" name="year">
                    </div>

                    <div class="form-group">
                       <label>Course:</label><br>
                       <input type="text" class="form-control" value="{{$fund->course->course_code}}" disabled>
                       <input type="hidden" class="form-control" value="{{$fund->course->id}}" name="course">
                    </div>

                    <div class="form-group">
                      <label>Amount:</label>
                      <input type="number" name="mticsfund_amt" required id="mticsfund_amt" tabindex="1" class="form-control" placeholder="Payment Amount" value="{{$fund->mticsfund_amt}}">
                    </div>

                  </div>
                </div>
              </div>

              <div class="modal-footer">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <button type="submit" class="btn btn-default w3-green w3-text-white"><i class="fa fa-save fa-fw"></i> Save</button>
              </div>

              </form>
              </div>
            </div>
          </div>
          <!-- END EDIT MODAL -->
          @endforeach
        </tbody>
      </table>
    </div>
  </div>

  <!-- ADD MODAL -->
  <div class="modal fade" id="payment" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
      <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
      <h4 class="modal-title w3-text-gray" id="myModalLabel">Manage Payment</h4>
      </div>
      <form action="{{ url('admin/finance/payment/mtics-store') }}" method="POST" enctype="multipart/form-data">
      <div class="modal-body">
        <div class="w3-container">
          <div class="row">

            <div class="form-group">
             <label>Year:</label>
              <select class="form-control" id="year" name="year" placeholder="Select Year">
               @foreach($years as $year)
                <option value='{{$year->id}}'>{{$year->year_desc}}</option>
               @endforeach
              </select>
            </div>

            <div class="form-group w3-center">
               <label>Course:</label><br>
                <div class="checkbox ">
                 @foreach($courses as $course)
                  <label><input type="checkbox" value="{{$course->id}}" name="course[]">{{$course->course_code}}</label>
                @endforeach
                </div>
            </div>

            <div class="form-group">
              <label>Amount:</label>
              <input type="number" name="mticsfund_amt" required id="mticsfund_amt" tabindex="1" class="form-control" placeholder="Payment Amount" >
            </div>

          </div>
        </div>
      </div>

      <div class="modal-footer">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
      </div>

      </form>
      </div>
    </div>
  </div>
  <!-- END MODAL -->
</div>

<script type="text/javascript">
$(document).ready( function () {
  $('#financeTable').DataTable();
});
</script>

<script type="text/javascript">
function deleteFinance( id ) {
  swal({
  title: "Are you sure?",
  text: "Once deleted, you will not be able to undo this action!",
  icon: "warning",
  buttons: true,
  dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      $("#formFinance-" + id).submit();
    }
  });
}
</script>

<script type="text/javascript">
  window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove();
    });
}, 4000);
</script>
@endsection
