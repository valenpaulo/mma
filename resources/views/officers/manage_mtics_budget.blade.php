@extends('layout/control_panel')

@section('title')
MTICS Manage Budget
@endsection

@section('middle')
<div class="w3-container w3-margin">
  <div class="row">
    <h3>
      <i class="fa fa-file-text-o fa-fw w3-xxlarge"></i>
      <strong>Manage Monthly Budget</strong>
    </h3>
  </div>

  <hr>

  <div class="row w3-margin-top">
    <h5>
      <a href="#manageBudget" class="w3-text-gray" data-toggle="modal" style="outline: 0">
        <i class="fa fa-plus-square fa-fw"></i>
        Create Budget
      </a>
    </h5>
  </div>

  <!-- CREATE BUDGET MODAL -->
  <div class="modal fade" id="manageBudget" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
      <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
      <h4 class="modal-title w3-text-gray" id="myModalLabel">Create Budget</h4>
      </div>
      <form action="{{ url('admin/finance/manage-mtics-budget/create-budget') }}" method="POST" enctype="multipart/form-data">
      <div class="modal-body">
        <div class="w3-container">
          <div class="row">

            <div class="form-group">
              <label>Budget for the month of:</label>
              <input id="month" name='budget_month' type="month" class="form-control">
            </div>

            <div class="form-group" align="center">
              <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="budget_remarks" placeholder="Remarks" rows="4"></textarea>

            </div>

            <hr>

            <h4>
            <i class="fa fa-file-text-o"></i>
            Budget Breakdown
            <a href="" class="w3-text-green" id="addBudget">
              <i class="fa fa-plus-square"></i>
            </a>
            </h4>

            <br>

            <p id="demo"></p>

            <div id="breakdown">

              <div>

                <div class="form-group">
                <label>Category: </label>
                  <select name="exp_cat_id[]" class="form-control" onchange="showOther(this, 1)">
                    @foreach($expcats as $expcat)
                        <option value="{{$expcat->id}}">{{$expcat->name}}</option>
                    @endforeach
                        <option value="others">others</option>
                  </select>
                </div>

                <div class="form-group" id="othersInput-1" style="display: none">
                    <input type="text" name="breakdown_name[]" id="breakdown_name" tabindex="1" class="form-control" placeholder="others" >
                </div>

                <div class="form-group">
                  <label>Amount:</label>
                  <input type="number" name="budget_amt[]" id="budget_amt" tabindex="1" class="form-control" placeholder="Amount">

                </div>

                <div class="form-group">
                  <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="breakdown_desc[]" placeholder="Description" rows="3"></textarea>
                </div>
              </div>

            </div>

          </div>
        </div>
      </div>

      <div class="modal-footer">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
      </div>

      </form>
      </div>
    </div>
  </div>
  <!-- END MODAL -->


  <div class="row w3-margin-top">
    <div class="table-responsive">
        <table class="table table-bordered table-hover" id="budgetTable">
          <thead>
            <tr>
             <th class="w3-center">Budget</th>
             <th class="w3-center">Month</th>
             <th class="w3-center">Amount</th>
             <th class="w3-center">Remarks</th>
             <th class="w3-center">Status</th>
             <th class="w3-center">Action</th>
            </tr>
          </thead>
          <tbody class="w3-text-gray">
          @foreach($mticsbudgets as $budget)
           <tr>
              <td class="w3-center"><a href="" class="w3-text-gray" data-toggle="modal" data-target="#info-{{$budget->id}}"><i class="fa fa-info-circle"></i> {{$budget->budget_name}}</a>
               </td>
             <td class="w3-center">{{$budget->budget_month}}</td>
             <td class="w3-center">{{$budget->budget_amt}}</td>
             <td class="w3-center">{{$budget->budget_remarks}}</td>
             <td class="w3-center">{{$budget->budget_status}}</td>
             <td class="w3-center">
              @if ($budget->budget_status == "pending" || $budget->budget_status == "denied")
              <a href="#editBudget-{{$budget->id}}" class="w3-text-orange" data-toggle="modal" style="outline: 0"><i class="fa fa-pencil-square-o fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Edit Budget"></i></a>

              <a class="w3-text-green" href="javascript:void(0)" onclick="submitBudget( {{$budget->id}} )" style="outline: 0"><i class="fa fa-paper-plane-o fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Send Budget Request"></i></a>

              @elseif ($budget->budget_status == "for validation")
              <i class="fa fa-spinner fa-fw w3-text-gray w3-large" data-toggle="tooltip" data-placement="bottom" title="For Validation"></i>
             
              @else
              <i class="fa fa-check-circle-o fa-fw w3-text-green w3-large" data-toggle="tooltip" data-placement="bottom" title="Approved"></i>
              @endif

              @if (!($budget->deny->isEmpty()))
              <a class="w3-text-red" href="#denyinfo-{{$budget->id}}" data-toggle="modal" style="outline: 0"><i class="fa fa-flag fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Deny info"></i></a>
              @endif

              <!-- EDIT BUDGET MODAL -->
              <div class="modal fade" id="editBudget-{{$budget->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel" align="left">
                <div class="modal-dialog modal-md" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Edit Budget</h4>
                  </div>
                  <form action="{{ url('admin/finance/manage-mtics-budget/' . $budget->id . '/edit') }}" method="POST">
                    <div class="modal-body">
                      <div class="w3-container">
                        <div class="row">

                          <div class="form-group">
                            <label>Budget for the month of:</label>
                            <input id="month" name='budget_month' type="month" class="form-control" value="{{$budget->budget_month}}">
                          </div>

                          <div class="form-group" align="center">
                            <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="budget_remarks" placeholder="Remarks" rows="4">{{$budget->budget_remarks}}</textarea>

                          </div>

                          <hr>

                          <h4>
                          <i class="fa fa-file-text-o"></i>
                          Budget Breakdown
                          <a href="javascript:void(0)" class="w3-text-green addBreakdown" onclick="addBreakdown( this, {{$budget->id}} )" id="{{count($budget->budget_breakdown)}}">
                            <i class="fa fa-plus-square"></i>
                          </a>
                          </h4>

                          <br>

                          <div id="editBreakdown-{{$budget->id}}">

                            <div>

                              @foreach($budget->budget_breakdown as $key => $breakdown)
                              <div class="form-group">
                              <label>Category: </label>
                                <select name="exp_cat_id[]" class="form-control" onchange="showEditOther(this, {{$budget->id}}, {{++$key}})">
                                  @foreach($expcats as $expcat)
                                      <option value="{{$expcat->id}}" {{$expcat->name == $breakdown->budget_expenses_cat->name ? 'selected' : ''}}>{{$expcat->name}}</option>
                                  @endforeach
                                      <option value="others">others</option>
                                </select>
                              </div>

                              <div class="form-group {{$budget->id}}-othersInput-{{$key}}" style="display: none">
                                  <input type="text" name="breakdown_name[]" id="breakdown_name" tabindex="1" class="form-control" placeholder="others">
                              </div>

                              <div class="form-group">
                                <label>Amount:</label>
                                <input type="number" name="budget_amt[]" id="budget_amt" tabindex="1" class="form-control" placeholder="Amount" value="{{$breakdown->budget_bd_amt}}">

                              </div>

                              <div class="form-group">
                                <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="breakdown_desc[]" placeholder="Description" rows="3">{{$breakdown->budget_bd_desc}}</textarea>
                              </div>
                              @endforeach
                            </div>

                          </div>

                        </div>
                      </div>
                    </div>

                    <div class="modal-footer">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
                    </div>


                  </form>
                  </div>
                </div>
              </div>
              <!-- END MODAL -->
             </td>

            <form action="{{ url('admin/finance/manage-mtics-budget/'.$budget->id.'/submit') }}" method="POST" enctype="multipart/form-data" id="formSubmit-{{$budget->id}}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
            </form>

            </tr>

            <!-- Denied Reason MODAL-->
            <div class="modal fade" id="denyinfo-{{$budget->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
            <h4 class="modal-title w3-text-gray" id="myModalLabel">Denied Reason/s</h4>
            </div>
            <form action="" method="POST" enctype="multipart/form-data">
            <div class="modal-body">
              <div class="w3-container">
                <div class="row">

                  @foreach($budget->deny as $deny)
                  <div class="form-group">
                    <p><b>Denied by : </b>{{$deny->admin->first_name}} {{$deny->admin->last_name}}</p>
                    <p><b>Denied reason : </b>{{$deny->denied_reason}}</p><br>
                  </div>
                  @endforeach
                </div>
              </div>
            </div>

            </form>
            </div>
            </div>
            </div>
            <!-- END MODAL -->

            <!--INFO MODAL-->
            <div class="modal fade" id="info-{{$budget->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
            <h4 class="modal-title w3-text-gray" id="myModalLabel">Budget Details</h4>
            </div>
            <form action="" method="POST" enctype="multipart/form-data">
            <div class="modal-body">
              <div class="w3-container">
                <div class="row">

                  @foreach($budget->budget_breakdown as $budget_breakdown)
                  <div class="form-group">
                    <p><b>{{$budget_breakdown->budget_expenses_cat->name}} : </b> P{{$budget_breakdown->budget_bd_amt}}.00</p>
                    <p>Description :  {{$budget_breakdown->budget_bd_desc}}</p>
                  </div>
                  @endforeach

                  ______________________
                  <p><b>Total </b> :  P{{$budget->budget_breakdown->sum('budget_bd_amt')}}.00</p>
                </div>
              </div>
            </div>

            </form>
            </div>
            </div>
            </div>
            <!-- END MODAL -->

          @endforeach
         </tbody>
        </table>
    </div>
  </div>

</div>

<script type="text/javascript">

  function showOther(me, id){
    if(me.value == 'others')
      $('#othersInput-' + id).show();
    else {
      $('#othersInput-' + id).hide();

    }
  }
</script>

<script type="text/javascript">

  function showEditOther(me, parent, id){
    if(me.value == 'others')
      $('.' + parent + '-othersInput-' + id).show();
    else {
      $('.' + parent + '-othersInput-' + id).hide();

    }
  }
</script>

<script>
  $(document).ready(function() {

    var otherId = 1;
    var wrapper         = $("#breakdown"); //Fields wrapper
    var add_button      = $("#addBudget"); //Add button ID

    $(add_button).click(function(e){ //on add input button click
        otherId = otherId + 1;
        e.preventDefault();
          $(wrapper).append('\
            <div>\
              <div class="form-group">\
                <label>Category: </label>\
                <select name="exp_cat_id[]" class="form-control" onchange="showOther(this, '+ otherId +')">\
                  @foreach($expcats as $expcat)\
                      <option value="{{$expcat->id}}">{{$expcat->name}}</option>\
                  @endforeach\
                      <option value="others">others</option>\
                </select>\
              </div>\
\
              <div class="form-group" id="othersInput-'+ otherId +'" style="display: none">\
                  <input type="text" name="breakdown_name[]" id="breakdown_name" tabindex="1" class="form-control" placeholder="others" >\
              </div>\
\
              <div class="form-group">\
                <label>Amount:</label>\
                <input type="number" name="budget_amt[]" id="budget_amt1" tabindex="1" class="form-control" placeholder="Amount">\
\
              </div>\
\
              <div class="form-group">\
                <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="breakdown_desc[]" placeholder="Description" rows="3"></textarea>\
              </div>\
\
              <a id="remove_item" href="javascript:void(0)" class="w3-text-red">\
                <p align="center">\
                  <i class="fa fa-remove fa-fw"></i> Remove\
                </p>\
              </a>\
            </div>\
          '); //add input box
    });


    $(wrapper).on("click","#remove_item", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })

  });
</script>

<script type="text/javascript">
  function addBreakdown( me, parent ) {
    var id = parseInt(me.id) + 1;
    $(".addBreakdown").attr('id', id);

    $("#editBreakdown-" + parent).append('\
      <div>\
        <div class="form-group">\
          <label>Category: </label>\
          <select name="exp_cat_id[]" class="form-control" onchange="showEditOther(this, '+ parent +', '+ id +')">\
            @foreach($expcats as $expcat)\
                <option value="{{$expcat->id}}">{{$expcat->name}}</option>\
            @endforeach\
                <option value="others">others</option>\
          </select>\
        </div>\
      \
        <div class="form-group '+ parent +'-othersInput-'+ id +'" style="display: none">\
            <input type="text" name="breakdown_name[]" id="breakdown_name" tabindex="1" class="form-control" placeholder="others" >\
        </div>\
      \
        <div class="form-group">\
          <label>Amount:</label>\
          <input type="number" name="budget_amt[]" id="budget_amt1" tabindex="1" class="form-control" placeholder="Amount">\
      \
        </div>\
      \
        <div class="form-group">\
          <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="breakdown_desc[]" placeholder="Description" rows="3"></textarea>\
        </div>\
      \
        <a id="remove_item" href="javascript:void(0)" class="w3-text-red">\
          <p align="center">\
            <i class="fa fa-remove fa-fw"></i> Remove\
          </p>\
        </a>\
      </div>\
      '); //add input box

    $("#editBreakdown-" + parent).on("click","#remove_item", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove();
    })
  }
</script>

<script type="text/javascript">
function submitBudget( id ) {
  swal({
  title: "Are you sure?",
  text: "Once submitted, you will not be able to undo this action!",
  icon: "warning",
  buttons: true,
  dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      $("#formSubmit-" + id).submit();
    }
  });
}
</script>

<script type="text/javascript">
$(document).ready( function () {
  var table = $('#budgetTable').DataTable();
  $(table.table().container()).removeClass('form-inline');
} );
</script>

@endsection
