@extends('layout/control_panel')

@section('title')
MTICS Bank
@endsection

@section('middle')
<div class="w3-container w3-margin">
  <div class="row">
    <h3>
      <i class="fa fa-bank fa-fw w3-xxlarge"></i>
      <strong>Bank Transaction</strong>
    </h3>
  </div>

  <hr>

  <div class="w3-row-padding w3-margin-top">
    <div class="col-lg-1 col-md-1 col-sm-12"></div>

    <div class="col-lg-3 col-md-3 col-sm-12 w3-margin-top">
      <div class="w3-card-4 w3-amber w3-text-black w3-padding w3-container">

        <div class="w3-container">
          <div class="row" align="center">
            <h4>
              <i class="fa fa-money w3-xxlarge fa-fw"></i>
              <strong>MTICS Fund</strong>
            </h4>
          </div>
          <hr style="border-color: black">
          <div class="row">
            <h4>
              <span>
                <i class="fa fa-ellipsis-v fa-fw"></i>
                <strong>{{$mticsfund_bucket}} pesos</strong>
              </span>
            </h4>
          </div>
        </div>

      </div>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-12">
      <div class="w3-card-4 w3-teal w3-text-black w3-padding w3-container">

        <div class="w3-container">
          <div class="row" align="center">
            <h4>
              <i class="fa fa-bank w3-xxxlarge fa-fw"></i>
              <strong>Money in Bank</strong>
            </h4>
          </div>
          <hr style="border-color: black">
          <div class="row">
            <h4>
              <span>
                <i class="fa fa-ellipsis-v fa-fw"></i>
                <strong>{{$totalBucket}} pesos</strong>
              </span>
            </h4>
          </div>
        </div>

      </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-12 w3-margin-top">
      <div class="w3-card-4 w3-purple w3-text-black w3-padding w3-container">

        <div class="w3-container">
          <div class="row" align="center">
            <h4>
              <i class="fa fa-money w3-xxlarge fa-fw"></i>
              <strong>Event Fund</strong>
            </h4>
          </div>
          <hr style="border-color: black">
          <div class="row">
            <h4>
              <span>
                <i class="fa fa-ellipsis-v fa-fw"></i>
                <strong>{{$eventfund_bucket}} pesos</strong>
              </span>
            </h4>
          </div>
        </div>

      </div>
    </div>

    <div class="col-lg-1 col-md-1 col-sm-12"></div>
  </div>

  <br>

  <div class="row w3-margin-top" align="center">

    <!-- WITHDRAW BUTTON -->
    <div class="w3-dropdown-hover w3-white w3-margin">
      <a href="javascript:void(0)" class="w3-red btn btn-default">

      <i class="fa fa-money w3-xlarge"></i>
      <i class="fa fa-arrow-down fa-fw w3-large"></i>

      </a>


      <div class="w3-dropdown-content w3-card-4 panel panel-default w3-animate-zoom" align="left" style="width: 200px;">
        <a href="#mticsWithdraw" data-toggle="modal" class="w3-text-gray w3-hover-teal"><i class="fa fa-institution fa-fw w3-large"></i> Withdraw from MTICS</a>

        <a href="#eventWithdraw" data-toggle="modal" class="w3-text-gray w3-hover-teal" style="outline: 0px"><i class="fa fa-calendar fa-fw w3-large"></i> Withdraw from Event</a>
      </div>

    </div>

    <!-- MTICS WITHDRAW -->
     <div class="modal fade" id="mticsWithdraw" tabindex="100" role="dialog" aria-labelledby="myModalLabel" align="left">
        <div class="modal-dialog modal-md" role="document">
          <div class="modal-content">
          <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
          <h4 class="modal-title w3-text-gray" id="myModalLabel">MTICS Withdraw Request</h4>
          </div>

          <form action="{{ url('admin/finance/manage-bank-transaction/mtics/withdraw') }}" method="POST" novalidate>

            <div class="modal-body">
                <label>Withdraw For:</label>
                <div class="form-group" align="center">
                  <label class="radio-inline">
                    <input type="radio" name="withdraw_type" value="withdraw_budget" checked onclick="showMticsWithdrawInput('withdraw_budget')">Monthly Budget
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="withdraw_type" value="withdraw_external" onclick="showMticsWithdrawInput('withdraw_external')">Chief Executive for External Task
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="withdraw_type" value="withdraw_other" onclick="showMticsWithdrawInput('withdraw_other')">Others
                  </label>
                </div>

                <div class="form-group" id="budget" style="display: block">
                  <label>Budget</label>
                  <select name="mticsbudget_id" class="form-control">
                      <option>-- Select Budget Appropriate for this Month --</option>
                  @foreach($mticsbudgets as $mticsbudget)
                      <option value="{{$mticsbudget->id}}">{{$mticsbudget->budget_name}}</option>
                  @endforeach
                  </select>
                </div>

                <div id="externalTaskMtics" class="form-group" style="display: none">
                  <label>Task:</label>
                  <select name="external_task_id" class="form-control">
                    <option>-- Select a Task --</option>

                  @foreach($moneyrequests as $moneyrequest)
                      <option value="{{$moneyrequest->task->id}}">{{$moneyrequest->task->task_name}}</option>
                  @endforeach
                  </select>
                </div>

                <div class="form-group" id="otherMtics" style="display: none">
                  <label>Amount</label>

                  <input type="number" name="withdraw_amt" id="withdraw_amt" tabindex="1" class="form-control" placeholder="Amount">

                  <br>

                  <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="withdraw_reason" placeholder="Reason" rows="3" required></textarea>
                </div>

                <div class="form-group">
                  <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="withdraw_desc" placeholder="Description" rows="3"></textarea>
                </div>

                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </div>

         <div class="modal-footer">
            <button type="submit" class="btn btn-default w3-green"><i class="fa fa-paper-plane-o fa-fw"></i> Submit</button>
          </div>
          </form>
          </div>
        </div>
      </div>
    <!-- END MTICS WITHDRAW -->

    <!-- EVENT WITHDRAW -->
    <div class="modal fade" id="eventWithdraw" tabindex="100" role="dialog" aria-labelledby="myModalLabel" align="left">
      <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
        <h4 class="modal-title w3-text-gray" id="myModalLabel">Event Withdraw Request</h4>
        </div>

        <form action="{{ url('admin/finance/manage-bank-transaction/event/withdraw') }}" method="POST">

          <div class="modal-body">
            <label>
              Withdraw For:
            </label>
            <div class="form-group" align="center">
              <label class="radio-inline">
                <input type="radio" name="withdraw_type" value="withdraw_external" onclick="showEventWithdrawInput('withdraw_external')" checked>Chief Executive for External Task
              </label>
              <label class="radio-inline">
                <input type="radio" name="withdraw_type" value="withdraw_other" onclick="showEventWithdrawInput('withdraw_other')">Others
              </label>
            </div>

            <div id="externalTaskEvent" class="form-group" style="display: block">
              <label>Task:</label>
              <select name="external_task_id" class="form-control">
                <option value="NULL">-- Select a Task --</option>

              @foreach($moneyrequests as $moneyrequest)
                @if($moneyrequest->task->event !== null )
                  <option value="{{$moneyrequest->task->id}}">{{$moneyrequest->task->event->event_title}} - {{$moneyrequest->task->task_name}}</option>
                @endif
              @endforeach
              </select>
            </div>

            <div class="form-group" id="otherEvent" style="display: none">

              <label>Event</label>

              <select name="event_id" class="form-control">
                  <option value="NULL">-- Select Event --</option>
              @foreach($events as $event)
                  <option value="{{$event->id}}">{{$event->event_title}}</option>
              @endforeach
              </select>

              <br>

              <label>Amount</label>

              <input type="number" name="withdraw_amt" id="withdraw_amt" tabindex="1" class="form-control" placeholder="Amount">

              <br>

              <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="withdraw_reason" placeholder="Reason" rows="3"></textarea>
            </div>

            <div class="form-group">
              <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="withdraw_desc" placeholder="Description" rows="3"></textarea>
            </div>

            <input type="hidden" name="_token" value="{{ csrf_token() }}">
          </div>

          <div class="modal-footer">
            <button type='Submit' class="btn btn-default w3-green"><i class="fa fa-paper-plane-o fa-fw"></i> Submit</button>
          </div>

        </form>
        </div>
      </div>
    </div>
    <!-- END EVENT WITHDRAW -->

    <!-- END WITHDRAW -->

    <!-- DEPOSIT BUTTON -->
    <div class="w3-dropdown-hover w3-white w3-margin">
      <a href="javascript:void(0)" class="w3-green btn btn-default">

      <i class="fa fa-money w3-xlarge"></i>
      <i class="fa fa-arrow-up fa-fw w3-large"></i>

      </a>


      <div class="w3-dropdown-content w3-card-4 panel panel-default w3-animate-zoom" align="left" style="width: 200px;">
        <a href="#mticsDeposit" data-toggle="modal" class="w3-text-gray w3-hover-teal"><i class="fa fa-institution fa-fw w3-large"></i> Deposit to MTICS</a>

        <a href="#eventDeposit" data-toggle="modal" class="w3-text-gray w3-hover-teal" style="outline: 0px"><i class="fa fa-calendar fa-fw w3-large"></i> Deposit to Event</a>
      </div>

    </div>

    <!-- MTICS DEPOSIT -->
    <div class="modal fade" id="mticsDeposit" tabindex="100" role="dialog" aria-labelledby="myModalLabel" align="left">
      <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
        <h4 class="modal-title w3-text-gray" id="myModalLabel">MTICS Deposit Request</h4>
        </div>
          <form action="{{ url('admin/finance/manage-bank-transaction/mtics/deposit') }}" method="POST" enctype="multipart/form-data">
            <div class="modal-body">
              <div class="w3-container">
                <label>Deposit From:</label>
                <div class="form-group" align="center">
                  <label class="radio-inline">
                    <input type="radio" name="deposit_type" value="mtics_payment" checked onclick="showMticsDepositInput('mtics_payment')">MTICS Fund Collection
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="deposit_type" value="event" onclick="showMticsDepositInput('event')">Event Collection
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="deposit_type" value="others" onclick="showMticsDepositInput('others')">Others
                  </label>
                </div>

                <div class="form-group" id="eventInput" style="display: none">
                  <label>Event</label>

                  <select name="event_id" class="form-control">
                      <option>-- Select Event --</option>
                  @foreach($eventDone as $event)
                      <option value="{{$event->id}}">{{$event->event_title}}</option>
                  @endforeach
                  </select>
                </div>

                <div class="form-group">
                  <label>Amount</label>
                  <input type="number" name="deposit_amt" id="deposit_amt" tabindex="1" class="form-control" placeholder="Amount">
                </div>

                <div class="form-group" id="descInput" style="display: none;">
                  <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="deposit_desc" placeholder="Description" rows="3"></textarea>
                </div>

                <div class="form-group">
                  <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="remarks" placeholder="Remarks" rows="3"></textarea>
                </div>

                <input type="hidden" name="_token" value="{{ csrf_token() }}">
              </div>
            </div>

            <div class="modal-footer">
              <button type="submit" class="btn btn-default w3-green"><i class="fa fa-paper-plane-o fa-fw"></i> Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- END MTICS DEPOSIT -->

    <!-- EVENT DEPOSIT -->
    <div class="modal fade" id="eventDeposit" tabindex="100" role="dialog" aria-labelledby="myModalLabel" align="left">
      <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
        <h4 class="modal-title w3-text-gray" id="myModalLabel">Event Deposit Request</h4>
        </div>
          <form action="{{ url('admin/finance/manage-bank-transaction/event/deposit') }}" method="POST" enctype="multipart/form-data">
            <div class="modal-body">
              <div class="w3-container">
                <label>Deposit From:</label>
                <div class="form-group" align="center">
                  <!-- <label class="radio-inline">
                    <input type="radio" name="deposit_type" value="mtics_payment" checked onclick="showEventDepositInput('mtics_payment')">MTICS Fund Collection
                  </label> -->
                  <label class="radio-inline">
                    <input type="radio" name="deposit_type" checked value="event" onclick="showEventDepositInput('event')">Event Collection
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="deposit_type" value="others" onclick="showEventDepositInput('others')">Others
                  </label>
                </div>

                <div class="form-group" id="eventInputEvent" style="display: show">
                  <label>Event</label>

                  <select name="event_id" class="form-control">
                      <option>-- Select Event --</option>
                  @foreach($events as $event)
                      <option value="{{$event->id}}">{{$event->event_title}}</option>
                  @endforeach
                  </select>
                </div>

                <div class="form-group">
                  <label>Amount</label>
                  <input type="number" name="deposit_amt" id="deposit_amt" tabindex="1" class="form-control" placeholder="Amount">
                </div>

                <div class="form-group">
                  <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="deposit_desc" placeholder="Description" rows="3"></textarea>
                </div>

                <div class="form-group" style="display: none" id="descInputEvent">
                  <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="remarks" placeholder="Remarks" rows="3"></textarea>
                </div>

                <input type="hidden" name="_token" value="{{ csrf_token() }}">
              </div>
            </div>

            <div class="modal-footer">
              <button type="submit" class="btn btn-default w3-green"><i class="fa fa-paper-plane-o fa-fw"></i> Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- END EVENT DEPOSIT -->

    <!-- END DEPOSIT -->

    <!-- TRANSFER BUTTON -->
    <span class="w3-white w3-margin">
      <a href="#transferRequest" data-toggle="modal" class="w3-orange w3-text-white btn btn-default" style="outline: 0">

      <i class="fa fa-money w3-xlarge"></i>
      <i class="fa fa-refresh fa-fw w3-large"></i>

      </a>
    </span>

    <!-- TRANSFER MODAL -->
    <div class="modal fade" id="transferRequest" tabindex="100" role="dialog" aria-labelledby="myModalLabel" align="left">
      <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
      <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
      <h4 class="modal-title w3-text-gray" id="myModalLabel">Money Transfer</h4>
      </div>
      <div class="modal-body">
        <div class="w3-container">
          <div class="row">
            <!-- MTICS FORM -->
            <div id="mticsDepositForm">

              <form action="{{ url('admin/finance/manage-bank-transaction/mtics/transfer') }}" method="POST" enctype="multipart/form-data">


              <div class="form-group" align="center">
                <label class="radio-inline">
                  <input type="radio" name="deposit_type" value="transfer_mtics_event" checked>MTICS to Event
                </label>
                <label class="radio-inline">
                  <input type="radio" name="deposit_type" value="transfer_event_mtics">Event to MTICS
                </label>
              </div>

              <div class="form-group" id="eventTransferInput">
                <label>Event</label>

                <select name="event_id" class="form-control">
                    <option>-- Select Event --</option>
                @foreach($eventDone as $event)
                    <option value="{{$event->id}}">{{$event->event_title}}</option>
                @endforeach
                </select>
              </div>

              <div class="form-group">
                <label>Amount</label>
                <input type="number" name="deposit_amt" id="deposit_amt" tabindex="1" class="form-control" placeholder="Amount">
              </div>

              <div class="form-group" id="descInput">
                <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="deposit_desc" placeholder="Description" rows="3" required></textarea>
              </div>

              <div class="form-group">
                <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="remarks" placeholder="Remarks" rows="3"></textarea>
              </div>

              <input type="hidden" name="_token" value="{{ csrf_token() }}">

              <div class="modal-footer">
                <button type="submit" class="btn btn-default w3-green"><i class="fa fa-paper-plane-o fa-fw"></i> Submit</button>
              </div>

              </form>

            <!-- END MTICS FORM -->
            </div>

          </div>
        </div>
      </div>

      </div>
      </div>
    </div>
    <!-- END TRANSFER MODAL -->
    <!-- END TRANSFER -->
  </div>

  <div class="row w3-margin-top">
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#Withdraw" class="w3-text-black">Withdraw Transaction</a></li>
      <li><a data-toggle="tab" href="#deposit" class="w3-text-black">
      Deposit Transaction
      </a></li>
      <li><a data-toggle="tab" href="#transfer" class="w3-text-black">
      Transfer Transaction
      </a></li>

    </ul>
  </div>

  <div class="tab-content">
    <div id="Withdraw" class="tab-pane fade in active">
      @if(count($eventWithdrawRequest) > 0 or count($mticsWithdrawRequest) > 0)
      <div class="row w3-margin-top">
        <div class="table-responsive">
            <table class="table table-bordered table-hover" id="withdrawRequestTable">
              <thead>
                <tr>
                 <th class="w3-center">Amount</th>
                 <th class="w3-center">For</th>
                 <th class="w3-center">Created At</th>
                 <th class="w3-center">Status</th>
                </tr>
              </thead>
              <tbody class="w3-text-gray">
              @foreach($eventWithdrawRequest as $request)
               <tr>
                 <td class="w3-center">{{$request->withdrawal_amt}}</td>
                 <td class="w3-center">{{$request->withdrawal_reason}}</td>
                 <td class="w3-center">{{$request->created_at}}</td>
                 <td class="w3-center">
                  @if($request->withdrawal_status == 'pending')
                   <i class="fa fa-spinner fa-fw w3-large"></i> Pending
                  @elseif($request->withdrawal_status == 'approved')
                   <i class="fa fa-thumbs-up fa-fw w3-large w3-text-green"></i> Approved
                  @endif
                 </td>
                </tr>
              @endforeach

              @foreach($mticsWithdrawRequest as $request)
               <tr>
                 <td class="w3-center">{{$request->withdrawal_amt}}</td>
                 <td class="w3-center">{{$request->withdrawal_reason}}</td>
                 <td class="w3-center">{{$request->created_at}}</td>
                 <td class="w3-center">
                  @if($request->withdrawal_status == 'pending')
                   <i class="fa fa-spinner fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="For Validation"></i>
                  @elseif($request->withdrawal_status == 'approved')
                   <i class="fa fa-thumbs-up fa-fw w3-large w3-text-green" data-toggle="tooltip" data-placement="bottom" title="Approved"></i>
                  @endif
                 </td>
                </tr>
              @endforeach
             </tbody>
            </table>
        </div>
      </div>
      @endif

      <div class="row w3-margin-top">
        <label class="radio-inline"><input type="radio" name="withdrawRadio" onclick="showWithdrawTable('mtics')" checked>Mtics</label>
        <label class="radio-inline"><input type="radio" name="withdrawRadio" onclick="showWithdrawTable('event')">Event</label>
      </div>

      <br>

      <div class="row" id="mticsWithdrawTrans123">
        <div class="table-responsive">
            <table class="table table-bordered table-hover" id="mticsWithdrawTransTable">
              <thead>
                <tr>
                 <th class="w3-center">Amount</th>
                 <th class="w3-center">For</th>
                 <th class="w3-center">Approved By</th>
                 <th class="w3-center">Created At</th>
                 <th class="w3-center">Reported</th>
                </tr>
              </thead>
              <tbody class="w3-text-gray">
              @foreach($mticsWithdrawTransaction as $trans)
               <tr>
                 <td class="w3-center">
                  <a href="#mticsWithdrawReceipt-{{$trans->id}}" data-toggle="modal" class="w3-text-gray">
                    <i class="fa fa-exclamation-circle"></i>
                    {{$trans->withdrawal_amt}}
                  </a>
                 </td>
                 <td class="w3-center">{{$trans->withdrawal_reason}}</td>
                 <td class="w3-center">{{$trans->faculty->first_name}} {{$trans->faculty->last_name}}</td>
                 <td class="w3-center">{{$trans->created_at}}</td>
                  <td class="w3-center">
                      @if (!($trans->report == null))
                      <a class="w3-text-red" href="#1reportinfo-{{$trans->id}}" data-toggle="modal" style="outline: 0"><i class="fa fa-flag fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Report info"></i></a>
                      @endif
                   </td>
                </tr>


                 <!-- Reported details MODAL-->
                  <div class="modal fade" id="1reportinfo-{{$trans->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-sm" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Report Details</h4>
                  </div>
                  <form action="" method="POST" enctype="multipart/form-data">
                  <div class="modal-body">
                    <div class="w3-container">
                      <div class="row">
                        <div class="form-group">
                      @if (!($trans->report == null))
                          <p><b>Reported by : </b>{{$trans->report->faculty->first_name}} {{$trans->report->faculty->last_name}}</p>
                          <p><b>Title : </b>{{$trans->report->deposit_title}} </p>
                          <p><b>Reason : </b>{{$trans->report->deposit_desc}} </p>
                          <p><b>Solution : </b>{{$trans->report->solution}} </p>
                        @endif
                        </div>
                      </div>
                    </div>
                  </div>

                  </form>
                  </div>
                  </div>
                  </div>
                  <!-- END MODAL -->

                <!-- MTICS WITHDRAW RECEIPT MODAL -->
                <div class="modal fade" id="mticsWithdrawReceipt-{{$trans->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title w3-text-gray" id="myModalLabel">Withdraw Receipt</h4>
                </div>

                <div class="modal-body">
                  <div class="w3-container">
                    <div class="row" align="center">
                    @if($trans->receipt)
                    <img src="{{url('/') == 'http:://mtics-ma.tuptaguig.com' ? asset('public/images/' . $trans->receipt->receipt_image) : asset('images/' . $trans->receipt->receipt_image)}}" style="width: 50%">
                    @endif
                    </div>
                  </div>
                </div>

                </div>
                </div>
                </div>
                <!-- END MODAL -->
              @endforeach
             </tbody>
            </table>
        </div>

    </div>

    <div class="row" id="eventWithdrawTrans123" style="display: none">
      <div class="table-responsive">
          <table class="table table-bordered table-hover" id="eventWithdrawTransTable">
            <thead>
              <tr>
               <th class="w3-center">Amount</th>
               <th class="w3-center">For</th>
               <th class="w3-center">Approved By</th>
               <th class="w3-center">Created At</th>
               <th class="w3-center">Reported</th>
              </tr>
            </thead>
            <tbody class="w3-text-gray">
            @foreach($eventWithdrawTransaction as $trans)
             <tr>
               <td class="w3-center">
                <a href="#eventWithdrawReceipt-{{$trans->id}}" data-toggle="modal" class="w3-text-gray">
                    <i class="fa fa-exclamation-circle"></i>
                    {{$trans->withdrawal_amt}}
                </a>
               </td>
               <td class="w3-center">{{$trans->withdrawal_reason}}</td>
               <td class="w3-center">{{$trans->faculty->first_name}} {{$trans->faculty->last_name}}</td>
               <td class="w3-center">{{$trans->created_at}}</td>
                <td class="w3-center">
                      @if (!($trans->report == null))
                      <a class="w3-text-red" href="#1reportinfo-{{$trans->id}}" data-toggle="modal" style="outline: 0"><i class="fa fa-flag fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Report info"></i></a>
                      @endif
                   </td>
              </tr>

                  <!-- Reported details MODAL-->
                  <div class="modal fade" id="1reportinfo-{{$trans->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-sm" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Report Details</h4>
                  </div>
                  <form action="" method="POST" enctype="multipart/form-data">
                  <div class="modal-body">
                    <div class="w3-container">
                      <div class="row">
                        <div class="form-group">
                      @if (!($trans->report == null))
                          <p><b>Reported by : </b>{{$trans->report->faculty->first_name}} {{$trans->report->faculty->last_name}}</p>
                          <p><b>Title : </b>{{$trans->report->deposit_title}} </p>
                          <p><b>Reason : </b>{{$trans->report->deposit_desc}} </p>
                          <p><b>Solution : </b>{{$trans->report->solution}} </p>
                        @endif
                        </div>
                      </div>
                    </div>
                  </div>

                  </form>
                  </div>
                  </div>
                  </div>
                  <!-- END MODAL -->

              <!-- EVENT WITHDRAW RECEIPT MODAL -->
              <div class="modal fade" id="eventWithdrawReceipt-{{$trans->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog modal-md" role="document">
              <div class="modal-content">
              <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
              <h4 class="modal-title w3-text-gray" id="myModalLabel">Withdraw Receipt</h4>
              </div>

              <div class="modal-body">
                <div class="w3-container">
                  <div class="row" align="center">

                  <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/' . $trans->receipt->receipt_image) : asset('images/' . $trans->receipt->receipt_image)}}" style="width: 50%">

                  </div>
                </div>
              </div>

              </div>
              </div>
              </div>
              <!-- END MODAL -->
            @endforeach
           </tbody>
          </table>
      </div>

    </div>
    <!-- end of withdraw tab -->
    </div>

    <div id="deposit" class="tab-pane fade">
      @if(count($mticsDepositRequest) > 0 or count($eventDepositRequest) > 0)

      <div class="row w3-margin-top">
        <div class="table-responsive">
            <table class="table table-bordered table-hover" id="depositRequestTable">
              <thead>
                <tr>
                 <th class="w3-center">Amount</th>
                 <th class="w3-center">From</th>
                 <th class="w3-center">Created At</th>
                 <th class="w3-center">Status</th>
                </tr>
              </thead>
              <tbody class="w3-text-gray">

              <!-- First, display event request -->
              @foreach($eventDepositRequest as $request)
               <tr>
                 <td class="w3-center">{{$request->deposit_amt}}</td>
                 <td class="w3-center">{{$request->deposit_source}}</td>
                 <td class="w3-center">{{$request->created_at}}</td>
                 <td class="w3-center">
                  @if($request->deposit_status == 'pending')
                   <i class="fa fa-spinner fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="For Validation"></i>
                  @elseif($request->deposit_status == 'approved')
                   <i class="fa fa-thumbs-up fa-fw w3-text-green w3-large" data-toggle="tooltip" data-placement="bottom" title="Approved"></i>
                  @endif
                 </td>
                </tr>
              @endforeach

              <!-- Second, display mtics request -->
              @foreach($mticsDepositRequest as $request)
               <tr>
                 <td class="w3-center">{{$request->deposit_amt}}</td>
                 <td class="w3-center">{{$request->deposit_source}}</td>
                 <td class="w3-center">{{$request->created_at}}</td>
                 <td class="w3-center">
                  @if($request->deposit_status == 'pending')
                   <i class="fa fa-spinner fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="For Validation"></i>
                  @elseif($request->deposit_status == 'approved')
                   <i class="fa fa-thumbs-up fa-fw w3-large w3-text-green" data-toggle="tooltip" data-placement="bottom" title="Approved"></i>
                  @endif
                 </td>
                </tr>
              @endforeach
             </tbody>
            </table>
        </div>
      </div>
      @endif

      <div class="row w3-margin-top">
        <label class="radio-inline"><input type="radio" name="depositRadio" onclick="showDepositTable('mtics')" checked>Mtics</label>
        <label class="radio-inline"><input type="radio" name="depositRadio" onclick="showDepositTable('event')">Event</label>
      </div>

      <br>

      <div class="row" id="mticsDepositTrans">

        <div class="table-responsive">
            <table class="table table-bordered table-hover" id="mticsDepositTransTable">
              <thead>
                <tr>
                 <th class="w3-center">Amount</th>
                 <th class="w3-center">From</th>
                 <th class="w3-center">Approved By</th>
                 <th class="w3-center">Created At</th>
                 <th class="w3-center">Reported</th>
                </tr>
              </thead>
              <tbody class="w3-text-gray">
              @foreach($mticsDepositTransaction as $trans)
               <tr>
                 <td class="w3-center">
                   <a href="#mticsReceipt-{{$trans->id}}" data-toggle="modal" class="w3-text-gray">
                    <i class="fa fa-exclamation-circle"></i>
                    {{$trans->deposit_amt}}
                   </a>
                 </td>
                 <td class="w3-center">{{$trans->deposit_source}}</td>
                 <td class="w3-center">{{$trans->faculty->first_name}} {{$trans->faculty->last_name}}</td>
                 <td class="w3-center">{{$trans->created_at}}</td>
                 <td class="w3-center">
                      @if (!($trans->report == null))
                      <a class="w3-text-red" href="#1reportinfo-{{$trans->id}}" data-toggle="modal" style="outline: 0"><i class="fa fa-flag fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Report info"></i></a>
                      @endif
                   </td>
                </tr>


                 <!-- Reported details MODAL-->
                  <div class="modal fade" id="1reportinfo-{{$trans->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-sm" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Report Details</h4>
                  </div>
                  <form action="" method="POST" enctype="multipart/form-data">
                  <div class="modal-body">
                    <div class="w3-container">
                      <div class="row">
                        <div class="form-group">
                      @if (!($trans->report == null))
                          <p><b>Reported by : </b>{{$trans->report->faculty->first_name}} {{$trans->report->faculty->last_name}}</p>
                          <p><b>Title : </b>{{$trans->report->deposit_title}} </p>
                          <p><b>Reason : </b>{{$trans->report->deposit_desc}} </p>
                          <p><b>Solution : </b>{{$trans->report->solution}} </p>
                        @endif
                        </div>
                      </div>
                    </div>
                  </div>

                  </form>
                  </div>
                  </div>
                  </div>
                  <!-- END MODAL -->

                <!-- MTICS DEPOSIT RECEIPT MODAL -->
                <div class="modal fade" id="mticsReceipt-{{$trans->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title w3-text-gray" id="myModalLabel">Deposit Receipt</h4>
                </div>

                <div class="modal-body">
                  <div class="w3-container">
                    <div class="row" align="center">
                    @if($trans->receipt)
                    <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/' . $trans->receipt->receipt_image) : asset('images/' . $trans->receipt->receipt_image)}}" style="width: 50%">
                    @endif

                    </div>
                  </div>
                </div>

                </div>
                </div>
                </div>
                <!-- END MODAL -->
              @endforeach
             </tbody>
            </table>
        </div>
      </div>

      <div class="row" id="eventDepositTrans" style="display: none">
        <div class="table-responsive">
            <table class="table table-bordered table-hover" id="eventDepositTransTable">
              <thead>
                <tr>
                 <th class="w3-center">Amount</th>
                 <th class="w3-center">From</th>
                 <th class="w3-center">Approved By</th>
                 <th class="w3-center">Created At</th>
                 <th class="w3-center">Reported</th>
                </tr>
              </thead>
              <tbody class="w3-text-gray">
              @foreach($eventDepositTransaction as $trans)
               <tr>
                 <td class="w3-center">
                   <a href="#eventReceipt-{{$trans->id}}" data-toggle="modal" class="w3-text-gray">
                    <i class="fa fa-exclamation-circle"></i>
                    {{$trans->deposit_amt}}
                   </a>
                 </td>
                 <td class="w3-center">{{$trans->deposit_source}}</td>
                 <td class="w3-center">{{$trans->faculty->first_name}} {{$trans->faculty->last_name}}</td>
                 <td class="w3-center">{{$trans->created_at}}</td>
                   <td class="w3-center">
                      @if (!($trans->report == null))
                      <a class="w3-text-red" href="#1reportinfo-{{$trans->id}}" data-toggle="modal" style="outline: 0"><i class="fa fa-flag fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Report info"></i></a>
                      @endif
                   </td>
                </tr>

                    <!-- Reported details MODAL-->
                  <div class="modal fade" id="1reportinfo-{{$trans->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-sm" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Report Details</h4>
                  </div>
                  <form action="" method="POST" enctype="multipart/form-data">
                  <div class="modal-body">
                    <div class="w3-container">
                      <div class="row">
                        <div class="form-group">
                      @if (!($trans->report == null))
                          <p><b>Reported by : </b>{{$trans->report->faculty->first_name}} {{$trans->report->faculty->last_name}}</p>
                          <p><b>Title : </b>{{$trans->report->deposit_title}} </p>
                          <p><b>Reason : </b>{{$trans->report->deposit_desc}} </p>
                          <p><b>Solution : </b>{{$trans->report->solution}} </p>
                        @endif
                        </div>
                      </div>
                    </div>
                  </div>

                  </form>
                  </div>
                  </div>
                  </div>
                  <!-- END MODAL -->


                <!-- EVENT DEPOSIT RECEIPT MODAL -->
                <div class="modal fade" id="eventReceipt-{{$trans->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title w3-text-gray" id="myModalLabel">Deposit Receipt</h4>
                </div>

                <div class="modal-body">
                  <div class="w3-container">
                    <div class="row" align="center">
                    @if($trans->receipt)
                    <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/' . $trans->receipt->receipt_image) : asset('images/' . $trans->receipt->receipt_image)}}" style="width: 50%">
                    @endif

                    </div>
                  </div>
                </div>

                </div>
                </div>
                </div>
                <!-- END MODAL -->
              @endforeach
             </tbody>
            </table>
        </div>

    </div>

    <!-- end of deposit tab -->
    </div>

    <!-- transfer tab -->
    <div id="transfer" class="tab-pane fade">

    <div class="row w3-margin-top">
        <label class="radio-inline"><input type="radio" name="transferRadio" onclick="showTransferTable('mtics')" checked>Mtics</label>
        <label class="radio-inline"><input type="radio" name="transferRadio" onclick="showTransferTable('event')">Event</label>
      </div>

      <br>

      <div class="row" id="mticsTransferTrans">

        <div class="table-responsive">
            <table class="table table-bordered table-hover" id="mticsTransferTransTable">
              <thead>
                <tr>
                 <th class="w3-center">Amount</th>
                 <th class="w3-center">From</th>
                 <th class="w3-center">Approved By</th>
                 <th class="w3-center">Created At</th>
                 <th class="w3-center">Status</th>
                </tr>
              </thead>
              <tbody class="w3-text-gray">
              @foreach($mticsTransferTransaction as $trans)
               <tr>
                 <td class="w3-center">
                    {{$trans->deposit_amt}}
                 </td>
                 <td class="w3-center">{{$trans->deposit_source}}</td>
                 <td class="w3-center">{{$trans->faculty->first_name}} {{$trans->faculty->last_name}}</td>
                 <td class="w3-center">{{$trans->created_at}}</td>
                 <td class="w3-center">{{$trans->deposit_status}}</td>
                </tr>
              @endforeach
             </tbody>
            </table>
        </div>

      </div>

      <div class="row" id="eventTransferTrans" style="display: none">
        <div class="table-responsive">
            <table class="table table-bordered table-hover" id="eventTransferTransTable">
              <thead>
                <tr>
                 <th class="w3-center">Amount</th>
                 <th class="w3-center">From</th>
                 <th class="w3-center">Approved By</th>
                 <th class="w3-center">Created At</th>
                 <th class="w3-center">Status</th>
                </tr>
              </thead>
              <tbody class="w3-text-gray">
              @foreach($eventTransferTransaction as $trans)
               <tr>
                 <td class="w3-center">
                    {{$trans->deposit_amt}}
                 </td>
                 <td class="w3-center">{{$trans->deposit_source}}</td>
                 <td class="w3-center">{{$trans->deposit_status == 'cleared' ? $trans->faculty->first_name . ' ' . $trans->faculty->last_name : ""}}</td>
                 <td class="w3-center">{{$trans->created_at}}</td>
                 <td class="w3-center">{{$trans->deposit_status}}</td>
                </tr>

              @endforeach
             </tbody>
            </table>
        </div>

    </div>


    </div>
    <!-- end of transfer tab -->
  </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
   $('input[type="radio"]').click(function() {
       if($(this).val() == 'event') {
            $('#eventSelect').show();
            $('#desc').hide();
       }

       else if($(this).val() == 'mtics_payment') {
            $('#eventSelect').hide();
            $('#desc').hide();
       }

       else if($(this).val() == 'others') {
            $('#desc').show();
            $('#eventSelect').hide();
       }
   });
});
</script>

<script type="text/javascript">
// Withdraw
function showMticsWithdraw() {
  $('#mticsWithdrawForm').show();
  $('#eventWithdrawForm').hide();
}

function showEventWithdraw() {
  $('#eventWithdrawForm').show();
  $('#mticsWithdrawForm').hide();
}

// DEPOSIT
function showMticsDeposit() {
  $('#mticsDepositForm').show();
  $('#eventDepositForm').hide();
}

function showEventDeposit() {
  $('#eventDepositForm').show();
  $('#mticsDepositForm').hide();
}

// WITHDRAW INPUT
function showMticsWithdrawInput(type) {
  if(type == 'withdraw_budget') {
    $('#budget').show();
    $('#externalTaskMtics').hide();
    $('#otherMtics').hide();
  }
  else if(type == 'withdraw_external') {
    $('#budget').hide();
    $('#externalTaskMtics').show();
    $('#otherMtics').hide();
  }
  else if(type == 'withdraw_other') {
    $('#budget').hide();
    $('#externalTaskMtics').hide();
    $('#otherMtics').show();
  }
}

function showEventWithdrawInput(type) {
  if(type == 'withdraw_external') {
    $('#externalTaskEvent').show();
    $('#otherEvent').hide();
  }
  else if(type == 'withdraw_other') {
    $('#externalTaskEvent').hide();
    $('#otherEvent').show();
  }
}

// DEPOSIT INPUT
function showMticsDepositInput(type) {
  if(type == 'mtics_payment') {
    $('#eventInput').hide();
    $('#descInput').hide();
  }
  else if(type == 'event') {
    $('#eventInput').show();
    $('#descInput').hide();
  }
  else if(type == 'others') {
    $('#eventInput').hide();
    $('#descInput').show();
  }
}

function showEventDepositInput(type) {
  if(type == 'mtics_payment') {
    $('#eventInputEvent').show();
    $('#descInputEvent').hide();
  }
  else if(type == 'event') {
    $('#eventInputEvent').show();
    $('#descInputEvent').hide();
  }
  else if(type == 'others') {
    $('#eventInputEvent').show();
    $('#descInputEvent').show();
  }
}
</script>

<script type="text/javascript">
function showWithdrawTable(type) {
  if(type == 'mtics') {
    $("#mticsWithdrawTrans123").show();
    $("#eventWithdrawTrans123").hide();
  }
  else if(type == 'event') {
    $("#eventWithdrawTrans123").show();
    $("#mticsWithdrawTrans123").hide();
  }
}

function showDepositTable(type) {
  if(type == 'mtics') {
    $("#mticsDepositTrans").show();
    $("#eventDepositTrans").hide();
  }
  else if(type == 'event') {
    $("#mticsDepositTrans").hide();
    $("#eventDepositTrans").show();

  }
}

function showTransferTable(type) {
  if(type == 'mtics') {
    $("#mticsTransferTrans").show();
    $("#eventTransferTrans").hide();
  }
  else if(type == 'event') {
    $("#mticsTransferTrans").hide();
    $("#eventTransferTrans").show();

  }
}

function showMticsEventTransfer(type) {
  if(type == 'mtics') {
    $('#eventDeposit').hide();
    $('#mticsDeposit').show();
  }
  else if(type == 'event') {
    $('#eventDeposit').show();
    $('#mticsDeposit').hide();
  }
}

function showEventMTICSTransfer(type) {
  if(type == 'mticsToEvent') {
    $('#eventTransferInput').hide();
  }
  else if(type == 'eventToMtics') {
    $('#eventTransferInput').show();
  }
}
</script>

<script type="text/javascript">
$(document).ready( function () {
  $('#eventWithdrawTransTable').DataTable();
  $('#mticsWithdrawTransTable').DataTable();
  $('#withdrawRequestTable').DataTable();

  $('#eventDepositTransTable').DataTable();
  $('#mticsDepositTransTable').DataTable();
  $('#depositRequestTable').DataTable();

  $('#eventTransferTransTable').DataTable();
  $('#mticsTransferTransTable').DataTable();

} );
</script>

@endsection
