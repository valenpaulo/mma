@extends('layout/control_panel')

@section('title')
Manage Documents
@endsection

@section('middle')
<div class="w3-container w3-margin">
  <div class="row">
    <h3>
      <i class="fa fa-file-text-o fa-fw w3-xxlarge"></i>
      <strong>Manage Documents</strong>
    </h3>
  </div>

  <hr>

  <div class="row w3-margin-top">

    <a href="#add" class="w3-text-gray" data-toggle="modal"><i class="fa fa-plus-square-o fa-fw"></i>Add</a>

  </div>
<!-- 
    <div class="col-lg-3 col-md-3 col-sm-12 w3-margin-top">
      <form action="{{route('admin.document')}}" method="POST">
      {{ csrf_field() }}
        <div class="w3-display-container">
          <input type="text" class="form-control" placeholder="Search" name="q">
          <div class="w3-display-right">
            <button class="btn btn-default w3-light-gray w3-text-black" type="submit">
              <i class="glyphicon glyphicon-search"></i>
            </button>
          </div>
        </div>
      </form>
    </div> -->
<!-- END HEADER -->

  <div class="row w3-margin-top">
    @foreach($documents as $document)
      <div class="col-sm-3 w3-center">
        <div class="w3-card-4 w3-pale-yellow w3-margin">
          <div class="w3-container w3-padding">
          <div class="w3-center">
          <div align="right" class="w3-margin-bottom">
          <p>
            <a href="{{url('admin/docu/download/'.$document->id)}}"><i class="fa fa-cloud-download w3-text-green fa-fw w3-large w3-hover-opacity"></i></a>

            <a href="#delete-{{$document->id}}" data-toggle="modal"><i class="fa fa-trash-o w3-text-red w3-large fa-fw w3-large w3-hover-opacity"></i></a>
          </p>
          </div>

          <hr>

          <h1>
           <i class="fa fa-file-word-o  w3-text-black"></i>
          </h1>
          </div>

          <div class="w3-center">
          <a href="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/documents/'.$document->filename) : asset('documents/'.$document->filename)}}" target="_blank" class="w3-text-black">
            <p>{{explode("/", $document->filename)[2]}}</p>
          </a>
          <p>Category: {{$document->category->category_name}}</p>
          </div>

          </div>

        </div>
      </div>

      <!-- Delete MODAL -->
        <div class="modal fade" id="delete-{{$document->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
        <h4 class="modal-title w3-text-gray" id="myModalLabel">Delete Document</h4>
        </div>
        <form action="{{url('admin/docu/delete/'.$document->id)}}" enctype="multipart/form-data">
        <div class="modal-body">
          <div class="w3-container">
            <div class="row">

              <div class="form-group">
              <p>Are you sure you want to delete <strong>{{explode("/", $document->filename)[2]}}</strong>?</p>
              </div>
            </div>
          </div>
        </div>

        <div class="modal-footer">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <button type="submit" class="btn btn-default w3-red w3-text-white"><i class="fa fa-trash fa-fw"></i> Delete</button>
        </div>

        </form>
        </div>
        </div>
        </div>
      <!-- END DELETE MODAL -->
    @endforeach

  </div>

  <!-- ADD MODAL -->
  <div class="modal fade" id="add" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
  <div class="modal-content">
  <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
  <h4 class="modal-title w3-text-gray" id="myModalLabel">Add Document</h4>
  </div>
  <form action="{{route('admin.document.store')}}" method="POST" enctype="multipart/form-data">
  <div class="modal-body">
    <div class="w3-container">
      <div class="row">
        <div class="form-group">
          <label>File Category:</label>
          <select class="form-control" name="file_category">
            @foreach($file_categories as $file_category)
              <option value="{{$file_category->id}}">{{$file_category->category_name}}</option>
            @endforeach
          </select>
        </div>

        <!-- <div class="form-group">
          <input id="year" name='year' type="number" class="form-control" placeholder="Year">
        </div> -->


        <div class="form-group filename" style="display: none">
              <p id="import-file-name"></p>
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <label for="import_file" style="cursor: pointer;" class="btn btn-default w3-red" title="Bulk Add"><i class="fa fa-paperclip fa-fw w3-text-white w3-large"></i></label>
    <input type="file" name="import_file" id="import_file" style="opacity: 0; position: absolute; z-index: -1;" />
    <button type="submit" class="btn btn-default w3-green w3-text-white"><i class="fa fa-plus-square"></i> Add</button>
  </div>
  </form>
  </div>
  </div>
  </div>
  <!-- END MODAL -->

</div>

<script type="text/javascript">
  var input = document.getElementById( 'import_file' );
  var infoArea = document.getElementById( 'import-file-name' );

  input.addEventListener('change', showFileName);

  function showFileName( event ) {

    $(".filename").show()
    // the change event gives us the input it occurred in
    var input = event.srcElement;

    // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
    var fileName = input.files[0].name;

    // use fileName however fits your app best, i.e. add it into a div
    infoArea.textContent = fileName;
  }

</script>



<script type="text/javascript">
  window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove();
    });
}, 4000);
</script>
@endsection
