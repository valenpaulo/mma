@extends('layout/control_panel')

@section('title')
MTICS Bank
@endsection

@section('middle')
<div class="w3-container w3-margin">
  <div class="row">
    <h3>
      <i class="fa fa-bank fa-fw w3-xxlarge"></i>
      <strong>Bank Transaction</strong>
    </h3>
  </div>

  <hr>

  <div class="w3-row-padding w3-margin-top">
    <div class="col-lg-1 col-md-1 col-sm-12"></div>

    <div class="col-lg-3 col-md-3 col-sm-12 w3-margin-top">
      <div class="w3-card-4 w3-amber w3-text-black w3-padding w3-container">

        <div class="w3-container">
          <div class="row" align="center">
            <h4>
              <i class="fa fa-money w3-xxlarge fa-fw"></i>
              <strong>MTICS Fund</strong>
            </h4>
          </div>
          <hr style="border-color: black">
          <div class="row">
            <h4>
              <span>
                <i class="fa fa-ellipsis-v fa-fw"></i>
                <strong>{{$mticsfund_bucket}} pesos</strong>
              </span>
            </h4>
          </div>
        </div>

      </div>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-12">
      <div class="w3-card-4 w3-teal w3-text-black w3-padding w3-container">

        <div class="w3-container">
          <div class="row" align="center">
            <h4>
              <i class="fa fa-bank w3-xxxlarge fa-fw"></i>
              <strong>Money in Bank</strong>
            </h4>
          </div>
          <hr style="border-color: black">
          <div class="row">
            <h4>
              <span>
                <i class="fa fa-ellipsis-v fa-fw"></i>
                <strong>{{$totalBucket}} pesos</strong>
              </span>
            </h4>
          </div>
        </div>

      </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-12 w3-margin-top">
      <div class="w3-card-4 w3-purple w3-text-black w3-padding w3-container">

        <div class="w3-container">
          <div class="row" align="center">
            <h4>
              <i class="fa fa-money w3-xxlarge fa-fw"></i>
              <strong>Event Fund</strong>
            </h4>
          </div>
          <hr style="border-color: black">
          <div class="row">
            <h4>
              <span>
                <i class="fa fa-ellipsis-v fa-fw"></i>
                <strong>{{$eventfund_bucket}} pesos</strong>
              </span>
            </h4>
          </div>
        </div>

      </div>
    </div>

    <div class="col-lg-1 col-md-1 col-sm-12"></div>
  </div>
  <br>

  <div class="row w3-margin-top">
    <ul class="nav nav-tabs">

      <li class="active"><a data-toggle="tab" href="#Withdraw" class="w3-text-black">
      Withdraw Transaction
      @if(count($mticsWithdrawRequest) > 0 or count($eventWithdrawRequest) > 0 or count($mticsWithdrawApprove) or count($eventWithdrawApprove))<span class="w3-badge w3-red"><?php echo count($mticsWithdrawRequest) + count($eventWithdrawRequest) + count($mticsWithdrawApprove) + count($eventWithdrawApprove); ?></span>@endif
      </a></li>

      <li><a data-toggle="tab" href="#deposit" class="w3-text-black">
      Deposit Transaction
      </a></li>
    </ul>
  </div>

    <div class="tab-content">

      <div id="Withdraw" class="tab-pane fade in active">
        @if(count($mticsWithdrawRequest) > 0 or count($eventWithdrawRequest) > 0)
        <div class="row">
          <h4>
              <i class="fa fa-paper-plane-o fa-fw"></i>
              <strong>Withdraw Requests</strong>
          </h4>
        </div>

        <div class="row w3-margin-top">
          <div class="table-responsive">
              <table class="table table-bordered table-hover" id="withdrawRequestTable">
                <thead>
                  <tr>
                   <th class="w3-center">Amount</th>
                   <th class="w3-center">Source of Money to Withdraw</th>
                   <th class="w3-center">Created At</th>
                   <th class="w3-center">Action</th>
                  </tr>
                </thead>
                <tbody class="w3-text-gray">

                @foreach($eventWithdrawRequest as $request)
                <tr>
                  <td class="w3-center">{{$request->withdrawal_amt}}</td>
                  <td class="w3-center">{{$request->withdrawal_reason}}</td>
                  <td class="w3-center">{{$request->created_at}}</td>
                  <td class="w3-center">
                    <a class="w3-text-green" href="javascript:void(0)" onclick="approvedEventWithdraw({{$request->id}})"><i class="fa fa-thumbs-up fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Approve Request"></i></a>
                    <a class="w3-text-red" href="#disapproveWithdrawEvent-{{$request->id}}" data-toggle="modal"><i class="fa fa-thumbs-down fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Disapprove Request"></i></a>

                    <form action="{{ url('admin/adviser/event-bank-transaction-request/'.$request->id.'/withdraw-approved') }}" method="POST" enctype="multipart/form-data" id="formApprovedEvent-{{$request->id}}">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </form>
                  </td>
                </tr>

                <!-- DISAPPROVE WITHDRAW MODAL -->
                <div class="modal fade" id="disapproveWithdrawEvent-{{$request->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title w3-text-gray" id="myModalLabel">Disapprove Withdraw Request</h4>
                </div>
                <form action="{{ url('admin/adviser/event-bank-transaction-request/'.$request->id.'/withdraw-denied') }}" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                  <div class="w3-container">
                    <div class="row">

                      <div class="form-group">
                        <p>You're about to disapprove this request. Please indicate the reason for your disapproval.</p>
                      </div>

                      <div class="form-group">
                       <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="denied_reason" placeholder="Denied Reason" rows="3"></textarea>
                      </div>

                      <div class="form-group">
                        <p>Do you want to proceed?</p>
                      </div>

                    </div>
                  </div>
                </div>

                <div class="modal-footer">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <button type="submit" class="btn btn-default w3-green"> Yes</button>
                  <button type="button" data-dismiss="modal" class="btn btn-default w3-red"> No</button>
                </div>

                </form>
                </div>
                </div>
                </div>
                <!-- END MODAL -->

                @endforeach

                @foreach($mticsWithdrawRequest as $request)
                <tr>
                  <td class="w3-center">{{$request->withdrawal_amt}}</td>
                  <td class="w3-center">{{$request->withdrawal_reason}}</td>
                  <td class="w3-center">{{$request->created_at}}</td>
                  <td class="w3-center">
                    <a class="w3-text-green" href="javascript:void(0)" onclick="approvedMticsWithdraw({{$request->id}})"><i class="fa fa-thumbs-up fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Approve Request"></i></a>
                    <a class="w3-text-red" href="#disapproveWithdrawMtics-{{$request->id}}" data-toggle="modal"><i class="fa fa-thumbs-down fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Disapprove Request"></i></a>

                    <form action="{{ url('admin/adviser/mtics-bank-transaction-request/'.$request->id.'/withdraw-approved') }}" method="POST" enctype="multipart/form-data" id="formApprovedMtics-{{$request->id}}">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </form>

                  </td>
                </tr>

                <!-- DISAPPROVE WITHDRAW MODAL -->
                <div class="modal fade" id="disapproveWithdrawMtics-{{$request->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title w3-text-gray" id="myModalLabel">Disapprove Withdraw Request</h4>
                </div>
                <form action="{{ url('admin/adviser/mtics-bank-transaction-request/'.$request->id.'/withdraw-denied') }}" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                  <div class="w3-container">
                    <div class="row">

                      <div class="form-group">
                        <p>You're about to disapprove this request. Please indicate the reason for your disapproval.</p>
                      </div>

                      <div class="form-group">
                       <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="denied_reason" placeholder="Denied Reason" rows="3"></textarea>
                      </div>

                      <div class="form-group">
                        <p>Do you want to proceed?</p>
                      </div>

                    </div>
                  </div>
                </div>

                <div class="modal-footer">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <button type="submit" class="btn btn-default w3-green"> Yes</button>
                  <button type="button" data-dismiss="modal" class="btn btn-default w3-red"> No</button>
                </div>

                </form>
                </div>
                </div>
                </div>
                <!-- END MODAL -->

                @endforeach
               </tbody>
              </table>
          </div>
        </div>

        <hr>
        @endif

        @if(count($mticsWithdrawApprove) > 0 or count($eventWithdrawApprove) > 0)
        <div class="row">
          <h4>
              <i class="fa fa-thumbs-up fa-fw"></i>
              <strong>Approved Withdraw Requests</strong>
          </h4>
        </div>


        <div class="row w3-margin-top">
          <div class="table-responsive">
              <table class="table table-bordered table-hover" id="approvedTable">
                <thead>
                  <tr>
                   <th class="w3-center">Amount</th>
                   <th class="w3-center">Source of Money to Withdraw</th>
                   <th class="w3-center">Created At</th>
                   <th class="w3-center">Action</th>
                  </tr>
                </thead>
                <tbody class="w3-text-gray">
                @foreach($eventWithdrawApprove as $request)
                <tr>
                  <td class="w3-center"><a href="#eventWithdrawReceipt-{{$request->id}}" data-toggle="modal" class="w3-text-gray">
                      <i class="fa fa-exclamation-circle"></i>
                      {{$request->withdrawal_amt}}
                    </a></td>
                  <td class="w3-center">{{$request->withdrawal_reason}}</td>
                  <td class="w3-center">{{$request->created_at}}</td>
                  <td class="w3-center">
                    <a class="w3-text-red" href="#uploadWithdrawReceiptEvent-{{$request->id}}" data-toggle="modal"><i class="fa fa-upload fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Upload Deposit Receipt"></i></a>
                    <a class="w3-text-orange" href="#reportWithdrawEvent-{{$request->id}}" data-toggle="modal"><i class="fa fa-exclamation-triangle fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Report"></i></a>

                    @if($request->receipt)
                    <a class="w3-text-green" href="javascript:void(0)" onclick="clearEventWithdraw({{$request->id}})"><i class="fa fa-check-circle fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Cleared"></i></a>
                    @endif

                    <form action="{{ url('admin/adviser/event-bank-transaction-request/'.$request->id.'/withdraw-cleared') }}" method="POST" enctype="multipart/form-data" id="formClearedEvent-{{$request->id}}">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </form>

                  </td>
                </tr>


                 <!-- EVENT WITHDRAW RECEIPT MODAL -->
                  <div class="modal fade" id="eventWithdrawReceipt-{{$request->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-md" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Withdraw Receipt</h4>
                  </div>

                  <div class="modal-body">
                    <div class="w3-container">
                      <div class="row" align="center">
                      @if($request->receipt)
                      <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/' . $request->receipt->receipt_image) : asset('images/' . $request->receipt->receipt_image)}}" style="width: 100%">
                      @endif

                      </div>
                    </div>
                  </div>

                  </div>
                  </div>
                  </div>
                  <!-- END MODAL -->

                <!-- UPLOAD WITHDRAW RECEIPT MODAL -->
                <div class="modal fade" id="uploadWithdrawReceiptEvent-{{$request->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title w3-text-gray" id="myModalLabel">Upload Withdraw Receipt</h4>
                </div>
                <form action="{{ url('admin/adviser/event-bank-transaction-request/'.$request->id.'/withdraw-receipt') }}" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                  <div class="w3-container">
                    <div class="row">

                      <div class="form-group">
                        <label id="ew-receipt-image-file-name-{{$request->id}}" for="receipt" style="cursor: pointer;" title="Bulk Add"><i class="fa fa-file-text fa-fw w3-text-green w3-large" ></i> Upload Receipt</label>
                        <input type="file" name="receipt" id="receipt" accept="image/*" style="opacity: 0; position: absolute; z-index: -1;" onchange="showEventWithdrawFileName(event, {{$request->id}})" />
                      </div>

                    </div>
                  </div>
                </div>

                <div class="modal-footer">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
                </div>

                </form>
                </div>
                </div>
                </div>
                <!-- END MODAL -->

                <!-- REPORT DEPOSIT MODAL -->
                <div class="modal fade" id="reportWithdrawEvent-{{$request->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title w3-text-gray" id="myModalLabel">Report</h4>
                </div>
                <form action="{{ url('admin/adviser/event-bank-transaction-request/'.$request->id.'/withdraw-report') }}" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                  <div class="w3-container">
                    <div class="row">

                      <div class="form-group">
                        <label>Report Name: </label>
                        <input type="text" name="report_name" id="report_name" tabindex="1" class="form-control" placeholder="Report Title" >
                      </div>

                      <div class="form-group">
                        <label>Reason: </label>
                        <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="report_reason" placeholder="Reason" rows="3"></textarea>
                      </div>

                    </div>
                  </div>
                </div>

                <div class="modal-footer">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <button type="submit" class="btn btn-default w3-green"><i class="fa fa-paper-plane-o"></i> Submit</button>
                </div>

                </form>
                </div>
                </div>
                </div>
                <!-- END MODAL -->

                @endforeach

                @foreach($mticsWithdrawApprove as $request)
                <tr>
                  <td class="w3-center"><a href="#mticsWithdrawReceipt-{{$request->id}}" data-toggle="modal" class="w3-text-gray">
                      <i class="fa fa-exclamation-circle"></i>
                      {{$request->withdrawal_amt}}
                    </a></td>
                  <td class="w3-center">{{$request->withdrawal_reason}}</td>
                  <td class="w3-center">{{$request->created_at}}</td>
                  <td class="w3-center">
                    <a class="w3-text-red" href="#uploadWithdrawReceiptMtics-{{$request->id}}" data-toggle="modal"><i class="fa fa-upload fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Upload Deposit Receipt"></i></a>
                    <a class="w3-text-orange" href="#reportWithdrawMtics-{{$request->id}}" data-toggle="modal"><i class="fa fa-exclamation-triangle fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Report"></i></a>

                    @if($request->receipt)
                    <a class="w3-text-green w3-large" href="javascript:void(0)" onclick="clearMticsWithdraw({{$request->id}})"><i class="fa fa-check-circle" data-toggle="tooltip" data-placement="bottom" title="Cleared"></i></a>
                    @endif

                    <form action="{{ url('admin/adviser/mtics-bank-transaction-request/'.$request->id.'/withdraw-cleared') }}" method="POST" enctype="multipart/form-data" id="formClearedMtics-{{$request->id}}">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </form>
                  </td>
                </tr>

                 <!-- MTICS WITHDRAW RECEIPT MODAL -->
                  <div class="modal fade" id="mticsWithdrawReceipt-{{$request->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-md" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Withdraw Receipt</h4>
                  </div>

                  <div class="modal-body">
                    <div class="w3-container">
                      <div class="row" align="center">
                      @if($request->receipt)
                      <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/' . $request->receipt->receipt_image) : asset('images/' . $request->receipt->receipt_image)}}" style="width: 100%">
                      @endif

                      </div>
                    </div>
                  </div>

                  </div>
                  </div>
                  </div>
                  <!-- END MODAL -->

                <!-- UPLOAD WITHDRAW RECEIPT MODAL -->
                <div class="modal fade" id="uploadWithdrawReceiptMtics-{{$request->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title w3-text-gray" id="myModalLabel">Upload Withdraw Receipt</h4>
                </div>
                <form action="{{ url('admin/adviser/mtics-bank-transaction-request/'.$request->id.'/withdraw-receipt') }}" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                  <div class="w3-container">
                    <div class="row">

                       <div class="form-group">
                        <label id="mw-receipt-image-file-name-{{$request->id}}" for="receipt" style="cursor: pointer;" title="Bulk Add"><i class="fa fa-file-text fa-fw w3-text-green w3-large" ></i> Upload Receipt</label>
                        <input type="file" name="receipt" id="receipt" accept="image/*" style="opacity: 0; position: absolute; z-index: -1;" onchange="showMticsWithdrawFileName(event, {{$request->id}})" />
                      </div>

                    </div>
                  </div>
                </div>

                <div class="modal-footer">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
                </div>

                </form>
                </div>
                </div>
                </div>
                <!-- END MODAL -->

                <!-- REPORT DEPOSIT MODAL -->
                <div class="modal fade" id="reportWithdrawMtics-{{$request->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title w3-text-gray" id="myModalLabel">Report</h4>
                </div>
                <form action="{{ url('admin/adviser/mtics-bank-transaction-request/'.$request->id.'/withdraw-report') }}" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                  <div class="w3-container">
                    <div class="row">

                      <div class="form-group">
                        <label>Report Name: </label>
                        <input type="text" name="report_name" id="report_name" tabindex="1" class="form-control" placeholder="Report Title" >
                      </div>

                      <div class="form-group">
                        <label>Reason: </label>
                        <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="report_reason" placeholder="Reason" rows="3"></textarea>
                      </div>

                    </div>
                  </div>
                </div>

                <div class="modal-footer">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <button type="submit" class="btn btn-default w3-green"><i class="fa fa-paper-plane-o"></i> Submit</button>
                </div>

                </form>
                </div>
                </div>
                </div>
                <!-- END MODAL -->

                @endforeach
               </tbody>
              </table>
          </div>
        </div>

        <hr>
        @endif

        <div class="row w3-margin-top">
          <label class="radio-inline"><input type="radio" name="withdrawRadio" onclick="showWithdrawTable('mtics')" checked>Mtics</label>
          <label class="radio-inline"><input type="radio" name="withdrawRadio" onclick="showWithdrawTable('event')">Event</label>
        </div>

        <br>

        <div class="row" id="mticsWithdrawTrans">
        @if(count($mticsWithdrawTransaction) > 0)
          <div class="table-responsive">
              <table class="table table-bordered table-hover" id="withdrawTransactionTable">
                <thead>
                  <tr>
                   <th class="w3-center">Amount</th>
                   <th class="w3-center">Source of Money to Deposit</th>
                   <th class="w3-center">Approved By</th>
                   <th class="w3-center">Created At</th>
                   <th class="w3-center">Reported</th>
                  </tr>
                </thead>
                <tbody class="w3-text-gray">
                @foreach($mticsWithdrawTransaction as $request)
                 <tr>
                   <td class="w3-center">
                    <a href="#mticsWithdrawReceipt-{{$request->id}}" data-toggle="modal" class="w3-text-gray">
                      <i class="fa fa-exclamation-circle"></i>
                      {{$request->withdrawal_amt}}
                    </a>
                   </td>
                   <td class="w3-center">{{$request->withdrawal_reason}}</td>
                   <td class="w3-center">{{$request->faculty->first_name}} {{$request->faculty->last_name}}</td>
                   <td class="w3-center">{{$request->created_at}}</td>
                   <td class="w3-center">
                      @if (!($request->report == null))
                      <a class="w3-text-red" href="#1reportinfo-{{$request->id}}" data-toggle="modal" style="outline: 0"><i class="fa fa-flag fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Report info"></i></a>
                      @endif
                   </td>
                  </tr>

                  <!-- Reported details MODAL-->
                  <div class="modal fade" id="1reportinfo-{{$request->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-sm" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Report Details</h4>
                  </div>
                  <form action="" method="POST" enctype="multipart/form-data">
                  <div class="modal-body">
                    <div class="w3-container">
                      <div class="row">
                        <div class="form-group">
                      @if (!($request->report == null))
                          <p><b>Reported by : </b>{{$request->report->faculty->first_name}} {{$request->report->faculty->last_name}}</p>
                          <p><b>Title : </b>{{$request->report->withdrawal_title}} </p>
                          <p><b>Reason : </b>{{$request->report->withdrawal_desc}} </p>
                          <p><b>Solution : </b>{{$request->report->solution}} </p>
                        @endif
                        </div>
                      </div>
                    </div>
                  </div>

                  </form>
                  </div>
                  </div>
                  </div>
                  <!-- END MODAL -->

                  <!-- MTICS WITHDRAW RECEIPT MODAL -->
                  <div class="modal fade" id="mticsWithdrawReceipt-{{$request->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-md" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Withdraw Receipt</h4>
                  </div>

                  <div class="modal-body">
                    <div class="w3-container">
                      <div class="row" align="center">
                      @if($request->receipt)
                      <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/' . $request->receipt->receipt_image) : asset('images/' . $request->receipt->receipt_image)}}" style="width: 100%">
                      @endif

                      </div>
                    </div>
                  </div>

                  </div>
                  </div>
                  </div>
                  <!-- END MODAL -->

                @endforeach
               </tbody>
              </table>
          </div>
        @endif
        </div>

        <div class="row" id="eventWithdrawTrans" style="display: none;">
        @if(count($eventWithdrawTransaction) > 0)
          <div class="table-responsive">
              <table class="table table-bordered table-hover" id="eventWithdrawTransactionTable">
                <thead>
                  <tr>
                   <th class="w3-center">Amount</th>
                   <th class="w3-center">Event</th>
                   <th class="w3-center">Source of Money to Deposit</th>
                   <th class="w3-center">Approved By</th>
                   <th class="w3-center">Created At</th>
                   <th class="w3-center">Reported</th>
                  </tr>
                </thead>
                <tbody class="w3-text-gray">


                @foreach($eventWithdrawTransaction as $request)

                 <tr>
                   <td class="w3-center">
                    <a href="#eventWithdrawReceipt-{{$request->id}}" data-toggle="modal" class="w3-text-gray">
                      <i class="fa fa-exclamation-circle"></i>
                      {{$request->withdrawal_amt}}
                    </a>
                   </td>
                   <td class="w3-center">{{$request->event->event_title}}</td>
                   <td class="w3-center">{{$request->withdrawal_reason}}</td>
                   <td class="w3-center">{{$request->faculty->first_name}} {{$request->faculty->last_name}}</td>
                   <td class="w3-center">{{$request->created_at}}</td>
                   <td class="w3-center">
                      @if (!($request->report == null))
                      <a class="w3-text-red" href="#reportinfo-{{$request->id}}" data-toggle="modal" style="outline: 0"><i class="fa fa-flag fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Report info"></i></a>
                      @endif
                   </td>
                  </tr>

                  <!-- Reported details MODAL-->
                  <div class="modal fade" id="reportinfo-{{$request->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-sm" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Report Details</h4>
                  </div>
                  <form action="" method="POST" enctype="multipart/form-data">
                  <div class="modal-body">
                    <div class="w3-container">
                      <div class="row">
                        <div class="form-group">
                      @if (!($request->report == null))
                          <p><b>Reported by : </b>{{$request->report->faculty->first_name}} {{$request->report->faculty->last_name}}</p>
                          <p><b>Title : </b>{{$request->report->withdrawal_title}} </p>
                          <p><b>Reason : </b>{{$request->report->withdrawal_desc}} </p>
                          <p><b>Solution : </b>{{$request->report->solution}} </p>
                        @endif
                        </div>
                      </div>
                    </div>
                  </div>

                  </form>
                  </div>
                  </div>
                  </div>
                  <!-- END MODAL -->


                  <!-- EVENT WITHDRAW RECEIPT MODAL -->
                  <div class="modal fade" id="eventWithdrawReceipt-{{$request->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-md" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Withdraw Receipt</h4>
                  </div>

                  <div class="modal-body">
                    <div class="w3-container">
                      <div class="row" align="center">

                      <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$request->receipt->receipt_image) : asset('images/'.$request->receipt->receipt_image)}}" style="width: 100%">

                      </div>
                    </div>
                  </div>

                  </div>
                  </div>
                  </div>
                  <!-- END MODAL -->
                @endforeach
               </tbody>
              </table>
          </div>

        @endif
        </div>
      <!-- end of payment_breakdown tab -->
      </div>

      <div id="deposit" class="tab-pane fade">

        @if(count($mticsDepositRequest) > 0 or count($eventDepositRequest) > 0)

        <div class="row w3-margin-top">
          <h4>
              <i class="fa fa-paper-plane-o fa-fw"></i>
              Deposit Requests
          </h4>
        </div>

        <div class="row w3-margin-top">
          <div class="table-responsive">
              <table class="table table-bordered table-hover" id="depositRequestTable">
                <thead>
                  <tr>
                   <th class="w3-center">Amount</th>
                   <th class="w3-center">Source of Money to Deposit</th>
                   <th class="w3-center">Created At</th>
                   <th class="w3-center">Action</th>
                  </tr>
                </thead>
                <tbody class="w3-text-gray">

                @foreach($eventDepositRequest as $request)
                <tr>
                  <td class="w3-center">{{$request->deposit_amt}}</td>
                  <td class="w3-center">{{$request->deposit_source}}</td>
                  <td class="w3-center">{{$request->created_at}}</td>
                  <td class="w3-center">
                    <a class="w3-text-green" href="javascript:void(0)" onclick="approvedEventDeposit({{$request->id}})"><i class="fa fa-thumbs-up fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Approve Request"></i></a>
                    <a class="w3-text-red" href="#disapproveDeposit-{{$request->id}}" data-toggle="modal"><i class="fa fa-thumbs-down fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Disapprove Request"></i></a>

                    <form action="{{ url('admin/adviser/event-bank-transaction-request/'.$request->id.'/deposit-approved') }}" method="POST" enctype="multipart/form-data" id="formApprovedEventDeposit-{{$request->id}}">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </form>
                  </td>
                </tr>

                <!-- DISAPPROVE DEPOSIT MODAL -->
                <div class="modal fade" id="disapproveDeposit-{{$request->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title w3-text-gray" id="myModalLabel">Disapprove Deposit Request</h4>
                </div>
                <form action="{{ url('admin/adviser/event-bank-transaction-request/'.$request->id.'/deposit-denied') }}" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                  <div class="w3-container">
                    <div class="row">

                      <div class="form-group">
                        <p>You're about to disapprove this request. Please indicate the reason for your disapproval.</p>
                      </div>

                      <div class="form-group">
                       <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="denied_reason" placeholder="Denied Reason" rows="3"></textarea>
                      </div>

                      <div class="form-group">
                        <p>Do you want to proceed?</p>
                      </div>

                    </div>
                  </div>
                </div>

                <div class="modal-footer">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <button type="submit" class="btn btn-default w3-green"> Yes</button>
                  <button type="button" data-dismiss="modal" class="btn btn-default w3-red"> No</button>
                </div>

                </form>
                </div>
                </div>
                </div>
                <!-- END MODAL -->

                @endforeach

                @foreach($mticsDepositRequest as $request)
                <tr>
                  <td class="w3-center">{{$request->deposit_amt}}</td>
                  <td class="w3-center">{{$request->deposit_source}}</td>
                  <td class="w3-center">{{$request->created_at}}</td>
                  <td class="w3-center">
                    <a class="w3-text-green" href="javascript:void(0)" onclick="approvedMticsDeposit({{$request->id}})"><i class="fa fa-thumbs-up fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Approve Request"></i></a>
                    <a class="w3-text-red" href="#disapproveDeposit-{{$request->id}}" data-toggle="modal"><i class="fa fa-thumbs-down fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Disapprove Request"></i></a>

                    <form action="{{ url('admin/adviser/mtics-bank-transaction-request/'.$request->id.'/deposit-approved') }}" method="POST" enctype="multipart/form-data" id="formApprovedMticsDeposit-{{$request->id}}">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </form>
                  </td>
                </tr>

                <!-- DISAPPROVE DEPOSIT MODAL -->
                <div class="modal fade" id="disapproveDeposit-{{$request->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title w3-text-gray" id="myModalLabel">Disapprove Deposit Request</h4>
                </div>
                <form action="{{ url('admin/adviser/mtics-bank-transaction-request/'.$request->id.'/deposit-denied') }}" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                  <div class="w3-container">
                    <div class="row">

                      <div class="form-group">
                        <p>You're about to disapprove this request. Please indicate the reason for your disapproval.</p>
                      </div>

                      <div class="form-group">
                       <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="denied_reason" placeholder="Denied Reason" rows="3"></textarea>
                      </div>

                      <div class="form-group">
                        <p>Do you want to proceed?</p>
                      </div>

                    </div>
                  </div>
                </div>

                <div class="modal-footer">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <button type="submit" class="btn btn-default w3-green"> Yes</button>
                  <button type="button" data-dismiss="modal" class="btn btn-default w3-red"> No</button>
                </div>

                </form>
                </div>
                </div>
                </div>
                <!-- END MODAL -->

                @endforeach
               </tbody>
              </table>
          </div>
        </div>
        @endif

        @if(count($mticsDepositApprove) > 0 or count($eventDepositApprove) > 0)

        <div class="row w3-margin-top">
          <h4>
              <i class="fa fa-thumbs-up fa-fw"></i>
              Approved Deposit Requests
          </h4>
        </div>

        <div class="row w3-margin-top">
          <div class="table-responsive">
              <table class="table table-bordered table-hover" id="depositApprovedTable">
                <thead>
                  <tr>
                   <th class="w3-center">Amount</th>
                   <th class="w3-center">Source of Money to Deposit</th>
                   <th class="w3-center">Created At</th>
                   <th class="w3-center">Action</th>
                  </tr>
                </thead>
                <tbody class="w3-text-gray">

                @foreach($eventDepositApprove as $request)
                <tr>
                  <td class="w3-center"><a href="#eventdepositReceipt-{{$request->id}}" data-toggle="modal" class="w3-text-gray">
                      <i class="fa fa-exclamation-circle"></i>
                      {{$request->deposit_amt}}
                    </a></td>
                  <td class="w3-center">{{$request->deposit_source}}</td>
                  <td class="w3-center">{{$request->created_at}}</td>
                  <td class="w3-center">
                    <a class="w3-text-red" href="#uploadDepositReceiptevent-{{$request->id}}" data-toggle="modal"><i class="fa fa-upload fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Upload Deposit Receipt"></i></a>
                    <a class="w3-text-orange" href="#reportDepositevent-{{$request->id}}" data-toggle="modal"><i class="fa fa-exclamation-triangle fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Report"></i></a>

                    @if($request->receipt)

                    <a class="w3-text-green w3-large" href="javascript:void
                    (0)" onclick="clearEventDeposit({{$request->id}})"><i class="fa fa-check-circle" data-toggle="tooltip" data-placement="bottom" title="Cleared"></i></a>
                    @endif

                    <form action="{{ url('admin/adviser/event-bank-transaction-request/'.$request->id.'/deposit-cleared') }}" method="POST" enctype="multipart/form-data" id="formClearedEventDeposit-{{$request->id}}">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </form>
                  </td>
                </tr>


                 <!-- EVENT Deposit RECEIPT MODAL -->
                  <div class="modal fade" id="eventdepositReceipt-{{$request->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-md" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Deposit Receipt</h4>
                  </div>

                  <div class="modal-body">
                    <div class="w3-container">
                      <div class="row" align="center">
                      @if($request->receipt)
                      <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/' . $request->receipt->receipt_image) : asset('images/' . $request->receipt->receipt_image)}}" style="width: 100%">
                      @endif

                      </div>
                    </div>
                  </div>

                  </div>
                  </div>
                  </div>
                  <!-- END MODAL -->


                <!-- UPLOAD DEPOSIT RECEIPT MODAL -->
                <div class="modal fade" id="uploadDepositReceiptevent-{{$request->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title w3-text-gray" id="myModalLabel">Upload Deposit Receipt</h4>
                </div>
                <form action="{{ url('admin/adviser/event-bank-transaction-request/'.$request->id.'/deposit-receipt') }}" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                  <div class="w3-container">
                    <div class="row">

                       <div class="form-group">
                        <label id="receipt-image-file-name-{{$request->id}}" for="receipt" style="cursor: pointer;" title="Bulk Add"><i class="fa fa-file-text fa-fw w3-text-green w3-large" ></i> Upload Receipt</label>
                        <input type="file" name="receipt" id="receipt" accept="image/*" style="opacity: 0; position: absolute; z-index: -1;" onchange="showFileName1(event, {{$request->id}})" />
                      </div>

                    </div>
                  </div>
                </div>

                <div class="modal-footer">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
                </div>

                </form>
                </div>
                </div>
                </div>
                <!-- END MODAL -->

                <!-- REPORT DEPOSIT MODAL -->
                <div class="modal fade" id="reportDepositevent-{{$request->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title w3-text-gray" id="myModalLabel">Report</h4>
                </div>
                <form action="{{ url('admin/adviser/event-bank-transaction-request/'.$request->id.'/deposit-report') }}" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                  <div class="w3-container">
                    <div class="row">

                      <div class="form-group">
                        <label>Report Name: </label>
                        <input type="text" name="report_name" id="report_name" tabindex="1" class="form-control" placeholder="Report Title" >
                      </div>

                      <div class="form-group">
                        <label>Reason: </label>
                        <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="report_reason" placeholder="Reason" rows="3"></textarea>
                      </div>

                    </div>
                  </div>
                </div>

                <div class="modal-footer">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <button type="submit" class="btn btn-default w3-green"><i class="fa fa-paper-plane-o"></i> Submit</button>
                </div>

                </form>
                </div>
                </div>
                </div>
                <!-- END MODAL -->

                @endforeach

                @foreach($mticsDepositApprove as $request)
                <tr>
                  <td class="w3-center"><a href="#mticsdepositReceipt-{{$request->id}}" data-toggle="modal" class="w3-text-gray">
                      <i class="fa fa-exclamation-circle"></i>
                      {{$request->deposit_amt}}
                    </a></td>
                  <td class="w3-center">{{$request->deposit_source}}</td>
                  <td class="w3-center">{{$request->created_at}}</td>
                  <td class="w3-center">
                    <a class="w3-text-red" href="#uploadDepositReceiptmtics-{{$request->id}}" data-toggle="modal"><i class="fa fa-upload fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Upload Deposit Receipt"></i></a>
                    <a class="w3-text-orange" href="#reportDepositmtics-{{$request->id}}" data-toggle="modal"><i class="fa fa-exclamation-triangle fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Report"></i></a>

                    @if($request->receipt)
                    <a class="w3-text-green w3-large" href="javascript:void(0)" onclick="clearMticsDeposit({{$request->id}})"><i class="fa fa-check-circle fa-fw" data-toggle="tooltip" data-placement="bottom" title="Cleared"></i></a>
                    @endif

                    <form action="{{ url('admin/adviser/mtics-bank-transaction-request/'.$request->id.'/deposit-cleared') }}" method="POST" enctype="multipart/form-data" id="formClearedMticsDeposit-{{$request->id}}">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </form>
                  </td>
                </tr>

                 <!-- MTICS Deposit RECEIPT MODAL -->
                  <div class="modal fade" id="mticsdepositReceipt-{{$request->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-md" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Deposit Receipt</h4>
                  </div>

                  <div class="modal-body">
                    <div class="w3-container">
                      <div class="row" align="center">
                      @if($request->receipt)
                      <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/' . $request->receipt->receipt_image) : asset('images/' . $request->receipt->receipt_image)}}" style="width: 100%">
                      @endif

                      </div>
                    </div>
                  </div>

                  </div>
                  </div>
                  </div>
                  <!-- END MODAL -->

                <!-- UPLOAD DEPOSIT RECEIPT MODAL -->
                <div class="modal fade" id="uploadDepositReceiptmtics-{{$request->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title w3-text-gray" id="myModalLabel">Upload Deposit Receipt</h4>
                </div>
                <form action="{{ url('admin/adviser/mtics-bank-transaction-request/'.$request->id.'/deposit-receipt') }}" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                  <div class="w3-container">
                    <div class="row">

                       <div class="form-group">
                        <label id="receipt-image-file-name-{{$request->id}}" for="receipt" style="cursor: pointer;" title="Bulk Add"><i class="fa fa-file-text fa-fw w3-text-green w3-large" ></i> Upload Receipt</label>
                        <input type="file" name="receipt" id="receipt" accept="image/*" style="opacity: 0; position: absolute; z-index: -1;" onchange="showFileName1(event, {{$request->id}})" />
                      </div>

                    </div>
                  </div>
                </div>

                <div class="modal-footer">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
                </div>

                </form>
                </div>
                </div>
                </div>
                <!-- END MODAL -->

                <!-- REPORT DEPOSIT MODAL -->
                <div class="modal fade" id="reportDepositmtics-{{$request->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title w3-text-gray" id="myModalLabel">Report</h4>
                </div>
                <form action="{{ url('admin/adviser/mtics-bank-transaction-request/'.$request->id.'/deposit-report') }}" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                  <div class="w3-container">
                    <div class="row">

                      <div class="form-group">
                        <label>Report Name: </label>
                        <input type="text" name="report_name" id="report_name" tabindex="1" class="form-control" placeholder="Report Title" >
                      </div>

                      <div class="form-group">
                        <label>Reason: </label>
                        <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="report_reason" placeholder="Reason" rows="3"></textarea>
                      </div>

                    </div>
                  </div>
                </div>

                <div class="modal-footer">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <button type="submit" class="btn btn-default w3-green"><i class="fa fa-paper-plane-o"></i> Submit</button>
                </div>

                </form>
                </div>
                </div>
                </div>
                <!-- END MODAL -->

                @endforeach
               </tbody>
              </table>
          </div>
        </div>
        @endif

        <div class="row w3-margin-top">
          <label class="radio-inline"><input type="radio" name="DepositRadio" onclick="showDepositTable('mtics')" checked>Mtics</label>
          <label class="radio-inline"><input type="radio" name="DepositRadio" onclick="showDepositTable('event')">Event</label>
        </div>

        <br>

        <div class="row" id="mticsDepositTrans">
        @if(count($mticsDepositTransaction) > 0)
          <div class="table-responsive">
              <table class="table table-bordered table-hover" id="depositMticsTable">
                <thead>
                  <tr>
                   <th class="w3-center">Amount</th>
                   <th class="w3-center">Source of Money to Deposit</th>
                   <th class="w3-center">Approved By</th>
                   <th class="w3-center">Created At</th>
                   <th class="w3-center">Reported</th>
                  </tr>
                </thead>
                <tbody class="w3-text-gray">
                @foreach($mticsDepositTransaction as $request)
                 <tr>
                   <td class="w3-center">
                     <a href="#mticsReceipt-{{$request->id}}" data-toggle="modal" class="w3-text-gray">
                      <i class="fa fa-exclamation-circle"></i>
                      {{$request->deposit_amt}}
                     </a>
                   </td>
                   <td class="w3-center">{{$request->deposit_source}}</td>
                   <td class="w3-center">{{$request->faculty->first_name}} {{$request->faculty->last_name}}</td>
                   <td class="w3-center">{{$request->created_at}}</td>
                   <td class="w3-center">
                      @if (!($request->report == null))
                      <a class="w3-text-red" href="#reportinfo-{{$request->id}}" data-toggle="modal" style="outline: 0"><i class="fa fa-flag fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Report info"></i></a>
                      @endif
                   </td>
                  </tr>

                   <!-- Reported details MODAL-->
                  <div class="modal fade" id="reportinfo-{{$request->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-sm" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Report Details</h4>
                  </div>
                  <form action="" method="POST" enctype="multipart/form-data">
                  <div class="modal-body">
                    <div class="w3-container">
                      <div class="row">
                        <div class="form-group">
                      @if (!($request->report == null))
                          <p><b>Reported by : </b>{{$request->report->faculty->first_name}} {{$request->report->faculty->last_name}}</p>
                          <p><b>Title : </b>{{$request->report->deposit_title}} </p>
                          <p><b>Reason : </b>{{$request->report->deposit_desc}} </p>
                          <p><b>Solution : </b>{{$request->report->solution}} </p>
                        @endif
                        </div>
                      </div>
                    </div>
                  </div>

                  </form>
                  </div>
                  </div>
                  </div>
                  <!-- END MODAL -->

                  <!-- MTICS DEPOSIT RECEIPT MODAL -->
                  <div class="modal fade" id="mticsReceipt-{{$request->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-md" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Deposit Receipt</h4>
                  </div>

                  <div class="modal-body">
                    <div class="w3-container">
                      <div class="row" align="center">
                      @if($request->receipt)
                      <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/' . $request->receipt->receipt_image) : asset('images/' . $request->receipt->receipt_image)}}" style="width: 100%">
                      @endif

                      </div>
                    </div>
                  </div>

                  </div>
                  </div>
                  </div>
                  <!-- END MODAL -->

                @endforeach
               </tbody>
              </table>
          </div>
        @endif
        </div>

        <div class="row" id="eventDepositTrans" style="display: none;">
        @if(count($eventDepositTransaction) > 0)
          <div class="table-responsive">
              <table class="table table-bordered table-hover" id="depositEventTable">
                <thead>
                  <tr>
                   <th class="w3-center">Amount</th>
                   <th class="w3-center">Event</th>
                   <th class="w3-center">Source of Money to Deposit</th>
                   <th class="w3-center">Approved By</th>
                   <th class="w3-center">Created At</th>
                   <th class="w3-center">Reported</th>
                  </tr>
                </thead>
                <tbody class="w3-text-gray">

                @foreach($eventDepositTransaction as $request)
                 <tr>
                   <td class="w3-center">
                    <a href="#eventReceipt-{{$request->id}}" data-toggle="modal" class="w3-text-gray">
                      <i class="fa fa-exclamation-circle"></i>
                      {{$request->deposit_amt}}
                     </a>
                   </td>
                   <td class="w3-center">{{$request->event->event_title}}</td>
                   <td class="w3-center">{{$request->deposit_source}}</td>
                   <td class="w3-center">{{$request->faculty->first_name}} {{$request->faculty->last_name}}</td>
                   <td class="w3-center">{{$request->created_at}}</td>
                   <td class="w3-center">
                    @if (!($request->report == null))
                      <a class="w3-text-red" href="#reportinfo-{{$request->id}}" data-toggle="modal" style="outline: 0"><i class="fa fa-flag fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Report info"></i></a>
                      @endif
                  </td>
                  </tr>


                  <!-- Reported details MODAL-->
                  <div class="modal fade" id="reportinfo-{{$request->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-sm" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Report Details</h4>
                  </div>
                  <form action="" method="POST" enctype="multipart/form-data">
                  <div class="modal-body">
                    <div class="w3-container">
                      <div class="row">
                        <div class="form-group">
                      @if (!($request->report == null))
                          <p><b>Reported by : </b>{{$request->report->faculty->first_name}} {{$request->report->faculty->last_name}}</p>
                          <p><b>Title : </b>{{$request->report->deposit_title}} </p>
                          <p><b>Reason : </b>{{$request->report->deposit_desc}} </p>
                          <p><b>Solution : </b>{{$request->report->solution}} </p>
                        @endif
                        </div>
                      </div>
                    </div>
                  </div>

                  </form>
                  </div>
                  </div>
                  </div>
                  <!-- END MODAL -->


                  <!-- EVENT DEPOSIT RECEIPT MODAL -->
                  <div class="modal fade" id="eventReceipt-{{$request->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-md" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Deposit Receipt</h4>
                  </div>

                  <div class="modal-body">
                    <div class="w3-container">
                      <div class="row" align="center">
                      @if($request->receipt)
                      <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/' . $request->receipt->receipt_image) : asset('images/' . $request->receipt->receipt_image)}}" style="width: 100%">
                      @endif

                      </div>
                    </div>
                  </div>

                  </div>
                  </div>
                  </div>
                  <!-- END MODAL -->

                @endforeach
               </tbody>
              </table>
          </div>
        @endif
        </div>
      <!-- end of money request tab -->
      </div>

    </div>

</div>

<script>
  // var input = document.getElementById( 'receipt' );

  // input.addEventListener('change', showFileName1);

  function showFileName1( event, id ) {

    // the change event gives us the input it occurred in
    var infoArea = document.getElementById( 'receipt-image-file-name-' + id );
    var input = event.srcElement;

    // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
    var fileName = input.files[0].name;

    // use fileName however fits your app best, i.e. add it into a div
    infoArea.textContent = fileName;
  };
</script>

<script>
  // var input = document.getElementById( 'receipt' );

  // input.addEventListener('change', showFileName1);

  function showEventWithdrawFileName( event, id ) {

    // the change event gives us the input it occurred in
    var infoArea = document.getElementById( 'ew-receipt-image-file-name-' + id );
    var input = event.srcElement;

    // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
    var fileName = input.files[0].name;

    // use fileName however fits your app best, i.e. add it into a div
    infoArea.textContent = fileName;
  };
</script>

<script>
  // var input = document.getElementById( 'receipt' );

  // input.addEventListener('change', showFileName1);

  function showMticsWithdrawFileName( event, id ) {

    // the change event gives us the input it occurred in
    var infoArea = document.getElementById( 'mw-receipt-image-file-name-' + id );
    var input = event.srcElement;

    // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
    var fileName = input.files[0].name;

    // use fileName however fits your app best, i.e. add it into a div
    infoArea.textContent = fileName;
  };
</script>

<script type="text/javascript">
function showDepositTable(type) {
  if(type == 'mtics') {
    $('#eventDepositTrans').hide();
    $('#mticsDepositTrans').show();
  }
  else if(type == 'event') {
    $('#eventDepositTrans').show();
    $('#mticsDepositTrans').hide();
  }
}

function showWithdrawTable(type) {
  if(type == 'mtics') {
    $('#eventWithdrawTrans').hide();
    $('#mticsWithdrawTrans').show();
  }
  else if(type == 'event') {
    $('#eventWithdrawTrans').show();
    $('#mticsWithdrawTrans').hide();
  }
}
</script>

<script type="text/javascript">
$(document).ready( function () {
  $('#depositRequestTable').DataTable();
} );
</script>


<script type="text/javascript">
function approvedEventWithdraw( id ) {
  swal({
  title: "Are you sure?",
  text: "Once approved, you will not be able to undo this action!",
  icon: "warning",
  buttons: true,
  dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      $("#formApprovedEvent-" + id).submit();
    }
  });
}

function approvedMticsWithdraw( id ) {
  swal({
  title: "Are you sure?",
  text: "Once approved, you will not be able to undo this action!",
  icon: "warning",
  buttons: true,
  dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      $("#formApprovedMtics-" + id).submit();
    }
  });
}

function clearEventWithdraw( id ) {
  swal({
  title: "Are you sure?",
  text: "Once cleared, you will not be able to undo this action!",
  icon: "warning",
  buttons: true,
  dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      $("#formClearedEvent-" + id).submit();
    }
  });
}

function clearMticsWithdraw( id ) {
  swal({
  title: "Are you sure?",
  text: "Once cleared, you will not be able to undo this action!",
  icon: "warning",
  buttons: true,
  dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      $("#formClearedMtics-" + id).submit();
    }
  });
}
</script>

<script type="text/javascript">
function approvedEventDeposit( id ) {
  swal({
  title: "Are you sure?",
  text: "Once approved, you will not be able to undo this action!",
  icon: "warning",
  buttons: true,
  dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      $("#formApprovedEventDeposit-" + id).submit();
    }
  });
}

function approvedMticsDeposit( id ) {
  swal({
  title: "Are you sure?",
  text: "Once approved, you will not be able to undo this action!",
  icon: "warning",
  buttons: true,
  dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      $("#formApprovedMticsDeposit-" + id).submit();
    }
  });
}

function clearEventDeposit( id ) {
  swal({
  title: "Are you sure?",
  text: "Once cleared, you will not be able to undo this action!",
  icon: "warning",
  buttons: true,
  dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      $("#formClearedEventDeposit-" + id).submit();
    }
  });
}

function clearMticsDeposit( id ) {
  swal({
  title: "Are you sure?",
  text: "Once cleared, you will not be able to undo this action!",
  icon: "warning",
  buttons: true,
  dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      $("#formClearedMticsDeposit-" + id).submit();
    }
  });
}
</script>

<script type="text/javascript">
$(document).ready( function () {
  var withdrawRequestTable = $('#withdrawRequestTable').DataTable();
  $(withdrawRequestTable.table().container()).removeClass('form-inline');

  var approvedTable = $('#approvedTable').DataTable();
  $(approvedTable.table().container()).removeClass('form-inline');

  $('#withdrawTransactionTable').DataTable();
  $('#eventWithdrawTransactionTable').DataTable();

  // deposit
  var depositRequestTable = $('#depositRequestTable').DataTable();
  $(depositRequestTable.table().container()).removeClass('form-inline');

  var depositApprovedTable = $('#depositApprovedTable').DataTable();
  $(depositApprovedTable.table().container()).removeClass('form-inline');

  $('#depositMticsTable').DataTable();
  $('#depositEventTable').DataTable();

} );
</script>
@endsection
