@extends('layout/control_panel')

@section('title')
Manage Event
@endsection

@section('middle')
<div class="container-fluid w3-margin">
  <div class="row">

  <div class="col-lg-3 col-md-3 col-sm-12">
    <h3>
        <i class="fa fa-calendar-check-o fa-fw"></i> <strong>Manage Event</strong>
    </h3>
  </div>

  <div class="col-lg-1 col-md-1 col-sm-12 w3-margin-top">
      <button class="btn btn-default w3-green" data-toggle="modal" data-target="#add" title="Add"><i class="fa fa-plus-square fa-fw" ></i> Add</button>
  </div>

  <div class="col-sm-4"></div>

  </div>

  <div class="row w3-margin-top">
    <!-- TABLE -->
    <table class="table table-hover table-responsive">
      <thead class="w3-teal">
        <tr>
         <th class="w3-center">Title</th>
         <th class="w3-center">Star Date</th>
         <th class="w3-center">End Date</th>
         <th class="w3-center">Status</th>
         <th class="w3-center">Action</th>
        </tr>
      </thead>
      <tbody class="w3-text-gray">
        @foreach($events as $event)
        <tr>
          <td class="w3-center">{{$event->event_title}}</td>
          <td class="w3-center">{{$event->start_date}}</td>
          <td class="w3-center">{{$event->end_date}}</td>
          <td class="w3-center">{{$event->status}}</td>
          <td class="w3-center">
            <a class="btn btn-default w3-blue w3-text-white" href="{{url('admin/president/manage-event/'.$event->id)}}" data-toggle="tooltip" data-placement="bottom" title="View"><i class="fa fa-eye"></i></a>

            <a class="btn btn-default w3-green w3-text-white" href="{{url('admin/president/manage-event/start/'.$event->id)}}" data-toggle="tooltip" data-placement="bottom" title="Start"><i class="fa fa-play"></i></a>

            <a class="btn btn-default w3-red w3-text-white" href="{{url('admin/president/manage-event/cancel/'.$event->id)}}" data-toggle="tooltip" data-placement="bottom" title="Cancel"><i class="fa fa-remove"></i></a>

            <button class="btn btn-default w3-green" data-toggle="modal" data-target="#participant-{{$event->id}}" title="Add"><i class="fa fa-plus-square fa-fw" ></i> participant</button>

           <a class="btn btn-default w3-green w3-text-white" href="{{url('admin/president/manage-event/done/'.$event->id)}}" data-toggle="tooltip" data-placement="bottom" title="Start"><i class="fa fa-play"></i>done</a>
          </td>
        </tr>

        @endforeach
      </tbody>
    </table>
    <!-- END TABLE -->
  </div>

<!-- GRID OF DOCUMENTS -->

  <!-- ADD MODAL -->
  <div class="modal fade" id="add" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-md" role="document">
  <div class="modal-content">
  <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
  <h4 class="modal-title w3-text-gray" id="myModalLabel">Add Event</h4>
  </div>
  <form action="{{ url('admin/president/manage-event/store') }}" method="POST" enctype="multipart/form-data">

  <div class="modal-body">
    <div class="w3-container">
      <div class="row">

      <h3>
        <i class="fa fa-calendar-check-o fa-fw"></i>
        Event
      </h3>

      <div class="form-group">
        <label for="title">Event Title:</label>
        <input type="text" name="event_title" class="form-control" placeholder="Event Title" required>
      </div>

      <div class="form-group">
        <label for="description">Description:</label>
        <input type="text" name="description" class="form-control" placeholder="Event Description">
      </div>

      <div class="form-group">
        <label for="start_date">Start Date:</label>
        <input type="date" name="start_date" class="form-control" placeholder="Event Title" required>
      </div>

      <div class="form-group">
        <label for="end_date">End Date:</label>
        <input type="date" name="end_date" class="form-control" placeholder="Event Title">
      </div>

      <br>
      <h3>
        <i class="fa fa-group fa-fw"></i>
        Meeting
      </h3>

      <div class="from-group">
        <label>Agenda</label>
        <input type="text" name="agenda" class="form-control" placeholder="Agenda of the meeting">
      </div>

      <div class="from-group">
        <label>Where</label>
        <input type="text" name="where" class="form-control" placeholder="Meeting Place">
      </div>

      <div class="from-group">
        <label>When</label>
        <input type="datetime-local" name="when" class="form-control" placeholder="Date and Time of the Meeting">
      </div>

      </div>
    </div>
  </div>

  <div class="modal-footer">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <button type="submit" class="btn btn-default w3-green"><i class="fa fa-plus-square fa-fw"></i> Add</button>
  </div>

  </form>
  </div>
  </div>
  </div>
  <!-- END MODAL -->




</div>

<script>
function showPayment(id) {
  var xhttp;
  if (id == "") {
    document.getElementById("payment").innerHTML = "";
    return;
  }
  xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      $("#mtics_payment").val(this.responseText);
    }
  };
  xhttp.open("GET", "{{url('admin/finance/payment/ajax-call')}}/"+id, true);
  xhttp.send();
}
</script>


<script type="text/javascript">
  window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove();
    });
}, 4000);
</script>
@endsection
