@extends('layout/control_panel')

@section('title')
Reimbursement Request
@endsection

@section('middle')
<div class="w3-container w3-margin">
  <div class="row">
    <h3>
      <i class="fa fa-paper-plane-o fa-fw w3-xxlarge"></i>
      <strong>Reimburse Request</strong>
    </h3>
  </div>

  <hr>

  <div class="w3-margin-top row">
    <div class="table-responsive">
      <table class="table table-bordered table-hover" id="reimburseTable">
        <thead>
          <tr>
           <th class="w3-center">Task</th>
           <th class="w3-center">Requested Money</th>
           <th class="w3-center">Reimbursement</th>
           <th class="w3-center">Status</th>
          </tr>
        </thead>
        <tbody class="w3-text-gray">
          @foreach($reimburses as $reimburse)
          <tr>
            <td class="w3-center">
            @if (count($reimburse->confirm) >= 1)
            <a href="{{url('admin/auditor/view-reimburse-info/' . $reimburse->id)}}" class="w3-text-gray">
            <i class="fa fa-exclamation-circle fa-fw"></i>
            {{$reimburse->moneyrequest->task->task_name}}
            </a>
            @else
            {{$reimburse->moneyrequest->task->task_name}}
            @endif
            </td>
            <td class="w3-center">{{$reimburse->moneyrequest->amount}}</td>
            <td class="w3-center">{{$reimburse->reimburse_amount}}</td>
            <td class="w3-center">{{$reimburse->reimburse_status}}</td>
          </tr>
          @endforeach
        </tbody>
      </table>

    </div>

  </div>

</div>

<script type="text/javascript">
$(document).ready( function () {
  $('#reimburseTable').DataTable();
});
</script>

@endsection
