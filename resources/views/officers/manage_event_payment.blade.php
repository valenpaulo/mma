@extends('layout/control_panel')

@section('title')
Manage MTICS Payment
@endsection

@section('middle')
  <b>EVENT</b><br><br>

    event id: {{$event->id}}<br>
    event title: {{$event->event_title}}<br>
    event desc: {{$event->description}}<br>
    event status: {{$event->description}}<br>
    event amount: {{$event->event_amount}}<br>
    event amount confirm status: {{$event->amount_confirm}}<br><br>

    @if($event->breakdown !== null)
      <b>BREAKDOWN</b><br><br>
      @foreach($event->breakdown as $breakdown)
        id: {{$breakdown->id}}<br>
        name: {{$breakdown->breakdown_name}}<br>
        desc: {{$breakdown->breakdown_desc}}<br>
        amt: {{$breakdown->breakdown_amt}}<br>
        <button class="btn btn-default w3-orange w3-text-white" data-toggle="modal" data-target="#edit-{{$breakdown->id}}" title="Edit"><i class="fa fa-pencil"></i>edit</button><br><br>

        <!-- EDIT BREAKDOWn -->
         <div class="modal fade" id="edit-{{$breakdown->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title w3-text-gray" id="myModalLabel">Breakdown</h4>
              </div>
    <form action="{{ url('admin/finance/event-fund/'.$event->id.'/manage-payment/'.$breakdown->id.'/edit') }}" method="POST" >
              <div class="modal-body">

          <b>EDIT BREAKDOWN</b><br><br>

                    <div class="form-group">
                    <label>Name: </label>
                    <input type="text" name="bname" id="bname" tabindex="1" class="form-control" placeholder="Name" value="{{$breakdown->breakdown_name}}">
                  </div>
                  <div class="form-group">
                    <label>Desc: </label>
                    <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="bdesc" placeholder="desc" rows="3">{{$breakdown->breakdown_desc}}</textarea>
                  </div>

                   <div class="form-group">
                    <label>amount: </label>
                    <input type="number" name="bamt" id="bamt" tabindex="1" class="form-control" placeholder="Amount" value="{{$breakdown->breakdown_amt}}"><br><br>
                  </div>


              <div class="modal-footer">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <button type="submit" class="btn btn-default w3-red"><i class="fa fa-plus-square fa-fw"></i> Yes</button>
              </div>

              </div>
            </form>
            </div>
          </div>
        </div>


      @endforeach
    @endif


    <button class="btn btn-default w3-orange w3-text-white" data-toggle="modal" data-target="#breakdown-{{$event->id}}" title="Edit"><i class="fa fa-pencil"></i>ADD BREAKDOWN</button>
    <button class="btn btn-default w3-orange w3-text-white" data-toggle="modal" data-target="#submit-{{$event->id}}" title="Edit"><i class="fa fa-pencil"></i>Submit</button><br><br>




<!-- ADD BREAKDOWn -->
 <div class="modal fade" id="breakdown-{{$event->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
        <h4 class="modal-title w3-text-gray" id="myModalLabel">Breakdown</h4>
      </div>
    <form action="{{ url('admin/finance/event-fund/'.$event->id.'/manage-payment/store') }}" method="POST" >
      <div class="modal-body">

  <b>ADD BREAKDOWN</b><br><br>

            <div class="form-group">
            <label>Name: </label>
            <input type="text" name="bname" id="bname" tabindex="1" class="form-control" placeholder="Name">
          </div>
          <div class="form-group">
            <label>Desc: </label>
            <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="bdesc" placeholder="desc" rows="3"></textarea>
          </div>

          <div class="form-group">
            <label>amount: </label>
            <input type="number" name="bamt" id="bamt" tabindex="1" class="form-control" placeholder="Amount"><br><br>
          </div>


      <div class="modal-footer">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <button type="submit" class="btn btn-default w3-red"><i class="fa fa-plus-square fa-fw"></i> Yes</button>
      </div>

      </div>
    </form>
    </div>
  </div>
</div>




<!-- DONE BREAKDOWn -->
 <div class="modal fade" id="submit-{{$event->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
        <h4 class="modal-title w3-text-gray" id="myModalLabel">Submit</h4>
      </div>
    <form action="{{ url('admin/finance/event-fund/'.$event->id.'/manage-payment/submit') }}" method="POST" >
      <div class="modal-body">

  <b>You are about to submit the breakdown, please double check breakdown's information</b><br><br>

           'are you sure you want to submit this breakdown?'

      <div class="modal-footer">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <button type="submit" class="btn btn-default w3-red"><i class="fa fa-plus-square fa-fw"></i> Yes</button>
      </div>

      </div>
    </form>
    </div>
  </div>
</div>





@endsection
