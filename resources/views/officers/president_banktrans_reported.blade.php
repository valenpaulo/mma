@extends('layout/control_panel')

@section('title')
Issues on Bank Transaction
@endsection

@section('middle')
<div class="w3-container w3-margin">
    <div class="row">
        <h3>
          <i class="fa fa-exclamation-triangle fa-fw w3-xxlarge"></i>
          <strong>Issues on Bank Transaction</strong>
        </h3>
    </div>

    <hr>

    <div class="row w3-margin-top">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#mticsTab" class="w3-text-black">
              MTICS
              @if((count($mticsDepositReported)) + (count($mticsWithdrawReported)) > 0)
              <span class="w3-badge w3-red">{{count($mticsDepositReported)+ count($mticsWithdrawReported)}}</span>
              @endif
            </a></li>

            <li><a data-toggle="tab" href="#eventTab" class="w3-text-black">Event
              @if((count($eventDepositReported)) + (count($eventWithdrawReported)) > 0)
              <span class="w3-badge w3-red">{{count($eventDepositReported)+ count($eventWithdrawReported)}}</span>
              @endif
            </a></li>
        </ul>
    </div>

     <div class="tab-content">

        <div id="mticsTab" class="tab-pane fade in active">
            <div class="row w3-margin-top">
            	<h4>
	                <strong>
	                	<i class="fa fa-money"></i>
	                	<i class="fa fa-arrow-up"></i>
		                Issues on Deposit
	                </strong>
                </h4>

                <br>

            	<!-- mtics deposit -->
                <div class="table-responsive">
                    <table class="table table-bordered table-hover" id="mticsDepositTable">
                    <thead>
                      <tr>
                       <th class="w3-center">Deposit Description</th>
                       <th class="w3-center">Deposit Amount</th>
                       <th class="w3-center">Deposit Source</th>
                       <th class="w3-center">Deposit Status</th>
                       <th class="w3-center">Report Title</th>
                       <th class="w3-center">Report Description</th>
                       <th class="w3-center">Report Solution</th>
                       <th class="w3-center">Action</th>
                      </tr>
                    </thead>
                    <tbody class="w3-text-gray">
                      @foreach($mticsDepositReported as $reported)
                      <tr>
                        <td class="w3-center">{{$reported->deposit_desc}}</td>
                        <td class="w3-center">{{$reported->deposit_amt}}</td>
                        <td class="w3-center">{{$reported->deposit_source}}</td>
                        <td class="w3-center">{{$reported->deposit_status}}</td>
                        <td class="w3-center">{{$reported->report->deposit_title}}</td>
                        <td class="w3-center">{{$reported->report->deposit_desc}}</td>
                        <td class="w3-center">{{$reported->report->solution}}</td>
                        <td class="w3-center">

                        @if($reported->deposit_status == 'reported')
                        <a class="w3-text-green w3-large" data-toggle="modal" href="#mticsDepositsolution-{{$reported->id}}" style="outline: 0"><i class="fa fa-wrench" data-toggle="tooltip" data-placement="bottom" title="Add Solution"></i></a>
                        @else
                        <i class="fa fa-check-circle-o w3-text-green w3-large" data-toggle="tooltip" data-placement="bottom" title="Resolved"></i>
                        @endif

                        <!-- ADD SOLUTION -->

                        <div class="modal fade" id="mticsDepositsolution-{{$reported->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel" align="left">
                        <div class="modal-dialog modal-md" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                        <h4 class="modal-title w3-text-gray" id="myModalLabel">Solution</h4>
                        </div>
                        <form action="{{ url('admin/president/bank-transaction-request-reported/'.$reported->id.'/add-mtics-deposit-solution') }}" method="POST" enctype="multipart/form-data">


                        <div class="modal-body">
                            <div class="w3-container">
                                <div class="row">

                                    <div class="form-group">
                                        <p><b>Solution: </b></p>
                                        <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="report_solution" placeholder="Solution" rows="3"></textarea>
                                    </div>

                                    <div class="form-group">
                                        <input type="number" name="amount" id="amount" tabindex="1" class="form-control" placeholder="Amount to be adjusted">
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
                        </div>

                        </form>
                        </div>
                        </div>
                        </div>
                        <!--ADD SOLUTION END MODAL -->

                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                    </table>
                </div>

                <hr>

                <h4>
	                <strong>
	                	<i class="fa fa-money"></i>
	                	<i class="fa fa-arrow-down"></i>
		                Issues on Withdraw
	                </strong>
                </h4>

                <br>

                <!-- mtics withdraw -->
                <div class="table-responsive">
                    <table class="table table-bordered table-hover" id="mticsWithdrawTable">
                    <thead>
                      <tr>
                       <th class="w3-center">Withdraw Reason</th>
                       <th class="w3-center">Withdraw Description</th>
                       <th class="w3-center">Withdraw Amount</th>
                       <th class="w3-center">Withdraw Status</th>
                       <th class="w3-center">Report Title</th>
                       <th class="w3-center">Report Description</th>
                       <th class="w3-center">Report Solution</th>
                       <th class="w3-center">Action</th>
                      </tr>
                    </thead>
                    <tbody class="w3-text-gray">
                      @foreach($mticsWithdrawReported as $reported)
                      <tr>
                        <td class="w3-center">{{$reported->withdrawal_reason}}</td>
                        <td class="w3-center">{{$reported->withdrawal_desc}}</td>
                        <td class="w3-center">{{$reported->withdrawal_amt}}</td>
                        <td class="w3-center">{{$reported->withdrawal_status}}</td>
                        <td class="w3-center">{{$reported->report->withdrawal_title}}</td>
                        <td class="w3-center">{{$reported->report->withdrawal_desc}}</td>
                        <td class="w3-center">{{$reported->report->solution}}</td>
                        <td class="w3-center">

                        @if ($reported->withdrawal_status == 'reported')
                        <a class="w3-text-green w3-large" data-toggle="modal" href="#mticsWithdrawsolution-{{$reported->id}}" style="outline: 0"><i class="fa fa-wrench" data-toggle="tooltip" data-placement="bottom" title="Add Solution"></i></a>
                        @else
                        <i class="fa fa-check-circle-o w3-text-green w3-large" data-toggle="tooltip" data-placement="bottom" title="Resolved"></i>
                        @endif

                        <!-- ADD SOLUTION -->

                        <div class="modal fade" id="mticsWithdrawsolution-{{$reported->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel" align="left">
                        <div class="modal-dialog modal-md" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                        <h4 class="modal-title w3-text-gray" id="myModalLabel">Solution</h4>
                        </div>
                        <form action="{{ url('admin/president/bank-transaction-request-reported/'.$reported->id.'/add-mtics-withdraw-solution') }}" method="POST" enctype="multipart/form-data">


                        <div class="modal-body">
                            <div class="w3-container">
                                <div class="row">

                                    <div class="form-group">
                                        <p><b>Solution: </b></p>
                                        <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="report_solution" placeholder="Solution" rows="3"></textarea>
                                    </div>

                                    <div class="form-group">
										<input type="number" name="amount" id="amount" tabindex="1" class="form-control" placeholder="Amount to be adjusted"><br><br>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
                        </div>

                        </form>
                        </div>
                        </div>
                        </div>
                        <!--ADD SOLUTION END MODAL -->

                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div id="eventTab" class="tab-pane fade in">
            <div class="row w3-margin-top">
            	<h4>
	                <strong>
	                	<i class="fa fa-money"></i>
	                	<i class="fa fa-arrow-up"></i>
		                Issues on Deposit
	                </strong>
                </h4>

                <br>

            	<!-- mtics deposit -->
                <div class="table-responsive">
                    <table class="table table-bordered table-hover" id="eventDepositTable">
                    <thead>
                      <tr>
                       <th class="w3-center">Deposit Description</th>
                       <th class="w3-center">Deposit Amount</th>
                       <th class="w3-center">Deposit Source</th>
                       <th class="w3-center">Deposit Status</th>
                       <th class="w3-center">Report Title</th>
                       <th class="w3-center">Report Description</th>
                       <th class="w3-center">Report Solution</th>
                       <th class="w3-center">Action</th>
                      </tr>
                    </thead>
                    <tbody class="w3-text-gray">
                      @foreach($eventDepositReported as $reported)
                      <tr>
                        <td class="w3-center">{{$reported->deposit_desc}}</td>
                        <td class="w3-center">{{$reported->deposit_amt}}</td>
                        <td class="w3-center">{{$reported->deposit_source}}</td>
                        <td class="w3-center">{{$reported->deposit_status}}</td>
                        <td class="w3-center">{$reported->report->deposit_title}}</td>
                        <td class="w3-center">{{$reported->report->deposit_desc}}</td>
                        <td class="w3-center">{{$reported->report->solution}}</td>
                        <td class="w3-center">

                        @if ($reported->deposit_status == 'reported')

                        <a class="w3-text-green w3-large" data-toggle="modal" href="#eventDepositsolution-{{$reported->id}}" style="outline: 0"><i class="fa fa-wrench" data-toggle="tooltip" data-placement="bottom" title="Add Solution"></i></a>

                        @else

                        <i class="fa fa-check-circle-o w3-large w3-text-green" data-toggle="tooltip" data-placement="bottom" title="Resolved"></i>

                        @endif
                        <!-- ADD SOLUTION -->

                        <div class="modal fade" id="eventDepositsolution-{{$reported->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel" align="left">
                        <div class="modal-dialog modal-md" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                        <h4 class="modal-title w3-text-gray" id="myModalLabel">Solution</h4>
                        </div>
                        <form action="{{ url('admin/president/bank-transaction-request-reported/'.$reported->id.'/add-event-deposit-solution') }}" method="POST" enctype="multipart/form-data">


                        <div class="modal-body">
                            <div class="w3-container">
                                <div class="row">

                                    <div class="form-group">
                                        <p><b>Solution: </b></p>
                                        <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="report_solution" placeholder="Solution" rows="3"></textarea>
                                    </div>

                                    <div class="form-group">
                                        <input type="number" name="amount" id="amount" tabindex="1" class="form-control" placeholder="Amount to be adjusted">
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
                        </div>

                        </form>
                        </div>
                        </div>
                        </div>
                        <!--ADD SOLUTION END MODAL -->

                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                    </table>
                </div>

                <hr>

                <h4>
	                <strong>
	                	<i class="fa fa-money"></i>
	                	<i class="fa fa-arrow-down"></i>
		                Issues on Withdraw
	                </strong>
                </h4>

                <br>

                <!-- event withdraw -->
                <div class="table-responsive">
                    <table class="table table-bordered table-hover" id="eventWithdrawTable">
                    <thead>
                      <tr>
                       <th class="w3-center">Withdraw Reason</th>
                       <th class="w3-center">Withdraw Description</th>
                       <th class="w3-center">Withdraw Amount</th>
                       <th class="w3-center">Withdraw Status</th>
                       <th class="w3-center">Report Title</th>
                       <th class="w3-center">Report Description</th>
                       <th class="w3-center">Report Solution</th>
                       <th class="w3-center">Action</th>
                      </tr>
                    </thead>
                    <tbody class="w3-text-gray">
                      @foreach($eventWithdrawReported as $reported)
                      <tr>
                        <td class="w3-center">{{$reported->withdrawal_reason}}</td>
                        <td class="w3-center">{{$reported->withdrawal_desc}}</td>
                        <td class="w3-center">{{$reported->withdrawal_amt}}</td>
                        <td class="w3-center">{{$reported->withdrawal_status}}</td>
                        <td class="w3-center">{{$reported->report->withdrawal_title}}</td>
                        <td class="w3-center">{{$reported->report->withdrawal_desc}}</td>
                        <td class="w3-center">{{$reported->report->solution}}</td>
                        <td class="w3-center">

                        @if ($reported->withdrawal_status == 'reported')
                        <a class="w3-text-green w3-large" data-toggle="modal" href="#eventWithdrawsolution-{{$reported->id}}" style="outline: 0"><i class="fa fa-wrench" data-toggle="tooltip" data-placement="bottom" title="Add Solution"></i></a>
                        @else
                        <i class="fa fa-check-circle-o w3-text-green w3-large" data-toggle="tooltip" data-placement="bottom" title="Resolved"></i>
                        @endif

                        <!-- ADD SOLUTION -->

                        <div class="modal fade" id="eventWithdrawsolution-{{$reported->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel" align="left">
                        <div class="modal-dialog modal-md" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                        <h4 class="modal-title w3-text-gray" id="myModalLabel">Solution</h4>
                        </div>
                        <form action="{{ url('admin/president/bank-transaction-request-reported/'.$reported->id.'/add-event-withdraw-solution') }}" method="POST" enctype="multipart/form-data">


                        <div class="modal-body">
                            <div class="w3-container">
                                <div class="row">

                                    <div class="form-group">
                                        <p><b>Solution: </b></p>
                                        <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="report_solution" placeholder="Solution" rows="3"></textarea>
                                    </div>

                                    <div class="form-group">
										<input type="number" name="amount" id="amount" tabindex="1" class="form-control" placeholder="Amount to be adjusted"><br><br>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
                        </div>

                        </form>
                        </div>
                        </div>
                        </div>
                        <!--ADD SOLUTION END MODAL -->

                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript">
$(document).ready( function () {
  var mticsDepositTable = $('#mticsDepositTable').DataTable();
  var mticsWithdrawTable = $('#mticsWithdrawTable').DataTable();
  var eventDepositTable = $('#eventDepositTable').DataTable();
  var eventWithdrawTable = $('#eventWithdrawTable').DataTable();

  $(mticsDepositTable.table().container()).removeClass('form-inline');
  $(mticsWithdrawTable.table().container()).removeClass('form-inline');
  $(eventDepositTable.table().container()).removeClass('form-inline');
  $(eventWithdrawTable.table().container()).removeClass('form-inline');

});
</script>

@endsection
