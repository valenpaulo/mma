@extends('layout/control_panel')

@section('title')
Purchased Items
@endsection

@section('middle')
<div class="w3-container w3-margin">
  <div class="row">
    <h3>
      <i class="fa fa-shopping-cart fa-fw w3-xxlarge"></i>
      <strong>Purchased Items</strong>
    </h3>
  </div>

  <hr>

  <div class="row w3-margin-top">
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#purchased" class="w3-text-black">Purchased</a></li>

      <li><a data-toggle="tab" href="#validation" class="w3-text-black">Validation @if(count($mtics_purchases_validation) > 0)<span class="w3-badge w3-red">{{count($mtics_purchases_validation)}}</span>@endif</a></li>
    </ul>
  </div>

  <div class="tab-content">
    <!-- start of purchased -->
    <div id="purchased" class="tab-pane fade in active">
      <div class="row w3-margin-top">
        <div class="table-responsive">
            <table class="table table-bordered table-hover" id="purchasedTable">
              <thead>
                <tr>
                 <th class="w3-center">Item</th>
                 <th class="w3-center">Category</th>
                 <th class="w3-center">Task</th>
                 <th class="w3-center">Purchased Quantity</th>
                </tr>
              </thead>
              <tbody class="w3-text-gray">
              @foreach($mtics_purchased_items as $item)
              <tr>
                <td class="w3-center">{{$item->mtics_itemname}}</td>
                <td class="w3-center">{{$item->mtics_inv_status == 'moved' ? $item->category->inv_cat_displayname : ''}}</td>
                <td class="w3-center">{{$item->task->task_name}}</td>
                <td class="w3-center">{{$item->mtics_act_quan}}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>

    <!-- end of purchased tab -->
    </div>

    <!-- start of validation -->
    <div id="validation" class="tab-pane fade in">
      <div class="row w3-margin-top">
        <div class="table-responsive">
          <table class="table table-bordered table-hover" id="validationTable">
            <thead>
              <tr>
               <th class="w3-center">Task</th>
               <th class="w3-center">Item</th>
               <th class="w3-center">Quantity</th>
               <th class="w3-center">Purchased Quantity</th>
               <th class="w3-center">Action</th>
              </tr>
            </thead>
            <tbody class="w3-text-gray">
            @foreach($mtics_purchases_validation as $item)
            <tr>
              <td class="w3-center">{{$item->task->task_name}}</td>
              <td class="w3-center">{{$item->mtics_itemname}}</td>
              <td class="w3-center">{{$item->mtics_orig_quan}}</td>
              <td class="w3-center">{{$item->mtics_act_quan}}</td>
              <td class="w3-center">
                @if(count($item->mticspurchasereport) == 0)
                <a class="w3-text-green" href="#approve-{{$item->id}}" data-toggle="modal"><i class="fa fa-thumbs-up fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Approve"></i></a>

                <a class="w3-text-red" href="#disapprove-{{$item->id}}" data-toggle="modal"><i class="fa fa-thumbs-down fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Disapprove"></i></a>
                @else
                <i class="fa fa-flag-o fa-fw w3-large w3-text-red" data-toggle="tooltip" data-placement="bottom" title="Disapprove">
                @endif
              </td>
            </tr>

            <!-- APPROVE MODAL -->
            <div class="modal fade" id="approve-{{$item->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
            <h4 class="modal-title w3-text-gray" id="myModalLabel">Approve Item Validation</h4>
            </div>
            <form action="{{ url('admin/logistics/purchased-items/'.$item->task->id.'/'.$item->id.'/pass') }}" method="POST" enctype="multipart/form-data">
            <div class="modal-body">
              <div class="w3-container">
                <div class="row">

                  <div class="form-group">
                    <p>Please make sure that you check this item carefully.</p>
                  </div>

                  <div class="form-group">
                    <p>Are you sure you want to approve this validation?</p>
                    <p>If yes, please select a category for this item.</p>

                    <select name="inv_cat_id" class="form-control">
                      @foreach($inventory_categories as $inventory_category)
                          <option value="{{$inventory_category->id}}">{{$inventory_category->inv_cat_displayname}}</option>
                      @endforeach
                    </select>

                    <div class="checkbox">
                      <label><input type="checkbox" name="dontmove" value="yes">Do not move this to Inventory</label>
                    </div>


                  </div>

                </div>
              </div>
            </div>

            <div class="modal-footer">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <button type="submit" class="btn btn-default w3-green">Yes</button>
              <button type="button" data-dismiss="modal" class="btn btn-default w3-red">No</button>
            </div>

            </form>
            </div>
            </div>
            </div>
            <!-- END MODAL -->

            <!-- DISAPPROVE MODAL -->
            <div class="modal fade" id="disapprove-{{$item->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
            <h4 class="modal-title w3-text-gray" id="myModalLabel">Disapprove Item Validation</h4>
            </div>
            <form action="{{ url('admin/logistics/purchased-items/'.$item->task->id.'/'.$item->id.'/report') }}" method="POST" enctype="multipart/form-data">
            <div class="modal-body">
              <div class="w3-container">
                <div class="row">

                  <div class="form-group">
                    <select name="report_cat" class="form-control">
                      <option value="minor">Minor</option>
                      <option value="critical">Critical</option>
                    </select>
                  </div>

                   <select name="inv_cat_id" class="form-control">
                      @foreach($inventory_categories as $inventory_category)
                          <option value="{{$inventory_category->id}}">{{$inventory_category->inv_cat_displayname}}</option>
                      @endforeach
                    </select>

                    <div class="checkbox">
                      <label><input type="checkbox" name="dontmove" value="yes">Do not move this to Inventory</label>
                    </div>

                  <div class="form-group">
                    <label>Report Title:</label>
                    <input type="text" name="report_name" id="report_name" tabindex="1" class="form-control" placeholder="Report Title" >
                  </div>

                  <div class="form-group">
                    <label>Reason for Disapproval:</label>
                    <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="report_reason" placeholder="Reason" rows="3"></textarea>
                  </div>

                  <div class="form-group">
                    <label>External Explaination:</label>
                    <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="external_explain" placeholder="Explaination" rows="3"></textarea>
                  </div>

                </div>
              </div>
            </div>

            <div class="modal-footer">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <button type="submit" class="btn btn-default w3-green">Yes</button>
              <button type="button" data-dismiss="modal" class="btn btn-default w3-red">No</button>
            </div>

            </form>
            </div>
            </div>
            </div>
            <!-- END MODAL -->
            @endforeach
           </tbody>
          </table>
        </div>
      </div>

    </div>
    <!-- end of validation tab -->
  </div>

</div>

<script type="text/javascript">
$(document).ready( function () {
  $('#purchasedTable').DataTable();
  $('#validationTable').DataTable();
} );
</script>

@endsection
