@extends('layout/control_panel')

@section('title')
Purchased Amount Validation
@endsection

@section('middle')
<div class="w3-container w3-margin">
  <div class="row">
    <h3>
      <i class="fa fa-check-circle-o fa-fw w3-xxlarge"></i>
      <strong>Validation <i class="fa fa-angle-double-right fa-fw"></i> {{$requestsValidation->task->task_name}}</strong>
    </h3>
  </div>

  <hr>

  <div class="row w3-margin-top w3-padding">

    <h4>
      <strong>Step 1: Validate Amount of Purchased Items</strong>
      <a class="w3-text-green" href="{{url('admin/finance/requested-money/validate/' . $requestsValidation->id . '/next')}}"><i class="fa fa-arrow-circle-right fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Next Step"></i></a>
    </h4>

  </div>


  <div class="row w3-padding">
    <p>Check the amount of the purchased items carefully, refer to the receipt.
   </p>
    <p>Report any issue that you encountered by clicking the <i class="fa fa-thumbs-down w3-text-red"></i> icon.</p>
    <p>Otherwise, click the <i class="fa fa-arrow-circle-right w3-text-green"></i> icon to proceed to the next step.</p>
  </div>


  <div class="row w3-margin-top">

    <div class="col-lg-3 col-md-3 col-sm-12"></div>

    <div class="col-lg-3 col-md-3 col-sm-12">
      <div class="w3-card-4 w3-amber w3-text-black w3-padding w3-container">

        <div class="w3-container">
          <div class="row">
            <h4>
              <i class="fa fa-money w3-xxxlarge fa-fw"></i>
              <span class="w3-right w3-margin-top">
                <strong>Requested Money</strong>
              </span>
            </h4>
          </div>
          <hr style="border-color: black">
          <div class="row">
            <h4>
              <span>
                <i class="fa fa-ellipsis-v fa-fw"></i>
                <strong>{{$requestsValidation->amount}} pesos</strong>
              </span>
            </h4>
          </div>
        </div>

      </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-12">
      <div class="w3-card-4 w3-purple w3-text-black w3-padding">

        <div class="w3-container">
          <div class="row" align="center">
            <h4>
              <i class="fa fa-shopping-bag w3-xxxlarge fa-fw"></i>
              <span class="w3-right w3-margin-top">
                <strong>Purchased Amount</strong>
              </span>
            </h4>
          </div>

          <hr style="border-color: black">

          <div class="row">
            <h4>
              <span>
                <i class="fa fa-ellipsis-v fa-fw"></i>
                <strong>{{$requestsValidation->task->mticspurchaselist->sum('mtics_total_price')}} pesos</strong>
              </span>
            </h4>
          </div>

        </div>

      </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-12"></div>

  </div>

  <br>

  <div class="row w3-margin-top">
    <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>
             <th class="w3-center">Task</th>
             <th class="w3-center">Item Name</th>
             <th class="w3-center">Quantity</th>
             <th class="w3-center">Amount</th>
             <th class="w3-center">Total</th>
             <th class="w3-center">Action</th>
            </tr>
          </thead>
          <tbody class="w3-text-gray">
          @foreach($requestsValidation->task->mticspurchaselist as $item)
            <tr>
              <td class="w3-center">{{$item->task->task_name}}</td>
              <td class="w3-center">{{$item->mtics_itemname}}</td>
              <td class="w3-center">{{$item->mtics_act_quan}}</td>
              <td class="w3-center">{{$item->mtics_act_price}}</td>
              <td class="w3-center">{{$item->mtics_act_price * $item->mtics_act_quan}}</td>
              <td class="w3-center">

                <a class="w3-text-red" href="#report-{{$item->id}}" data-toggle="modal"><i class="fa fa-thumbs-down fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Disapprove"></i></a>

              </td>

            </tr>

            <!-- DISAPPROVE ITEM VALIDATION MODAL -->
            <div class="modal fade" id="report-{{$item->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
            <h4 class="modal-title w3-text-gray" id="myModalLabel">Disapprove Item Validation</h4>
            </div>
            <form action="{{ url('admin/finance/purchased-items/'.$item->task->id.'/'.$item->id.'/report') }}" method="POST" enctype="multipart/form-data">
            <div class="modal-body">
              <div class="w3-container">
                <div class="row">

                  <div class="form-group">
                   <label>Report Title: </label>
                    <input type="text" name="report_name" id="report_name" tabindex="1" class="form-control" placeholder="Report Title" >
                  </div>

                  <div class="form-group">
                    <label>Reason: </label>
                    <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="report_reason" placeholder="Reason" rows="3"></textarea>
                  </div>

                  <div class="form-group">
                    <label>External Explaination: </label>
                    <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="external_explain" placeholder="Explaination" rows="3"></textarea>
                  </div>

                </div>
              </div>
            </div>

            <div class="modal-footer">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
            </div>

            </form>
            </div>
            </div>
            </div>
            <!-- END MODAL -->

          @endforeach
         </tbody>
        </table>
    </div>
  </div>

</div>


@endsection
