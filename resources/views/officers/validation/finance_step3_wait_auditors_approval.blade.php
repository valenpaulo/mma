@extends('layout/control_panel')

@section('title')
Purchased Amount Validation
@endsection

@section('middle')
<div class="container-fluid w3-margin">
  <div class="row">
      <h3>
        <i class="fa fa-calendar-check-o fa-fw"></i>
        <strong>Validation <i class="fa fa-angle-double-right fa-fw"></i> {{$requestsValidation->task->task_name}}</strong>
      </h3>

  </div>

  <div class="row w3-margin-top">

    <h4>
      <strong>Step 3: Reimbursement Request</strong>
    </h4>

  </div>

  <div class="row w3-margin-top" align="center">
    <i class="fa fa-spinner w3-xxxlarge"></i>
    <h4>Waiting for Auditor's Approval ...</h4>
  </div>

</div>

@endsection
