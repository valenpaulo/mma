@extends('layout/control_panel')

@section('title')
Purchased Amount Validation
@endsection

@section('middle')
<div class="w3-container w3-margin">

  <div class="row">
    <h3>
      <i class="fa fa-check-circle-o fa-fw w3-xxlarge"></i>
      <strong>Validation <i class="fa fa-angle-double-right fa-fw"></i> {{$requestsValidation->task->task_name}}</strong>
    </h3>
  </div>

  <hr>

  <div class="row w3-margin-top w3-padding">

    <h4>
      <strong>Step 3: Collect Excess Money</strong>
      @if(!is_null($requestsValidation->excess_amount))
      <a class="w3-text-green" href="{{url('admin/finance/requested-money/validate/' . $requestsValidation->id . '/next')}}"><i class="fa fa-arrow-circle-right fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Next Step"></i></a>
      @endif
    </h4>

  </div>

  @if(is_null($requestsValidation->excess_amount))
  <div class="row w3-margin-top" align="center" style="outline: 0">
    <a class="w3-text-gray w3-large" href="#collectExcess" data-toggle="modal"><i class="fa fa-plus-square-o fa-fw"></i> Collect Excess Money</a>
  </div>
  @else
  <div class="row w3-padding">
  <p>Click the <i class="fa fa-arrow-circle-right fa-fw w3-text-green"></i> icon to proceed to the next step.</p>
  </div>
  @endif

  <br>

  <!-- COLLECT EXCESS MONEY MODAL -->
  <div class="modal fade" id="collectExcess" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
  <div class="modal-content">
  <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
  <h4 class="modal-title w3-text-gray" id="myModalLabel">Collect Excess Money</h4>
  </div>
  <form action="{{ url('admin/finance/payment/excess-money') }}" method="POST" enctype="multipart/form-data">
  <div class="modal-body">
    <div class="w3-container">
      <div class="row">

      <div class="form-group">
        <label>Excess Money</label>
        <input type="number" name="amount" id="amount" tabindex="1" class="form-control" value="{{$excess}}" readonly>
      </div>

      <div class="form-group">
        <p>Do you want to proceed?</p>
      </div>

      </div>
    </div>
  </div>

  <div class="modal-footer">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="task_id" value="{{ $requestsValidation->task_id }}">
    <button type="submit" class="btn btn-default w3-green">Yes</button>
    <button type="button" data-dismiss="modal" class="btn btn-default w3-red">No</button>
  </div>

  </form>
  </div>
  </div>
  </div>
  <!-- END MODAL -->

  <div class="row w3-margin-top">

    <div class="col-lg-4 col-md-4 col-sm-12">
      <div class="w3-card-4 w3-amber w3-text-black w3-padding w3-container">

        <div class="w3-container">
          <div class="row" align="center">
            <h4>
              <i class="fa fa-money w3-xxxlarge fa-fw"></i>
              <strong>Requested Money</strong>
            </h4>
          </div>
          <hr style="border-color: black">
          <div class="row">
            <h4>
              <span>
                <i class="fa fa-ellipsis-v fa-fw"></i>
                <strong>{{$requestsValidation->amount}} pesos</strong>
              </span>
            </h4>
          </div>
        </div>

      </div>

    </div>

    <div class="col-lg-4 col-md-4 col-sm-12">
      <div class="w3-card-4 w3-purple w3-text-black w3-padding w3-container">

        <div class="w3-container">
          <div class="row" align="center">
            <h4>
              <i class="fa fa-shopping-bag w3-xxxlarge fa-fw"></i>
              <strong>Purchased Amount</strong>
            </h4>
          </div>
          <hr style="border-color: black">
          <div class="row">
            <h4>
              <span>
                <i class="fa fa-ellipsis-v fa-fw"></i>
                <strong>{{$requestsValidation->task->mticspurchaselist->sum('mtics_total_price')}} pesos</strong>
              </span>
            </h4>
          </div>
        </div>

      </div>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-12">
      <div class="w3-card-4 w3-teal w3-text-black w3-padding">

        <div class="w3-container">
          <div class="row" align="center">
            <h4>
              <i class="fa fa-mail-forward w3-xxxlarge fa-fw"></i>
              <strong>Excess Money</strong>
            </h4>
          </div>

          <hr style="border-color: black">

          <div class="row">
            <h4>
              <span>
                <i class="fa fa-ellipsis-v fa-fw"></i>
                <strong>{{$excess}} pesos</strong>
              </span>
            </h4>
          </div>

        </div>

      </div>
    </div>

  </div>

  <br>

  <div class="row w3-margin-top">
    <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>
             <th class="w3-center">Item Name</th>
             <th class="w3-center">Quantity</th>
             <th class="w3-center">Amount</th>
             <th class="w3-center">Total</th>
            </tr>
          </thead>
          <tbody class="w3-text-gray">
          @foreach($requestsValidation->task->mticspurchaselist as $item)
            <tr>
              <td class="w3-center">{{$item->mtics_itemname}}</td>
              <td class="w3-center">{{$item->mtics_act_quan}}</td>
              <td class="w3-center">{{$item->mtics_act_price}}</td>
              <td class="w3-center">{{$item->mtics_act_price * $item->mtics_act_quan}}</td>
            </tr>

          @endforeach
         </tbody>
        </table>
    </div>
  </div>

</div>

@endsection
