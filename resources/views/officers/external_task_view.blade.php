@extends('layout/control_panel')

@section('title')
Purchase Request
@endsection

@section('middle')

<div class="w3-container w3-margin">
  <div class="row">
    <h3>
      <i class="fa fa-shopping-basket fa-fw w3-xxlarge"></i>
      <strong>{{$task->task_name}}</strong>
    </h3>
  </div>

  <hr>

  @if(is_null($task->requestmoney))
  <div class="row">
    <p><strong> Note</strong>: Please request money for this task. In order to do so, just click the <b>'<i class="fa fa-gear fa-fw"></i> Manage'</b> link.
    <br>
    Before sending a money request, you can indicate an estimated price by clicking the <b>'<i class="fa fa-plus-square-o fa-fw"></i> Estimated Price'</b> link.</p>
  </div>

  @elseif($task->requestmoney->amount - $task->mticspurchaselist->sum('mtics_total_price') < 0 and count($task->requestmoney->reimburse) == 0)
  <div class="row">
    <p>Note: You can request a reimbursement by clicking the <b>'<i class="fa fa-gear fa-fw"></i> Manage'</b> link.</p>
  </div>
  @endif

  <br>

  <div class="row w3-margin-top">

    <div class="col-lg-3 col-md-3 col-sm-12"></div>

    <div class="col-lg-3 col-md-3 col-sm-12">
      <div class="w3-card-4 w3-purple w3-text-black w3-padding w3-container">

        <div class="w3-container">
          <div class="row" align="center">
            <h4>
              <i class="fa fa-shopping-bag w3-xxxlarge fa-fw"></i>
              <strong>Purchased Items</strong>
            </h4>
          </div>
          <hr style="border-color: black">
          <div class="row">
            <h4>
              <span>
                <i class="fa fa-ellipsis-v fa-fw"></i>
                <strong>{{count($purchasedItems)}}/{{count($task->mticspurchaselist)}}</strong>
              </span>
            </h4>
          </div>
        </div>

      </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-12">
      <div class="w3-card-4 w3-light-green w3-text-black w3-padding">

        <div class="w3-container">
          <div class="row" align="center">
            <h4>
              <i class="fa fa-money w3-xxxlarge fa-fw"></i>
              <strong>Money Bucket</strong>
            </h4>
          </div>

          <hr style="border-color: black">

          <div class="row">
            <h4>
              <span>
                <i class="fa fa-ellipsis-v fa-fw"></i>
                <strong>{{$total_money_bucket}} pesos</strong>
              </span>
            </h4>
          </div>

          <div class="row" align="center">
            @if(!is_null($task->requestmoney))
            @if($task->requestmoney->mon_req_status == 'ongoing' and $total_money_bucket < 0 and ($task->task_status == 'done' or $task->task_status == 'failed') and count($task->requestmoney->reimburse) == 0)

            <hr style="border-color: black">

            <a class="w3-text-black" href="#reimburse" data-toggle="modal"><i class="fa fa-gear fa-fw" data-toggle="tooltip" data-placement="bottom" title="Request for Reimbursement"></i>Manage</a>
            @elseif($task->requestmoney->mon_req_status == 'pending')
            <label>
            <i class="fa fa-spinner fa-fw"></i>
            Your request is being processed ...
            </label>
            @endif
            @else
            <hr style="border-color: black">

            <a class="w3-text-black" href="#money" data-toggle="modal"><i class="fa fa-gear fa-fw" data-toggle="tooltip" data-placement="bottom" title="Request Money"></i>Manage</a>
            @endif
          </div>

        </div>

      </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-12"></div>

  </div>

  <!-- REQUEST MONEY MODAL -->
  <div class="modal fade" id="money" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-md" role="document">
  <div class="modal-content">
  <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
  <h4 class="modal-title w3-text-gray" id="myModalLabel">Request Money</h4>
  </div>
  <form action="{{ url('admin/external/task/'.$task->id.'/request') }}" method="POST" enctype="multipart/form-data">
  <div class="modal-body">
    <div class="w3-container">
      <div class="row">

      <div class="form-group">
        <p>You are about to request money for this task.</p>
      </div>

      <div class="form-group">
        <p>Please leave a note why you're making this request.</p>
        <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="remarks" placeholder="Remarks" rows="4"></textarea>
      </div>

      </div>
    </div>
  </div>

  <div class="modal-footer">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <button type="submit" class="btn btn-default w3-green"></i>Yes</button>
    <button type="button" class="btn btn-default w3-red" data-dismiss="modal"></i>No</button>
  </div>

  </form>
  </div>
  </div>
  </div>
  <!-- END MODAL -->

  <!-- REIMBURSEMENT MODAL -->
  <div class="modal fade" id="reimburse" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
  <div class="modal-content">
  <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
  <h4 class="modal-title w3-text-gray" id="myModalLabel">Request for Reimbursement</h4>
  </div>
  <form action="{{ url('admin/external/task/'.$task->id.'/reimburse') }}" method="POST" enctype="multipart/form-data">
  <div class="modal-body">
    <div class="w3-container">
      <div class="row">

      <p>Do you want to proceed?</p>

      </div>
    </div>
  </div>

  <div class="modal-footer">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <button type="submit" class="btn btn-default w3-green">Yes</button>
    <button type="button" data-dismiss="modal" class="btn btn-default w3-red">No</button>
  </div>

  </form>
  </div>
  </div>
  </div>
  <!-- END MODAL -->

  @if(is_null($task->requestmoney))
  <div class="row w3-margin-top">
    <h5>
      <a href="#addEstPrice" class="w3-text-gray" data-toggle="modal">
        <i class="fa fa-plus-square-o fa-fw"></i>
        Estimated Price
      </a>
    </h5>
  </div>

  <!-- ESTIMATED PRICE MODAL -->
  <div class="modal fade" id="addEstPrice" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-md" role="document">
  <div class="modal-content">
  <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
  <h4 class="modal-title w3-text-gray" id="myModalLabel">Input Estimated Price</h4>
  </div>
  <form action="{{ url('admin/external/task/'.$task->id.'/edit') }}" method="POST" enctype="multipart/form-data">
  <div class="modal-body">
    <div class="w3-container">
      <div class="row">

      @if($task->eventpurchaselist)
        @foreach ($task->mticspurchaselist as $item)
        <div class="form-group">
          <label>Item Name:</label>
          <input type="text" class="form-control" value="{{$item->mtics_itemname}}" disabled>
        </div>

        <div class="form-group">
          <label>Quantity:</label>
          <input type="text" class="form-control" value="{{$item->mtics_orig_quan}}" disabled>
        </div>

        <div class="form-group">
          <input type="number" name="est_amt[{{$item->id}}][price]" id="est_amt" tabindex="1" class="form-control" placeholder="Estimated Amount">
        </div>

        <br>

        @endforeach
     @endif

      </div>
    </div>
  </div>

  <div class="modal-footer">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
  </div>

  </form>
  </div>
  </div>
  </div>
  <!-- END MODAL -->
  @endif

   <div class="row w3-margin-top">
    <div class="table-responsive">
        <table class="table table-bordered table-hover" id="purchaseTable">
          <thead>
            <tr>
             <th class="w3-center">Item</th>
             <th class="w3-center">Quantity</th>
             <th class="w3-center">Estimated Price</th>
             <th class="w3-center">Purchased Ouantity</th>
             <th class="w3-center">Purchased Price</th>
             <th class="w3-center">Status</th>
             <th class="w3-center">Action</th>
            </tr>
          </thead>
          <tbody class="w3-text-gray">
          @foreach($task->mticspurchaselist as $item)
            <tr>
              <td class="w3-center">{{$item->mtics_itemname}}</td>
              <td class="w3-center">{{$item->mtics_orig_quan}}</td>
              <td class="w3-center">{{$item->mtics_est_price > 0 ? $item->mtics_est_price : 0}}</td>
              <td class="w3-center">{{$item->mtics_act_quan > 0 ? $item->mtics_act_quan : 0}}</td>
              <td class="w3-center">{{$item->mtics_total_price > 0 ? $item->mtics_total_price : 0}}</td>
              <td class="w3-center">{{$item->mtics_itemstatus}}</td>
              <td class="w3-center">
                @if(!is_null($task->requestmoney) and $task->requestmoney->mon_req_status != 'pending' and $item->mtics_itemstatus != 'purchased')
                <a class="w3-text-orange" href="#specification-{{$item->id}}" data-toggle="modal"><i class="fa fa-wrench fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Add Specification"></i></a>

                <a class="w3-text-green" href="#purchased-{{$item->id}}" data-toggle="modal"><i class="fa fa-cart-arrow-down fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Purchased"></i></a>
                @elseif(is_null($task->requestmoney) or $task->requestmoney->mon_req_status == 'pending')
                <i class="fa fa-wrench fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Not Enough Balance"></i>

                <i class="fa fa-cart-arrow-down fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Not Enough Balance"></i>

                @elseif ($item->mtics_itemstatus == 'purchased')
                <i class="fa fa-check-circle-o fa-fw w3-large w3-text-green" data-toggle="tooltip" data-placement="bottom" title="Purchased"></i>
                @endif
              </td>
            </tr>

            <!-- PURCHASED MODAL -->
            <div class="modal fade" id="purchased-{{$item->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
            <h4 class="modal-title w3-text-gray" id="myModalLabel">Purchased Item</h4>
            </div>
            <form action="{{ url('admin/external/task/'.$task->id.'/'.$item->id.'/purchased') }}" method="POST" enctype="multipart/form-data">
            <div class="modal-body">
              <div class="w3-container">
                <div class="row">

                <div class="form-group w3-text-gray">
                  <label>{{$item->mtics_itemname}}</label>
                </div>

                <div class="form-group">
                  <label>Quantity:</label>
                  <input type="text" name="quan" id="quan" tabindex="1" class="form-control" placeholder="Quantity" value="{{$item->mtics_orig_quan}}">
                </div>

                <div class="form-group">
                  <label>Total price: </label>
                  <input type="number" name="tprice" id="tprice" tabindex="1" class="form-control" placeholder="Total Price" value="<?php echo $item->mtics_est_price * $item->mtics_orig_quan;?>">
                </div>
                </div>
              </div>
            </div>

            <div class="modal-footer">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
            </div>

            </form>
            </div>
            </div>
            </div>
            <!-- END MODAL -->

            <!-- SPECIFICATION MODAL -->
            <div class="modal fade" id="specification-{{$item->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
            <h4 class="modal-title w3-text-gray" id="myModalLabel">Add Specification</h4>
            </div>
            <form action="{{ url('admin/external/task/'.$task->id.'/'.$item->id.'/addequip') }}" method="POST" enctype="multipart/form-data">
            <div class="modal-body">
              <div class="w3-container">
                <div class="row">

                <div class="form-group w3-text-gray">
                  <label>{{$item->mtics_itemname}}</label>
                </div>

                <div class="form-group">
                  <label>Brand Name: </label>
                  <input type="text" name="brandname" id="brandname" tabindex="1" class="form-control" placeholder="Brand Name">
                </div>

                <div class="form-group">
                  <label>Serial Number: </label>
                  <input type="text" name="serialno" id="serialno" tabindex="1" class="form-control" placeholder="Serial Number">
                </div>

                <div class="form-group">
                  <label>Specs: </label>
                  <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="specs" placeholder="Item Specs" rows="3"></textarea>
                </div>

                <div class="form-group">
                  <label>Price: </label>
                <input type="number" name="price" id="price" tabindex="1" class="form-control" placeholder="Price">
              </div>

                </div>
              </div>
            </div>

            <div class="modal-footer">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
            </div>

            </form>
            </div>
            </div>
            </div>
            <!-- END MODAL -->
          @endforeach
         </tbody>
        </table>
    </div>
  </div>

</div>

<script type="text/javascript">
$(document).ready( function () {
  $('#purchaseTable').DataTable();
} );
</script>

@endsection
