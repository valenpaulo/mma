@extends('layout/control_panel')

@section('title')
Manage Calendar of Activites
@endsection

@section('middle')
<div class="w3-container w3-margin">
  <div class="row">
    <h3>
      <i class="fa fa-calendar fa-fw w3-xxlarge"></i>
      <strong>Manage Calendar of Activites</strong>
    </h3>
  </div>

    <br>
    <a href="{{ route('admin.activities.print')}}" class="w3-text-blue" data-toggle="modal">
      <i class="fa fa-print fa-fw"></i>
      Print Calendar of Activities
    </a>
  <hr>

  <div class="row">
    <div id="calendar"></div>
  </div>
</div>

<a href="#addEvent" id="add" data-toggle="modal" style="display: none">Open</a>

<a href="#editEvent" id="edit" data-toggle="modal" style="display: none">Open</a>

<!-- ADD MODAL -->
<div class="modal fade" id="addEvent" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
    <h4 class="modal-title w3-text-gray" id="myModalLabel">Add Event</h4>
    </div>
    <form action="{{ url('admin/president/manage-event/store') }}" method="POST" enctype="multipart/form-data">

    <div class="modal-body">
      <div class="w3-container">
        <div class="row">

        <div id="eventBody">

          <div class="form-group">
            <label for="title">Event Title:</label>
            <input type="text" name="event_title" class="form-control" placeholder="Event Title" required>
          </div>

          <div class="form-group">
            <p><b>Description: </b></p>
            <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="description" placeholder="Description" rows="3" required></textarea>
          </div>

          <div class="form-group">
            <p><b>Goal: </b></p>
            <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="goal" placeholder="Goal" rows="3" required></textarea>
          </div>

          <div class="form-group">
            <label for="start_date">Start Date:</label>
            <input type="date" name="start_date" id="add_start_date" class="form-control" required>
          </div>

          <div class="form-group">
            <label for="end_date">End Date:</label>
            <input type="date" name="end_date" class="form-control" required>
          </div>

      </div>

      </div>
    </div>
  </div>

  <div class="modal-footer">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="id">
    <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
  </div>

  </form>
  </div>
  </div>
</div>
<!-- END MODAL -->

<!-- EDIT MODAL -->
<div class="modal fade" id="editEvent" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
    <h4 class="modal-title w3-text-gray" id="myModalLabel">Event Information</h4>
    </div>
    <form action="" method="POST" enctype="multipart/form-data" id="formEdit">

    <div class="modal-body">
      <div class="w3-container">
        <div class="row">

        <div id="eventBody">

          <div class="form-group">
            <label for="title">Event Title:</label>
            <input type="text" name="event_title" id="event_title" class="form-control" placeholder="Event Title" required>
          </div>

          <div class="form-group">
            <p><b>Description: </b></p>
            <textarea class="form-control richTextBox" tabindex="1" id="description" name="description" placeholder="Description" rows="3" required></textarea>
          </div>

          <div class="form-group">
            <p><b>Goal: </b></p>
            <textarea class="form-control richTextBox" tabindex="1" id="goal" name="goal" placeholder="Goal" rows="3" required></textarea>
          </div>


          <div class="form-group">
            <p><b>Accomplishment: </b></p>
            <textarea class="form-control richTextBox" tabindex="1" id="accomplishment" name="accomplishment" placeholder="Accomplishment" rows="3"></textarea>
          </div>

          <div class="form-group">
            <label for="start_date">Start Date:</label>
            <input type="date" name="start_date" id="start_date" class="form-control" required>
          </div>

          <div class="form-group">
            <label for="end_date">End Date:</label>
            <input type="date" name="end_date" id="end_date" class="form-control" required >
          </div>

      </div>

      </div>
    </div>
  </div>

  <div class="modal-footer">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="id" id="_id">
    <button type="submit" id="editButton" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
    <button type="button" onclick="deleteEvent()" id="deleteButton" class="btn btn-default w3-red"><i class="fa fa-trash-o fa-fw"></i> Delete</button>
  </div>

  </form>

  <form action="" method="POST" enctype="multipart/form-data" id="formDelete">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
  </form>

  </div>
  </div>
</div>
<!-- END MODAL -->

<script>
$(window).on('load', function() {
    $('#calendar').fullCalendar({
      // themeSystem: 'bootstrap3',
      bootstrapGlyphicons: {
          prev: 'glyphicon-circle-arrow-left',
          next: 'glyphicon-circle-arrow-right',
      },
        eventClick: function(event) {

            $('#edit').click();
            $('#_id').val(event.id);
            $('#event_title').val(event.title);
            $('#description').html(event.desc);
            $('#goal').html(event.goal);
            $('#accomplishment').html(event.accomplishment);

            var now = new Date(event.start);

            var day = ("0" + now.getDate()).slice(-2);
            var month = ("0" + (now.getMonth() + 1)).slice(-2);

            var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
            $('#start_date').val(today);

            var now = new Date(event.end);

            var day = ("0" + now.getDate()).slice(-2);
            var month = ("0" + (now.getMonth() + 1)).slice(-2);

            var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
            $('#end_date').val(today);


            if(event.status == 'pending') {
              $('#formDelete').attr('action', "{{ url('admin/president/manage-event/delete/') }}/" + event.id);

            } else {
              $('#deleteButton').hide();
            }

            if(event.organizer == {{Auth::id()}}) {
              $('#formEdit').attr('action', "{{ url('admin/president/manage-event/edit/') }}/" + event.id);
            } else {
              $('#formEdit').attr('action', "{{ url('admin/president/manage-event/edit/') }}/" + event.id);
              $('#event_title').attr('disabled', 'disabled');
              $('#start_date').attr('disabled', 'disabled');
              $('#end_date').attr('disabled', 'disabled');
            }
        },

        dayClick: function(date, jsEvent, view, resourceObj) {
          var officer = '{{Gate::allows('docu-only')}}';
          if(officer == '') {
              $('#add').click();
              $('#add_start_date').val(date.format());
            }
          },

        header: {
          left: 'prev, next, today',
          center: 'title',
          right: 'month, agendaWeek, agendaDay, listMonth'
        }, // buttons for switching between views

        views: {
            agenda: { // name of view
          // other view-specific options here
            },
        },

        eventSources: [

        // your event source
        {
          events: [ // put the array in the `events` property

            @foreach($events as $event)
            {
              id  : '{{$event->id}}',
              title  : '{{$event->event_title}}',
              goal  : '{{$event->goal}}',
              accomplishment  : '{{$event->accomplishment}}',
              start  : '{{$event->start_date}}',
              end  : '{{$event->end_date}}',
              status : '{{$event->status}}',
              organizer : '{{$event->organizer}}'
            },
            @endforeach

          ],
          editable: true,
          color: 'teal',
          textColor: 'white', // an option!
        }

        // any other event sources...

      ],
      selectable: true,
      editable: true,
      selectHelper: true

    });
});
</script>

<script type="text/javascript">
function deleteEvent() {
  swal({
  title: "Are you sure?",
  text: "Once deleted, you will not be able to undo this action!",
  icon: "warning",
  buttons: true,
  dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      $("#formDelete").submit();
    }
  });
}
</script>
@endsection
