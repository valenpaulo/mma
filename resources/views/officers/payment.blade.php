@extends('layout/control_panel')

@section('title')
Collect Payment
@endsection

@section('middle')

<div class="w3-container w3-margin">

  <div class="row">
    <h3>
      <i class="fa fa-money fa-fw w3-xxlarge"></i>
      <strong>Payments</strong>
    </h3>
  </div>

  <hr>

  <div class="w3-row-padding w3-margin-top">
    <div class="col-lg-4 col-md-4 col-sm-12"></div>

    <div class="col-lg-4 col-md-4 col-sm-12 w3-margin-top">
      <div class="w3-card-4 w3-amber w3-text-black w3-padding w3-container">

        <div class="w3-container">
          <div class="row" align="center">
            <h4>
              <i class="fa fa-money w3-xxlarge fa-fw"></i>
              <strong>MTICS Fund Payment</strong>
            </h4>
          </div>
          <hr style="border-color: black">
          <div class="row">
            <h4>
              <span>
                <i class="fa fa-ellipsis-v fa-fw"></i>
                <strong>{{$mticsFundOnHand}} pesos</strong>
              </span>
            </h4>
          </div>
        </div>

      </div>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-12"></div>
  </div>

  <br>

  <div class="row w3-margin-top">
    <span class="w3-dropdown-hover w3-white">
      <a href="javascript:void(0)" class="w3-text-gray"><i class="fa fa-plus-square-o fa-fw"></i>Add Payment</a>


      <div class="w3-dropdown-content w3-card-4 panel panel-default w3-animate-zoom" align="left" style="left: 0">
        <a href="#paymentmtics" data-toggle="modal" class="w3-text-gray w3-hover-teal"><i class="fa fa-institution fa-fw w3-large"></i> MTICS</a>

        <a href="#paymentevent" data-toggle="modal" class="w3-text-gray w3-hover-teal" style="outline: 0px"><i class="fa fa-calendar fa-fw w3-large"></i> Event</a>
      </div>

    </span>

    <a class="w3-text-gray" data-toggle="modal" href="#paymentpenalty"><i class="fa fa-plus-square-o fa-fw"></i> Collect Penalty fines</a>

    <a class="w3-text-gray" data-toggle="modal" href="#order"><i class="fa fa-plus-square-o fa-fw"></i> Order Event orders</a>

  </div>

     <!-- PENALTY PAYMENT -->
    <div class="modal fade" id="paymentpenalty" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
    <h4 class="modal-title w3-text-gray" id="myModalLabel">Penalty Payment Slip</h4>
    </div>
    <form action="{{ url('admin/finance/payment/pay-penalty') }}" method="POST" enctype="multipart/form-data">
    <div class="modal-body">
      <div class="w3-container">
        <div class="row">
          <div class="form-group">
            <label for="member_id">Name:</label>
            <select name="member_id" class="form-control" onchange="getFine()" id="officerId">
                <option value="">-- Select Student Name --</option>
            @foreach($penalties as $penalty)
                <option value="{{$penalty->member_id}}">{{$penalty->member->first_name}} {{$penalty->member->last_name}}</option>
            @endforeach
            </select>
          </div>

          <hr>
          <label>Payment Transaction:</label>

          <div class="form-group">
            <input type="number" name="mtics_payment" id="penaltyPayment" tabindex="1" class="form-control" placeholder="Payment Amount">
          </div>

        </div>
      </div>
    </div>

    <div class="modal-footer">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <button type="submit" class="btn btn-default w3-green"><i class="fa fa-reply fa-fw"></i> Receive</button>
    </div>

    </form>
    </div>
    </div>
    </div>
    <!-- END MODAL -->

    <!-- ORDER PAYMENT -->
    <div class="modal fade" id="order" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
    <h4 class="modal-title w3-text-gray" id="myModalLabel">Item Order Slip</h4>
    </div>
    <form action="{{ url('admin/finance/payment/pay-event-offers') }}" method="POST" enctype="multipart/form-data">
    <div class="modal-body">
      <div class="w3-container">
        <div class="row">
          <div class="form-group">
            <label for="member_id">Name:</label>
            <select name="member_id" class="form-control">
                <option value="">-- Select Student Name --</option>
            @foreach($members as $member)
                <option value="{{$member->id}}">{{$member->first_name}} {{$member->last_name}}</option>
            @endforeach
            </select>
          </div>

          <hr>
          <label>Payment Transaction:</label>

          <select name="eventoffers_id" class="form-control">
                <option value="">-- Select Item --</option>
          @foreach($eventoffers as $eventoffer)
              <option value="{{$eventoffer->id}}">{{$eventoffer->offer_name}}</option>
          @endforeach
          </select><br>

          <div class="form-group">
            <label>Quantity: </label>
            <input type="number" name="quantity" id="quantity" tabindex="1" class="form-control" placeholder="Quantity">
          </div>

          <div class="form-group">
            <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="desc" placeholder="Description" rows="4"></textarea>
          </div>

        </div>
      </div>
    </div>

    <div class="modal-footer">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <button type="submit" class="btn btn-default w3-green">Receive<i class="fa fa-reply fa-fw"></i></button>
    </div>

    </form>
    </div>
    </div>
    </div>
    <!-- END MODAL -->



  <!-- MTICS PAYMENT -->
  <div class="modal fade" id="paymentmtics" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
      <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
      <h4 class="modal-title w3-text-gray" id="myModalLabel">MTICS Fund Payment Slip</h4>
      </div>
      <form action="{{ url('admin/finance/payment/pay-store') }}" method="POST" enctype="multipart/form-data">
      <div class="modal-body">
        <div class="w3-container">
          <div class="row">

            <div class="dropdown">
              <button class="btn btn-default dropdown-toggle form-control" type="button" data-toggle="dropdown" id="mticsSelect">
              Select Student Name
              <span class="caret"></span></button>
              <ul class="dropdown-menu" style="right: 0">
                <input class="form-control" id="myInput-mtics" type="text" placeholder="Search..">
                @foreach($members as $member)
                <li value="{{$member->id}}"><a href="javascript:void(0)" onclick="setMticsStudent('{{$member->first_name}} {{$member->last_name}}', {{$member->id}})">{{$member->first_name}} {{$member->last_name}}</a></li>
                @endforeach
              </ul>
            </div>

            <input type="hidden" name="member_id" id="member_id-mtics" value="">

            <hr>
            <label>Payment Transaction:</label>

            <div class="form-group">
              <input type="number" name="mtics_payment" id="mtics_payment" tabindex="1" class="form-control" placeholder="Payment Amount">
            </div>

            <div class="form-group">
              <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="remarks" placeholder="Remarks" rows="3"></textarea>
            </div>

          </div>
        </div>
      </div>

      <div class="modal-footer">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <button type="submit" class="btn btn-default w3-green"><i class="fa fa-reply fa-fw"></i> Receive</button>
      </div>

      </form>
      </div>
    </div>
  </div>
  <!-- END MODAL -->

  <!-- EVENT PAYMENT -->
  <div class="modal fade" id="paymentevent" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
      <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
      <h4 class="modal-title w3-text-gray" id="myModalLabel">Event Payment Slip</h4>
      </div>
      <form action="{{ url('admin/finance/payment/pay-event-store') }}" method="POST" enctype="multipart/form-data">
      <div class="modal-body">
        <div class="w3-container">
          <div class="row">

            <div class="dropdown">
              <button class="btn btn-default dropdown-toggle form-control" type="button" data-toggle="dropdown" id="eventSelect">
              Select Student Name
              <span class="caret"></span></button>
              <ul class="dropdown-menu" style="right: 0">
                <input class="form-control" id="myInput-event" type="text" placeholder="Search..">
                @foreach($members as $member)
                <li><a href="javascript:void(0)" onclick="setEventStudent('{{$member->first_name}} {{$member->last_name}}', {{$member->id}})">{{$member->first_name}} {{$member->last_name}}</a></li>
                @endforeach
              </ul>
            </div>

            <input type="hidden" name="member_id" id="member_id-eventid" value="">

            <hr>
            <label>Payment Transaction:</label>

            <select name="event_id" class="form-control" onchange="showEventPayment(this.value)">
                  <option value="">-- Select Event --</option>
            @foreach($events as $event)
                <option value="{{$event->id}}">{{$event->event_title}}</option>
            @endforeach
            </select><br>

            <div class="form-group">
              <label>Amount: </label>
              <input type="number" name="event_payment" id="event_payment" tabindex="1" class="form-control" placeholder="Amount">
            </div>

            <div class="form-group">
              <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="remarks" placeholder="Remarks" rows="4"></textarea>
            </div>
        </div>
      </div>
    </div>

    <div class="modal-footer">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <button type="submit" class="btn btn-default w3-green"><i class="fa fa-reply fa-fw"></i> Receive</button>
    </div>

    </form>
    </div>
    </div>
    </div>
    <!-- END MODAL -->

  <div class="row w3-margin-top">
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#mtics" class="w3-text-black">MTICS</a></li>

      <li><a data-toggle="tab" href="#event" class="w3-text-black">Event</a></li>
    </ul>
  </div>

  <div class="tab-content">
    <div id="mtics" class="tab-pane fade in active">
      <div class="row w3-margin-top">
        <div class="table-responsive">
          <table class="table table-bordered table-hover" id="mticsTable">
            <thead>
              <tr>
               <th class="w3-center">Student No.</th>
               <th class="w3-center">Name</th>
               <th class="w3-center">Section</th>
               <th class="w3-center">Amount</th>
               <th class="w3-center">Status</th>
               <th class="w3-center">Receiver</th>
               <th class="w3-center">Action</th>
              </tr>
            </thead>
            <tbody class="w3-text-gray">
            @foreach($mticspayments as $payment)
              <tr>
                <td class="w3-center">{{$payment->member->id_num}}</td>
                <td class="w3-center">{{$payment->member->first_name}} {{$payment->member->last_name}}</td>
                <td class="w3-center">{{$payment->member->section->section_code}}</td>
                <td class="w3-center">{{$payment->paymticsfund_amt}}</td>
                <td class="w3-center">{{$payment->paymticsfund_status}}</td>
                <td class="w3-center">{{$payment->admin->first_name}} {{$payment->admin->last_name}}</td>
                <td class="w3-center">
                <a class="w3-text-green" data-toggle="modal" href="#mticsreceipt-{{$payment->id}}"><i class="fa fa-print fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Print Receipt"></i></a>


              @if(Gate::allows('superadmin-only'))
                <a href="javascript:void" onclick="$('#deletePayment-{{$payment->id}}').submit()" class="w3-text-red w3-large"><i class="fa fa-remove fa-fw" data-toggle="tooltip" data-placement="bottom" title="Delete Payment"></i></a>

                <form action="{{ url('admin/finance/payment/'.$payment->id.'/delete') }}" method="POST" enctype="multipart/form-data" id="deletePayment-{{$payment->id}}">
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </form>

                </td>
              @endif

              </tr>

              <!-- MTICS RECEIPT -->
              <div class="modal fade" id="mticsreceipt-{{$payment->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog modal-sm" role="document">
              <div class="modal-content">
              <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
              <h4 class="modal-title w3-text-gray" id="myModalLabel">MTICS Fund Payment Receipt</h4>
              </div>
              <form action="{{ url('admin/finance/payment/'.$payment->id.'/mtics-receipt') }}" method="POST" enctype="multipart/form-data">
              <div class="modal-body">
                <div class="w3-container">

                  Student ID: {{$payment->member->id_num}}<br>
                  Student Name: {{$payment->member->first_name}} {{$payment->member->last_name}}<br>
                  Section: {{$payment->member->section->section_code}}<br>
                  Amount: {{$payment->paymticsfund_amt}}<br>
                  Status: {{$payment->paymticsfund_status}}<br>
                  Receiver: {{$payment->admin->first_name}} {{$payment->admin->last_name}}</br>

                </div>
              </div>
              <div class="modal-footer">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <button type="submit" class="btn btn-default w3-green"><i class="fa fa-reply fa-fw"></i> Print</button>
              </div>

              </form>
              </div>
              </div>
              </div>
              <!-- END MODAL -->

            @endforeach
           </tbody>
          </table>
        </div>
      </div>
    </div>

    <div id="event" class="tab-pane fade in">
      <div class="row w3-margin-top">
        <a href="#eventreceiptIndividual" class="w3-text-gray" data-toggle="modal" style="outline: 0">
          <i class="fa fa-print fa-fw"></i>
          Print Receipt
        </a>
      </div>

      <!-- EVENT RECEIPT per specific student -->
      <div class="modal fade" id="eventreceiptIndividual" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
          <div class="modal-content">
          <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
          <h4 class="modal-title w3-text-gray" id="myModalLabel">EVENT Payment Receipt per selected student</h4>
          </div>
          <form action="{{ url('admin/finance/payment/selected-event-receipt') }}" method="POST" enctype="multipart/form-data">
          <div class="modal-body">
            <div class="w3-container">

             <div class="form-group">
          <label for="member_id">Name:</label>
          <select name="member_id" class="form-control">
              <option value="">-- Select Student Name --</option>
          @foreach($members as $member)
              <option value="{{$member->id}}">{{$member->first_name}} {{$member->last_name}}</option>
          @endforeach
          </select>
        </div>

            <select name="event_id" class="form-control">
              <option value="">-- Select Event --</option>
            @foreach($events as $event)
                <option value="{{$event->id}}">{{$event->event_title}}</option>
            @endforeach
            </select><br>

            </div>
          </div>
          <div class="modal-footer">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <button type="submit" class="btn btn-default w3-green"><i class="fa fa-reply fa-fw"></i> Print</button>
          </div>

          </form>
          </div>
        </div>
      </div>
      <!-- END MODAL -->

      <div class="row w3-margin-top">
        <div class="table-responsive">
          <div class="table-responsive">
            <table class="table table-bordered table-hover" id="eventTable">
              <thead>
                <tr>
                 <th class="w3-center">Student No.</th>
                 <th class="w3-center">Name</th>
                 <th class="w3-center">Year</th>
                 <th class="w3-center">Section</th>
                 <th class="w3-center">Event</th>
                 <th class="w3-center">Amount</th>
                 <th class="w3-center">Status</th>
                 <th class="w3-center">Receiver</th>
                 <th class="w3-center">Action</th>
                </tr>
              </thead>
              <tbody class="w3-text-gray">
              @foreach($eventPayments as $payment)
                <tr>
                  <td class="w3-center">{{$payment->member->id_num}}</td>
                  <td class="w3-center">{{$payment->member->first_name}} {{$payment->member->last_name}}</td>
                  <td class="w3-center">{{$payment->member->year->year_code}}</td>
                  <td class="w3-center">{{$payment->member->section->section_code}}</td>

                  @if($payment->event !== null)
                  <td class="w3-center">{{$payment->event->event_title}}</td>
                  @else
                  <td class="w3-center"></td>
                  @endif
                  <td class="w3-center">{{$payment->payevent_amt}}</td>
                  <td class="w3-center">{{$payment->paymevent_status}}</td>
                  <td class="w3-center">{{$payment->admin->first_name}} {{$payment->admin->last_name}}</td>
                  <td class="w3-center">
                  <a class="w3-text-green" data-toggle="modal" href="#eventreceipt-{{$payment->id}}">
                    <i class="fa fa-print fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Print Receipt"></i>
                  </a>

                    @if(Gate::allows('superadmin-only'))
                      <a href="javascript:void" onclick="$('#deletePayment-{{$payment->id}}').submit()" class="w3-text-red w3-large"><i class="fa fa-remove fa-fw" data-toggle="tooltip" data-placement="bottom" title="Delete Payment"></i></a>

                      <form action="{{ url('admin/finance/payment/'.$payment->id.'/event-delete') }}" method="POST" enctype="multipart/form-data" id="deletePayment-{{$payment->id}}">
                       <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      </form>

                      </td>
                    @endif

                  </td>
                </tr>


                <!-- EVENT RECEIPT -->
                <div class="modal fade" id="eventreceipt-{{$payment->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title w3-text-gray" id="myModalLabel">EVENT Payment Receipt</h4>
                </div>
                <form action="{{ url('admin/finance/payment/'.$payment->id.'/event-receipt') }}" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                  <div class="w3-container">
                    @if($payment->event !== null)
                    EVENT TITLE: {{$payment->event->event_title}}<br>

                    Student ID: {{$payment->member->id_num}}<br>
                    Student Name: {{$payment->member->first_name}} {{$payment->member->last_name}}<br>
                    Section: {{$payment->member->section->section_code}}<br>
                    Amount: {{$payment->payevent_amt}}<br>
                    Status: {{$payment->paymevent_status}}<br>
                    Receiver: {{$payment->admin->first_name}} {{$payment->admin->last_name}}</br>
                    @endif
                  </div>
                </div>
                <div class="modal-footer">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <button type="submit" class="btn btn-default w3-green"><i class="fa fa-reply fa-fw"></i> Print</button>
                </div>

                </form>
                </div>
                </div>
                </div>
                <!-- END MODAL -->

              @endforeach
             </tbody>
          </table>
        </div>
      </div>
    </div>

    </div>
    </div>
    </div>
    <!-- END MODAL -->

  </div>
</div>


<script type="text/javascript">
$(document).ready( function () {
  $('#mticsTable').DataTable();
  $('#eventTable').DataTable();

  $("#myInput-mtics").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $(".dropdown-menu li").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });

  $("#myInput-event").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $(".dropdown-menu li").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>


<script>
function setMticsStudent(name, id) {

  $("#mticsSelect").html(name + ' <span class="caret"></span></button>');
  $("#member_id-mtics").attr('value', id);

  var xhttp;
  if (id == "") {
    document.getElementById("mtics_payment").innerHTML = "";
    return;
  }
  xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      $("#mtics_payment").val(this.responseText);
    }
  };
  xhttp.open("GET", "{{url('admin/finance/payment/ajax-call')}}/"+id, true);
  xhttp.send();
}

function setEventStudent(name, id) {

  $("#eventSelect").html(name + ' <span class="caret"></span></button>');
  $("#member_id-eventid").attr('value', id);

  /*var xhttp;
  if (id == "") {
    $("#event_payment").val("");
    return;
  }
  xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      $("#event_payment").val(this.responseText);
    }
  };
  xhttp.open("GET", "{{url('admin/finance/payment/event/ajax-call')}}/"+id, true);
  xhttp.send();*/
}
</script>

<script type="text/javascript">
  function getFine(id) {
    $.ajax({
          url: '{{url("admin/finance/payment/get-penalty-amount")}}',
          data: {'officer_id': $("#officerId").val()},
          type: 'GET',
          success: function(response) {
            $("#penaltyPayment").val(response);
          }
        });
  };
</script>
@endsection
