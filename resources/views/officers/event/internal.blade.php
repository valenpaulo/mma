@extends('layout/control_panel')

@section('title')
{{$event->event_title}}
@endsection

@section('middle')
<div class="container-fluid w3-margin">
  <div class="row">
    <h3>
      <i class="fa fa-calendar fa-fw w3-xxlarge"></i>
      <strong>{{$event->event_title}}</strong>
    </h3>
  </div>

  <hr>

  <div class="row">
    <div class="table-responsive">
        <table class="table table-bordered table-hover" id="taskTable">
          <thead>
            <tr>
             <th class="w3-center">Task Name</th>
             <th class="w3-center">Description</th>
             <th class="w3-center">Due Date</th>
             <th class="w3-center">Status</th>
             <th class="w3-center">Action</th>
            </tr>
          </thead>
          <tbody class="w3-text-gray">
          @foreach($tasks as $task)
            <tr>
              <td class="w3-center">{{$task->task_name}}</td>
              <td class="w3-center">{{$task->task_desc}}</td>
              <td class="w3-center">{{$task->due_date}}</td>
              <td class="w3-center">{{$task->task_status}}</td>
              <td class="w3-center">
                @if ($task->task_status == 'pending')
                <a class="w3-text-green" href="{{url('admin/internal/manage-event/start-task/' . $task->id)}}"><i class="fa fa-play fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Start Task"></i></a>
                @elseif ($task->task_status == 'on-going' || $task->task_status == 'disapproved')
                <a class="w3-text-red" href="#add-{{$task->id}}" data-toggle="modal"><i class="fa fa-upload fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Upload File"></i></a>

                <a class="w3-text-green" href="{{url('admin/internal/manage-event/validate/'.$task->id)}}"><i class="fa fa-check-square fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Validate"></i></a>
                @elseif ($task->task_status == 'for validation')
                <i class="fa fa-spinner fa-fw w3-large w3-text-gray" data-toggle="tooltip" data-placement="bottom" title="For Validation"></i>
                @else
                <i class="fa fa-check-circle-o fa-fw w3-large w3-text-green" data-toggle="tooltip" data-placement="bottom" title="Done"></i>
                @endif
              </td>

            </tr>

            <!-- ADD MODAL -->
            <div class="modal fade" id="add-{{$task->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
          <h4 class="modal-title w3-text-gray" id="myModalLabel">Add Documents</h4>
            </div>
            <form action="{{url('admin/docu/manage-event/store/'.$task->id)}}" method="POST" enctype="multipart/form-data">
            <div class="modal-body">
              <div class="w3-container">
                <div class="row">

                  <div class="form-group filename-{{$task->id}}" style="display: none">
                        <p id="import-file-name-{{$task->id}}"></p>
                  </div>

                  <div class="form-group">
                    <div align="right">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <input type="hidden" name="task_id" value="{{ $task->id }}">
                      <input type="hidden" name="internal" value="internal">

                      <label for="import_file-{{$task->id}}" style="cursor: pointer;" class="btn btn-default w3-red" title="Bulk Add" align="right"><i class="fa fa-paperclip fa-fw w3-text-white w3-large"></i></label>
                      <input type="file" name="import_file" id="import_file-{{$task->id}}" style="opacity: 0; position: absolute; z-index: -1;" onchange="showFileName(event, {{$task->id}})" />
                      <button type="submit" class="btn btn-default w3-green w3-text-white"><i class="fa fa-plus-square"></i> Add</button>
                    </div>
                  </div>

                </div>
              </div>
            </div>
            </form>
            </div>
            </div>
            </div>
            <!-- END MODAL -->
          @endforeach
         </tbody>
        </table>
    </div>
  </div>

  @if(count($disapproved_documents) > 0)

  <div class="row w3-margin-top">
    <h4>
      <i class="fa fa-thumbs-down fa-fw"></i>
      <strong>Disapproved</strong>
    </h4>
  </div>

    <div class="row w3-margin-top">
    @foreach($disapproved_documents as $document)
      <div class="col-sm-3 w3-center">

        <div class="w3-card-4 w3-pale-yellow w3-margin">
          <div class="w3-container w3-padding">

          <div align="right">
            <a class="w3-text-green" href="{{url('admin/internal/download/'.$document->id)}}"><i class="fa fa-cloud-download fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Download"></i></a>

            <a class="w3-text-red" href="#delete-{{$document->id}}" data-toggle="modal"><i class="fa fa-trash-o fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Delete"></i></a>
          </div>

          <hr>

          <div class="w3-center">
          <h1>
           <i class="fa fa-file-word-o  w3-text-black"></i>
          </h1>
          </div>

          <a href="">
          <div class="w3-center w3-text-black">
          <a href="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/documents/'.$document->filename) : asset('documents/'.$document->filename)}}" target="_blank" class="w3-text-black">
            <p>{{explode("/", $document->filename)[3]}}</p>
          </a>
          <p>Task: {{$document->task->task_name}}</p>
          </div>
          </a>

          </div>

        </div>

        <div align="left">
          <label>Reason:</label>
          @foreach ($document->task->disapprovedLetter as $key => $disapproved)
            <p>{{++$key}}. <?php echo($disapproved->reason);?></p>
          @endforeach
        </div>

      </div>

      <!-- Delete MODAL -->
            <div class="modal fade" id="delete-{{$document->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
            <h4 class="modal-title w3-text-gray" id="myModalLabel">Delete Document</h4>
            </div>
            <form action="{{url('admin/docu/delete/'.$document->id)}}" enctype="multipart/form-data">
            <div class="modal-body">
              <div class="w3-container">
                <div class="row">

                  <div class="form-group">
                  <p>Are you sure you want to delete <strong>{{explode("/", $document->filename)[2]}}</strong>?</p>
                  </div>
                </div>
              </div>
            </div>

            <div class="modal-footer">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <button type="submit" class="btn btn-default w3-green w3-text-white">Yes</button>
            <button type="button" data-dismiss="modal" class="btn btn-default w3-red w3-text-white">No</button>
            </div>

            </form>
            </div>
            </div>
            </div>
          <!-- END DELETE MODAL -->

    @endforeach
  </div>
  @endif

  @if(count($ready_for_validation_documents) > 0)

  <div class="row w3-margin-top">
    <h4>
      <i class="fa fa-check-square fa-fw"></i>
      <strong>Ready for Validation</strong>
    </h4>
  </div>

    <div class="row w3-margin-top">
    @foreach($ready_for_validation_documents as $document)
      <div class="col-sm-3 w3-center">

        <div class="w3-card-4 w3-pale-yellow w3-margin">
          <div class="w3-container w3-padding">

          <div align="right">
            <a class="w3-text-green" href="{{url('admin/internal/download/'.$document->id)}}"><i class="fa fa-cloud-download fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Download"></i></a>

            <a class="w3-text-red" href="#delete-{{$document->id}}" data-toggle="modal"><i class="fa fa-trash-o fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Delete"></i></a>
          </div>

          <hr>

          <div class="w3-center">
          <h1>
           <i class="fa fa-file-word-o  w3-text-black"></i>
          </h1>
          </div>

          <a href="">
          <div class="w3-center w3-text-black">
          <a href="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/documents/'.$document->filename) : asset('documents/'.$document->filename)}}" target="_blank" class="w3-text-black">
            <p>{{explode("/", $document->filename)[3]}}</p>
          </a>
          <p>Task: {{$document->task->task_name}}</p>
          </div>
          </a>

          </div>

        </div>

      </div>

      <!-- Delete MODAL -->
            <div class="modal fade" id="delete-{{$document->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
            <h4 class="modal-title w3-text-gray" id="myModalLabel">Delete Document</h4>
            </div>
            <form action="{{url('admin/docu/delete/'.$document->id)}}" enctype="multipart/form-data">
            <div class="modal-body">
              <div class="w3-container">
                <div class="row">

                  <div class="form-group">
                  <p>Are you sure you want to delete <strong>{{explode("/", $document->filename)[2]}}</strong>?</p>
                  </div>
                </div>
              </div>
            </div>

            <div class="modal-footer">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <button type="submit" class="btn btn-default w3-green w3-text-white">Yes</button>
            <button type="button" data-dismiss="modal" class="btn btn-default w3-red w3-text-white">No</button>
            </div>

            </form>
            </div>
            </div>
            </div>
          <!-- END DELETE MODAL -->

    @endforeach
  </div>
  @endif

  @if(count($tobe_validated_documents) > 0)

  <div class="row w3-margin-top">
    <h4>
      <i class="fa fa-spinner fa-fw"></i>
      <strong>To be Validated</strong>
    </h4>
  </div>

    <div class="row w3-margin-top">
    @foreach($tobe_validated_documents as $document)
      <div class="col-sm-3 w3-center">

        <div class="w3-card-4 w3-pale-yellow w3-margin">
          <div class="w3-container w3-padding">

          <div align="right">
            <a class="w3-text-green" href="{{url('admin/internal/download/'.$document->id)}}"><i class="fa fa-cloud-download fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Download"></i></a>
          </div>

          <hr>

          <div class="w3-center">
          <h1>
           <i class="fa fa-file-word-o  w3-text-black"></i>
          </h1>
          </div>

          <a href="">
          <div class="w3-center w3-text-black">
          <a href="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/documents/'.$document->filename) : asset('documents/'.$document->filename)}}" target="_blank" class="w3-text-black">
            <p>{{explode("/", $document->filename)[3]}}</p>
          </a>
          <p>Task: {{$document->task->task_name}}</p>
          </div>
          </a>

          </div>

        </div>

      </div>

    @endforeach
  </div>
  @endif

  <div class="row w3-margin-top">
    <h4>
      <i class="fa fa-file-word-o fa-fw"></i>
      <strong>Letters</strong>
    </h4>
  </div>

    <div class="row w3-margin-top">
    @if(isset($documents))
    @foreach($documents as $document)
      <div class="col-sm-3 w3-center">

        <div class="w3-card-4 w3-pale-yellow w3-margin">
          <div class="w3-container w3-padding">

          <div align="right">
            <a class="w3-text-green" href="{{url('admin/internal/download/'.$document->id)}}"><i class="fa fa-cloud-download fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Download"></i></a>
          </div>

          <hr>

          <div class="w3-center">
          <h1>
           <i class="fa fa-file-word-o  w3-text-black"></i>
          </h1>
          </div>

          <a href="">
          <div class="w3-center w3-text-black">
          <a href="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/documents/'.$document->filename) : asset('documents/'.$document->filename)}}" target="_blank" class="w3-text-black">
            <p>{{explode("/", $document->filename)[3]}}</p>
          </a>
          <p>Task: {{$document->task->task_name}}</p>
          </div>
          </a>

          </div>

        </div>

      </div>

    @endforeach
    @endif
  </div>

</div>

<script type="text/javascript">

  function showFileName( event, id ) {

    $(".filename-" + id).show()
    // the change event gives us the input it occurred in
    var input = event.srcElement;

    // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
    var fileName = input.files[0].name;

    // use fileName however fits your app best, i.e. add it into a div
    $("#import-file-name-" + id).html(fileName)
  }

</script>

<script type="text/javascript">
  var input = document.getElementById( 'import_file' );
  var infoArea = document.getElementById( 'import-file-name' );

  input.addEventListener('change', showFileName);

  function showFileName( event ) {

    $(".filename").show()
    // the change event gives us the input it occurred in
    var input = event.srcElement;

    // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
    var fileName = input.files[0].name;

    // use fileName however fits your app best, i.e. add it into a div
    infoArea.textContent = fileName;
  }

</script>

<script type="text/javascript">
$(document).ready( function () {
  $('#taskTable').DataTable();
} );
</script>
@endsection
