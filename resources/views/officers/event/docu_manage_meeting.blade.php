@extends('layout/control_panel')

@section('title')
Manage Meeting
@endsection

@section('middle')

<b>meeting</b><br><br>
@foreach($meetings as $meeting)
agenda: {{$meeting->agenda}}<br>
where: {{$meeting->where}}<br>
when: {{$meeting->when}}<br>
status: {{$meeting->status}}<br>
<a href="{{ url('admin/docu/manage-event/'.$event->id.'/meeting/'.$meeting->id.'/attendance') }}">
<button class="btn btn-default w3-orange w3-text-white" title="Edit"><i class="fa fa-pencil"></i>manage attendance</button></a><br><br><br>
@endforeach



@endsection