@extends('layout/control_panel')

@section('title')
Manage Event
@endsection

@section('middle')
<div class="container-fluid w3-margin">

  <div class="row">
    <h3>
      <i class="fa fa-calendar fa-fw w3-xxlarge"></i>
      <strong>Manage Event</strong>
    </h3>
  </div>

  <hr>

  @if(Gate::allows('president-only'))
  <div class="row">
    <p>
      <a href="#add" data-toggle="modal" class="w3-text-gray" style="outline: 0"><i class="fa fa-plus-square-o fa-fw"></i> Add</a>
    </p>
  </div>

  <!-- ADD MODAL -->
  <div class="modal fade" id="add" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-md" role="document">
  <div class="modal-content">
  <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
  <h4 class="modal-title w3-text-gray" id="myModalLabel">Add Event</h4>
  </div>
  <form action="{{ url('admin/president/manage-event/store') }}" method="POST" enctype="multipart/form-data">

  <div class="modal-body">
    <div class="w3-container">
      <div class="row">

      <div class="form-group">
        <label for="title">Event Title:</label>
        <input type="text" name="event_title" class="form-control" placeholder="Event Title" required>
      </div>

      <div class="form-group">
        <label for="description">Description:</label>
        <input type="text" name="description" class="form-control" placeholder="Event Description">
      </div>

      <div class="form-group">
        <label for="start_date">Start Date:</label>
        <input type="date" name="start_date" class="form-control" placeholder="Event Title" required>
      </div>

      <div class="form-group">
        <label for="end_date">End Date:</label>
        <input type="date" name="end_date" class="form-control" placeholder="Event Title">
      </div>

      </div>
    </div>
  </div>

  <div class="modal-footer">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
  </div>

  </form>
  </div>
  </div>
  </div>
  <!-- END MODAL -->


  @endif

  <div class="row w3-margin-top">
    <!-- TABLE -->
    <div class="table-responsive">
    <table class="table table-hover table-bordered" id="eventTable">
      <thead>
        <tr>
         <th class="w3-center">Title</th>
         <th class="w3-center">Star Date</th>
         <th class="w3-center">End Date</th>
         <th class="w3-center">Status</th>
         <th class="w3-center">Action</th>
        </tr>
      </thead>
      <tbody class="w3-text-gray">
        @foreach($events as $event)
        <tr>
          <td class="w3-center">{{$event->event_title}}</td>
          <td class="w3-center">{{$event->start_date}}</td>
          <td class="w3-center">{{$event->end_date}}</td>
          <td class="w3-center">{{$event->status}}</td>
          <td class="w3-center">

            @if(Gate::allows('internal-only'))

            <a href="{{url('admin/internal/manage-event/' . $event->id)}}" class="w3-text-blue"><i class="fa fa-eye fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="View"></i>
            </a>

            @elseif(Gate::allows('docu-only'))

            <a href="{{url('admin/docu/manage-event/' . $event->id)}}" class="w3-text-blue"><i class="fa fa-eye fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="View"></i>
            </a>

            @elseif(Gate::allows('external-only'))

            <a href="{{url('admin/external/manage-event/' . $event->id)}}" class="w3-text-blue"><i class="fa fa-eye fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="View"></i>
            </a>


            @elseif(Gate::allows('finance-only'))

            <a href="{{url('admin/finance/manage-event/' . $event->id)}}" class="w3-text-blue"><i class="fa fa-eye fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="View"></i>
            </a>

            @elseif(Gate::allows('logistics-only'))

            <a href="{{url('admin/logistics/manage-event/' . $event->id)}}" class="w3-text-blue"><i class="fa fa-eye fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="View"></i>
            </a>

            @elseif(Gate::allows('auditor-only'))

            <a href="{{url('admin/auditor/manage-event/' . $event->id)}}" class="w3-text-blue"><i class="fa fa-eye fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="View"></i>
            </a>

            @elseif(Gate::allows('activity-only'))

            <a href="{{url('admin/activities/manage-event/' . $event->id)}}" class="w3-text-blue"><i class="fa fa-eye fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="View"></i>
            </a>

            @elseif(Gate::allows('president-only'))

            <a href="{{url('admin/president/manage-event/' . $event->id)}}" class="w3-text-blue"><i class="fa fa-eye fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="View"></i></a>

            @if ($event->status == "pending")
            <a href="javascript:void(0)" onclick="eventStart( {{$event->id}} ) " class="w3-text-green"><i class="fa fa-play fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Start"></i></a>

            <form action="{{url('admin/president/manage-event/start/'.$event->id)}}" id="formStart-{{$event->id}}"></form>
            @elseif ($event->status == "on-going")

            <a href="javascript:void(0)" onclick="eventDone( {{$event->id}} )" class="w3-text-green"><i class="fa fa-check-circle-o fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Done"></i></a>

            <a href="javascript:void(0)" onclick="eventCancel( {{$event->id}} )"  class="w3-text-red"><i class="fa fa-remove fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Cancel"></i></a>

            <form action="{{url('admin/president/manage-event/done/'.$event->id)}}" id="formDone-{{$event->id}}"></form>

            <form action="{{url('admin/president/manage-event/cancel/'.$event->id)}}" id="formCancel-{{$event->id}}"></form>
            @endif

            @elseif(Gate::allows('info-only'))

            <a href="{{url('admin/info/manage-event/' . $event->id)}}" class="w3-text-blue"><i class="fa fa-eye fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="View"></i>
            </a>


            @endif
          </td>
        </tr>

        @endforeach
      </tbody>
    </table>
    </div>
    <!-- END TABLE -->
  </div>
</div>

<script>
function showPayment(id) {
  var xhttp;
  if (id == "") {
    document.getElementById("payment").innerHTML = "";
    return;
  }
  xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      $("#mtics_payment").val(this.responseText);
    }
  };
  xhttp.open("GET", "{{url('admin/finance/payment/ajax-call')}}/"+id, true);
  xhttp.send();
}
</script>


<script type="text/javascript">
$(document).ready( function () {
  $('#eventTable').DataTable();
} );
</script>

<script type="text/javascript">
function eventStart( id ) {
  swal({
  title: "Are you sure?",
  text: "Once started, you will not be able to undo this action!",
  icon: "warning",
  buttons: true,
  dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      $("#formStart-" + id).submit();
    }
  });
}

function eventCancel( id ) {
  swal({
  title: "Are you sure?",
  text: "Once cancelled, you will not be able to undo this action!",
  icon: "warning",
  buttons: true,
  dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      $("#formCancel-" + id).submit();
    }
  });
}

function eventDone( id ) {
  swal({
  title: "Are you sure?",
  text: "Once finished, you will not be able to undo this action!",
  icon: "warning",
  buttons: true,
  dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      $("#formDone-" + id).submit();
    }
  });
}
</script>


<script type="text/javascript">
  window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove();
    });
}, 4000);
</script>
@endsection
