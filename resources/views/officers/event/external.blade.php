@extends('layout/control_panel')

@section('title')
{{$event->event_title}}
@endsection

@section('middle')

<div class="w3-container w3-margin">

  <div class="row">
    <h3>
        <i class="fa fa-calendar fa-fw w3-xxlarge"></i>
        <strong>{{$event->event_title}}</strong>
      </h3>
  </div>

  <hr>

  <div class="row">
    <div class="table-responsive">
        <table class="table table-bordered table-hover" id="taskTable">
          <thead>
            <tr>
             <th class="w3-center">Task Name</th>
             <th class="w3-center">Description</th>
             <th class="w3-center">Due Date</th>
             <th class="w3-center">Status</th>
            </tr>
          </thead>
          <tbody class="w3-text-gray">
          @foreach($tasks as $task)
            <tr>
              <td class="w3-center">
                <a href="{{url('admin/external/manage-event/'. $event->id .'/task/'. $task->id)}}" class="w3-text-gray">
                  <i class="fa fa-exclamation-circle fa-fw"></i>
                  {{$task->task_name}}
                </a>
              </td>
              <td class="w3-center">{{$task->task_desc}}</td>
              <td class="w3-center">{{$task->due_date}}</td>
              <td class="w3-center">{{$task->task_status}}</td>
            </tr>
          @endforeach
         </tbody>
        </table>
    </div>
  </div>

</div>

<script type="text/javascript">
$(document).ready( function () {
  $('#taskTable').DataTable();
} );
</script>

@endsection
