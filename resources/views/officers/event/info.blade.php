@extends('layout/control_panel')

@section('title')
{{$event->event_title}}
@endsection

@section('middle')




    <style type="text/css">
      .no-border, .no-border:focus {
        border: 0;
        box-shadow: none;
        outline: none; /* You may want to include this as bootstrap applies these styles too */
      }

      .input-lg {
        font-size: 32px
      }
    </style>

    <script src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/js/tinymce/js/tinymce/tinymce.min.js') : asset('js/tinymce/js/tinymce/tinymce.min.js')}}"></script>

    <script>tinymce.init({selector:'textarea', plugins: "autoresize"});</script>

    <div class="w3-container w3-margin">
      <div class="row">
        <h3>
          <i class="fa fa-calendar fa-fw w3-xxlarge"></i>
          <strong>Manage Event</strong>
        </h3>
      </div>

      <hr>

      <div class="row">
      <ul class="nav nav-tabs">

          <li class="active">
            <a data-toggle="tab" href="#eventpost" class="w3-text-black">Event Post
            </a>
          </li>


          <li>
            <a data-toggle="tab" href="#task" class="w3-text-black">Task
            @if(count($info_tasks) > 0)
            <span class="w3-badge w3-red">{{count($info_tasks)}}</span>
            @endif
            </a>
          </li>
        </ul>
      </div>

  <div class="tab-content">
    <div id="eventpost" class="tab-pane fade in active">
      <div class="row w3-margin-top w3-padding">
        <p>
          <a href="#full-{{$event->id}}" data-toggle="modal" class="w3-text-gray" style="outline: 0"><i class="fa fa-newspaper-o fa-fw"></i> Full Post</a>
        </p>
      </div>

      <div class="w3-card-4 w3-white w3-container w3-padding w3-margin">
        <br>
        <form action="{{ url('admin/info/manage-event/'.$event->id.'/add') }}" method="post" enctype="multipart/form-data">

          <div class="form-group" align="right">
             <button type="submit" class="btn btn-default w3-green"><i class="fa fa-pencil-square-o fa-fw w3-large"></i> Publish</button>
          </div>

          <hr>

          <div class="row">
            <div class="form-group">
              <div class="col-lg-8 col-md-8">
                <strong>
                  <input type="text" name="title" id="title" class="form-control no-border input-lg w3-text-black" placeholder="Title" required>
                </strong>
              </div>
            </div>

          </div>

          <hr>

          <div class="form-group">
            <textarea rows="2" placeholder="what's on your mind?" name="body" class="form-control thisarea"></textarea>
          </div>

          <hr>

          <input type="hidden" name="_token" value="{{ csrf_token() }}">
        </form>

      </div>

      <br>

      <div class="row w3-margin-top">
        <div class="table-responsive">
            <table class="table table-bordered table-hover" id="postTable">
              <thead>
                <tr>
                 <th class="w3-center">Title</th>
                 <th class="w3-center">Body</th>
                 <th class="w3-center">Status</th>
                 <th class="w3-center">Created At</th>
                 <th class="w3-center">Action</th>
                </tr>
              </thead>
              <tbody class="w3-text-gray">
              @foreach($eventpartials as $eventpartial)
                <tr>
                  <td class="w3-center">{{$eventpartial->title}}</td>
                  <td class="w3-center">{{(substr(strip_tags($eventpartial->body), 0, 50))}} ...</td>
                  <td class="w3-center">{{$eventpartial->status}}</td>
                  <td class="w3-center">{{$eventpartial->created_at}}</td>
                  <td class="w3-center">
                    <a href="{{url('admin/info/manage-event/'.$event->id.'/'.$eventpartial->id.'/'.$eventpartial->slug.'/edit-view')}}" class="w3-text-orange w3-large"><i class="fa fa-pencil-square-o fa-fw" data-toggle="tooltip" data-placement="bottom" title="Edit Offer"></i></a>

                    <a href="#delete-{{$eventpartial->id}}" class="w3-text-red w3-large" data-toggle="modal"><i class="fa fa-trash-o fa-fw" data-toggle="tooltip" data-placement="bottom" title="Archive"></i></a>
                  </td>

                  <!-- MODAL -->
                  <div class="modal fade" id="delete-{{$eventpartial->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-sm" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                          <h4 class="modal-title w3-text-gray" id="myModalLabel">Delete Post</h4>
                        </div>
                      <form action="{{ url('admin/info/manage-event/'.$event->id.'/'.$eventpartial->id.'/'.$eventpartial->slug.'/delete' ) }}" method="POST" enctype="multipart/form-data">
                        <div class="modal-body">

                          <p>Are you sure you want to delete '{{$eventpartial->title}}' post?</p>

                        </div>

                        <div class="modal-footer">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button type="submit" class="btn btn-default w3-green">Yes</button>
                        <button type="button" data-dismiss="modal" class="btn btn-default w3-red">No</button>
                      </div>
                      </form>
                      </div>
                    </div>
                  </div>
                  <!-- MODAL -->
              </tr>
              @endforeach
             </tbody>
            </table>
        </div>
      </div>
    </div>

    <!-- TASK PANEL -->
    <div id="task" class="tab-pane fade">
        <div class="row w3-margin-top">

          <h4>
              <i class="fa fa-tasks fa-fw"></i>
              <strong>Task</strong>
          </h4>

        </div>

        <div class="row">
          <div class="table-responsive">
              <table class="table table-bordered">
                <thead>
                  <tr>
                   <th class="w3-center">Task Name</th>
                   <th class="w3-center">Description</th>
                   <th class="w3-center">Due Date</th>
                   <th class="w3-center">Status</th>
                   <th class="w3-center">Action</th>
                  </tr>
                </thead>
                <tbody class="w3-text-gray">
                @foreach($info_tasks as $task)
                  <tr>
                    <td class="w3-center">{{$task->task_name}}</td>
                    <td class="w3-center">{{$task->task_desc}}</td>
                    <td class="w3-center">{{$task->due_date}}</td>
                    <td class="w3-center">{{$task->task_status}}</td>
                    <td class="w3-center">
                      @if ($task->task_status == 'pending')
                      <a class="w3-text-green" href="{{url('admin/finance/manage-event/start-task/' . $task->id)}}"><i class="fa fa-play fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Start Task"></i></a>
                      @elseif ($task->task_status == 'on-going' || $task->task_status == 'disapproved')

                      <a class="w3-text-green" href="{{url('admin/finance/manage-event/validate/'.$task->id)}}"><i class="fa fa-check-square fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Validate"></i></a>

                      @elseif ($task->task_status == 'for validation')
                      <i class="fa fa-check-square fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Validate"></i>
                      @endif
                    </td>

                  </tr>


                @endforeach
               </tbody>
              </table>
          </div>
        </div>
      </div>


    </div>
  </div>


    <!-- ADD MODAL -->
    <div class="modal fade" id="full-{{$event->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
    <h4 class="modal-title w3-text-gray" id="myModalLabel">Full Post</h4>
    </div>
    <form action="{{url('/admin/info/manage-event/'.$event->id.'/post')}}" method="POST" enctype="multipart/form-data">
    <div class="modal-body">
      <div class="w3-container">
        <div class="row">
             are you sure, you want to post?
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <button type="submit" class="btn btn-default w3-green w3-text-white"> Yes</button>
      <button type="button" data-dismiss="modal" class="btn btn-default w3-red w3-text-white"> No</button>
    </div>
    </form>
    </div>
    </div>
    </div>
    <!-- END MODAL -->



<script type="text/javascript">
$(document).ready( function () {

  $('#postTable').DataTable();

} );
</script>
@endsection
