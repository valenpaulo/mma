@extends('layout/control_panel')

@section('title')
Event Breakdown
@endsection

@section('middle')
<div class="container-fluid w3-margin">
  <div class="row">
      <h3>
        <i class="fa fa-paper-plane-o fa-fw w3-xxlarge"></i>
        <strong>Breakdown Request</strong>
      </h3>
  </div>

  <hr>

  <div class="row w3-margin-top">
    <div class="table-responsive">
        <table class="table table-bordered table-hover" id="breakdownTable">
          <thead>
            <tr>
             <th class="w3-center">Event</th>
             <th class="w3-center">Amount</th>
             <th class="w3-center">Status</th>
             <th class="w3-center">Action</th>
            </tr>
          </thead>
          <tbody class="w3-text-gray">
          @foreach($events as $event)
           <tr>
             <td class="w3-center">{{$event->event_title}}</td>
             <td class="w3-center">

             <a class="w3-text-gray" href="#view-{{$event->id}}" data-toggle="modal"><i class="fa fa-exclamation-circle fa-fw"></i>{{$event->event_amount}}</a>

             </td>
             <td class="w3-center">{{$event->amount_confirm}}</td>
             <td class="w3-center">
              @if ($event->amount_confirm == "pending")
              <a class="w3-text-green" href="#approveBudget-{{$event->id}}" data-toggle="modal"><i class="fa fa-thumbs-up fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Approve Request"></i></a>
              <a class="w3-text-red" href="#denyBudget-{{$event->id}}" data-toggle="modal"><i class="fa fa-thumbs-down fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Deny Request"></i></a>
              @else
              <i class="fa fa-check-circle-o fa-fw w3-large w3-text-green" data-toggle="tooltip" data-placement="bottom" title="Approved"></i>
              @endif

              <!-- Approve Request -->
              <div class="modal fade" id="view-{{$event->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel" align="left">
              <div class="modal-dialog modal-sm" role="document">
              <div class="modal-content">
              <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
              <h4 class="modal-title w3-text-gray" id="myModalLabel">Approve Event Breakdown</h4>
              </div>
              <div class="modal-body">
                <div class="w3-container">
                  <div class="row">

                    @foreach ($event->breakdown as $breakdown)
                    <div class="form-group">
                      <label>{{$breakdown->breakdown_name}}</label>
                      <input type="text" class="form-control" value="{{$breakdown->breakdown_amt}}">
                    </div>
                    @endforeach

                  </div>
                </div>
              </div>

              </div>
              </div>
              </div>
              <!-- END MODAL -->

              <!-- Approve Request -->
              <div class="modal fade" id="approveBudget-{{$event->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel" align="left">
              <div class="modal-dialog modal-sm" role="document">
              <div class="modal-content">
              <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
              <h4 class="modal-title w3-text-gray" id="myModalLabel">Approve Event Breakdown</h4>
              </div>
              <form action="{{ url('admin/president/requested-event-breakdown/'.$event->id.'/approved') }}" method="POST" enctype="multipart/form-data">
              <div class="modal-body">
                <div class="w3-container">
                  <div class="row">

                    <div class="form-group">
                      <p>Do you want to proceed?</p>
                    </div>

                  </div>
                </div>
              </div>

              <div class="modal-footer">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <button type="submit" class="btn btn-default w3-green"> Yes</button>
                <button type="button" data-dismiss="modal" class="btn btn-default w3-red"> No</button>

              </div>

              </form>
              </div>
              </div>
              </div>
              <!-- END MODAL -->

              <!-- Deny Request -->
              <div class="modal fade" id="denyBudget-{{$event->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel" align="left">
              <div class="modal-dialog modal-md" role="document">
              <div class="modal-content">
              <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
              <h4 class="modal-title w3-text-gray" id="myModalLabel">Deny Event Breakdown</h4>
              </div>
              <form action="{{ url('admin/president/requested-event-breakdown/'.$event->id.'/denied') }}" method="POST" enctype="multipart/form-data">
              <div class="modal-body">
                <div class="w3-container">
                  <div class="row">
                    <div class="form-group">
                      <p>Please indicate the reason for denying the request.</p>
                    </div>

                    <div class="form-group">
                      <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="denied_reason" placeholder="Reason" rows="3"></textarea>
                    </div>

                    <div class="form-group">
                      <p>Do you want to proceed?</p>
                    </div>

                  </div>
                </div>
              </div>

              <div class="modal-footer">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <button type="submit" class="btn btn-default w3-green"> Yes</button>
                <button type="button" data-dismiss="modal" class="btn btn-default w3-red"> No</button>

              </div>

              </form>
              </div>
              </div>
              </div>
              <!-- END MODAL -->
             </td>
            </tr>

          @endforeach
         </tbody>
        </table>
    </div>
  </div>

</div>

<script type="text/javascript">
$(document).ready( function () {
  var table = $('#breakdownTable').DataTable();
  $(table.table().container()).removeClass('form-inline');
});
</script>

@endsection
