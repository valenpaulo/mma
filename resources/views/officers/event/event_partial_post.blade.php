''
@extends('layout/layout')
@section('title')

{{$eventpartialpost->title}}

@endsection

@section('content')
<style type="text/css">
  @media only screen and (max-width: 1365px) {
    .main {
      margin-top: 70px;
    }
  }

  @media (min-width: 1366px) {
    .main {
      margin-top: 105px;
    }
  }
</style>


<div class="container main">
  <div class="row">
  <div class="container-fluid w3-margin-right">
  <div class="row">
    <div class="col-sm-8"></div>
    <div class="col-sm-4">
    @if($errors->any())
      @foreach ($errors->all() as $error)
      <div class="alert alert-danger alert-dismissable fade in w3-right col-sm-12" id="success-alert">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>
      {{$error}}
      </strong>
      </div>
      @endforeach
    @endif
    </div>
  </div>

  <div class="row w3-padding">
    <br class="w3-hide-small w3-hide-medium">
    <p class="w3-text-gray w3-right">{{$eventpartialpost->created_at}}</p>
    <br>
    <hr>
  </div>

  <div class="row w3-padding" align="center">
    <h1>{{$eventpartialpost->title}}</h1>
    <p>{{$eventpartialpost->event->event_title}}</p>
  </div>
  @if($eventpartialpost->event->theme_image)
  <div class="row" align="center">
    <img src="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$eventpartialpost->event->theme_image) : asset('images/'.$eventpartialpost->event->theme_image) }}" style="width: 50%">
  </div>
  @else
  <div class="row" align="center">
    <img src="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/posts/default.jpg') : asset('images/posts/default.jpg') }}" style="width: 50%">
  </div>
  @endif

  <br>

  <div class="row w3-padding" align="justify">
    <p><?php echo($eventpartialpost->body);?></p>
  </div>

  <hr>

  <div class="row">
    <div class="w3-card-4 w3-white w3-padding w3-margin">
      <div class="w3-container">
        <div class="row w3-padding-small">
          <img src="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.Auth::user()->avatar) : asset('images/'.Auth::user()->avatar) }}" alt=" Avatar" class="w3-left w3-circle w3-margin-right w3-hide-small" style="width: 80px; height: 80px">

          <img src="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.Auth::user()->avatar) : asset('images/'.Auth::user()->avatar) }}" alt="Avatar" class="w3-left w3-circle w3-margin-right w3-hide-large w3-hide-medium" style="width: 50px; height: 50px">

          <span class="w3-right w3-large">
            <button class="btn btn-default w3-green" type="submit" form="reply"><i class="fa fa-paper-plane-o fa-fw w3-large"></i>Send</button>
          </span>

          <span>
            <strong>{{Auth::user()->username}}</strong>
            <br>
            {{Auth::user()->id_num}}
          </span>
        </div>
        <br>
        <div class="row">
          <form action="{{ url('member/event/'.$eventpartialpost->id.'/'.$eventpartialpost->slug.'/reply' ) }}" id="reply" method="POST">
            <div class="form-group">
              <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="reply_body" required placeholder="Join the discussion" rows="4"></textarea>
            </div>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
          </form>
        </div>
      </div>
    </div>
  </div>
  <br>

  @foreach($eventpartialpost->reply as $reply)
  <div class="row w3-margin">
      <img src="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$reply->member->avatar) :  asset('images/'.$reply->member->avatar) }}" alt="Avatar" class="w3-left w3-circle w3-margin-right w3-hide-small" style="width: 80px; height: 80px">

      <img src="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$reply->member->avatar) : asset('images/'.$reply->member->avatar) }}" alt="Avatar" class="w3-left w3-circle w3-margin-right w3-hide-large w3-hide-medium" style="width: 50px; height: 50px">

      <span>
        <strong>{{ $reply->member->first_name }} {{ $reply->member->last_name }}</strong>
        <br>
        {{$reply->created_at}}
        <br>
        <br>
        <i class="fa fa-comments-o fa-fw w3-large"></i>
        {{$reply->reply_body}}
      </span>
  </div>
  @if(!$loop->last)
  <hr>
  @endif
  @endforeach
</div>

<script type="text/javascript">
 window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove();
    });
}, 4000);
</script>

@endsection
