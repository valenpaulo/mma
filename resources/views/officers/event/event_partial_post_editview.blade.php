@extends('layout/layout')
@section('title')

{{$eventpartial->title}}

@endsection

@section('content')

<style type="text/css">
  .no-border, .no-border:focus {
    border: 0;
    box-shadow: none;
    outline: none; /* You may want to include this as bootstrap applies these styles too */
  }

  .input-lg {
    font-size: 32px
  }
</style>

<style type="text/css">
  @media only screen and (max-width: 1365px) {
    .main {
      margin-top: 60px;
    }
  }

  @media (min-width: 1366px) {
    .main {
      margin-top: 105px;
    }
  }
</style>

<script src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/js/tinymce/js/tinymce/tinymce.min.js') : asset('js/tinymce/js/tinymce/tinymce.min.js')}}"></script>
<script>
    tinymce.init({ selector:'textarea', plugins: "autoresize" });
</script>

<div class="container main">
  <div class="w3-card-4 w3-white w3-white w3-padding w3-margin">
    <div class="w3-container">
        <form action="{{ url('admin/info/manage-event/'.$eventpartial->event->id.'/'.$eventpartial->id.'/'.$eventpartial->slug.'/edit' ) }}" method="post" enctype="multipart/form-data">

          <div class="form-group" align="right">
             <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw w3-large"></i> Save</button>
          </div>

          <div class="form-group postImageDiv" align="center">
            <img src="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$eventpartial->event->theme_image) : asset('images/'.$eventpartial->event->theme_image) }}" style="width: 50%;" id="postImageContainer">
          </div>

          <hr>

          <div class="row">
            <div class="form-group">
              <div class="col-lg-8 col-md-8">
                <strong>
                  <input type="text" name="title" id="title" class="form-control no-border input-lg w3-text-black" placeholder="Title" required value="{{$eventpartial->title}}">
                </strong>
              </div>

            </div>

          </div>

          <hr>

          <div class="form-group">
            <textarea rows="3" placeholder="what's on your mind?" name="body" class="form-control">{{$eventpartial->body}}</textarea>
          </div>

          <input type="hidden" name="_token" value="{{ csrf_token() }}">
        </form>
    </div>
  </div>
</div>

<script>

    input.addEventListener('change', showFileName1);

    function showFileName1( event ) {

        // the change event gives us the input it occurred in
        var input = event.srcElement;

        // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
        var fileName = input.files[0].name;

        // use fileName however fits your app best, i.e. add it into a div
        infoArea.textContent = fileName;
      };

     window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove();
    });
}, 4000);

</script>
@endsection
