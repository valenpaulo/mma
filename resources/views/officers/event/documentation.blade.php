@extends('layout/control_panel')

@section('title')
{{$event->event_title}}
@endsection

@section('middle')
<style type="text/css">
  .no-border, .no-border:focus {
    border: 0;
    box-shadow: none;
    outline: none; /* You may want to include this as bootstrap applies these styles too */
  }

  .input-lg {
    font-size: 32px
  }
</style>

<div class="container-fluid w3-margin">

  <div class="row">
    <h3>
      <i class="fa fa-calendar fa-fw w3-xxlarge"></i>
      <strong>Manage Event</strong>
    </h3>
  </div>

  <hr>

  <div class="row">
    <ul class="nav nav-tabs">
      <li class="active">
        <a data-toggle="tab" href="#task" class="w3-text-black">Task</a>
      </li>

      <li>
        <a data-toggle="tab" href="#validation" class="w3-text-black">For Validation
        @if(count($validate_tasks))
        <span class="w3-badge w3-red">{{count($validate_tasks)}}</span>
        @endif
        </a>
      </li>

      <li>
        <a data-toggle="tab" href="#eventInfo" class="w3-text-black">Event Information
        @if($event->theme_image == null)
        <span class="w3-badge w3-red">1</span>
        @endif
      </a>
      </li>
    </ul>
  </div>

  <div class="tab-content">
    <div id="task" class="tab-pane fade in active">

       <div class="row w3-margin-top">
        <div class="table-responsive">
            <table class="table table-bordered table-hover" id="taskTable">
              <thead>
                <tr>
                 <th class="w3-center">Task Name</th>
                 <th class="w3-center">Description</th>
                 <th class="w3-center">Due Date</th>
                 <th class="w3-center">Status</th>
                 <th class="w3-center">Action</th>
                </tr>
              </thead>
              <tbody class="w3-text-gray">
              @foreach($tasks as $task)
                <tr>
                  <td class="w3-center">{{$task->id}} - {{$task->task_name}}</td>
                  <td class="w3-center">{{$task->task_desc}}</td>
                  <td class="w3-center">{{$task->due_date}}</td>
                  <td class="w3-center">{{$task->task_status}}</td>
                  <td class="w3-center">

                     @if ($task->task_status == 'pending')
                      <a class="w3-text-green" href="{{url('admin/internal/manage-event/start-task/' . $task->id)}}"><i class="fa fa-play fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Start Task"></i></a>
                      @elseif ($task->task_status == 'on-going' || $task->task_status == 'disapproved')
                      <a class="w3-text-red" href="#add-{{$task->id}}" data-toggle="modal"><i class="fa fa-upload fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Upload File"></i></a>

                      <a class="w3-text-green" href="{{url('admin/docu/manage-event/validate/'.$task->id)}}"><i class="fa fa-check-square fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Validate"></i></a>
                      @elseif ($task->task_status == 'for validation')
                      <i class="fa fa-spinner fa-fw w3-large w3-text-gray" data-toggle="tooltip" data-placement="bottom" title="For Validation"></i>

                      @elseif ($task->task_status == 'done')
                      <i class="fa fa-check-circle-o fa-fw w3-large w3-text-green" data-toggle="tooltip" data-placement="bottom" title="For Validation"></i>
                      @endif

                  </td>

                </tr>

                     <!-- ADD MODAL -->
                      <div class="modal fade" id="add-{{$task->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                      <div class="modal-dialog modal-sm" role="document">
                      <div class="modal-content">
                      <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                      <h4 class="modal-title w3-text-gray" id="myModalLabel">Add Document</h4>
                      </div>
                      <form action="{{url('admin/docu/manage-event/store/'.$task->id)}}" method="POST" enctype="multipart/form-data">
                      <div class="modal-body">
                        <div class="w3-container">
                          <div class="row">
                            <div class="form-group">
                              <label>File Category:</label>
                              <select class="form-control" name="file_category">
                                @foreach($file_categories as $file_category)
                                  <option value="{{$file_category->id}}">{{$file_category->category_name}}</option>
                                @endforeach
                              </select>

                            </div>


                            <div class="form-group filename-{{$task->id}}" style="display: none">
                                  <p id="import-file-name-{{$task->id}}"></p>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <label for="import_file-{{$task->id}}" style="cursor: pointer;" class="btn btn-default w3-red" title="Bulk Add"><i class="fa fa-paperclip fa-fw w3-text-white w3-large"></i></label>
                        <input type="file" name="import_file" id="import_file-{{$task->id}}" style="opacity: 0; position: absolute; z-index: -1;" onchange="showFileName( event, {{$task->id}} )" />
                        <button type="submit" class="btn btn-default w3-green w3-text-white"><i class="fa fa-plus-square"></i> Add</button>
                      </div>
                      </form>
                      </div>
                      </div>
                      </div>
                      <!-- END MODAL -->

               @endforeach
             </tbody>
            </table>
        </div>
      </div>

      @if(count($disapproved_documents) > 0)

      <div class="row w3-margin-top">
        <h4>
          <i class="fa fa-thumbs-down fa-fw"></i>
          <strong>Disapproved</strong>
        </h4>
      </div>

        <div class="row w3-margin-top">
        @foreach($disapproved_documents as $document)
          <div class="col-sm-3 w3-center">

            <div class="w3-card-4 w3-pale-yellow w3-margin">
              <div class="w3-container w3-padding">

              <div align="right">
                <a class="w3-text-green" href="{{url('admin/internal/download/'.$document->id)}}"><i class="fa fa-cloud-download fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Download"></i></a>

                <a class="w3-text-red" href="#delete-{{$document->id}}" data-toggle="modal"><i class="fa fa-trash-o fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Delete"></i></a>
              </div>

              <hr>

              <div class="w3-center">
              <h1>
               <i class="fa fa-file-word-o  w3-text-black"></i>
              </h1>
              </div>

              <a href="">
              <div class="w3-center w3-text-black">
              <a href="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/documents/'.$document->filename) : asset('documents/'.$document->filename)}}" target="_blank" class="w3-text-black">
                <p>{{explode("/", $document->filename)[3]}}</p>
              </a>
              <p>Task: {{$document->task->task_name}}</p>
              </div>
              </a>

              </div>

            </div>

            <div align="left">
              <label>Reason:</label>
              @foreach ($document->task->task_deny as $key => $disapproved)
                <p>{{++$key}}. <?php echo($disapproved->reason);?></p>
              @endforeach
            </div>

          </div>

          <!-- Delete MODAL -->
            <div class="modal fade" id="delete-{{$document->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
            <h4 class="modal-title w3-text-gray" id="myModalLabel">Delete Document</h4>
            </div>
            <form action="{{url('admin/docu/delete/'.$document->id)}}" enctype="multipart/form-data">
            <div class="modal-body">
              <div class="w3-container">
                <div class="row">

                  <div class="form-group">
                  <p>Are you sure you want to delete <strong>{{explode("/", $document->filename)[2]}}</strong>?</p>
                  </div>
                </div>
              </div>
            </div>

            <div class="modal-footer">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <button type="submit" class="btn btn-default w3-green w3-text-white">Yes</button>
            <button type="button" data-dismiss="modal" class="btn btn-default w3-red w3-text-white">No</button>
            </div>

            </form>
            </div>
            </div>
            </div>
          <!-- END DELETE MODAL -->

        @endforeach
      </div>
      @endif

      @if(count($ready_for_validation_documents) > 0)

        <div class="row w3-margin-top">
          <h4>
            <i class="fa fa-check-square fa-fw"></i>
            <strong>Ready for Validation</strong>
          </h4>
        </div>

          <div class="row w3-margin-top">
          @foreach($ready_for_validation_documents as $document)
            <div class="col-sm-3 w3-center">

              <div class="w3-card-4 w3-pale-yellow w3-margin">
                <div class="w3-container w3-padding">

                <div align="right">
                  <a class="w3-text-green" href="{{url('admin/internal/download/'.$document->id)}}"><i class="fa fa-cloud-download fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Download"></i></a>

                  <a class="w3-text-red" href="#delete-{{$document->id}}" data-toggle="modal"><i class="fa fa-trash-o fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Delete"></i></a>
                </div>

                <hr>

                <div class="w3-center">
                <h1>
                 <i class="fa fa-file-word-o  w3-text-black"></i>
                </h1>
                </div>

                <a href="">
                <div class="w3-center w3-text-black">
                <a href="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/documents/'.$document->filename) : asset('documents/'.$document->filename)}}" target="_blank" class="w3-text-black">
                  <p>{{explode("/", $document->filename)[3]}}</p>
                </a>
                <p>Task: {{$document->task->task_name}}</p>
                </div>
                </a>

                </div>

              </div>

            </div>

            <!-- Delete MODAL -->
            <div class="modal fade" id="delete-{{$document->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
            <h4 class="modal-title w3-text-gray" id="myModalLabel">Delete Document</h4>
            </div>
            <form action="{{url('admin/docu/delete/'.$document->id)}}" enctype="multipart/form-data">
            <div class="modal-body">
              <div class="w3-container">
                <div class="row">

                  <div class="form-group">
                  <p>Are you sure you want to delete <strong>{{explode("/", $document->filename)[2]}}</strong>?</p>
                  </div>
                </div>
              </div>
            </div>

            <div class="modal-footer">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <button type="submit" class="btn btn-default w3-green w3-text-white">Yes</button>
            <button type="button" data-dismiss="modal" class="btn btn-default w3-red w3-text-white">No</button>
            </div>

            </form>
            </div>
            </div>
            </div>
          <!-- END DELETE MODAL -->

          @endforeach
        </div>
        @endif

        @if(count($tobe_validated_documents) > 0)
        <div class="row w3-margin-top">
          <h4>
            <i class="fa fa-spinner fa-fw"></i>
            <strong>To be Validated</strong>
          </h4>
        </div>

          <div class="row w3-margin-top">
          @foreach($tobe_validated_documents as $document)
            <div class="col-sm-3 w3-center">

              <div class="w3-card-4 w3-pale-yellow w3-margin">
                <div class="w3-container w3-padding">

                <div align="right">
                  <a class="w3-text-green" href="{{url('admin/internal/download/'.$document->id)}}"><i class="fa fa-cloud-download fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Download"></i></a>
                </div>

                <hr>

                <div class="w3-center">
                <h1>
                 <i class="fa fa-file-word-o  w3-text-black"></i>
                </h1>
                </div>

                <a href="">
                <div class="w3-center w3-text-black">
                <a href="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/documents/'.$document->filename) : asset('documents/'.$document->filename)}}" target="_blank" class="w3-text-black">
                  <p>{{explode("/", $document->filename)[3]}}</p>
                </a>
                <p>Task: {{$document->task->task_name}}</p>
                </div>
                </a>

                </div>

              </div>

            </div>

          @endforeach
        </div>
        @endif


        <div class="row">
          <h4><i class="fa fa-file-word-o fa-fw w3-xlarge"></i><strong> Documents</strong></h4>
        </div>
        <div class="row">
          @foreach($task_docus as $document)
            <div class="col-sm-3 w3-center">
              <div class="w3-card-4 w3-pale-yellow w3-margin">
                <div class="w3-container w3-padding">
                <div class="w3-center">
                <div align="right" class="w3-margin-bottom">
                <p>
                  <a href="{{url('admin/docu/download/'.$document->id)}}">
                    <i class="fa fa-cloud-download w3-text-green fa-fw w3-large w3-hover-opacity"></i>
                  </a>
                </p>
                </div>

                <hr>

                <h1>
                 <i class="fa fa-file-word-o  w3-text-black"></i>
                </h1>
                </div>

                <div class="w3-center">
                <a href="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/documents/'.$document->filename) : asset('documents/'.$document->filename)}}" target="_blank" class="w3-text-black">
                  <p>{{explode("/", $document->filename)[2]}}</p>
                </a>
                <p>Task Name: {{$document->task->id}} - {{$document->task->task_name}}</p>
                <p>Category: {{$document->category->category_name}}</p>

                </div>

                </div>

              </div>
            </div>


          @endforeach

        </div>
    </div>



    <div id="validation" class="tab-pane fade in">
      <div class="row w3-margin-top">
        <div class="table-responsive">
            <table class="table table-bordered table-hover" id="validationTable">
              <thead>
                <tr>
                 <th class="w3-center">Task Name</th>
                 <th class="w3-center">Description</th>
                 <th class="w3-center">Due Date</th>
                 <th class="w3-center">Status</th>
                 <th class="w3-center">Action</th>
                </tr>
              </thead>
              <tbody class="w3-text-gray">
              @foreach($validate_tasks as $task)
                <tr>
                  <td class="w3-center">{{$task->task_name}}</td>
                  <td class="w3-center">{{$task->task_desc}}</td>
                  <td class="w3-center">{{$task->due_date}}</td>
                  <td class="w3-center">{{$task->task_status}}</td>
                  <td class="w3-center">
                    @if($task->task_status == 'for validation')
                    <a class="w3-text-green" href="{{url('admin/docu/manage-event/approve/' . $task->id)}}"><i class="fa fa-thumbs-up fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Approve"></i></a>

                    <a class="w3-text-red" href="#disapprove-{{$task->id}}" data-toggle="modal"><i class="fa fa-thumbs-down fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Disapprove"></i></a>
                    @else
                    <i class="fa fa-spinner fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Pending"></i>
                    @endif

                  </td>

                </tr>

                <!-- DISAPPROVE MODAL -->
                <div class="modal fade" id="disapprove-{{$task->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title w3-text-gray" id="myModalLabel">Disapproval</h4>
                </div>
                <form action="{{url('/admin/docu/manage-event/disapprove/' . $task->id)}}" method='post' enctype="multipart/form-data">
                <div class="modal-body">
                  <div class="w3-container">
                    <div class="row">

                      <div class="form-group">
                        <label>Reason:</label>
                        <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="reason" rows="5" placeholder="Reason for Disapproval"></textarea>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="modal-footer">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <button type="submit" class="btn btn-default w3-green w3-text-white"><i class="fa fa-reply fa-fw"></i> Submit</button>
                </div>

                </form>
                </div>
                </div>
                </div>
              <!-- END DISAPPROVE MODAL -->
              @endforeach
             </tbody>
            </table>
        </div>
      </div>

      @if(count($validate_documents) > 0)
      <div class="row w3-margin-top">
        <h4>
          <strong><i class="fa fa-spinner fa-fw"></i> For Validation</strong>
        </h4>
      </div>

       <div class="row w3-margin-top">
        @foreach($validate_documents as $document)
          <div class="col-sm-3 w3-center">

            <div class="w3-card-4 w3-white w3-margin">
              <div class="w3-container w3-padding">

              <div align="right">
                <a class="w3-text-green" href="{{url('admin/internal/download/'.$document->id)}}"><i class="fa fa-cloud-download fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Download"></i></a>
              </div>

              <hr>

              <div class="w3-center">
              <h1>
               <i class="fa fa-file-word-o  w3-text-black"></i>
              </h1>
              </div>

              <a href="">
              <div class="w3-center w3-text-black">
              <a href="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/documents/'.$document->filename) : asset('documents/'.$document->filename)}}" target="_blank" class="w3-text-black">
                <p>{{explode("/", $document->filename)[3]}}</p>
              </a>
              <p>Category: {{$document->category->category_name}}</p>
              </div>
              </a>

              </div>

            </div>

          </div>

        @endforeach
      </div>
      @endif
    </div>

    <div id="eventInfo" class="tab-pane fade in">
      <div class="row w3-margin-top">
        <p>
          <a href="{{ url('admin/docu/manage-event/'.$event->id.'/gallery') }}" class="w3-text-gray"><i class="fa fa-file-image-o fa fw w3-large"></i> Manage Gallery</a>

          <i class="fa fa-ellipsis-v fa-fw"></i>

         <!--  <a href="{{ url('admin/docu/manage-event/'.$event->id.'/attendance') }}" class="w3-text-gray"><i class="fa fa-calendar-check-o fa fw w3-large"></i> Manage Attendance</a>

          <i class="fa fa-ellipsis-v fa-fw"></i>
 -->
          <!-- <a href="{{ url('admin/docu/manage-event/'.$event->id.'/meeting') }}" class="w3-text-gray"><i class="fa fa-users fa fw w3-large"></i> Manage Meeting</a>

          <i class="fa fa-ellipsis-v fa-fw"></i> -->

          @if($event->theme_image == null)
          <a href="#theme" class="w3-text-red" data-toggle="modal" title="Please upload Event Theme/Banner"><i class="fa fa-flag-o fa fw w3-large"></i> Manage Theme</a>
          @else
          <a href="#theme" class="w3-text-gray" data-toggle="modal"><i class="fa fa-flag-o fa fw w3-large"></i> Manage Theme</a>
          @endif
        </p>
      </div>

      <!-- MANAGE EVENT INFORMATION --><!-- MANAGE EVENT INFORMATION --><!-- MANAGE EVENT INFORMATION -->

      <script src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/js/tinymce/js/tinymce/tinymce.min.js') : asset('js/tinymce/js/tinymce/tinymce.min.js')}}"></script>
      <script>tinymce.init({selector:'.texthere', plugins: "autoresize"});</script>

      <div class="w3-card-4 w3-white w3-container w3-padding w3-margin">
      <br>
      <form action="{{ url('admin/docu/manage-event/'.$event->id.'/edit') }}" method="post" enctype="multipart/form-data">

        <div class="form-group" align="right">
           <button type="submit" class="btn btn-default w3-green"><i class="fa fa-pencil-square-o fa-fw w3-large"></i> Publish</button>
        </div>

        <hr>

        <div class="row">
          <div class="form-group">
            <div class="col-lg-8 col-md-8">
              @if($event->post_status == 'posted')
                <strong>

                  <input type="text" name="title" id="title" class="form-control no-border input-lg w3-text-black" placeholder="Title" value="{{$event->event_title}}" required>
                </strong>
              @else
                <strong>

                  <input type="text" name="title" id="title" class="form-control no-border input-lg w3-text-black" placeholder="Title" value="{{$event->event_title}}" required>
                </strong>
              @endif

            </div>

          </div>

        </div>

        <hr>

        <div class="form-group">
          <textarea rows="2" placeholder="what's on your mind?" name="body" class="form-control texthere">{{$event->description}}</textarea>
        </div>
        <!-- <div class="form-group">
         <input type="file" id="image" name="image" accept="image/*" >
        </div> -->
        <hr>

        <input type="hidden" name="_token" value="{{ csrf_token() }}">
      </form>

    </div>


    <!-- Set Theme modal -->
    <div class="modal fade" id="theme" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
            <h4 class="modal-title w3-text-gray" id="myModalLabel">Theme</h4>
          </div>
        <form action="{{ url('admin/docu/manage-event/'.$event->id.'/upload-theme') }}" method="POST" enctype="multipart/form-data">
          <div class="modal-body">

          <input type="file" name="theme" id="upload">

            <div class="modal-footer">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <button type="submit" class="btn btn-default w3-red"><i class="fa fa-plus-square fa-fw"></i> Yes</button>
            </div>

          </div>
        </form>
        </div>
      </div>
    </div>
  <!-- End set theme modal -->
    </div>
  </div>
</div>

<script type="text/javascript">
  function showFileName( event, id ) {

    $(".filename-" + id).show()
    // the change event gives us the input it occurred in
    var input = event.srcElement;

    // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
    var fileName = input.files[0].name;

    // use fileName however fits your app best, i.e. add it into a div
    $('#import-file-name-' + id).html(fileName);
  }

</script>

<script type="text/javascript">
$(document).ready( function () {
  $('#taskTable').DataTable();
  $('#validationTable').DataTable();
} );
</script>

@endsection
