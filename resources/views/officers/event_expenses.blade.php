@extends('admin/layout/admin_panel')

@section('middle')

<b><h3>EVENT BUDGET: {{$event_budget}} </h3></b>

<button class="btn btn-default w3-orange w3-text-white" data-toggle="modal" data-target="#expenses" title="Edit"><i class="fa fa-pencil"></i>Add Expenses</button>
<br><br><br>

@foreach($event_expenses as $event_expense)
Expenses id: {{$event_expense->id}}<br>
Expenses Name: {{$event_expense->expense_name}}<br>
Expenses desc: {{$event_expense->expense_desc}}<br>
Expenses amt: {{$event_expense->expense_amt}}<br><br>
@endforeach

 <div class="modal fade" id="expenses" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
            <h4 class="modal-title w3-text-gray" id="myModalLabel">Add Expenses</h4>
          </div>
      <form action="{{ url('admin/finance/event-fund/'.$event->id.'/expenses/store') }}" method="POST">
          <div class="modal-body">

      		  <select name="role_id" class="form-control">
              @foreach($roles as $role)
                  <option value="{{$role->id}}">{{$role->name}}</option>
              @endforeach
              </select>
                <select name="breakdown_id" class="form-control">
              @foreach($breakdowns as $breakdown)
                  <option value="{{$breakdown->id}}">{{$breakdown->breakdown_name}}</option>
              @endforeach
              </select>

               <input type="text" name="expense_name" id="expense_name" tabindex="1" class="form-control" placeholder="Expense Name">

              <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="expense_desc" placeholder="Expense Desc" rows="3"></textarea>
              
              <input type="number" name="expense_amt" id="expense_amt" tabindex="1" class="form-control" placeholder="Amount"><br><br>


            <div class="modal-footer">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <button type="submit" class="btn btn-default w3-red"><i class="fa fa-plus-square fa-fw"></i> Yes</button>
            </div>

          </div>
        </form>
        </div>
      </div>
    </div>
@endsection