@extends('layout/control_panel')

@section('title')
MTICS By-laws
@endsection

@section('middle')
<div class="w3-container w3-margin">
    <div class="row">
        <h3>
          <i class="fa fa-book fa-fw w3-xxlarge"></i>
          <strong>MTICS By-laws</strong>
        </h3>
    </div>

    <hr>

    <div class="row">
        <p>
          <a href="#updateBylaws" data-toggle="modal" class="w3-text-gray" style="outline: 0"><i class="fa fa-file-pdf-o fa-fw"></i> Update Mtics By-laws</a>
        </p>
    </div>

    <div class="modal fade" id="updateBylaws" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
          <h4 class="modal-title w3-text-gray" id="myModalLabel">Update MTICS By-laws</h4>
        </div>
        <form action="{{url('admin/docu/bylaws/update')}}" method="POST" enctype="multipart/form-data">
          <div class="modal-body">
            <div class="w3-container">
              <div class="row">
                <div class="form-group">
                  <label id="import-file-name" for="import_file" style="cursor: pointer;"><i class="fa fa-file-pdf-o fa-fw w3-large"></i> Choose a File</label>
                  <input type="file" name="import_file" id="import_file" style="opacity: 0; position: absolute; z-index: -1;" />
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <button type="submit" class="btn btn-default w3-green"><i class="fa fa-cloud-upload fa-fw"></i> Upload</button>
          </div>
        </form>
      </div>
    </div>
    </div>

    <!-- END -->


  <div class="row">
    <div class="col-sm-12 col-lg-4 col-md-4"></div>
    <div class="col-sm-12 col-lg-4 col-md-4">
        <a href="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? url('public/documents/bylaws/mtics_bylaws.pdf') : url('documents/bylaws/mtics_bylaws.pdf')}}" target="_blank" class="w3-text-white">
            <div class="w3-card-4 w3-deep-orange w3-padding w3-margin">
                <div class="w3-container" align="center">
                    <div class="row">
                        <i class="fa fa-file-pdf-o fa-fw w3-xxxlarge"></i>
                    </div>
                    <hr>
                    <div class="row">
                        <h4><strong>MTICS By-laws</strong></h4>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-12 col-lg-4 col-md-4"></div>
  </div>
</div>

<script>
  var input = document.getElementById( 'import_file' );
  var infoArea = document.getElementById( 'import-file-name' );

  input.addEventListener('change', showFileName);

  function showFileName( event ) {

    // the change event gives us the input it occurred in
    var input = event.srcElement;

    // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
    var fileName = input.files[0].name;

    // use fileName however fits your app best, i.e. add it into a div
    infoArea.textContent = fileName;
  };

   window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove();
    });
}, 4000);
</script>
@endsection
