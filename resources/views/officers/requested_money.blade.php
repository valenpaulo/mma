@extends('layout/control_panel')

@section('title')
Budget Request
@endsection

@section('middle')

<div class="container-fluid w3-margin">
  <div class="row">
    <h3>
      <i class="fa fa-paper-plane-o fa-fw w3-xxlarge"></i>
      <strong>Money Request</strong>
    </h3>
  </div>

  <hr>

  <div class="row w3-margin-top">

    <div class="col-lg-3 col-md-3 col-sm-12"></div>

    <div class="col-lg-3 col-md-3 col-sm-12">
      <div class="w3-card-4 w3-purple w3-text-black w3-padding w3-container">

        <div class="w3-container">
          <div class="row" align="center">
            <h4>
              <i class="fa fa-shopping-bag w3-xxxlarge fa-fw"></i>
              <strong>MTICS Budget</strong>
            </h4>
          </div>
          <hr style="border-color: black">
          <div class="row">
            <h4>
              <span>
                <i class="fa fa-ellipsis-v fa-fw"></i>
                <strong>{{$mticsBudget > 0 ? $mticsBudget : 0}} pesos</strong>
              </span>
            </h4>
          </div>
        </div>

      </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-12">
      <div class="w3-card-4 w3-light-green w3-text-black w3-padding">

        <div class="w3-container">
          <div class="row" align="center">
            <h4>
              <i class="fa fa-money w3-xxxlarge fa-fw"></i>
              <strong>Task Budget</strong>
            </h4>
          </div>

          <hr style="border-color: black">

          <div class="row">
            <h4>
              <span>
                <i class="fa fa-ellipsis-v fa-fw"></i>
                <strong>{{$externalBudget}} pesos</strong>
              </span>
            </h4>
          </div>

        </div>

      </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-12"></div>

  </div>

  <div class="row w3-margin-top">
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#request" class="w3-text-black">Requests @if(count($moneyrequests) > 0)<span class="w3-badge w3-red">{{count($moneyrequests)}}</span>@endif</a></li>

      <li><a data-toggle="tab" href="#validation" class="w3-text-black">Validation @if(count($requestValidation) > 0)<span class="w3-badge w3-red">{{count($requestValidation)}}</span>@endif</a></li>

      <li><a data-toggle="tab" href="#transaction" class="w3-text-black">Transactions</a></li>
    </ul>
  </div>

    <div class="tab-content">

      <!-- start of request -->
      <div id="request" class="tab-pane fade in active">
        <div class="row w3-margin-top">
          <div class="table-responsive">
              <table class="table table-bordered table-hover" id="moneyRequestTable">
                <thead>
                  <tr>
                   <th class="w3-center">Task</th>
                   <th class="w3-center">Items</th>
                   <th class="w3-center">Requested Money</th>
                   <th class="w3-center">Status</th>
                   <th class="w3-center">Miscellaneous</th>
                   <th class="w3-center">Action</th>
                  </tr>
                </thead>
                <tbody class="w3-text-gray">
                @foreach($moneyrequests as $request)
                  <tr>
                    <td class="w3-center">{{$request->task->task_name}}</td>
                    <td class="w3-center">
                      @foreach($request->task->mticspurchaselist as $item)
                        {{$item->mtics_itemname}}
                        <a href="#editEstPrice-{{$item->id}}" class="w3-text-orange" data-toggle="modal"><i class="fa fa-pencil" data-toggle="tooltip" title="Edit Estimated Price"></i></a>
                        <br>
                        <!-- EDIT ESTIMATED PRICE MODAL -->
                        <div class="modal fade" id="editEstPrice-{{$item->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel" align="left">
                        <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                        <h4 class="modal-title w3-text-gray" id="myModalLabel">Edit Estimated Price</h4>
                        </div>
                        <form action="{{ url('admin/finance/requested-money/'.$item->id.'/edit/'.$request->task_id) }}" method="POST" enctype="multipart/form-data">
                        <div class="modal-body">
                          <div class="w3-container">
                            <div class="row">
                              <div class="form-group">
                                <label>Item Name:</label>
                                <input type="text" class="form-control" value="{{$item->mtics_itemname}}" disabled>
                              </div>

                              <div class="form-group">
                                <label>Quantity:</label>
                                <input type="text" class="form-control" value="{{$item->mtics_orig_quan}}" disabled>
                              </div>

                              <div class="form-group">
                                <label>Estimated Price: (Per PCS) </label>
                                <input type="number" name="est_price" id="est_price" tabindex="1" class="form-control" placeholder="Estimated Amount" value="{{$item->mtics_est_price}}">
                              </div>


                            </div>
                          </div>
                        </div>

                        <div class="modal-footer">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                          <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
                        </div>

                        </form>
                        </div>
                        </div>
                        </div>
                        <!-- END MODAL -->
                      @endforeach
                    </td>
                    <td class="w3-center">{{$request->amount}}</td>
                    <td class="w3-center">{{$request->mon_req_status}}</td>
                    <td class="w3-center">
                      <a class="w3-text-red" href="#fare-{{$request->id}}" data-toggle="modal"><i class="fa fa-automobile fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Add Fare Budget"></i></a>

                      <a class="w3-text-orange" href="#meal-{{$request->id}}" data-toggle="modal"><i class="fa fa-cutlery fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Add Meal Budget"></i></a>
                    </td>
                    <td class="w3-center">
                      <a class="w3-text-green" href="javascript:void(0)" onclick="approveRequest({{$request->task_id}})"><i class="fa fa-thumbs-up fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Accept Request"></i></a>

                      <a class="w3-text-red" href="#denial-{{$request->id}}" data-toggle="modal"><i class="fa fa-thumbs-down fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Deny Request"></i></a>

                      <form action="{{ url('admin/finance/requested-money/'.$request->task_id.'/accept') }}" method="POST" enctype="multipart/form-data" id="formApprove-{{$request->task_id}}">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      </form>


                    </td>
                  </tr>

                  <!-- FARE MODAL -->
                  <div class="modal fade" id="fare-{{$request->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-sm" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Add Fare Budget</h4>
                  </div>
                  <form action="{{ url('admin/finance/requested-money/'.$request->task_id.'/add-fare') }}" method="POST" enctype="multipart/form-data">
                  <div class="modal-body">
                    <div class="w3-container">
                      <div class="row">

                        <div class="form-group">
                          <label>Estimated Fare Amount</label>
                          <input type="number" class="form-control" placeholder="Estimated Fare Amount" name="est_fare">
                        </div>

                      </div>
                    </div>
                  </div>

                  <div class="modal-footer">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="btn btn-default w3-green"><i class="fa fa-plus-square fa-fw"></i> Add</button>
                  </div>

                  </form>
                  </div>
                  </div>
                  </div>
                  <!-- END MODAL -->

                  <!-- MEAL MODAL -->
                  <div class="modal fade" id="meal-{{$request->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-sm" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Add Meal Budget</h4>
                  </div>
                  <form action="{{ url('admin/finance/requested-money/'.$request->task_id.'/add-meal') }}" method="POST" enctype="multipart/form-data">
                  <div class="modal-body">
                    <div class="w3-container">
                      <div class="row">

                        <div class="form-group">
                          <label>Estimated Meal Amount</label>
                          <input type="number" class="form-control" placeholder="Estimated Meal Amount" name="est_meal">
                        </div>

                      </div>
                    </div>
                  </div>

                  <div class="modal-footer">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="btn btn-default w3-green"><i class="fa fa-plus-square fa-fw"></i> Add</button>
                  </div>

                  </form>
                  </div>
                  </div>
                  </div>
                  <!-- END MODAL -->

                  <!-- DENY MODAL -->
                  <div class="modal fade" id="denial-{{$request->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-md" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Deny Request</h4>
                  </div>
                  <form action="{{ url('admin/finance/requested-money/'.$request->task_id.'/deny') }}" method="POST" enctype="multipart/form-data">
                  <div class="modal-body">
                    <div class="w3-container">
                      <div class="row">

                        <div class="form-group">
                          <label>Reason:</label>
                          <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="deny_reason" placeholder="Please indicate the reason for denying this request" rows="4"></textarea>
                        </div>

                        <div class="form-group">
                          <p>Note: Please review the request details before denying this request.</p>
                          <p>Do you want to proceed?</p>
                        </div>


                      </div>
                    </div>
                  </div>

                  <div class="modal-footer">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="btn btn-default w3-green">Yes</button>
                    <button type="button" class="btn btn-default w3-red" data-dismiss="modal">No</button>
                  </div>

                  </form>
                  </div>
                  </div>
                  </div>
                  <!-- END MODAL -->

                @endforeach
               </tbody>
              </table>
          </div>
        </div>
      <!-- end of payment_breakdown tab -->
      </div>

      <!-- start of validation -->
      <div id="validation" class="tab-pane fade">
         <div class="row w3-margin-top">
          <div class="table-responsive">
              <table class="table table-bordered table-hover" id="validationTable">
                <thead>
                  <tr>
                   <th class="w3-center">Task</th>
                   <th class="w3-center">Items</th>
                   <th class="w3-center">Requested Money</th>
                   <th class="w3-center">Purchased Amount</th>
                   <th class="w3-center">Status</th>
                   <th class="w3-center">Action</th>
                  </tr>
                </thead>
                <tbody class="w3-text-gray">
                @foreach($requestValidation as $request)
                  <tr>
                    <td class="w3-center">{{$request->task->task_name}}</td>
                    <td class="w3-center">
                      @foreach($request->task->mticspurchaselist as $item)
                        @if($loop->last)
                          {{$item->mtics_itemname}}
                        @else
                          {{$item->mtics_itemname}},
                        @endif
                      @endforeach
                    </td>
                    <td class="w3-center">{{$request->amount}}</td>
                    <td class="w3-center">{{$request->task->mticspurchaselist->sum('mtics_total_price')}}</td>
                    <td class="w3-center">{{$request->mon_req_status}}</td>
                    <td class="w3-center">
                      <a class="w3-text-green" href="{{url('admin/finance/requested-money/validate/' . $request->id)}}" data-toggle="modal"><i class="fa fa-arrow-circle-right fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="validate"></i></a>
                    </td>
                  </tr>
                @endforeach
               </tbody>
              </table>
          </div>
        </div>

      <!-- end of money request tab -->
      </div>

      <!-- start of transaction -->
      <div id="transaction" class="tab-pane fade">
        <div class="row w3-margin-top">
          <div class="table-responsive">
              <table class="table table-bordered table-hover" id="transactionTable">
                <thead>
                  <tr>
                   <th class="w3-center">Task</th>
                   <th class="w3-center">Items</th>
                   <th class="w3-center">Requested Money</th>
                   <th class="w3-center">Purchased Amount</th>
                   <th class="w3-center">Status</th>
                  </tr>
                </thead>
                <tbody class="w3-text-gray">
                @foreach($transactions as $request)
                  <tr>
                    <td class="w3-center">{{$request->task->task_name}}</td>
                    <td class="w3-center">
                      @foreach($request->task->mticspurchaselist as $item)
                        @if($loop->last)
                          {{$item->mtics_itemname}}
                        @else
                          {{$item->mtics_itemname}},
                        @endif
                      @endforeach
                    </td>
                    <td class="w3-center">{{$request->amount}}</td>
                    <td class="w3-center">{{$request->task->mticspurchaselist->sum('mtics_total_price')}}</td>
                    <td class="w3-center">{{$request->mon_req_status}}</td>
                  </tr>
                @endforeach
               </tbody>
              </table>
          </div>
        </div>
      <!-- end of validation tab -->
      </div>


</div>



<script type="text/javascript">
$(document).ready( function () {
  var moneyRequestTable = $('#moneyRequestTable').DataTable();
  $('#validationTable').DataTable();
  $('#transactionTable').DataTable();

  $(moneyRequestTable.table().container()).removeClass('form-inline');

} );
</script>

<script type="text/javascript">
function approveRequest( id ) {
  swal({
  title: "Are you sure?",
  text: "Once approved, you will not be able to undo this action!",
  icon: "warning",
  buttons: true,
  dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      $("#formApprove-" + id).submit();
    }
  });
}
</script>

@endsection
