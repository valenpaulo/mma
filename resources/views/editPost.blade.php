@extends('layout/layout')
@section('title')

{{$post->title}}

@endsection

@section('content')
<style type="text/css">
  .no-border, .no-border:focus {
    border: 0;
    box-shadow: none;
    outline: none; /* You may want to include this as bootstrap applies these styles too */
  }

  .input-lg {
    font-size: 32px
  }
</style>

<style type="text/css">
  @media only screen and (max-width: 1365px) {
    .main {
      margin-top: 60px;
    }
  }

  @media (min-width: 1366px) {
    .main {
      margin-top: 105px;
    }
  }
</style>

<script src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/js/tinymce/js/tinymce/tinymce.min.js') : asset('js/tinymce/js/tinymce/tinymce.min.js')}}"></script>
<script>
    tinymce.init({ selector:'textarea', plugins: "autoresize" });
</script>

<div class="container main">
  <div class="w3-card-4 w3-white w3-white w3-padding w3-margin">
    <div class="w3-container">
        <form action="{{ url('member/profile/post/'.$post->id.'/'.$post->slug.'/edit' ) }}" method="post" enctype="multipart/form-data">

          <div class="form-group" align="right">
            <label id="post-image-file-name" for="image" style="cursor: pointer;" class="w3-center"><i class="fa fa-file-image-o fa-fw w3-text-deep-orange w3-large w3-padding" data-toggle="tooltip" title="Upload Featured Photo" data-placement="bottom"></i></label>
            <input type="file" name="image" id="image" accept="image/*" style="opacity: 0; position: absolute; z-index: -1;" onchange="showFileName1.call(this, event)" />

            <i class="fa fa-ellipsis-v fa-fw"></i>

             <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw w3-large"></i> Save</button>
          </div>

          <div class="form-group postImageDiv" align="center">
            <img src="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$post->image) : asset('images/'.$post->image) }}" style="width: 50%;" id="postImageContainer">
          </div>

          <hr>

          <div class="row">
            <div class="form-group">
              <div class="col-lg-8 col-md-8">
                <strong>
                  <input type="text" name="title" id="title" class="form-control no-border input-lg w3-text-black" placeholder="Title" required value="{{$post->title}}">
                </strong>
              </div>

              <div class="col-lg-4 col-md-4">
                <select class="form-control" id="post_category" name="post_category">
                  <option class="w3-center"> -- Select Category -- </option>
                  @foreach($post_categories as $category)
                    @if ($post->post_category_id == $category->id)
                      <option value="{{$category->id}}" selected>{{$category->type}}</option>
                    @else
                      <option value="{{$category->id}}">{{$category->type}}</option>
                    @endif
                  @endforeach
                </select>
              </div>

            </div>

          </div>

          <hr>

          <div class="form-group">
            <textarea rows="3" placeholder="what's on your mind?" name="body" class="form-control">{{$post->body}}</textarea>
          </div>

          <input type="hidden" name="_token" value="{{ csrf_token() }}">
        </form>
    </div>
  </div>
</div>

<script>
  function showFileName1( event ) {

    var infoArea = document.getElementById( 'post-image-file-name' );
    // the change event gives us the input it occurred in
    var input = event.srcElement;

    // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
    var fileName = input.files[0].name;

    // use fileName however fits your app best, i.e. add it into a div
    $('#post-image-file-name').html("<i class='fa fa-file-image-o fa-fw w3-text-deep-orange w3-large'></i> " + fileName);

    var reader = new FileReader();
    reader.onload = function (e) {
      $("#postImageContainer").attr('src', e.target.result);
    };

    reader.readAsDataURL(input.files[0]);
  };
</script>
@endsection
