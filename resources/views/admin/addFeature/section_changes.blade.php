@extends('admin/layout/addFeature')

@section('middle')

<!-- LEGENDS -->
<div class="container">
<div class="row">
<div class="col-sm-3"></div>
<div class="col-sm-6">
<div class="w3-card-4 w3-white w3-margin">
    <div class="w3-container w3-padding w3-margin-top">
        <br>
        <p>
            <i class="fa fa-circle fa-fw w3-text-green"></i>
            <label>
            New
            </label>
            - new student from the master list who is not registered in the database (Need to Register)
        </p>

        <p>
            <i class="fa fa-circle fa-fw w3-text-purple"></i>
            <label>New Alumni Student</label>
            - alumni taking another course, ex. ladderize
        </p>

        <p>
            <i class="fa fa-circle fa-fw w3-text-blue"></i>
            <label>Transferee</label>
            - student who is transferred to this section
        </p>

        <p>
            <i class="fa fa-circle fa-fw w3-text-amber"></i>
            <label>Irregular (taking back subjects)</label>
            - student who is taking back subjects
        </p>

        <p>
            <i class="fa fa-circle fa-fw w3-text-orange"></i>
            <label>Irregular (exceeded course duration)</label>
            - student who don't graduate on time
        </p>
        

        <p>
            <i class="fa fa-circle fa-fw w3-text-red"></i>
            <label>Removed</label>
            - student who is not in the master list (will set as inactive)
        </p>
    </div>
</div>
</div>
</div>
</div>

<!-- END LEGENDS -->

<div class="container w3-margin-top w3-margin-bottom" align="center">
  <h3>
      <i class="fa fa-group fa-fw"></i> <strong>Changes in {{$section->course->course_code}} - {{$year->year_code}} Year - {{$section->section_code}} Master List</strong>
     

  </h3>
</div>

<!-- LISTS -->

<!-- NEW STUDENTS -->
<form action="{{ url('admin/manage-sections/make-changes') }}" method="POST" enctype="multipart/form-data">

<input type="hidden" name="section_id" id="section_id" tabindex="1" class="form-control" value="{{$section->id}}">
<input type="hidden" name="year_id" id="year_id" tabindex="1" class="form-control" value="{{$year->id}}">


@if(isset($NotInDB))

<div class="row">
<div class="col-sm-2"></div>
<div class="col-sm-8 w3-padding">
<div class="w3-card-6  w3-white">

    <div class="w3-container">
      <!-- Row -->
      <div class="row">

      <div class="col-sm-12">
      <h3>
          <i class="fa fa-circle fa-fw w3-text-green"></i> New Students
      </h3>
      </div>

      </div>
    </div>

     <div class="tab-content">
    <div id="courseTab" class="tab-pane fade in active">
      <div class="row w3-margin-top">
        <div class="table-responsive">
        <table class="table table-bordered table-responsive">
          <thead class="w3-teal">
            <tr>
             <th class="w3-center">Student ID</th>
             <th class="w3-center">First Name</th>
             <th class="w3-center">Last Name</th>
            </tr>
          </thead>
          <tbody class="w3-text-black">
            @foreach($NotInDB as $key => $value)
            <tr>
              <td class="w3-center">{{$key}}</td>
              <input type="hidden" name="NotInDB[{{$key}}][stud_id]" id="stud_id" tabindex="1" class="form-control" placeholder="student id"  value="{{$key}}">
              
              <td class="w3-center">{{$value['first_name']}}</td>

              <input type="hidden" name="NotInDB[{{$key}}][first_name]" id="first_name" tabindex="1" class="form-control" value="{{$value['first_name']}}">

              <td class="w3-center">{{$value['last_name']}}</td>
              
              <input type="hidden" name="NotInDB[{{$key}}][last_name]" id="last_name" tabindex="1" class="form-control" value="{{$value['last_name']}}">
            </tr>
            @endforeach
          </tbody>
        </table>
    </div>
    </div>

</div>
</div>
</div>
</div>
</div>

@endif

<!-- END NEW STUDENTS -->

<!-- TRANSFERRED STUDENTS -->
@if(isset($changes))
<div class="row">
<div class="col-sm-3"></div>
<div class="col-sm-6 w3-padding">
<div class="w3-card-6 w3-white">

<div class="w3-container">
  <!-- Row -->
  <div class="row">

  <div class="col-sm-12">
  <h3>
      <i class="fa fa-circle fa-fw w3-text-blue"></i> Transferee Students
  </h3>
  </div>

  </div>
</div>

<div class="tab-content">
    <div id="courseTab" class="tab-pane fade in active">
      <div class="row w3-margin-top">
        <div class="table-responsive">    <table class="table table-bordered table-responsive">
      <thead class="w3-teal">
        <tr>
         <th class="w3-center">Student ID</th>
         <th class="w3-center">First Name</th>
         <th class="w3-center">Last Name</th>
         <th class="w3-center">From</th>
         <th class="w3-center">Moved To</th>

        </tr>
      </thead>
      <tbody class="w3-text-black">
        @foreach($changes as $key => $value)
        <tr>
          <td class="w3-center">{{$value['stud_id']}}</td>
            <input type="hidden" name="changes[{{$value['stud_id']}}][stud_id]" id="stud_id" tabindex="1" class="form-control" placeholder="student id"  value="{{$value['stud_id']}}">

          <td class="w3-center">{{$value['first_name']}}</td>
            <input type="hidden" name="changes[{{$value['stud_id']}}][first_name]" id="first_name" tabindex="1" class="form-control" placeholder="First Name"  value="{{$value['first_name']}}">

          <td class="w3-center">{{$value['last_name']}}</td>
            <input type="hidden" name="changes[{{$value['stud_id']}}][last_name]" id="last_name" tabindex="1" class="form-control" placeholder="Last Name"  value="{{$value['last_name']}}">

          <td class="w3-center">{{$value['prev']}}</td>
            <input type="hidden" name="" id="last_name" tabindex="1" class="form-control" placeholder="Last Name"  value="{{$value['prev']}}">

          <td class="w3-center">{{$value['cur']}}</td>
            <input type="hidden" name="changes[{{$value['stud_id']}}][section]" id="last_name" tabindex="1" class="form-control" placeholder="Last Name"  value="{{$value['cur']}}">

        </tr>
        @endforeach
      </tbody>
    </table>
</div>
</div>

</div>
</div>
</div>
</div>
</div>
@endif

<!-- END TRANSFERRED STUDENTS -->

<!-- Alumni new student -->
@if(isset($irreg_alumni))
<div class="row">
<div class="col-sm-3"></div>
<div class="col-sm-6 w3-padding">
<div class="w3-card-6 w3-white">

<div class="container">
  <!-- Row -->
  <div class="row">

  <div class="col-sm-12">
  <h3>
      <i class="fa fa-circle fa-fw w3-text-purple"></i> New Alumni Student
  </h3>
  </div>


  </div>
</div>

<div class="tab-content">
    <div id="courseTab" class="tab-pane fade in active">
      <div class="row w3-margin-top">
        <div class="table-responsive">
          <table class="table table-bordered table-responsive">
      <thead class="w3-teal">
        <tr>
         <th class="w3-center">Student ID</th>
         <th class="w3-center">First Name</th>
         <th class="w3-center">Last Name</th>
         <th class="w3-center">Student Type</th>
        </tr>
      </thead>
      <tbody class="w3-text-black">
        @foreach($irreg_alumni as $key => $value)
        @if($value['type'] == 'Regular')
        <tr>
          <td class="w3-center">{{$value['stud_id']}}</td>
            <input type="hidden" name="alumni[{{$value['stud_id']}}][stud_id]" id="stud_id" tabindex="1" class="form-control" placeholder="student id"  value="{{$value['stud_id']}}">

          <td class="w3-center">{{$value['first_name']}}</td>
            <input type="hidden" name="alumni[{{$value['stud_id']}}][first_name]" id="first_name" tabindex="1" class="form-control" placeholder="First Name"  value="{{$value['first_name']}}">

          <td class="w3-center">{{$value['last_name']}}</td>
            <input type="hidden" name="alumni[{{$value['stud_id']}}][last_name]" id="last_name" tabindex="1" class="form-control" placeholder="Last Name"  value="{{$value['last_name']}}">

          <td class="w3-center">{{$value['type']}}</td>
            <input type="hidden" name="alumni[{{$value['stud_id']}}][type]" id="last_name" tabindex="1" class="form-control" placeholder="Last Name"  value="{{$value['type']}}">

          <input type="hidden" name="alumni[{{$value['stud_id']}}][year]" id="last_name" tabindex="1" class="form-control" placeholder="Last Name"  value="{{$value['year_id']}}">
          <input type="hidden" name="alumni[{{$value['stud_id']}}][section]" id="last_name" tabindex="1" class="form-control" placeholder="Last Name"  value="{{$value['section_id']}}">
        </tr>
        @endif
        @endforeach
      </tbody>
    </table>
</div>
</div>

</div>
</div>
</div>
</div>
</div>
@endif

<!-- END IRREG STUDENTS (exceeded student) -->

<!-- IRREGULAR STUDENTS (TAKING BACK SUBjECTS) -->
@if(isset($irreg))
<div class="row">
<div class="col-sm-3"></div>
<div class="col-sm-6 w3-padding">
<div class="w3-card-6 w3-white">

<div class="container">
  <!-- Row -->
  <div class="row">

  <div class="col-sm-12">
  <h3>
      <i class="fa fa-circle fa-fw w3-text-amber"></i> Irregular Students (taking back subjects)
  </h3>
  </div>

  </div>
</div>

<div class="tab-content">
    <div id="courseTab" class="tab-pane fade in active">
      <div class="row w3-margin-top">
        <div class="table-responsive">
            <table class="table table-bordered table-responsive">
      <thead class="w3-teal">
        <tr>
         <th class="w3-center">Student ID</th>
         <th class="w3-center">First Name</th>
         <th class="w3-center">Last Name</th>
         <th class="w3-center">Year</th>
         <th class="w3-center">Section</th>

        </tr>
      </thead>
      <tbody class="w3-text-black">
        @foreach($irreg as $key => $value)
        <tr>
          <td class="w3-center">{{$value['id_num']}}</td>
            <input type="hidden" name="irreg[{{$value['id_num']}}][stud_id]" id="stud_id" tabindex="1" class="form-control" placeholder="student id"  value="{{$value['id_num']}}">

          <td class="w3-center">{{$value['first_name']}}</td>
            <input type="hidden" name="irreg[{{$value['id_num']}}][first_name]" id="first_name" tabindex="1" class="form-control" placeholder="First Name"  value="{{$value['first_name']}}">

          <td class="w3-center">{{$value['last_name']}}</td>
            <input type="hidden" name="irreg[{{$value['id_num']}}][last_name]" id="last_name" tabindex="1" class="form-control" placeholder="Last Name"  value="{{$value['last_name']}}">

          <td class="w3-center">{{$value['year']}}</td>
            <input type="hidden" name="irreg[{{$value['id_num']}}][year]" id="last_name" tabindex="1" class="form-control" placeholder="Last Name"  value="{{$value['year']}}">

          <td class="w3-center">{{$value['section']}}</td>
            <input type="hidden" name="irreg[{{$value['id_num']}}][section]" id="last_name" tabindex="1" class="form-control" placeholder="Last Name"  value="{{$value['section']}}">

        </tr>
        @endforeach
      </tbody>
    </table>
</div>
</div>

</div>
</div>
</div>
</div>
</div>

@endif

<!-- END IRREG STUDENTS (TAKING BACK SUBJECTS) -->

<!-- IRREGULAR STUDENTS (TAKING BACK SUBjECTS) -->
@if(isset($irreg_alumni))
<div class="row">
<div class="col-sm-3"></div>
<div class="col-sm-6 w3-padding">
<div class="w3-card-6 w3-white">

<div class="container">
  <!-- Row -->
  <div class="row">

  <div class="col-sm-12">
  <h3>
      <i class="fa fa-circle fa-fw w3-text-orange"></i> Irregular Students (exceeded course duration)
  </h3>
  </div>

  </div>
</div>

<div class="tab-content">
    <div id="courseTab" class="tab-pane fade in active">
      <div class="row w3-margin-top">
        <div class="table-responsive">
    <table class="table table-bordered table-responsive">
      <thead class="w3-teal">
        <tr>
         <th class="w3-center">Student ID</th>
         <th class="w3-center">First Name</th>
         <th class="w3-center">Last Name</th>
         <th class="w3-center">Student Type</th>
        </tr>
      </thead>
      <tbody class="w3-text-black">
        @foreach($irreg_alumni as $key => $value)
        @if($value['type'] == 'Irregular')
        <tr>
          <td class="w3-center">{{$value['stud_id']}}</td>
            <input type="hidden" name="alumni[{{$value['stud_id']}}][stud_id]" id="stud_id" tabindex="1" class="form-control" placeholder="student id"  value="{{$value['stud_id']}}">

          <td class="w3-center">{{$value['first_name']}}</td>
            <input type="hidden" name="alumni[{{$value['stud_id']}}][first_name]" id="first_name" tabindex="1" class="form-control" placeholder="First Name"  value="{{$value['first_name']}}">

          <td class="w3-center">{{$value['last_name']}}</td>
            <input type="hidden" name="alumni[{{$value['stud_id']}}][last_name]" id="last_name" tabindex="1" class="form-control" placeholder="Last Name"  value="{{$value['last_name']}}">

          <td class="w3-center">{{$value['type']}}</td>
            <input type="hidden" name="alumni[{{$value['stud_id']}}][type]" id="last_name" tabindex="1" class="form-control" placeholder="Last Name"  value="{{$value['type']}}">

          <input type="hidden" name="alumni[{{$value['stud_id']}}][year]" id="last_name" tabindex="1" class="form-control" placeholder="Last Name"  value="{{$value['year_id']}}">
          <input type="hidden" name="alumni[{{$value['stud_id']}}][section]" id="last_name" tabindex="1" class="form-control" placeholder="Last Name"  value="{{$value['section_id']}}">
        </tr>
        @endif
        @endforeach
      </tbody>
    </table>
</div>
</div>

</div>
</div>
</div>
</div>
</div>
@endif

<!-- END IRREG STUDENTS (exceeded student) -->

<!-- IRREGULAR STUDENTS (TAKING BACK SUBjECTS) -->
@if(isset($NotInList))
<div class="row">
<div class="col-sm-3"></div>
<div class="col-sm-6 w3-padding">
<div class="w3-card-6 w3-white">

<div class="container">
  <!-- Row -->
  <div class="row">

  <div class="col-sm-12">
  <h3>
      <i class="fa fa-circle fa-fw w3-text-red"></i> Removed From Masterlist (This will set as <strong>'Inactive'</strong>)
  </h3>
  </div>

  </div>
</div>

<div class="tab-content">
    <div id="courseTab" class="tab-pane fade in active">
      <div class="row w3-margin-top">
        <div class="table-responsive">
    <table class="table table-bordered table-responsive">
      <thead class="w3-teal">
        <tr>
         <th class="w3-center">Student ID</th>
         <th class="w3-center">First Name</th>
         <th class="w3-center">Last Name</th>
        </tr>
      </thead>
      <tbody class="w3-text-black">
        @foreach($NotInList as $key => $value)
        <tr>
          <td class="w3-center">{{$value['stud_id']}}</td>
            <input type="hidden" name="NotInList[{{$value['stud_id']}}][stud_id]" id="stud_id" tabindex="1" class="form-control" placeholder="student id"  value="{{$value['stud_id']}}">

          <td class="w3-center">{{$value['first_name']}}</td>
            <input type="hidden" name="NotInList[{{$value['stud_id']}}][first_name]" id="first_name" tabindex="1" class="form-control" placeholder="First Name"  value="{{$value['first_name']}}">

          <td class="w3-center">{{$value['last_name']}}</td>
            <input type="hidden" name="NotInList[{{$value['stud_id']}}][last_name]" id="last_name" tabindex="1" class="form-control" placeholder="Last Name"  value="{{$value['last_name']}}">

        </tr>

        @endforeach
      </tbody>
    </table>
</div>
</div>

</div>
</div>
</div>
</div>
</div>
@endif

</div>

<hr style="border: 0.5px solid gray ">

<div class="container-fluid w3-margin-bottom" align="center">
<button type="submit" class="btn btn-default w3-green w3-text-white"><i class="fa fa-pencil"></i> Save Changes</button>
<input type="hidden" name="_token" value="{{ csrf_token() }}">
</div>
</form>

<!-- END IRREG STUDENTS (exceeded student) -->

<!-- END OF LISTS -->

<script type="text/javascript">
  window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove();
    });
}, 4000);
</script>

@endsection
