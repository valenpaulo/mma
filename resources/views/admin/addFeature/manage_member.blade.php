@extends('layout/control_panel')

@section('title')
Manage Member
@endsection

@section('middle')
<div class="w3-container w3-margin">

  <div class="row">
    <h3>
      <i class="fa fa-user-circle-o fa-fw w3-xxlarge"></i>
      <strong>Manage Member</strong>
    </h3>
  </div>

  <hr>

  <div class="row">
    <p>
      <a href="#add" data-toggle="modal" class="w3-text-gray" style="outline: 0"><i class="fa fa-plus-square-o fa-fw"></i> Add</a>
      <i class="fa fa-ellipsis-v fa-fw"></i>
      <a href="#importExcel" data-toggle="modal" class="w3-text-gray" style="outline: 0"><i class="fa fa-file-excel-o fa-fw"></i> Import</a>
      <i class="fa fa-ellipsis-v fa-fw"></i>
      <a  href="{{url('admin/manage-members/template-download')}}" class="w3-text-gray" style="outline: 0" title="Excel Template for Import"><i class="fa fa-cloud-download fa-fw"></i> Download Excel Template</a>
    </p>
  </div>

  <!-- IMPORT FILE -->
  <div class="modal fade" id="importExcel" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
          <h4 class="modal-title w3-text-gray" id="myModalLabel">Import File</h4>
        </div>
        <form action="{{url('admin/manage-members/import-excel')}}" method="POST" enctype="multipart/form-data">
          <div class="modal-body">
            <div class="w3-container">
              <div class="row">
                <div class="form-group">
                  <label id="import-file-name" for="import_file" style="cursor: pointer;"><i class="fa fa-file-excel-o fa-fw w3-large"></i> Choose a File</label>
                  <input type="file" onchange="showFileName(event)" name="import_file" id="import_file" style="opacity: 0; position: absolute; z-index: -1;" />
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <button type="submit" class="btn btn-default w3-green"><i class="fa fa-cloud-upload fa-fw"></i> Upload</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- END IMPORT -->

  <br>

  <div class="row">
    <ul class="nav nav-tabs">
      <li class="active">
        <a data-toggle="tab" href="#student" class="w3-text-black">Student</a>
      </li>

      <li>
        <a data-toggle="tab" href="#alumni" class="w3-text-black">Alumni</a>
      </li>

      <li>
        <a data-toggle="tab" href="#faculty" class="w3-text-black">Faculty</a>
      </li>

       <li>
        <a data-toggle="tab" href="#deact" class="w3-text-black">Deactivated</a>
      </li>
    </ul>
  </div>

  <div class="tab-content">
    <div id="student" class="tab-pane fade in active">
      <div class="row w3-margin-top">
        <div class="table-responsive">
          <table class="table table-bordered table-hover" id="studentTable">
          <thead>
            <tr>
             <th class="w3-center">Username</th>
             <th class="w3-center">Avatar</th>
             <th class="w3-center">Student ID</th>
             <th class="w3-center">First Name</th>
             <th class="w3-center">Last Name</th>
             <th class="w3-center">Course</th>
             <th class="w3-center">Year</th>
             <th class="w3-center">Section</th>
             <th class="w3-center">Type</th>
             <th class="w3-center">Status</th>
             <th class="w3-center">Action</th>
            </tr>
          </thead>
          <tbody class="w3-text-gray">
            @foreach($students as $member)
            <tr>
              <td class="w3-center">{{$member->username}}</td>
              <td class="w3-center"><img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$member->avatar) : asset('images/'.$member->avatar)}}" style="width: 64px; height: 64px"></td>
              <td class="w3-center">{{$member->id_num}}</td>
              <td class="w3-center">{{$member->first_name}}</td>
              <td class="w3-center">{{$member->last_name}}</td>
              <td class="w3-center">{{$member->section->course->course_code}}</td>
              <td class="w3-center">{{$member->year->year_desc}}</td>
              <td class="w3-center">{{$member->section->section_code}}</td>
              <td class="w3-center">{{$member->type}}</td>
              <td class="w3-center">

              <form action="{{ url('admin/manage-members/'.$member->id.'/status/') }}" method="POST">
              <select class="form-control
              @if($member->status == 'deactivated') w3-red @endif
              @if($member->status == 'inactive') w3-yellow @endif
              @if($member->status == 'active') w3-green @endif
            " id="status" name="status" onchange="this.form.submit()">
            @if (isset($filter))
            @if ($filter != 'inactive')
                  <option value="active" @if($member->status == 'active') selected @endif>Active</option>
            @endif
            @else
                  <option value="active" @if($member->status == 'active') selected @endif>Active</option>
            @endif
                  <option value="inactive" @if($member->status == 'inactive') selected @endif>Inactive</option>
                  <option value="deactivated" @if($member->status == 'deactivated') selected @endif>Deactivate</option>
                </select>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </form>
              </td>
              <td class="w3-center">

                <a data-toggle="modal" href="#view-{{$member->id}}" style="outline: 0"><i class="fa fa-eye fa-fw w3-large w3-text-blue" data-toggle="tooltip" data-placement="bottom" title="View Profile"></i></a>
                <a data-toggle="modal" href="#edit-{{$member->id}}" style="outline: 0"><i class="fa fa-pencil w3-text-orange fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Edit Profile"></i></a>
                <!-- <button class="btn btn-default w3-red" data-toggle="modal" data-target="#delete-{{$member->id}}" title="Delete"><i class="fa fa-trash"></i> Deactivate</button> -->

              </td>
            </tr>

          <!-- VIEW -->
            <div class="modal fade" id="view-{{$member->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
              <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
              <h4 class="modal-title w3-text-gray" id="myModalLabel">Profile</h4>
              </div>

              <div class="modal-body">
                <div class="row">
                  <div class="form-group w3-center">
                    <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$member->avatar) : asset('images/'.$member->avatar)}}" style="width:50%">
                  </div>
                </div>

                <div class="row">
                <!-- FIRST COLUMN -->
                  <div class="col-md-6">

                    <div class="form-group">
                      <label>Student ID: </label>
                      <input type="text" name="student_id" id="student_id" tabindex="1" class="form-control" required placeholder="Student ID" value="{{$member->id_num}}">
                    </div>

                    <div class="form-group">
                      <label>First Name: </label>
                      <input type="text" name="first_name" id="first_name" tabindex="1" class="form-control" required placeholder="First Name" value="{{$member->first_name}}">
                    </div>

                    <div class="form-group">
                     <label>Last Name: </label>
                      <input type="text" name="last_name" id="last_name" tabindex="1" class="form-control" required placeholder="Last Name" value="{{$member->last_name}}">
                    </div>

                    <div class="form-group">
                     <label>Section:</label>
                      <input type="text" name="section" id="section" tabindex="1" class="form-control" required placeholder="Section" value="{{$member->section->section_code}}">
                    </div>

                    <div class="form-group">
                      <label>Username: </label>
                      <input type="text" name="username" id="username" tabindex="1" class="form-control" required placeholder="Username" value="{{$member->username}}">
                    </div>
                    <div class="form-group">
                      <label>Password: </label>
                       <input id="password" type="password" tabindex="1" class="form-control" name="password" required placeholder="Password">
                    </div>

                    <div class="form-group">
                      <label>Email: </label>
                      <input type="text" name="email" id="email" tabindex="1" class="form-control" placeholder="optional" value="{{$member->email}}">
                    </div>

                </div>
                <!-- SECOND COLUMN -->
                <div class="col-md-6">

                  <div class="form-group">
                    <label>Mobile No.: </label>
                    <input type="text" name="mobile_no" id="mobile_no" tabindex="1" class="form-control" placeholder="optional" value="{{$member->mobile_no}}">
                  </div>

                  <div class="form-group">
                   <label>Age: </label>
                    <input type="text" name="age" id="age" tabindex="1" class="form-control" placeholder="optional" value="{{$member->age}}">
                  </div>

                  <div class="form-group">
                    <label for="bday">Birthday:</label>
                    <input type="date" class="form-control" id="birthday" name="birthday" value="{{$member->birthday}}">
                  </div>

                  <div class="form-group">
                   <label>Address: </label>
                    <input type="text" name="address" id="address" tabindex="1" class="form-control" placeholder="optional" value="{{$member->address}}">
                  </div>

                  <div class="form-group">
                   <label>Work: </label>
                    <input type="text" name="work" id="work" tabindex="1" class="form-control" placeholder="optional" value="{{$member->work}}">
                  </div>

                  <div class="form-group">
                   <label>Guardian Name: </label>
                    <input type="text" name="guardian_name" id="guardian_name" tabindex="1" class="form-control" placeholder="optional" value="{{$member->guardian_name}}">
                  </div>

                  <div class="form-group">
                   <label>Guardian Mobile No.: </label>
                    <input type="text" name="guardian_mobile_no" id="guardian_mobile_no" tabindex="1" class="form-control" placeholder="optional" value="{{$member->guardian_mobile_no}}">
                  </div>

                </div>

              </div>
            </div>

            </div>
            </div>
            </div>

            <!-- EDIT MEMBER -->
            <div class="modal fade" id="edit-{{$member->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title w3-text-gray" id="myModalLabel">Profile</h4>
              </div>
              <form action="{{ url('admin/manage-members/'.$member->id.'/edit/') }}" method="POST" enctype="multipart/form-data">

              <div class="modal-body">
                <div class="row">
                  <div class="form-group w3-margin-left">
                    <label id="studentAvatarFileName-{{$member->id}}" for="student-avatar-{{$member->id}}" style="cursor: pointer;" title="Edit Picture"><i class="fa fa-file-image-o fa-fw w3-text-deep-orange w3-large w3-left"></i></label>
                    <input type="file" onchange="student( {{$member->id}} )" name="avatar" id="student-avatar-{{$member->id}}" style="opacity: 0; position: absolute; z-index: -1;" />
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  </div>
                </div>

                <div class="row">
                  <div class="form-group w3-center">
                      <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$member->avatar) : asset('images/'.$member->avatar)}}" id="studentProfileImageContainer-{{$member->id}}" style="width: 50%">
                  </div>
                  <br>
                </div>

                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>First Name: </label>
                      <input type="text" name="first_name" id="first_name" tabindex="1" class="form-control" required placeholder="First Name" value="{{$member->first_name}}">
                    </div>

                    <div class="form-group">
                     <label>Last Name: </label>
                      <input type="text" name="last_name" id="last_name" tabindex="1" class="form-control" required placeholder="Last Name" value="{{$member->last_name}}">
                    </div>

                    <div class="radio w3-center w3-padding">
                      <label><input type="radio" name="type" value="Student" onclick="studentEdit(this.value)"
                      @if ($member->section->section_code != 'Faculty' AND $member->section->section_code != 'Alumni') checked @endif> Student</label>
                      <label><input type="radio" name="type" value="Alumni" onclick="studentEdit(this.value)"
                      @if ($member->section->section_code == 'Alumni') checked @endif> Alumni</label>
                      <label><input type="radio" name="type" value="Faculty" onclick="studentEdit(this.value)"
                      @if ($member->section->section_code == 'Faculty') checked @endif> Faculty</label>
                    </div>

                  <!-- start student div -->
                  <div @if ($member->section->section_code != 'Faculty' AND $member->section->section_code != 'Alumni') style="display: block" @else style="display: none" @endif class="student-edit">
                  <div class="form-group{{ $errors->has('id_num') ? ' has-error' : '' }}">
                    <label>Student Id:</label>
                    <input type="text" class="form-control" id="id_num" name="id_num" placeholder="Student ID" value="{{$member->id_num}}">
                    @if ($errors->has('id_num'))
                      <span class="help-block">
                       <strong>{{ $errors->first('id_num') }}</strong>
                      </span>
                    @endif
                  </div>

                  <div class="form-group{{ $errors->has('year') ? ' has-error' : '' }}">
                    <label>Year:</label>
                      <select class="form-control" id="year" name="year" required placeholder="Select your year">
                       @foreach($years as $year)

                        <option value='{{$year->id}}' @if($member->year->year_desc == $year->year_desc) selected @endif>{{$year->year_desc}}</option>

                       @endforeach
                      </select>
                      @if ($errors->has('year'))
                      <span class="help-block">
                       <strong>{{ $errors->first('year') }}</strong>
                      </span>
                    @endif
                  </div>

                  <div class="form-group{{ $errors->has('section') ? ' has-error' : '' }}">
                    <label>Section:</label>
                      <select class="form-control" id="section" name="section" required placeholder="Select your Section">
                       @foreach($sections as $section)

                            <option value='{{$section->id}}' @if($member->section->section_code == $section->section_code) selected @endif>{{$section->course_code}} {{$section->section_code}}</option>

                       @endforeach
                      </select>
                      @if ($errors->has('section'))
                      <span class="help-block">
                       <strong>{{ $errors->first('section') }}</strong>
                      </span>
                    @endif
                  </div>

                  <div class="form-group{{ $errors->has('section') ? ' has-error' : '' }}">
                    <select class="form-control" id="stud_type" name="stud_type" required placeholder="Select your type">
                      @if($member->type == 'Regular')
                      <option value='Regular' selected>Regular</option>
                      <option value='Irregular'>Irregular</option>
                      @elseif($member->type == 'Irregular')
                      <option value='Irregular' selected>Irregular</option>
                      <option value='Regular' >Regular</option>
                      @endif
                    </select>
                  </div>

                  </div>

              <div class="form-group">
                    <label>Username: </label>
                      <input type="text" name="username" id="username" tabindex="1" class="form-control" required placeholder="Username" value="{{$member->username}}">
                    </div>
                    <div class="form-group">
                      <label>Password: </label>
                       <input id="password" type="password" tabindex="1" class="form-control" name="password" placeholder="Password">
                    </div>

                     <div class="form-group">
                     <label>Email: </label>
                      <input type="text" name="email" id="email" tabindex="1" class="form-control" placeholder="optional" value="{{$member->email}}">
                    </div>

              </div>

                  <div class="col-md-6">

                    <div class="form-group">
                      <label>Mobile No.: </label>
                      <input type="text" name="mobile_no" id="mobile_no" tabindex="1" class="form-control" placeholder="optional" value="{{$member->mobile_no}}">
                    </div>

                    <div class="form-group">
                     <label>Age: </label>
                      <input type="number" min="0" name="age" id="age" tabindex="1" class="form-control" placeholder="optional" value="{{$member->age}}">
                    </div>

                    <div class="form-group">
                      <label for="bday">Birthday:</label>
                      <input type="date" class="form-control" id="birthday" name="birthday" value="{{$member->birthday}}">
                    </div>

                    <div class="form-group">
                     <label>Address: </label>
                      <input type="text" name="address" id="address" tabindex="1" class="form-control" placeholder="optional" value="{{$member->address}}">
                    </div>

                    <div class="form-group">
                     <label>Work: </label>
                      <input type="text" name="work" id="work" tabindex="1" class="form-control" placeholder="optional" value="{{$member->work}}">
                    </div>

                    <div class="form-group">
                     <label>Guardian Name: </label>
                      <input type="text" name="guardian_name" id="guardian_name" tabindex="1" class="form-control" placeholder="optional" value="{{$member->guardian_name}}">
                    </div>

                    <div class="form-group">
                     <label>Guardian Mobile No.: </label>
                      <input type="text" name="guardian_mobile_no" id="guardian_mobile_no" tabindex="1" class="form-control" placeholder="optional" value="{{$member->guardian_mobile_no}}">
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <button type="submit" class="btn btn-default w3-green w3-text-white"><i class="fa fa-save"></i> Save</button>
              </div>
              </form>
            </div>
            </div>
            </div>

            @endforeach
          </tbody>
        </table>
        </div>
      </div>
    </div>

    <div id="alumni" class="tab-pane fade in">
      <div class="row w3-margin-top">
        <div class="table-responsive">
            <table class="table table-bordered table-hover" id="alumniTable">
            <thead>
              <tr>
               <th class="w3-center">Username</th>
               <th class="w3-center">Avatar</th>
               <th class="w3-center">First Name</th>
               <th class="w3-center">Last Name</th>
               <th class="w3-center">Work</th>
               <th class="w3-center">Email</th>
               <th class="w3-center">Mobile No.</th>
               <th class="w3-center">Status</th>
               <th class="w3-center">Action</th>
              </tr>
            </thead>
            <tbody class="w3-text-gray">
              @foreach($alumni as $member)
              <tr>
                <td class="w3-center">{{$member->username}}</td>
                <td class="w3-center"><img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$member->avatar) : asset('images/'.$member->avatar)}}" style="width: 64px; height: 64px"></td>
                <td class="w3-center">{{$member->first_name}}</td>
                <td class="w3-center">{{$member->last_name}}</td>
                <td class="w3-center">{{$member->work}}</td>
                <td class="w3-center">{{$member->email}}</td>
                <td class="w3-center">{{$member->mobile_no}}</td>
                <td class="w3-center">

                <form action="{{ url('admin/manage-members/'.$member->id.'/status/') }}" method="POST">
                <select class="form-control
                @if($member->status == 'deactivated') w3-red @endif
                @if($member->status == 'inactive') w3-yellow @endif
                @if($member->status == 'active') w3-green @endif
              " id="status" name="status" onchange="this.form.submit()">
              @if (isset($filter))
              @if ($filter != 'inactive')
                    <option value="active" @if($member->status == 'active') selected @endif>Active</option>
              @endif
              @else
                    <option value="active" @if($member->status == 'active') selected @endif>Active</option>
              @endif
                    <option value="inactive" @if($member->status == 'inactive') selected @endif>Inactive</option>
                    <option value="deactivated" @if($member->status == 'deactivated') selected @endif>Deactivate</option>
                  </select>
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  </form>
                </td>
                <td class="w3-center">
                  <a data-toggle="modal" href="#view-{{$member->id}}" style="outline: 0"><i class="fa fa-eye fa-fw w3-large w3-text-blue" data-toggle="tooltip" data-placement="bottom" title="View Profile"></i></a>

                  <a data-toggle="modal" href="#edit-{{$member->id}}" style="outline: 0"><i class="fa fa-pencil w3-text-orange fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Edit Profile"></i></a>
                  <!-- <button class="btn btn-default w3-red" data-toggle="modal" data-target="#delete-{{$member->id}}" title="Delete"><i class="fa fa-trash"></i> Deactivate</button> -->

                </td>
              </tr>

            <!-- VIEW -->
              <div class="modal fade" id="view-{{$member->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog modal-md" role="document">
              <div class="modal-content">
              <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
              <h4 class="modal-title w3-text-gray" id="myModalLabel">Profile</h4>
              </div>
              <div class="modal-body">
                <div class="row">
                   <div class="form-group w3-center">
                      <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$member->avatar) : asset('images/'.$member->avatar)}}" style="width:50%">
                    </div>
                </div>

                <div class="row">
                  <div class="col-md-6">

                    <div class="form-group">
                      <label>Student ID: </label>
                      <input type="text" name="student_id" id="student_id" tabindex="1" class="form-control" required placeholder="Student ID" value="{{$member->id_num}}">
                    </div>

                    <div class="form-group">
                      <label>First Name: </label>
                      <input type="text" name="first_name" id="first_name" tabindex="1" class="form-control" required placeholder="First Name" value="{{$member->first_name}}">
                    </div>

                    <div class="form-group">
                     <label>Last Name: </label>
                      <input type="text" name="last_name" id="last_name" tabindex="1" class="form-control" required placeholder="Last Name" value="{{$member->last_name}}">
                    </div>

                    <div class="form-group">
                     <label>Section:</label>
                      <input type="text" name="section" id="section" tabindex="1" class="form-control" required placeholder="Section" value="{{$member->section->section_code}}">
                    </div>

                    <div class="form-group">
                      <label>Username: </label>
                      <input type="text" name="username" id="username" tabindex="1" class="form-control" required placeholder="Username" value="{{$member->username}}">
                    </div>
                    <div class="form-group">
                      <label>Password: </label>
                       <input id="password" type="password" tabindex="1" class="form-control" name="password" required placeholder="Password">
                    </div>

                    <div class="form-group">
                      <label>Email: </label>
                      <input type="text" name="email" id="email" tabindex="1" class="form-control" placeholder="optional" value="{{$member->email}}">
                    </div>

              </div>

                  <div class="col-md-6">

                    <div class="form-group">
                      <label>Mobile No.: </label>
                      <input type="text" name="mobile_no" id="mobile_no" tabindex="1" class="form-control" placeholder="optional" value="{{$member->mobile_no}}">
                    </div>

                    <div class="form-group">
                     <label>Age: </label>
                      <input type="text" name="age" id="age" tabindex="1" class="form-control" placeholder="optional" value="{{$member->age}}">
                    </div>

                    <div class="form-group">
                      <label for="bday">Birthday:</label>
                      <input type="date" class="form-control" id="birthday" name="birthday" value="{{$member->birthday}}">
                    </div>

                    <div class="form-group">
                     <label>Address: </label>
                      <input type="text" name="address" id="address" tabindex="1" class="form-control" placeholder="optional" value="{{$member->address}}">
                    </div>

                    <div class="form-group">
                     <label>Work: </label>
                      <input type="text" name="work" id="work" tabindex="1" class="form-control" placeholder="optional" value="{{$member->work}}">
                    </div>

                    <div class="form-group">
                     <label>Guardian Name: </label>
                      <input type="text" name="guardian_name" id="guardian_name" tabindex="1" class="form-control" placeholder="optional" value="{{$member->guardian_name}}">
                    </div>

                    <div class="form-group">
                     <label>Guardian Mobile No.: </label>
                      <input type="text" name="guardian_mobile_no" id="guardian_mobile_no" tabindex="1" class="form-control" placeholder="optional" value="{{$member->guardian_mobile_no}}">
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
              </div>
              </div>
              </div>
              </div>

              <!-- EDIT MEMBER -->
              <div class="modal fade" id="edit-{{$member->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog modal-md" role="document">
              <div class="modal-content">
              <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
              <h4 class="modal-title w3-text-gray" id="myModalLabel">Profile</h4>
              </div>
              <form action="{{ url('admin/manage-members/'.$member->id.'/edit/') }}" method="POST" enctype="multipart/form-data">
              <div class="modal-body">
                <div class="row">
                  <div class="form-group w3-margin-left">
                    <label id="alumni-avatar-file-name-{{$member->id}}" for="alumni-avatar-{{$member->id}}" style="cursor: pointer;" title="Edit Picture"><i class="fa fa-file-image-o fa-fw w3-text-deep-orange w3-large w3-left"></i></label>
                      <input type="file" name="avatar" id="alumni-avatar-{{$member->id}}" onchange="alumni( event, {{$member->id}})" style="opacity: 0; position: absolute; z-index: -1;" />
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  </div>
                </div>

                <div class="row">
                  <div class="form-group w3-center">
                      <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$member->avatar) : asset('images/'.$member->avatar)}}" id="alumniProfileImageContainer-{{$member->id}}" style="width: 50%">
                  </div>
                  <br>
                </div>

                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>First Name: </label>
                      <input type="text" name="first_name" id="first_name" tabindex="1" class="form-control" required placeholder="First Name" value="{{$member->first_name}}">
                    </div>

                    <div class="form-group">
                     <label>Last Name: </label>
                      <input type="text" name="last_name" id="last_name" tabindex="1" class="form-control" required placeholder="Last Name" value="{{$member->last_name}}">
                    </div>

                    <div class="radio w3-center w3-padding">


                    <label><input type="radio" name="type" value="Student" onclick="studentEdit(this.value)"
                    @if ($member->section->section_code != 'Faculty' AND $member->section->section_code != 'Alumni') checked @endif> Student</label>
                    <label><input type="radio" name="type" value="Alumni" onclick="studentEdit(this.value)"
                    @if ($member->section->section_code == 'Alumni') checked @endif> Alumni</label>
                    <label><input type="radio" name="type" value="Faculty" onclick="studentEdit(this.value)"
                    @if ($member->section->section_code == 'Faculty') checked @endif> Faculty</label>
                  </div>

                  <!-- start student div -->
                  <div @if ($member->section->section_code != 'Faculty' AND $member->section->section_code != 'Alumni') style="display: block" @else style="display: none" @endif class="student-edit">
                  <div class="form-group{{ $errors->has('id_num') ? ' has-error' : '' }}">
                    <label>Student Id:</label>
                    <input type="text" class="form-control" id="id_num" name="id_num" placeholder="Student ID" value="{{$member->id_num}}">
                    @if ($errors->has('id_num'))
                      <span class="help-block">
                       <strong>{{ $errors->first('id_num') }}</strong>
                      </span>
                    @endif
                  </div>

                  <div class="form-group{{ $errors->has('year') ? ' has-error' : '' }}">
                    <label>Year:</label>
                      <select class="form-control" id="year" name="year" required placeholder="Select your year">
                       @foreach($years as $year)

                        <option value='{{$year->id}}' @if($member->year->year_desc == $year->year_desc) selected @endif>{{$year->year_desc}}</option>

                       @endforeach
                      </select>
                      @if ($errors->has('year'))
                      <span class="help-block">
                       <strong>{{ $errors->first('year') }}</strong>
                      </span>
                    @endif
                  </div>

                  <div class="form-group{{ $errors->has('section') ? ' has-error' : '' }}">
                    <label>Section:</label>
                      <select class="form-control" id="section" name="section" required placeholder="Select your Section">
                       @foreach($sections as $section)

                            <option value='{{$section->id}}' @if($member->section->section_code == $section->section_code) selected @endif>{{$section->course_code}} {{$section->section_code}}</option>

                       @endforeach
                      </select>
                      @if ($errors->has('section'))
                      <span class="help-block">
                       <strong>{{ $errors->first('section') }}</strong>
                      </span>
                    @endif
                  </div>

                  <div class="form-group{{ $errors->has('section') ? ' has-error' : '' }}">
                    <select class="form-control" id="stud_type" name="stud_type" required placeholder="Select your type">
                      <option value='Regular'>Regular</option>
                      <option value='Irregular'>Irregular</option>
                    </select>
                  </div>

                  </div>

              <div class="form-group">
                    <label>Username: </label>
                      <input type="text" name="username" id="username" tabindex="1" class="form-control" required placeholder="Username" value="{{$member->username}}">
                    </div>
                    <div class="form-group">
                      <label>Password: </label>
                       <input id="password" type="password" tabindex="1" class="form-control" name="password" placeholder="Password">
                    </div>

                     <div class="form-group">
                     <label>Email: </label>
                      <input type="text" name="email" id="email" tabindex="1" class="form-control" placeholder="optional" value="{{$member->email}}">
                    </div>

              </div>

                  <div class="col-md-6">

                    <div class="form-group">
                      <label>Mobile No.: </label>
                      <input type="text" name="mobile_no" id="mobile_no" tabindex="1" class="form-control" placeholder="optional" value="{{$member->mobile_no}}">
                    </div>

                    <div class="form-group">
                     <label>Age: </label>
                      <input type="number" min="0" name="age" id="age" tabindex="1" class="form-control" placeholder="optional" value="{{$member->age}}">
                    </div>

                    <div class="form-group">
                      <label for="bday">Birthday:</label>
                      <input type="date" class="form-control" id="birthday" name="birthday" value="{{$member->birthday}}">
                    </div>

                    <div class="form-group">
                     <label>Address: </label>
                      <input type="text" name="address" id="address" tabindex="1" class="form-control" placeholder="optional" value="{{$member->address}}">
                    </div>

                    <div class="form-group">
                     <label>Work: </label>
                      <input type="text" name="work" id="work" tabindex="1" class="form-control" placeholder="optional" value="{{$member->work}}">
                    </div>

                    <div class="form-group">
                     <label>Guardian Name: </label>
                      <input type="text" name="guardian_name" id="guardian_name" tabindex="1" class="form-control" placeholder="optional" value="{{$member->guardian_name}}">
                    </div>

                    <div class="form-group">
                     <label>Guardian Mobile No.: </label>
                      <input type="text" name="guardian_mobile_no" id="guardian_mobile_no" tabindex="1" class="form-control" placeholder="optional" value="{{$member->guardian_mobile_no}}">
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <button type="submit" class="btn btn-default w3-green w3-text-white"><i class="fa fa-save"></i> Save</button>
              </div>
              </form>
              </div>
              </div>
              </div>

              @endforeach
            </tbody>
          </table>

        </div>
      </div>
    </div>

    <div id="faculty" class="tab-pane fade in">
      <div class="row w3-margin-top">
        <div class="table-responsive">
          <table class="table table-bordered table-hover" id="facultyTable">
          <thead>
            <tr>
             <th class="w3-center">Username</th>
             <th class="w3-center">Avatar</th>
             <th class="w3-center">First Name</th>
             <th class="w3-center">Last Name</th>
             <th class="w3-center">Email</th>
             <th class="w3-center">Mobile No.</th>
             <th class="w3-center">Status</th>
             <th class="w3-center">Action</th>
            </tr>
          </thead>
          <tbody class="w3-text-gray">
            @foreach($faculty as $member)
            <tr>
              <td class="w3-center">{{$member->username}}</td>
              <td class="w3-center"><img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$member->avatar) : asset('images/'.$member->avatar)}}" style="width: 64px; height: 64px"></td>
              <td class="w3-center">{{$member->first_name}}</td>
              <td class="w3-center">{{$member->last_name}}</td>
              <td class="w3-center">{{$member->email}}</td>
              <td class="w3-center">{{$member->mobile_no}}</td>
              <td class="w3-center">

              <form action="{{ url('admin/manage-members/'.$member->id.'/status/') }}" method="POST">
              <select class="form-control
              @if($member->status == 'deactivated') w3-red @endif
              @if($member->status == 'inactive') w3-yellow @endif
              @if($member->status == 'active') w3-green @endif
            " id="status" name="status" onchange="this.form.submit()">
            @if (isset($filter))
            @if ($filter != 'inactive')
                  <option value="active" @if($member->status == 'active') selected @endif>Active</option>
            @endif
            @else
                  <option value="active" @if($member->status == 'active') selected @endif>Active</option>
            @endif
                  <option value="inactive" @if($member->status == 'inactive') selected @endif>Inactive</option>
                  <option value="deactivated" @if($member->status == 'deactivated') selected @endif>Deactivate</option>
                </select>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </form>
              </td>
              <td class="w3-center">
                <a data-toggle="modal" href="#view-{{$member->id}}" style="outline: 0"><i class="fa fa-eye fa-fw w3-large w3-text-blue" data-toggle="tooltip" data-placement="bottom" title="View Profile"></i></a>
                <a data-toggle="modal" href="#edit-{{$member->id}}" style="outline: 0"><i class="fa fa-pencil w3-text-orange fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Edit Profile"></i></a>
              </td>
            </tr>

          <!-- VIEW -->
            <div class="modal fade" id="view-{{$member->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
            <h4 class="modal-title w3-text-gray" id="myModalLabel">Profile</h4>
            </div>
            <div class="modal-body">
              <div class="row">
                 <div class="form-group w3-center">
                    <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$member->avatar) : asset('images/'.$member->avatar)}}" style="width:50%">
                  </div>
              </div>

              <div class="row">
                <div class="col-md-6">

                  <div class="form-group">
                    <label>Student ID: </label>
                    <input type="text" name="student_id" id="student_id" tabindex="1" class="form-control" required placeholder="Student ID" value="{{$member->id_num}}">
                  </div>

                  <div class="form-group">
                    <label>First Name: </label>
                    <input type="text" name="first_name" id="first_name" tabindex="1" class="form-control" required placeholder="First Name" value="{{$member->first_name}}">
                  </div>

                  <div class="form-group">
                   <label>Last Name: </label>
                    <input type="text" name="last_name" id="last_name" tabindex="1" class="form-control" required placeholder="Last Name" value="{{$member->last_name}}">
                  </div>

                  <div class="form-group">
                   <label>Section:</label>
                    <input type="text" name="section" id="section" tabindex="1" class="form-control" required placeholder="Section" value="{{$member->section->section_code}}">
                  </div>

                  <div class="form-group">
                    <label>Username: </label>
                    <input type="text" name="username" id="username" tabindex="1" class="form-control" required placeholder="Username" value="{{$member->username}}">
                  </div>
                  <div class="form-group">
                    <label>Password: </label>
                     <input id="password" type="password" tabindex="1" class="form-control" name="password" required placeholder="Password">
                  </div>

                  <div class="form-group">
                    <label>Email: </label>
                    <input type="text" name="email" id="email" tabindex="1" class="form-control" placeholder="optional" value="{{$member->email}}">
                  </div>

            </div>

                <div class="col-md-6">

                  <div class="form-group">
                    <label>Mobile No.: </label>
                    <input type="text" name="mobile_no" id="mobile_no" tabindex="1" class="form-control" placeholder="optional" value="{{$member->mobile_no}}">
                  </div>

                  <div class="form-group">
                   <label>Age: </label>
                    <input type="text" name="age" id="age" tabindex="1" class="form-control" placeholder="optional" value="{{$member->age}}">
                  </div>

                  <div class="form-group">
                    <label for="bday">Birthday:</label>
                    <input type="date" class="form-control" id="birthday" name="birthday" value="{{$member->birthday}}">
                  </div>

                  <div class="form-group">
                   <label>Address: </label>
                    <input type="text" name="address" id="address" tabindex="1" class="form-control" placeholder="optional" value="{{$member->address}}">
                  </div>

                  <div class="form-group">
                   <label>Work: </label>
                    <input type="text" name="work" id="work" tabindex="1" class="form-control" placeholder="optional" value="{{$member->work}}">
                  </div>

                  <div class="form-group">
                   <label>Guardian Name: </label>
                    <input type="text" name="guardian_name" id="guardian_name" tabindex="1" class="form-control" placeholder="optional" value="{{$member->guardian_name}}">
                  </div>

                  <div class="form-group">
                   <label>Guardian Mobile No.: </label>
                    <input type="text" name="guardian_mobile_no" id="guardian_mobile_no" tabindex="1" class="form-control" placeholder="optional" value="{{$member->guardian_mobile_no}}">
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
            </div>
            </div>
            </div>
            </div>

            <!-- EDIT MEMBER -->
            <div class="modal fade" id="edit-{{$member->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
            <h4 class="modal-title w3-text-gray" id="myModalLabel">Profile</h4>
            </div>
            <form action="{{ url('admin/manage-members/'.$member->id.'/edit/') }}" method="POST" enctype="multipart/form-data">
            <div class="modal-body">
              <div class="row">
                <div class="form-group w3-margin-left">
                  <label id="faculty-avatar-file-name-{{$member->id}}" for="faculty-avatar-{{$member->id}}" style="cursor: pointer;" title="Edit Picture"><i class="fa fa-file-image-o fa-fw w3-text-deep-orange w3-large w3-left"></i></label>
                    <input type="file" name="avatar" onchange="faculty( event, {{$member->id}} )" id="faculty-avatar-{{$member->id}}" style="opacity: 0; position: absolute; z-index: -1;" />
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </div>
              </div>

              <div class="row">
                <div class="form-group w3-center">
                    <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$member->avatar) : asset('images/'.$member->avatar)}}" id="facultyProfileImageContainer-{{$member->id}}" style="width: 50%">
                </div>
                <br>
              </div>

              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>First Name: </label>
                    <input type="text" name="first_name" id="first_name" tabindex="1" class="form-control" required placeholder="First Name" value="{{$member->first_name}}">
                  </div>

                  <div class="form-group">
                   <label>Last Name: </label>
                    <input type="text" name="last_name" id="last_name" tabindex="1" class="form-control" required placeholder="Last Name" value="{{$member->last_name}}">
                  </div>

                  <div class="radio w3-center w3-padding">


                  <label><input type="radio" name="type" value="Student" onclick="studentEdit(this.value)"
                  @if ($member->section->section_code != 'Faculty' AND $member->section->section_code != 'Alumni') checked @endif> Student</label>
                  <label><input type="radio" name="type" value="Alumni" onclick="studentEdit(this.value)"
                  @if ($member->section->section_code == 'Alumni') checked @endif> Alumni</label>
                  <label><input type="radio" name="type" value="Faculty" onclick="studentEdit(this.value)"
                  @if ($member->section->section_code == 'Faculty') checked @endif> Faculty</label>
                </div>

                <!-- start student div -->
                <div @if ($member->section->section_code != 'Faculty' AND $member->section->section_code != 'Alumni') style="display: block" @else style="display: none" @endif class="student-edit">
                <div class="form-group{{ $errors->has('id_num') ? ' has-error' : '' }}">
                  <label>Student Id:</label>
                  <input type="text" class="form-control" id="id_num" name="id_num" placeholder="Student ID" value="{{$member->id_num}}">
                  @if ($errors->has('id_num'))
                    <span class="help-block">
                     <strong>{{ $errors->first('id_num') }}</strong>
                    </span>
                  @endif
                </div>

                <div class="form-group{{ $errors->has('year') ? ' has-error' : '' }}">
                  <label>Year:</label>
                    <select class="form-control" id="year" name="year" required placeholder="Select your year">
                     @foreach($years as $year)

                      <option value='{{$year->id}}' @if($member->year->year_desc == $year->year_desc) selected @endif>{{$year->year_desc}}</option>

                     @endforeach
                    </select>
                    @if ($errors->has('year'))
                    <span class="help-block">
                     <strong>{{ $errors->first('year') }}</strong>
                    </span>
                  @endif
                </div>

                <div class="form-group{{ $errors->has('section') ? ' has-error' : '' }}">
                  <label>Section:</label>
                    <select class="form-control" id="section" name="section" required placeholder="Select your Section">
                     @foreach($sections as $section)

                          <option value='{{$section->id}}' @if($member->section->section_code == $section->section_code) selected @endif>{{$section->course_code}} {{$section->section_code}}</option>

                     @endforeach
                    </select>
                    @if ($errors->has('section'))
                    <span class="help-block">
                     <strong>{{ $errors->first('section') }}</strong>
                    </span>
                  @endif
                </div>

                <div class="form-group{{ $errors->has('section') ? ' has-error' : '' }}">
                  <select class="form-control" id="stud_type" name="stud_type" required placeholder="Select your type">
                    <option value='Regular'>Regular</option>
                    <option value='Irregular'>Irregular</option>
                  </select>
                </div>

                </div>

            <div class="form-group">
                  <label>Username: </label>
                    <input type="text" name="username" id="username" tabindex="1" class="form-control" required placeholder="Username" value="{{$member->username}}">
                  </div>
                  <div class="form-group">
                    <label>Password: </label>
                     <input id="password" type="password" tabindex="1" class="form-control" name="password" placeholder="Password">
                  </div>

                   <div class="form-group">
                   <label>Email: </label>
                    <input type="text" name="email" id="email" tabindex="1" class="form-control" placeholder="optional" value="{{$member->email}}">
                  </div>

            </div>

                <div class="col-md-6">

                  <div class="form-group">
                    <label>Mobile No.: </label>
                    <input type="text" name="mobile_no" id="mobile_no" tabindex="1" class="form-control" placeholder="optional" value="{{$member->mobile_no}}">
                  </div>

                  <div class="form-group">
                   <label>Age: </label>
                    <input type="number" min="0" name="age" id="age" tabindex="1" class="form-control" placeholder="optional" value="{{$member->age}}">
                  </div>

                  <div class="form-group">
                    <label for="bday">Birthday:</label>
                    <input type="date" class="form-control" id="birthday" name="birthday" value="{{$member->birthday}}">
                  </div>

                  <div class="form-group">
                   <label>Address: </label>
                    <input type="text" name="address" id="address" tabindex="1" class="form-control" placeholder="optional" value="{{$member->address}}">
                  </div>

                  <div class="form-group">
                   <label>Work: </label>
                    <input type="text" name="work" id="work" tabindex="1" class="form-control" placeholder="optional" value="{{$member->work}}">
                  </div>

                  <div class="form-group">
                   <label>Guardian Name: </label>
                    <input type="text" name="guardian_name" id="guardian_name" tabindex="1" class="form-control" placeholder="optional" value="{{$member->guardian_name}}">
                  </div>

                  <div class="form-group">
                   <label>Guardian Mobile No.: </label>
                    <input type="text" name="guardian_mobile_no" id="guardian_mobile_no" tabindex="1" class="form-control" placeholder="optional" value="{{$member->guardian_mobile_no}}">
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <button type="submit" class="btn btn-default w3-green w3-text-white"><i class="fa fa-save"></i> Save</button>
            </div>
            </form>
            </div>
            </div>
            </div>

            @endforeach
          </tbody>
        </table>
        </div>
      </div>
    </div>

    <div id="deact" class="tab-pane fade in">
      <div class="row w3-margin-top">
        <div class="table-responsive">
        <table class="table table-bordered table-hover" id="deactTable">
          <thead>
            <tr>
             <th class="w3-center">Username</th>
             <th class="w3-center">Avatar</th>
             <th class="w3-center">First Name</th>
             <th class="w3-center">Last Name</th>
             <th class="w3-center">Status</th>
             <th class="w3-center">Action</th>
            </tr>
          </thead>
          <tbody class="w3-text-gray">
            @foreach($deactivated as $member)
            <tr>
              <td class="w3-center">{{$member->username}}</td>
              <td class="w3-center"><img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$member->avatar) : asset('images/'.$member->avatar)}}" style="width: 64px; height: 64px"></td>
              <td class="w3-center">{{$member->first_name}}</td>
              <td class="w3-center">{{$member->last_name}}</td>
              <td class="w3-center">

              <form action="{{ url('admin/manage-members/'.$member->id.'/status/') }}" method="POST">
              <select class="form-control
              @if($member->status == 'deactivated') w3-red @endif
              @if($member->status == 'inactive') w3-yellow @endif
              @if($member->status == 'active') w3-green @endif
            " id="status" name="status" onchange="this.form.submit()">
            @if (isset($filter))
            @if ($filter != 'inactive')
                  <option value="active" @if($member->status == 'active') selected @endif>Active</option>
            @endif
            @else
                  <option value="active" @if($member->status == 'active') selected @endif>Active</option>
            @endif
                  <option value="inactive" @if($member->status == 'inactive') selected @endif>Inactive</option>
                  <option value="deactivated" @if($member->status == 'deactivated') selected @endif>Deactivate</option>
                </select>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </form>
              </td>
              <td class="w3-center">
                 <a data-toggle="modal" href="#view-{{$member->id}}" style="outline: 0"><i class="fa fa-eye fa-fw w3-large w3-text-blue" data-toggle="tooltip" data-placement="bottom" title="View Profile"></i></a>
              </td>
            </tr>

          <!-- VIEW -->
            <div class="modal fade" id="view-{{$member->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
            <h4 class="modal-title w3-text-gray" id="myModalLabel">Profile</h4>
            </div>
            <div class="modal-body">
              <div class="row">
                 <div class="form-group w3-center">
                    <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$member->avatar) : asset('images/'.$member->avatar)}}" style="width:50%">
                  </div>
              </div>

              <div class="row">
                <div class="col-md-6">

                  <div class="form-group">
                    <label>Student ID: </label>
                    <input type="text" name="student_id" id="student_id" tabindex="1" class="form-control" required placeholder="Student ID" value="{{$member->id_num}}">
                  </div>

                  <div class="form-group">
                    <label>First Name: </label>
                    <input type="text" name="first_name" id="first_name" tabindex="1" class="form-control" required placeholder="First Name" value="{{$member->first_name}}">
                  </div>

                  <div class="form-group">
                   <label>Last Name: </label>
                    <input type="text" name="last_name" id="last_name" tabindex="1" class="form-control" required placeholder="Last Name" value="{{$member->last_name}}">
                  </div>

                  <div class="form-group">
                   <label>Section:</label>
                    <input type="text" name="section" id="section" tabindex="1" class="form-control" required placeholder="Section" value="{{$member->section->section_code}}">
                  </div>

                  <div class="form-group">
                    <label>Username: </label>
                    <input type="text" name="username" id="username" tabindex="1" class="form-control" required placeholder="Username" value="{{$member->username}}">
                  </div>
                  <div class="form-group">
                    <label>Password: </label>
                     <input id="password" type="password" tabindex="1" class="form-control" name="password" required placeholder="Password">
                  </div>

                  <div class="form-group">
                    <label>Email: </label>
                    <input type="text" name="email" id="email" tabindex="1" class="form-control" placeholder="optional" value="{{$member->email}}">
                  </div>

            </div>

                <div class="col-md-6">

                  <div class="form-group">
                    <label>Mobile No.: </label>
                    <input type="text" name="mobile_no" id="mobile_no" tabindex="1" class="form-control" placeholder="optional" value="{{$member->mobile_no}}">
                  </div>

                  <div class="form-group">
                   <label>Age: </label>
                    <input type="text" name="age" id="age" tabindex="1" class="form-control" placeholder="optional" value="{{$member->age}}">
                  </div>

                  <div class="form-group">
                    <label for="bday">Birthday:</label>
                    <input type="date" class="form-control" id="birthday" name="birthday" value="{{$member->birthday}}">
                  </div>

                  <div class="form-group">
                   <label>Address: </label>
                    <input type="text" name="address" id="address" tabindex="1" class="form-control" placeholder="optional" value="{{$member->address}}">
                  </div>

                  <div class="form-group">
                   <label>Work: </label>
                    <input type="text" name="work" id="work" tabindex="1" class="form-control" placeholder="optional" value="{{$member->work}}">
                  </div>

                  <div class="form-group">
                   <label>Guardian Name: </label>
                    <input type="text" name="guardian_name" id="guardian_name" tabindex="1" class="form-control" placeholder="optional" value="{{$member->guardian_name}}">
                  </div>

                  <div class="form-group">
                   <label>Guardian Mobile No.: </label>
                    <input type="text" name="guardian_mobile_no" id="guardian_mobile_no" tabindex="1" class="form-control" placeholder="optional" value="{{$member->guardian_mobile_no}}">
                  </div>
                </div>
              </div>
            </div>
            </div>
            </div>
            </div>

            @endforeach
          </tbody>
        </table>
        </div>
      </div>
    </div>
  </div>

  <!-- ADD MODAL -->
  <div class="modal fade" id="add" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
      <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
      <h4 class="modal-title w3-text-gray" id="myModalLabel">Profile</h4>
      </div>
      <form action="{{ url('admin/manage-members/store') }}" method="POST" enctype="multipart/form-data">
      <div class="modal-body">
        <div class="row w3-margin-left">
          <label id="avatarAdd-file-name" for="avatar-add" style="cursor: pointer;" title="Edit Picture"><i class="fa fa-file-image-o fa-fw w3-text-deep-orange w3-large w3-left"></i></label>
            <input type="file" onchange="showAddPictureFileName(event)" name="avatar-add" id="avatar-add" style="opacity: 0; position: absolute; z-index: -1;" />
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        </div>

        <div class="row">
          <div class="form-group w3-center">
              <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/members/default.png') : asset('images/members/default.png')}}" id="profileImageContainer" style="width: 50%">
            </div>
        </div>
        <div class="row">
          <div class="col-md-6">

            <div class="form-group">
              <p><b>First Name: </b></p>
              <input type="text" name="first_name" id="first_name" tabindex="1" class="form-control" required placeholder="First Name">
            </div>

            <div class="form-group">
             <p><b>Last Name: </b></p>
              <input type="text" name="last_name" id="last_name" tabindex="1" class="form-control" required placeholder="Last Name">
            </div>

          <div class="radio w3-center w3-padding">
            <label><input type="radio" name="type" onclick="studentAdd(this.value)" value="Student"> Student</label>
            <label><input type="radio" name="type" onclick="studentAdd(this.value)" value="Alumni"> Alumni</label>
            <label><input type="radio" name="type" onclick="studentAdd(this.value)" value="Faculty"> Faculty</label>
          </div>

          <!-- start student div -->
          <div style="display: none" class="student-add">
          <div class="form-group{{ $errors->has('id_num') ? ' has-error' : '' }}">
            <label>Student Id:</label>
            <input type="text" class="form-control" id="id_num" name="id_num" placeholder="Student ID">
            @if ($errors->has('id_num'))
              <span class="help-block">
               <strong>{{ $errors->first('id_num') }}</strong>
              </span>
            @endif
          </div>

          <div class="form-group{{ $errors->has('year') ? ' has-error' : '' }}">
            <label>Year:</label>
              <select class="form-control" id="year" name="year" required placeholder="Select your year">
               @foreach($years as $year)

                <option value='{{$year->id}}'>{{$year->year_desc}}</option>

               @endforeach
              </select>
              @if ($errors->has('year'))
              <span class="help-block">
               <strong>{{ $errors->first('year') }}</strong>
              </span>
            @endif
          </div>

          <div class="form-group{{ $errors->has('section') ? ' has-error' : '' }}">
            <label>Section:</label>
              <select class="form-control" id="section" name="section" required placeholder="Select your Section">
               @foreach($sections as $section)

                    <option value='{{$section->id}}'>{{$section->course_code}} {{$section->section_code}}</option>

               @endforeach
              </select>
              @if ($errors->has('section'))
              <span class="help-block">
               <strong>{{ $errors->first('section') }}</strong>
              </span>
            @endif
          </div>

          <div class="form-group{{ $errors->has('section') ? ' has-error' : '' }}">
            <select class="form-control" id="stud_type" name="stud_type" required placeholder="Select your type">
              <option value='Regular'>Regular</option>
              <option value='Irregular'>Irregular</option>
            </select>
          </div>

          </div>
          <!-- end student div -->

          <div class="form-group">
            <p><b>Username: </b></p>
              <input type="text" name="username" id="username" tabindex="1" class="form-control" required placeholder="Username" >
            </div>
            <div class="form-group">
              <p><b>Password: </b></p>
               <input id="password" type="password" tabindex="1" class="form-control" name="password" required placeholder="Password" >
            </div>

             <div class="form-group">
             <p><b>Email: </b></p>
              <input type="text" name="email" id="email" tabindex="1" class="form-control" placeholder="optional" >
            </div>


          </div>

          <div class="col-md-6">
            <div class="form-group">
              <p><b>Mobile No.: </b></p>
              <input type="text" name="mobile_no" id="mobile_no" tabindex="1" class="form-control" placeholder="optional">
            </div>

            <div class="form-group">
             <p><b>Age: </b></p>
              <input type="number" min="0" name="age" id="age" tabindex="1" class="form-control" placeholder="optional" >
            </div>

            <div class="form-group">
              <label for="bday">Birthday:</label>
              <input type="date" class="form-control" id="birthday" name="birthday" >
            </div>

            <div class="form-group">
             <p><b>Address: </b></p>
              <input type="text" name="address" id="address" tabindex="1" class="form-control" placeholder="optional" >
            </div>

            <div class="form-group">
             <p><b>Work: </b></p>
              <input type="text" name="work" id="work" tabindex="1" class="form-control" placeholder="optional" >
            </div>

            <div class="form-group">
             <p><b>Guardian Name: </b></p>
              <input type="text" name="guardian_name" id="guardian_name" tabindex="1" class="form-control" placeholder="optional" >
            </div>

            <div class="form-group">
             <p><b>Guardian Mobile No.: </b></p>
              <input type="text" name="guardian_mobile_no" id="guardian_mobile_no" tabindex="1" class="form-control" placeholder="optional" >
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <button type="submit" class="btn btn-default w3-green w3-text-white"><i class="fa fa-save"></i> Save</button>
      </div>
      </form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
$(document).ready( function () {
  $('#studentTable').DataTable();
  $('#alumniTable').DataTable();
  $('#facultyTable').DataTable();
  $('#deactTable').DataTable();
} );
</script>

<script>

  function showFileName( event ) {

    // the change event gives us the input it occurred in
    var input = event.srcElement;

    // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
    var fileName = input.files[0].name;

    // use fileName however fits your app best, i.e. add it into a div
    $('#import-file-name').html(fileName);
  }

  function student( id ) {
    // the change event gives us the input it occurred in
    var input = event.srcElement;

    // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
    var fileName = input.files[0].name;

    // use fileName however fits your app best, i.e. add it into a div
    $("#studentAvatarFileName-" + id).html(fileName);

    var reader = new FileReader();
    reader.onload = function (e) {
      $("#studentProfileImageContainer-" + id).attr('src', e.target.result);
    };
    reader.readAsDataURL(input.files[0]);

  };

  function alumni( event, id ) {
    // the change event gives us the input it occurred in
    var input = event.srcElement;

    // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
    var fileName = input.files[0].name;

    // use fileName however fits your app best, i.e. add it into a div
    $("#alumni-avatar-file-name-" + id).html(fileName);

    var reader = new FileReader();
    reader.onload = function (e) {
      $("#alumniProfileImageContainer-" + id).attr('src', e.target.result);
    };
    reader.readAsDataURL(input.files[0]);

  };

  function faculty( event, id ) {

    // the change event gives us the input it occurred in
    var input = event.srcElement;

    // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
    var fileName = input.files[0].name;

    // use fileName however fits your app best, i.e. add it into a div

    $("#faculty-avatar-file-name-" + id).html(fileName);

    var reader = new FileReader();
    reader.onload = function (e) {
      $("#facultyProfileImageContainer-" + id).attr('src', e.target.result);
    };
    reader.readAsDataURL(input.files[0]);

  };

  function showAddPictureFileName( event ) {

    // the change event gives us the input it occurred in
    var input = event.srcElement;

    // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
    var fileName = input.files[0].name;

    // use fileName however fits your app best, i.e. add it into a div
    $('#avatarAdd-file-name').html(fileName);

    var reader = new FileReader();
    reader.onload = function (e) {
      $("#profileImageContainer").attr('src', e.target.result);
    };
    reader.readAsDataURL(input.files[0]);

  };

  window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove();
    });
}, 4000);
</script>

<script type="text/javascript">
function studentAdd(value) {
  if (value == "Student") {
      $('.student-add').show();
  }
  else {
      $('.student-add').hide();
  }

};

function studentEdit(value) {
  if (value == "Student") {
      $('.student-edit').show();
  }
  else {
      $('.student-edit').hide();
  }

};
</script>
@endsection
