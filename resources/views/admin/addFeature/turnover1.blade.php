@extends('layout/control_panel')

@section('title')
Turn Over
@endsection

@section('middle')
<div class="w3-container w3-margin">
  <div class="row">
    <h3>
      <i class="fa fa-rotate-right fa-fw w3-xxlarge"></i>
      <strong>Turn Over</strong>
    </h3>
  </div>

  <hr>

  <div class="row w3-margin-top w3-padding">
      <h4><i class="fa fa-file-text-o fa-fw w3-xlarge"></i> <strong>MTICS Batch Report</strong></h4>
  </div>

  <hr>

  <div class="row w3-padding">
    <form action="{{ url('admin/turn-over/view-details') }}" method="POST">

        <div class="form-group form-inline" align="center">
            <input type="number" name="year" placeholder="YYYY" min="2017" max="2100" class="form-control">
            <button type="submit" class="btn btn-default w3-teal w3-text-white"><i class="fa fa-eye fa-fw"></i> View</button>
        </div>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <script>
          document.querySelector("input[type=number]")
          .oninput = e => console.log(new Date(e.target.valueAsNumber, 0, 1))
        </script>
    </form>
  </div>

  <div class="row w3-margin-top w3-padding">
    <p><strong>Warning:</strong> This action can lead to the corruption of the data if not used properly. This action is only used if it is the end of the school year and old officers are ready to hand over the MTICS organization to the next set of officers.</p>
    <a href="#turnOver" data-toggle="modal" class="w3-red btn btn-default">Turn Over</a>
  </div>
</div>

<!-- Turn Over -->
<div class="modal fade" id="turnOver" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog modal-md" role="document">
  <div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
    <h4 class="modal-title w3-text-gray" id="myModalLabel">Turn Over</h4>
  </div>
    <form action="{{ url('admin/turn-over/end') }}" method="POST">
    <div class="modal-body">
    <div class="w3-container">
        <div class="row w3-padding">
            <p>Warning: Do not proceed if it is not the end of the school year and if it is not the end term of the current officers.</p>
            <p>This action will lead to the following:</p>
            <p>
                <i class="fa fa-dot-circle-o fa-fw"></i> The set of officers will be removed
                <br>
                <i class="fa fa-dot-circle-o fa-fw"></i> Tasks will be removed
                <br>
                <i class="fa fa-dot-circle-o fa-fw"></i> Transactions will be removed
            </p>
            <p>Are you sure you want to proceed?</p>
        </div>
    </div>
    </div>
    </form>

    <div class="modal-footer">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <button type="submit" class="btn btn-default w3-green">Yes</button>
        <button type="button" class="btn btn-default w3-red" data-dismiss="modal">No</button>
    </div>

  </div>
</div>
</div>
<!-- END TURN OVER -->
@endsection
