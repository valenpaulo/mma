@extends('layout/layout')

@section('title')
Additional Feature
@endsection
@section('content')
<div class="container" style="margin-top: 120px">
    <div class="row">
        <h3>
            <i class="fa fa-superpowers fa-fw w3-xxlarge"></i>
            <strong>Additional Feature</strong>
        </h3>
    </div>

    <hr>


    @if(Gate::allows('ReceivePayment-only'))
    <div class="row">
        <div class="col-lg-3 col-lg-3"></div>

        <div class="col-lg-6 col-lg-6">
            <a href="{{route('admin.manage.payment')}}" class="w3-text-black">
                <div class="w3-card-4 w3-teal w3-margin w3-padding-large w3-hover-opacity">
                    <div class="w3-container">
                        <div class="row">
                            <i class="fa fa-money fa-fw w3-xxlarge"></i>
                            <strong>Collect Payment</strong>
                            <span class="w3-right"><i class="fa fa-external-link-square fa-fw"></i></span>
                        </div>
                        <hr>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-lg-3"></div>
    </div>
    @endif

    @if(Gate::allows('BorrowReturn-only'))
    <div class="row">
        <div class="col-lg-3 col-lg-3"></div>

        <div class="col-lg-6 col-lg-6">
            <a href="{{route('admin.borrow.return')}}" class="w3-text-black">
                <div class="w3-card-4 w3-teal w3-margin w3-padding-large w3-hover-opacity">
                    <div class="w3-container">
                        <div class="row">
                            <i class="fa fa-suitcase fa-fw w3-xxlarge"></i>
                            <strong>Manage Borrow/Return Item</strong>
                            <span class="w3-right"><i class="fa fa-external-link-square fa-fw"></i></span>
                        </div>
                        <hr>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-lg-3"></div>
    </div>
    @endif

    @if(Gate::allows('accntreq_approval-only'))
    <div class="row">
        <div class="col-lg-3 col-lg-3"></div>

        <div class="col-lg-6 col-lg-6">
            <a href="{{route('admin.request.account')}}" class="w3-text-black">
                <div class="w3-card-4 w3-teal w3-margin w3-padding-large w3-hover-opacity">
                    <div class="w3-container">
                        <div class="row">
                            <i class="fa fa-paper-plane-o fa-fw w3-xxlarge"></i>
                            <strong>Manage Request</strong>
                            <span class="w3-right"><i class="fa fa-external-link-square fa-fw"></i></span>
                        </div>
                        <hr>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-lg-3"></div>
    </div>
    @endif

    @if(Gate::allows('superadmin-only'))
    <div class="row">
        <div class="col-lg-3 col-lg-3"></div>

        <div class="col-lg-6 col-lg-6">
            <a href="{{route('admin.manage.user')}}" class="w3-text-black">
                <div class="w3-card-4 w3-teal w3-margin w3-padding-large w3-hover-opacity">
                    <div class="w3-container">
                        <div class="row">
                            <i class="fa fa-user-circle-o fa-fw w3-xxlarge"></i>
                            <strong>Manage Member</strong>
                            <span class="w3-right"><i class="fa fa-external-link-square fa-fw"></i></span>
                        </div>
                        <hr>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-lg-3"></div>
    </div>

    <div class="row">
        <div class="col-lg-3 col-lg-3"></div>

        <div class="col-lg-6 col-lg-6">
            <a href="{{route('admin.manage.privilege')}}" class="w3-text-black">
                <div class="w3-card-4 w3-teal w3-margin w3-padding-large w3-hover-opacity">
                    <div class="w3-container">
                        <div class="row">
                            <i class="fa fa-key fa-fw w3-xxlarge"></i>
                            <strong>Manage Privilege</strong>
                            <span class="w3-right"><i class="fa fa-external-link-square fa-fw"></i></span>
                        </div>
                        <hr>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-lg-3"></div>
    </div>

    <div class="row">
        <div class="col-lg-3 col-lg-3"></div>

        <div class="col-lg-6 col-lg-6">
            <a href="{{route('admin.manage.section')}}" class="w3-text-black">
                <div class="w3-card-4 w3-teal w3-margin w3-padding-large w3-hover-opacity">
                    <div class="w3-container">
                        <div class="row">
                            <i class="fa fa-group fa-fw w3-xxlarge"></i>
                            <strong>Manage Section</strong>
                            <span class="w3-right"><i class="fa fa-external-link-square fa-fw"></i></span>
                        </div>
                        <hr>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-lg-3"></div>
    </div>
    @endif

    @if(Gate::allows('mticsadviser-only'))
    <div class="row">
        <div class="col-lg-3 col-lg-3"></div>

        <div class="col-lg-6 col-lg-6">
            <a href="{{route('admin.turn-over')}}" class="w3-text-black">
                <div class="w3-card-4 w3-teal w3-margin w3-padding-large w3-hover-opacity">
                    <div class="w3-container">
                        <div class="row">
                            <i class="fa fa-rotate-right fa-fw w3-xxlarge"></i>
                            <strong>Turn Over</strong>
                            <span class="w3-right"><i class="fa fa-external-link-square fa-fw"></i></span>
                        </div>
                        <hr>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-lg-3"></div>
    </div>
    @endif

     @if(Gate::allows('superadmin-only'))
    <div class="row">
        <div class="col-lg-3 col-lg-3"></div>

        <div class="col-lg-6 col-lg-6">
            <a href="{{route('admin.backup')}}" class="w3-text-black">
                <div class="w3-card-4 w3-teal w3-margin w3-padding-large w3-hover-opacity">
                    <div class="w3-container">
                        <div class="row">
                            <i class="fa fa-database fa-fw w3-xxlarge"></i>
                            <strong>Backup & Restore</strong>
                            <span class="w3-right"><i class="fa fa-external-link-square fa-fw"></i></span>
                        </div>
                        <hr>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-lg-3"></div>
    </div>
    @endif
</div>
@endsection
