@extends('layout/control_panel')

@section('title')
Manage Course and Section
@endsection

@section('middle')
<div class="w3-container w3-margin">
  <div class="row" id="courseBanner">
    <h3>
      <i class="fa fa-graduation-cap fa-fw w3-xxlarge"></i>
      <strong>Manage Course</strong>
    </h3>
  </div>

  <div class="row" id="sectionBanner" style="display: none">
    <h3>
      <i class="fa fa-group fa-fw w3-xxlarge"></i>
      <strong>Manage Section</strong>
    </h3>
  </div>

  <hr>

  <div class="row w3-margin-top">

    <span class="w3-dropdown-hover w3-white">
      <a href="javascript:void(0)" class="w3-text-gray"><i class="fa fa-plus-square-o fa-fw"></i>Add</a>


      <div class="w3-dropdown-content w3-card-4 panel panel-default w3-animate-zoom" align="left" style="left: 0">
        <a href="#add-course" data-toggle="modal" class="w3-text-gray w3-hover-teal"><i class="fa fa-graduation-cap fa-fw w3-large"></i>Course</a>

        <a href="#addsec" data-toggle="modal" class="w3-text-gray w3-hover-teal" style="outline: 0px"><i class="fa fa-group fa-fw w3-large"></i>Section</a>

      </div>

    </span>

    <i class="fa fa-ellipsis-v fa-fw"></i>

    <a href="#master_list" class="w3-text-gray" data-toggle="modal" style="outline: 0" title="Sync Masterlist">
      <i class="fa fa-file-excel-o fa-fw"></i>
      Import Master List
    </a>
     <i class="fa fa-ellipsis-v fa-fw"></i>
      <a  href="{{url('admin/manage-sections/template-download')}}" class="w3-text-gray" style="outline: 0" title="Excel Template for Import Master List"><i class="fa fa-cloud-download fa-fw"></i> Download Excel Template</a>
      <i class="fa fa-ellipsis-v fa-fw"></i>
        <a href="#print_master_list" class="w3-text-gray" data-toggle="modal" style="outline: 0">
          <i class="fa fa-print fa-fw"></i>
          Print Master List
        </a>
  </div>

    <!-- Print MASTER LIST -->
  <div class="modal fade" id="print_master_list" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
      <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
      <h4 class="modal-title w3-text-gray" id="myModalLabel">Print Master List</h4>
      </div>
      <form action="{{ url('admin/manage-sections/print-masterlist') }}" method="POST" enctype="multipart/form-data">
      <div class="modal-body">
      <div class="w3-container">
        <div class="row">

          <div class="form-group">
             <label>Year:</label>
              <select class="form-control" id="year" name="year" placeholder="Select your Year">
               @foreach($years as $year)
                <option value='{{$year->id}}'>{{$year->year_desc}}</option>
               @endforeach
              </select>
          </div>

          <div class="form-group">
             <label>Section:</label>
              <select class="form-control" id="section" name="section" placeholder="Select your Section">
               @foreach($sections as $section)
                @if($section->section_status == 'active')
                <option value='{{$section->id}}'>{{$section->course_code}} {{$section->section_code}}</option>
                @endif
               @endforeach
              </select>
            </div>
        </div>
      </div>
      </div>
      <div class="modal-footer">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <button type="submit" class="btn btn-primary w3-text-white" id="proceed"><i class="fa fa-arrow-circle-print"></i> Print</button>
      </div>
      </form>
      </div>
    </div>
  </div>
  <!-- END Print MASTER LIST -->



  <!-- ADD MODAL (COURSE) -->
  <div class="modal fade" id="add-course" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
      <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
      <h4 class="modal-title w3-text-gray" id="myModalLabel">Course Information</h4>
      </div>
      <form action="{{ url('admin/manage-sections/course-store') }}" method="POST" enctype="multipart/form-data">
      <div class="modal-body">
      <div class="w3-container">
        <div class="row">

          <div class="form-group">
             <p><b>Course Code: </b></p>
              <input type="text" name="course_code" id="course_code" tabindex="1" class="form-control" placeholder="Course Code" >
            </div>

            <div class="form-group">
             <p><b>Course Name: </b></p>
              <input type="text" name="course_name" id="course_name" tabindex="1" class="form-control" placeholder="Course Name" >
            </div>

            <div class="form-group">
             <p><b>Course Duration: </b></p>
              <input type="text" name="course_year_count" id="course_year_count" tabindex="1" class="form-control" placeholder="Course Duration" >
            </div>
        </div>
      </div>
      </div>
      <div class="modal-footer">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <button type="submit" class="btn btn-default w3-green w3-text-white"><i class="fa fa-save"></i> Save</button>
      </div>
      </form>
      </div>
    </div>
  </div>
  <!-- END ADD MODAL (COURSE) -->

  <!-- ADD (SECTION) -->
  <div class="modal fade" id="addsec" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
      <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
      <h4 class="modal-title w3-text-gray" id="myModalLabel">Section Information</h4>
      </div>
      <form action="{{ url('admin/manage-sections/section-store') }}" method="POST" enctype="multipart/form-data">
      <div class="modal-body">
      <div class="w3-container">
        <div class="row">

          <div class="form-group">
             <label>Course:</label>
              <select class="form-control" id="course" name="course" placeholder="Select Course Code">
               @foreach($courses as $course)


                <option value='{{$course->id}}'>{{$course->course_code}}</option>

               @endforeach
              </select>
            </div>

          <div class="form-group">
             <p><b>Section Code: </b></p>
              <input type="text" name="section_code" id="section_code" tabindex="1" class="form-control" placeholder="Section Code" >
            </div>
        </div>
      </div>
      </div>
      <div class="modal-footer">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <button type="submit" class="btn btn-default w3-green w3-text-white"><i class="fa fa-save"></i> Save</button>
      </div>
      </form>
      </div>
    </div>
  </div>
  <!-- END ADD -->

  <!-- MASTER LIST -->
  <div class="modal fade" id="master_list" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
      <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
      <h4 class="modal-title w3-text-gray" id="myModalLabel">Master List</h4>
      </div>
      <form action="{{ url('admin/manage-sections/manage-masterlist') }}" method="POST" enctype="multipart/form-data">
      <div class="modal-body">
      <div class="w3-container">
        <div class="row">

          <div class="form-group">
             <label>Year:</label>
              <select class="form-control" id="year" name="year" placeholder="Select your Year">
               @foreach($years as $year)
                <option value='{{$year->id}}'>{{$year->year_desc}}</option>
               @endforeach
              </select>
          </div>

          <div class="form-group">
             <label>Section:</label>
              <select class="form-control" id="section" name="section" placeholder="Select your Section">
               @foreach($sections as $section)

                <option value='{{$section->id}}'>{{$section->course_code}} {{$section->section_code}}</option>

               @endforeach
              </select>
            </div>
        </div>
      </div>
      </div>
      <div class="modal-footer">
      <label id="import-file-name" for="import_file" style="cursor: pointer;" class="btn btn-default w3-green" title="Bulk Add"><i class="fa fa-file-excel-o fa-fw w3-text-white w3-large"></i> Import Master List</label>
              <input type="file" name="import_file" id="import_file" style="opacity: 0; position: absolute; z-index: -1;" />
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <button type="submit" class="btn btn-primary w3-text-white" id="proceed" style="display: none"><i class="fa fa-arrow-circle-right"></i> Proceed</button>
      </div>
      </form>
      </div>
    </div>
  </div>
  <!-- END MASTER LIST -->

  <div class="row w3-margin-top">
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#courseTab" class="w3-text-black" onclick="changeBanner('course')">Course</a></li>

      <li><a data-toggle="tab" href="#sectionTab" class="w3-text-black" onclick="changeBanner('section')">Section</a></li>
    </ul>
  </div>

  <div class="tab-content">
    <div id="courseTab" class="tab-pane fade in active">
      <div class="row w3-margin-top">
        <div class="table-responsive">
          <table class="table table-bordered table-hover" id="courseTable">
            <thead>
              <tr>
               <th class="w3-center">Course Code</th>
               <th class="w3-center">Course Name Name</th>
               <th class="w3-center">Course Duration</th>
               <th class="w3-center">Action</th>
              </tr>
            </thead>
            <tbody class="w3-text-gray">
              @foreach($courses as $course)
              <tr>
                <td class="w3-center">{{$course->course_code}}</td>
                <td class="w3-center">{{$course->course_name}}</td>
                <td class="w3-center">{{$course->course_year_count}} year/s</td>
                <td class="w3-center">
                  <a class="w3-text-orange w3-large" data-toggle="modal" href="#editcourse-{{$course->id}}" style="outline: 0"><i class="fa fa-pencil-square-o" data-toggle="tooltip" data-placement="bottom" title="Edit Course"></i></a>

                  <a href="javascript:void" onclick="$('#formDeleteCourse-{{$course->id}}').submit()" class="w3-text-red w3-large"><i class="fa fa-remove fa-fw" data-toggle="tooltip" data-placement="bottom" title="Delete Course"></i></a>


                <form action="{{ url('admin/manage-sections/'.$course->id.'/course-delete') }}" method="POST" enctype="multipart/form-data" id="formDeleteCourse-{{$course->id}}">
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </form>

                </td>


                </td>
              </tr>

              <!-- EDIT MODAL (COURSE) -->
              <div class="modal fade" id="editcourse-{{$course->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog modal-md" role="document">
              <div class="modal-content">
              <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
              <h4 class="modal-title w3-text-gray" id="myModalLabel">Course Information</h4>
              </div>
              <form action="{{ url('admin/manage-sections/'.$course->id.'/course-edit') }}" method="POST" enctype="multipart/form-data">
              <div class="modal-body">
              <div class="w3-container">
                <div class="row">

                  <div class="form-group">
                     <label>Course Code:</label>
                      <input type="text" name="course_code" id="course_code" tabindex="1" class="form-control" placeholder="Course Code" value="{{$course->course_code}}" >
                    </div>

                    <div class="form-group">
                     <label>Course Name:</label>
                      <input type="text" name="course_name" id="course_name" tabindex="1" class="form-control" placeholder="Course Name" value="{{$course->course_name}}" >
                    </div>

                    <div class="form-group">
                     <label>Course Duration:</label>
                      <input type="text" name="course_year_count" id="course_year_count" tabindex="1" class="form-control" placeholder="Course Year Count" value="{{$course->course_year_count}}">
                    </div>
                </div>
              </div>
              </div>
              <div class="modal-footer">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <button type="submit" class="btn btn-default w3-green w3-text-white"><i class="fa fa-save"></i> Save</button>
              </div>
              </form>
              </div>
              </div>
              </div>
              <!-- END EDIT MODAL (COURSE) -->
              @endforeach
            </tbody>
          </table>

        </div>
      </div>
    </div>

    <div id="sectionTab" class="tab-pane fade in">
      <div class="row w3-margin-top">
        <div class="table-responsive">
          <table class="table table-bordered table-hover" id="sectionTable">
            <thead>
              <tr>
               <th class="w3-center">Section Code</th>
               <th class="w3-center">Course Code</th>
               <th class="w3-center">Status</th>
               <th class="w3-center">Action</th>
              </tr>
            </thead>
            <tbody class="w3-text-gray">
              @foreach($allsections as $section)
              <tr>
                <td class="w3-center">{{$section->section_code}}</td>
                <td class="w3-center">{{$section->course_code}}</td>
                <td class="w3-center">{{$section->section_status}}</td>
                <td class="w3-center">
                  <a class="w3-text-orange w3-large" data-toggle="modal" href="#editsec-{{$section->id}}" style="outline: 0"><i class="fa fa-pencil-square-o" data-toggle="tooltip" data-placement="bottom" title="Edit Section"></i></a>

                  <a href="javascript:void" onclick="$('#formDeleteSection-{{$section->id}}').submit()" class="w3-text-red w3-large"><i class="fa fa-remove fa-fw" data-toggle="tooltip" data-placement="bottom" title="Delete Section"></i></a>

                  <form action="{{ url('admin/manage-sections/'.$section->id.'/section-delete') }}" method="POST" enctype="multipart/form-data" id="formDeleteSection-{{$section->id}}">
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </form>
                </td>
              </tr>
              <!-- EDIT SECTION MODAL -->
              <div class="modal fade" id="editsec-{{$section->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog modal-md" role="document">
              <div class="modal-content">
              <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
              <h4 class="modal-title w3-text-gray" id="myModalLabel">Section Information</h4>
              </div>
              <form action="{{ url('admin/manage-sections/'.$section->id.'/section-edit') }}" method="POST" enctype="multipart/form-data">
              <div class="modal-body">
              <div class="w3-container">
                <div class="row">

                  <div class="form-group">
                     <label>Course:</label>
                      <select class="form-control" id="course" name="course" placeholder="Select Course Code">
                       @foreach($courses as $course)
                        @if($course->id == $section->course_id)
                        <option selected value='{{$course->id}}'>{{$course->course_code}}</option>
                        @else
                        <option value='{{$course->id}}'>{{$course->course_code}}</option>
                        @endif
                       @endforeach
                      </select>
                    </div>

                  <div class="form-group">
                     <label>Section Code:</label>
                      <input type="text" name="section_code" id="section_code" tabindex="1" class="form-control" placeholder="Section Code" value="{{$section->section_code}}" >
                    </div>

                    <div class="form-group">
                     <label>Status:</label>
                      <select class="form-control" id="status" name="status" placeholder="Select Section Status">

                        @if($section->section_status == 'active')
                        <option selected value='active'>Active</option>
                        <option value='inactive'>Inactive</option>
                        @else
                        <option value='active'>Active</option>
                        <option selected value='inactive'>Inactive</option>
                        @endif

                      </select>
                    </div>

                </div>
              </div>
              </div>
              <div class="modal-footer">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <button type="submit" class="btn btn-default w3-green w3-text-white"><i class="fa fa-save"></i> Save</button>
              </div>
              </form>
              </div>
              </div>
              </div>
              <!-- END EDIT MODAL(SECTION) -->
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>


</div>

<input type="text" id="refreshed" value="no" style="display: none">

<!-- END MASTER LIST -->
<script type="text/javascript">

  $(document).ready(function(){
  var e=document.getElementById("refreshed");
  if(e.value=="no"){
    e.value="yes"
}
  else{location.reload();}
  });
// jQuery(window).load(function ($) {
  var input = document.getElementById( 'import_file' );
  var infoArea = document.getElementById( 'import-file-name' );

  input.addEventListener('change', showFileName);

  function showFileName( event ) {

    $(".filename").show()
    // the change event gives us the input it occurred in
    var input = event.srcElement;

    // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
    var fileName = input.files[0].name;

    // use fileName however fits your app best, i.e. add it into a div
    infoArea.textContent = fileName;

    $("#proceed").show();
  };

  // });

</script>

<script type="text/javascript">
$(document).ready( function () {
  $('#courseTable').DataTable();
  $('#sectionTable').DataTable();
});
</script>

<script type="text/javascript">
  window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove();
    });
}, 4000);
</script>

<script type="text/javascript">
  function changeBanner(tab) {
    if (tab == "course") {
      $("#courseBanner").show()
      $("#sectionBanner").hide()
    }

    else if (tab == "section") {
      $("#sectionBanner").show()
      $("#courseBanner").hide()
    }
  }
</script>
@endsection
