@extends('layout/control_panel')

@section('title')
Manage Form
@endsection

@section('middle')
<div class="w3-container w3-margin">
  <div class="row">
    <h3>
      <i class="fa fa-pie-chart fa-fw w3-xxlarge"></i>
      <strong>Manage Form</strong>
    </h3>
  </div>

  <hr>

  <div class="row">
    <p>
      <a href="{{url('admin/manage-form/store')}}" class="w3-text-gray" style="outline: 0"><i class="fa fa-plus-square-o fa-fw"></i> Add</a>
    </p>
  </div>

  <br>

  <div class="row">
    <div class="table-responsive">
        <table class="table table-bordered table-hover" id="surveyTable">
          <thead>
            <tr>
             <th class="w3-center">Title</th>
             <th class="w3-center">Description</th>
             <th class="w3-center">Author</th>
             <th class="w3-center">Created at</th>
             <th class="w3-center">Action</th>
            </tr>
          </thead>
          <tbody class="w3-text-gray">
            @foreach($forms as $form)
            <tr>
              <td class="w3-center">{{$form->title}}</td>
              <td class="w3-center">{{$form->description}}</td>
                <td class="w3-center">
                  {{$form->member['first_name']}} {{$form->member['last_name']}}
                </td>
              <td class="w3-center">{{$form->created_at}}</td>
              <td class="w3-center">
                <a class="w3-text-blue" href="{{url('admin/manage-form/view/' . $form->id)}}" style="outline: 0"><i class="fa fa-eye fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="View Form"></i></a>

                <a class="w3-text-orange" href="{{url('admin/manage-form/edit-view/'.$form->id)}}" style="outline: 0"><i class="fa fa-pencil-square-o fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Edit Form"></i></a>

                <a class="w3-text-red" href="javascript:void(0)" onclick="deleteSurvey( {{$form->id}} )" style="outline: 0"><i class="fa fa-trash-o fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Delete Form"></i></a>

                <a class="w3-text-amber" href="{{url('admin/manage-form/view-response/'.$form->id)}}" style="outline: 0"><i class="fa fa-pie-chart fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="View Analytics"></i></a>


                <a class="w3-text-green" href="javascript:void(0)" onclick="doneSurvey( {{$form->id}} )" style="outline: 0"><i class="fa fa-check-circle-o fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Done Form"></i></a>

                <form action="{{url('admin/manage-form/delete/'.$form->id)}}" method="POST" id="formSurvey-{{$form->id}}">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </form>                

                <form action="{{url('admin/manage-form/done/'.$form->id)}}" method="POST" id="formSurveydone-{{$form->id}}">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </form>

              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
    </div>
  </div>
</div>


<script type="text/javascript">
$(document).ready( function () {
  $('#surveyTable').DataTable();
} );
</script>

<script type="text/javascript">
function deleteSurvey( id ) {
  swal({
  title: "Are you sure?",
  text: "Once deleted, you will not be able to undo this action!",
  icon: "warning",
  buttons: true,
  dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      $("#formSurvey-" + id).submit();
    }
  });
}

function doneSurvey( id ) {
  swal({
  title: "Are you sure?",
  text: "Once Done, you will not be able to undo this action!",
  icon: "warning",
  buttons: true,
  dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      $("#formSurveydone-" + id).submit();
    }
  });
}


</script>

<script type="text/javascript">
  window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove();
    });
}, 4000);
</script>


@endsection
