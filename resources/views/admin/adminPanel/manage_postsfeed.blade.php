@extends('layout/control_panel')

@section('title')
Manage Post
@endsection

@section('middle')
<div class="w3-container w3-margin">

  <div class="row">
    <h3>
      <i class="fa fa-newspaper-o fa-fw w3-xxlarge"></i>
      <strong>Manage Posts</strong>
    </h3>
  </div>

  <hr>

  <div class="row">
    <ul class="nav nav-tabs">
      <li class="active">
        <a data-toggle="tab" href="#published" class="w3-text-black">Published</a>
      </li>

      <li>
        <a data-toggle="tab" href="#reported" class="w3-text-black">Reported</a>
      </li>
    </ul>
  </div>

  <div class="tab-content">
    <div id="published" class="tab-pane fade in active">
      <div class="row w3-margin-top">
        <div class="table-responsive">
        <!-- Published -->
          <table class="table table-bordered table-hover" id="publishedTable">
            <thead>
              <tr>
               <th class="w3-center">Title</th>
               <th class="w3-center">Author</th>
               <th class="w3-center">Category</th>
               <th class="w3-center">Post Image</th>
               <th class="w3-center">Status</th>
               <th class="w3-center">Created at</th>
               <th class="w3-center">Action</th>
              </tr>
            </thead>
            <tbody class="w3-text-gray">
              @foreach($posts as $post)
              <tr>
                <td class="w3-center">{{$post->title}}</td>
                <td class="w3-center">{{$post->first_name}} {{$post->last_name}}</td>
                <td class="w3-center">{{$post->type}}</td>
                <td class="w3-center"><img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$post->image) : asset('images/'.$post->image)}}" style="width: 140px"></td>
                <td class="w3-center">
                <form action="{{ url('admin/manage-postsfeed/status/'.$post->id) }}" method="POST">
                    <select class="form-control
                    @if($post->status == 'reported') w3-orange @endif
                    @if($post->status == 'published') w3-green @endif
                  " id="status" name="status" @if($post->status == 'published') onchange="btnTrigger.click()" @else onchange="this.form.submit()" @endif>
                      <option value="published" @if($post->status == 'published') selected @endif>Published</option>
                      <option value="reported" @if($post->status == 'reported') selected @endif>Reported</option>
                    </select>
                    <button type="button" data-toggle="modal" data-target="#report-{{$post->id}}" id="btnTrigger" style="display: none"></button>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  </form>
                </td>
                <td class="w3-center">{{$post->created_at}}</td>
                <td class="w3-center">
                  <a href="{{url('/member/profile/post/'.$post->id.'/'.$post->slug)}}" class="w3-text-blue" style="outline: 0"><i class="fa fa-eye fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="View Post"></i></a>

                  <a href="javascript::void(0)" onclick="deletePublished( {{$post->id}} )" class="w3-text-red" style="outline: 0"><i class="fa fa-archive fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Archive Post"></i></a>

                  <form action="{{ url('admin/manage-postsfeed/'.$post->id.'/delete/') }}" method="POST" id="formPublished-{{$post->id}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  </form>
                </td>
              </tr>

              <!-- REPORT -->

              <!-- RELOAD ON ESCAPE -->
              <script>
                $(document).keyup(function(e) {
                  if (e.keyCode == 27) {
                      window.location.reload();
                  }
                });
              </script>
              <!-- END -->

              <div class="modal fade" id="report-{{$post->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-md" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" id="btnClose" class="close" data-dismiss="modal" aria-label="Close" onclick="window.location.reload()"><span aria-hidden="true">x</span></button>
                      <h4 class="modal-title w3-text-gray" id="myModalLabel">Report Post</h4>
                    </div>
                  <form action="{{ url('member/profile/post/'.$post->id.'/'.$post->slug.'/report' ) }}" method="POST" enctype="multipart/form-data">
                    <div class="modal-body">
                      <p>Let's us know why you want to report '{{$post->title}}' Post?</p>

                    <div class="form-group">
                      <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="reason" required placeholder="Reason" rows="3"></textarea>
                    </div>


                      <div class="modal-footer">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button type="submit" class="btn btn-default w3-green w3-text-white"><i class="fa fa-arrow-circle-o-right fa-fw w3-text-white"></i> Proceed</button>
                      </div>

                    </div>
                  </form>
                  </div>
                </div>
              </div>

              <!-- END REPORT -->

              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>

    <div id="reported" class="tab-pane fade">
      <div class="row w3-margin-top">
        <div class="table-responsive">
          <table class="table table-bordered table-hover" id="reportedTable">
            <thead>
              <tr>
               <th class="w3-center">Title</th>
               <th class="w3-center">Author</th>
               <th class="w3-center">Category</th>
               <th class="w3-center">Post Image</th>
               <th class="w3-center">Status</th>
               <th class="w3-center">Created at</th>
               <th class="w3-center">Action</th>
              </tr>
            </thead>
            <tbody class="w3-text-gray">
              @foreach($reported as $post)
              <tr>
                <td class="w3-center">{{$post->title}}</td>
                <td class="w3-center">{{$post->first_name}} {{$post->last_name}}</td>
                <td class="w3-center">{{$post->type}}</td>
                <td class="w3-center"><img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$post->image) : asset('images/'.$post->image)}}" style="width: 140px"></td>
                <td class="w3-center">
                <form action="{{ url('admin/manage-postsfeed/status/'.$post->id) }}" method="POST">
                    <select class="form-control
                    @if($post->status == 'reported') w3-orange @endif
                    @if($post->status == 'published') w3-green @endif
                  " id="status" name="status" @if($post->status == 'published') onchange="btnTrigger.click()" @else onchange="this.form.submit()" @endif>
                      <option value="published" @if($post->status == 'published') selected @endif>Published</option>
                      <option value="reported" @if($post->status == 'reported') selected @endif>Reported</option>
                    </select>
                    <button type="button" data-toggle="modal" data-target="#report-{{$post->id}}" id="btnTrigger" style="display: none"></button>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  </form>
                </td>
                <td class="w3-center">{{$post->created_at}}</td>
                <td class="w3-center">
                  <a href="{{url('/member/profile/post/'.$post->id.'/'.$post->slug)}}" class="w3-text-blue" style="outline: 0"><i class="fa fa-eye fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="View Post"></i></a>

                  <a href="#reports-{{$post->id}}" data-toggle="modal" class="w3-text-amber" style="outline: 0"><i class="fa fa-exclamation-triangle fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="View Reports"></i></a>


                  <a href="javascript::void(0)" onclick="deletePublished( {{$post->id}} )" class="w3-text-red" style="outline: 0"><i class="fa fa-archive fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Archive Post"></i></a>

                  <form action="{{ url('admin/manage-postsfeed/'.$post->id.'/delete/') }}" method="POST" id="formPublished-{{$post->id}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  </form>
                </td>
              </tr>

              <!-- REPORT -->

              <!-- RELOAD ON ESCAPE -->
              <script>
                $(document).keyup(function(e) {
                  if (e.keyCode == 27) {
                      window.location.reload();
                  }
                });
              </script>
              <!-- END -->

              <!-- View -->
              <div class="modal fade" id="reports-{{$post->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-md" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                      <h4 class="modal-title w3-text-gray" id="myModalLabel">Reports</h4>
                    </div>
                    <div class="modal-body">

                      @foreach($reports as $report)
                      @if($report->post_id == $post->id)
                      <div class="row w3-margin">
                        <img src="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$report->member->avatar) : asset('images/'.$report->member->avatar) }}" alt="Avatar" class="w3-left w3-circle w3-margin-right w3-hide-small" style="width: 80px; height: 80px">

                        <img src="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$report->member->avatar) : asset('images/'.$report->member->avatar) }}" alt="Avatar" class="w3-left w3-circle w3-margin-right w3-hide-large" style="width: 50px; height: 50px">

                        <span>
                          <strong>{{ $report->member->first_name }} {{ $report->member->last_name }}</strong>
                          <br>
                          {{$report->created_at}}
                          <br>
                          <br>
                          <i class="fa fa-comments-o fa-fw w3-large"></i>
                          {{$report->reason}}
                        </span>
                      </div>
                      @if(!$loop->last)
                        <hr>
                      @endif
                      @endif
                      @endforeach

                    </div>
                  </div>
                </div>
              </div>
              <!-- END VIEW -->

              @endforeach
            </tbody>
          </table>

        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
$(document).ready( function () {
  $('#publishedTable').DataTable();
  $('#reportedTable').DataTable();
} );
</script>

<script type="text/javascript">
function deletePublished( id ) {
  swal({
  title: "Are you sure?",
  text: "Once deleted, you will not be able to undo this action!",
  icon: "warning",
  buttons: true,
  dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      $("#formPublished-" + id).submit();
    }
  });
}
</script>

<script type="text/javascript">
  window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove();
    });
}, 4000);
</script>
@endsection
