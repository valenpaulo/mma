@extends('layout/layout')

@section('title')
Admin Panel
@endsection

@section('content')
<div class="container" style="margin-top: 120px">
    <div class="row">
        <h3>
            <i class="fa fa-id-badge fa-fw w3-xxlarge"></i>
            <strong>Admin Panel</strong>
        </h3>
    </div>

    <hr>

    <div class="row">
        <div class="col-lg-3 col-lg-3"></div>

        <div class="col-lg-6 col-lg-6">
            <a href="{{url('admin/manage-event')}}" class="w3-text-black">
                <div class="w3-card-4 w3-teal w3-margin w3-padding-large w3-hover-opacity">
                    <div class="w3-container">
                        <div class="row">
                            <i class="fa fa-calendar fa-fw w3-xxlarge"></i>
                            <strong>Manage Event</strong>
                            <span class="w3-right"><i class="fa fa-external-link-square fa-fw"></i></span>
                        </div>
                        <hr>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-lg-3"></div>
    </div>

    @if(Gate::allows('president-only'))
    <div class="row">
        <div class="col-lg-3 col-lg-3"></div>

        <div class="col-lg-6 col-lg-6">
            <a href="{{route('pres.manage.budget')}}" class="w3-text-black">
                <div class="w3-card-4 w3-teal w3-margin w3-padding-large w3-hover-opacity">
                    <div class="w3-container">
                        <div class="row">
                            <i class="fa fa-paper-plane-o fa-fw w3-xxlarge"></i>
                            <strong>Budget Request</strong>
                            <span class="w3-right"><i class="fa fa-external-link-square fa-fw"></i></span>
                        </div>
                        <hr>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-lg-3"></div>
    </div>


    <div class="row">
        <div class="col-lg-3 col-lg-3"></div>

        <div class="col-lg-6 col-lg-6">
            <a href="{{url('admin/president/requested-event-breakdown')}}" class="w3-text-black">
                <div class="w3-card-4 w3-teal w3-margin w3-padding-large w3-hover-opacity">
                    <div class="w3-container">
                        <div class="row">
                            <i class="fa fa-paper-plane-o fa-fw w3-xxlarge"></i>
                            <strong>Event Breakdown Request</strong>
                            <span class="w3-right"><i class="fa fa-external-link-square fa-fw"></i></span>
                        </div>
                        <hr>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-lg-3"></div>
    </div>


    <div class="row">
        <div class="col-lg-3 col-lg-3"></div>

        <div class="col-lg-6 col-lg-6">
            <a href="{{url('admin/president/purchased-item-reported')}}" class="w3-text-black">
                <div class="w3-card-4 w3-teal w3-margin w3-padding-large w3-hover-opacity">
                    <div class="w3-container">
                        <div class="row">
                            <i class="fa fa-exclamation-triangle fa-fw w3-xxlarge"></i>
                            <strong>Manage Reported Issues on Purchased Item</strong>
                            <span class="w3-right"><i class="fa fa-external-link-square fa-fw"></i></span>
                        </div>
                        <hr>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-lg-3"></div>
    </div>

    <div class="row">
        <div class="col-lg-3 col-lg-3"></div>

        <div class="col-lg-6 col-lg-6">
            <a href="{{url('admin/president/bank-transaction-request-reported')}}" class="w3-text-black">
                <div class="w3-card-4 w3-teal w3-margin w3-padding-large w3-hover-opacity">
                    <div class="w3-container">
                        <div class="row">
                            <i class="fa fa-exclamation-triangle fa-fw w3-xxlarge"></i>
                            <strong>Manage Reported Issues on Bank Transaction</strong>
                            <span class="w3-right"><i class="fa fa-external-link-square fa-fw"></i></span>
                        </div>
                        <hr>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-lg-3"></div>
    </div>
    @endif

    @if(Gate::allows('finance-only'))
    <div class="row">
        <div class="col-lg-3 col-lg-3"></div>

        <div class="col-lg-6 col-lg-6">
            <a href="{{route('admin.manage.fund')}}" class="w3-text-black">
                <div class="w3-card-4 w3-teal w3-margin w3-padding-large w3-hover-opacity">
                    <div class="w3-container">
                        <div class="row">
                            <i class="fa fa-money fa-fw w3-xxlarge"></i>
                            <strong>Manage MTICS Payment</strong>
                            <span class="w3-right"><i class="fa fa-external-link-square fa-fw"></i></span>
                        </div>
                        <hr>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-lg-3"></div>
    </div>




    <div class="row">
        <div class="col-lg-3 col-lg-3"></div>

        <div class="col-lg-6 col-lg-6">
            <a href="{{route('admin.manage.budget')}}" class="w3-text-black">
                <div class="w3-card-4 w3-teal w3-margin w3-padding-large w3-hover-opacity">
                    <div class="w3-container">
                        <div class="row">
                            <i class="fa fa-file-text-o fa-fw w3-xxlarge"></i>
                            <strong>Manage Monthly Budget</strong>
                            <span class="w3-right"><i class="fa fa-external-link-square fa-fw"></i></span>
                        </div>
                        <hr>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-lg-3"></div>
    </div>

    <div class="row">
        <div class="col-lg-3 col-lg-3"></div>

        <div class="col-lg-6 col-lg-6">
            <a href="{{url('admin/finance/manage-mtics-budget/expenses')}}" class="w3-text-black">
                <div class="w3-card-4 w3-teal w3-margin w3-padding-large w3-hover-opacity">
                    <div class="w3-container">
                        <div class="row">
                            <i class="fa fa-calculator fa-fw w3-xxlarge"></i>
                            <strong>Manage Expenses</strong>
                            <span class="w3-right"><i class="fa fa-external-link-square fa-fw"></i></span>
                        </div>
                        <hr>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-lg-3"></div>
    </div>

    <div class="row">
        <div class="col-lg-3 col-lg-3"></div>

        <div class="col-lg-6 col-lg-6">
            <a href="{{url('admin/finance/manage-bank-transaction/mtics')}}" class="w3-text-black">
                <div class="w3-card-4 w3-teal w3-margin w3-padding-large w3-hover-opacity">
                    <div class="w3-container">
                        <div class="row">
                            <i class="fa fa-bank fa-fw w3-xxlarge"></i>
                            <strong>Bank Transaction</strong>
                            <span class="w3-right"><i class="fa fa-external-link-square fa-fw"></i></span>
                        </div>
                        <hr>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-lg-3"></div>
    </div>

    <div class="row">
        <div class="col-lg-3 col-lg-3"></div>

        <div class="col-lg-6 col-lg-6">
            <a href="{{url('admin/finance/requested-money')}}" class="w3-text-black">
                <div class="w3-card-4 w3-teal w3-margin w3-padding-large w3-hover-opacity">
                    <div class="w3-container">
                        <div class="row">
                            <i class="fa fa-paper-plane-o fa-fw w3-xxlarge"></i>
                            <strong>Money Request</strong>
                            <span class="w3-right"><i class="fa fa-external-link-square fa-fw"></i></span>
                        </div>
                        <hr>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-lg-3"></div>
    </div>

    @endif

    @if(Gate::allows('mticsadviser-only'))
    <div class="row">
        <div class="col-lg-3 col-lg-3"></div>

        <div class="col-lg-6 col-lg-6">
            <a href="{{url('admin/adviser/mtics-bank-transaction-request')}}" class="w3-text-black">
                <div class="w3-card-4 w3-teal w3-margin w3-padding-large w3-hover-opacity">
                    <div class="w3-container">
                        <div class="row">
                            <i class="fa fa-bank fa-fw w3-xxlarge"></i>
                            <strong>Bank Transaction</strong>
                            <span class="w3-right"><i class="fa fa-external-link-square fa-fw"></i></span>
                        </div>
                        <hr>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-lg-3"></div>
    </div>
    @endif

    @if(Gate::allows('activity-only') or Gate::allows('docu-only') or Gate::allows('president-only')  )
    <div class="row">
        <div class="col-lg-3 col-lg-3"></div>

        <div class="col-lg-6 col-lg-6">
            <a href="{{url('admin/activities')}}" class="w3-text-black">
                <div class="w3-card-4 w3-teal w3-margin w3-padding-large w3-hover-opacity">
                    <div class="w3-container">
                        <div class="row">
                            <i class="fa fa-calendar fa-fw w3-xxlarge"></i>
                            <strong>Calendar of Activities</strong>
                            <span class="w3-right"><i class="fa fa-external-link-square fa-fw"></i></span>
                        </div>
                        <hr>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-lg-3"></div>
    </div>
    @endif

    @if(Gate::allows('docu-only'))
    <div class="row">
        <div class="col-lg-3 col-lg-3"></div>

        <div class="col-lg-6 col-lg-6">
            <a href="{{route('admin.document')}}" class="w3-text-black">
                <div class="w3-card-4 w3-teal w3-margin w3-padding-large w3-hover-opacity">
                    <div class="w3-container">
                        <div class="row">
                            <i class="fa fa-file-o fa-fw w3-xxlarge"></i>
                            <strong>Manage Documents</strong>
                            <span class="w3-right"><i class="fa fa-external-link-square fa-fw"></i></span>
                        </div>
                        <hr>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-lg-3"></div>
    </div>

    <div class="row">
        <div class="col-lg-3 col-lg-3"></div>

        <div class="col-lg-6 col-lg-6">
            <a href="{{route('admin.document.bylaws')}}" class="w3-text-black">
                <div class="w3-card-4 w3-teal w3-margin w3-padding-large w3-hover-opacity">
                    <div class="w3-container">
                        <div class="row">
                            <i class="fa fa-book fa-fw w3-xxlarge"></i>
                            <strong>Manage MTICS By-laws</strong>
                            <span class="w3-right"><i class="fa fa-external-link-square fa-fw"></i></span>
                        </div>
                        <hr>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-lg-3"></div>
    </div>
    @endif

    @if(Gate::allows('internal-only'))
    <div class="row">
        <div class="col-lg-3 col-lg-3"></div>

        <div class="col-lg-6 col-lg-6">
            <a href="{{route('admin.internal')}}" class="w3-text-black">
                <div class="w3-card-4 w3-teal w3-margin w3-padding-large w3-hover-opacity">
                    <div class="w3-container">
                        <div class="row">
                            <i class="fa fa-file-word-o fa-fw w3-xxlarge"></i>
                            <strong>Manage Letters</strong>
                            <span class="w3-right"><i class="fa fa-external-link-square fa-fw"></i></span>
                        </div>
                        <hr>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-lg-3"></div>
    </div>
    @endif

    @if(Gate::allows('logistics-only'))
    <div class="row">
        <div class="col-lg-3 col-lg-3"></div>

        <div class="col-lg-6 col-lg-6">
            <a href="{{route('admin.logistics.inventory')}}" class="w3-text-black">
                <div class="w3-card-4 w3-teal w3-margin w3-padding-large w3-hover-opacity">
                    <div class="w3-container">
                        <div class="row">
                            <i class="fa fa-suitcase fa-fw w3-xxlarge"></i>
                            <strong>Manage Inventory</strong>
                            <span class="w3-right"><i class="fa fa-external-link-square fa-fw"></i></span>
                        </div>
                        <hr>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-lg-3"></div>
    </div>




    <div class="row">
        <div class="col-lg-3 col-lg-3"></div>

        <div class="col-lg-6 col-lg-6">
            <a href="{{route('admin.manage.purchases')}}" class="w3-text-black">
                <div class="w3-card-4 w3-teal w3-margin w3-padding-large w3-hover-opacity">
                    <div class="w3-container">
                        <div class="row">
                            <i class="fa fa-shopping-cart fa-fw w3-xxlarge"></i>
                            <strong>Manage Purchases</strong>
                            <span class="w3-right"><i class="fa fa-external-link-square fa-fw"></i></span>
                        </div>
                        <hr>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-lg-3"></div>
    </div>
    @endif



    @if(Gate::allows('auditor-only') or Gate::allows('finance-only') or Gate::allows('president-only') or Gate::allows('mticsadviser-only') )
    <div class="row">
        <div class="col-lg-3 col-lg-3"></div>

        <div class="col-lg-6 col-lg-6">
            <a href="{{url('admin/finance/financial-reports')}}" class="w3-text-black">
                <div class="w3-card-4 w3-teal w3-margin w3-padding-large w3-hover-opacity">
                    <div class="w3-container">
                        <div class="row">
                            <i class="fa fa-files-o fa-fw w3-xxlarge"></i>
                            <strong>Financial Report</strong>
                            <span class="w3-right"><i class="fa fa-external-link-square fa-fw"></i></span>
                        </div>
                        <hr>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-lg-3"></div>
    </div>
    @endif

    @if(Gate::allows('auditor-only'))
    <div class="row">
        <div class="col-lg-3 col-lg-3"></div>

        <div class="col-lg-6 col-lg-6">
            <a href="{{url('admin/auditor')}}" class="w3-text-black">
                <div class="w3-card-4 w3-teal w3-margin w3-padding-large w3-hover-opacity">
                    <div class="w3-container">
                        <div class="row">
                            <i class="fa fa-paper-plane-o fa-fw w3-xxlarge"></i>
                            <strong>Reimbursement Request</strong>
                            <span class="w3-right"><i class="fa fa-external-link-square fa-fw"></i></span>
                        </div>
                        <hr>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-lg-3"></div>
    </div>
    @endif

     @if(Gate::allows('info-only') )
    <div class="row">
        <div class="col-lg-3 col-lg-3"></div>

        <div class="col-lg-6 col-lg-6">
            <a href="{{url('admin/info')}}" class="w3-text-black">
                <div class="w3-card-4 w3-teal w3-margin w3-padding-large w3-hover-opacity">
                    <div class="w3-container">
                        <div class="row">
                            <i class="fa fa-suitcase fa-fw w3-xxlarge"></i>
                            <strong>Send SMS</strong>
                            <span class="w3-right"><i class="fa fa-external-link-square fa-fw"></i></span>
                        </div>
                        <hr>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-lg-3"></div>
    </div>
    @endif

    <br>
    <hr>


    <div class="row">
        <div class="col-lg-3 col-lg-3"></div>

        <div class="col-lg-6 col-lg-6">
            <div align="center">
                <h3>
                    <i class="fa fa-lock fa-fw w3-xxlarge"></i>
                    <strong>Default Control</strong>
                </h3>
            </div>
        <a href="{{url('admin/request-to-buy')}}" class="w3-text-black">
                <div class="w3-card-4 w3-teal w3-margin w3-padding-large w3-hover-opacity">
                    <div class="w3-container">
                        <div class="row">
                            <i class="fa fa-shopping-basket fa-fw w3-xxlarge"></i>
                            <strong>Purchase Request</strong>
                            <span class="w3-right"><i class="fa fa-external-link-square fa-fw"></i></span>
                        </div>
                        <hr>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-lg-3"></div>
    </div>

    <div class="row">
        <div class="col-lg-3 col-lg-3"></div>

        <div class="col-lg-6 col-lg-6">
            <a href="{{route('admin.manage.postsfeed')}}" class="w3-text-black">
                <div class="w3-card-4 w3-teal w3-margin w3-padding-large w3-hover-opacity">
                    <div class="w3-container">
                        <div class="row">
                            <i class="fa fa-newspaper-o fa-fw w3-xxlarge"></i>
                            <strong>Manage Post</strong>
                            <span class="w3-right"><i class="fa fa-external-link-square fa-fw"></i></span>
                        </div>
                        <hr>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-lg-3"></div>
    </div>

    <div class="row">
        <div class="col-lg-3 col-lg-3"></div>

        <div class="col-lg-6 col-lg-6">
            <a href="{{route('admin.manage.form')}}" class="w3-text-black">
                <div class="w3-card-4 w3-teal w3-margin w3-padding-large w3-hover-opacity">
                    <div class="w3-container">
                        <div class="row">
                            <i class="fa fa-pie-chart fa-fw w3-xxlarge"></i>
                            <strong>Manage Survey</strong>
                            <span class="w3-right"><i class="fa fa-external-link-square fa-fw"></i></span>
                        </div>
                        <hr>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-lg-3"></div>
    </div>
</div>
@endsection
