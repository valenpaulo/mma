@extends('layout/layout')

@section('title')
Admin Panel
@endsection

@section('content')

<div class="container-fluid">

<div class="w3-container w3-text-black w3-center">
  <a href="{{route('home')}}">
  <img src="{{asset('public/images/logo/mtics.png')}}" class="w3-margin-bottom w3-margin-top" style="height: 100px; width: 100px"></a>
  <h4>Manila Technician Institute Computer Society</h4>

</div>


<div class="container" align="center">

        @can('docu-only')
        <a class=" w3-text-teal w3-xlarge" href="{{route('admin.document')}}" title="Manage Documents"><i class="fa fa-file fa-fw"></i></a>
        @endcan

        @can('internal-only')
        <a class=" w3-text-teal w3-xlarge" href="{{route('admin.internal')}}" title="Manage Letters"><i class="fa fa-file-word-o fa-fw"></i></a>
        @endcan

        @can('logistics-only')
        <a class=" w3-text-teal w3-xlarge" href="{{url('admin/logistics/inventory')}}" title="Manage Post"><i class="fa fa-database fa-fw"></i></a>
        @endcan

        @can('finance-only')
        <a class=" w3-text-teal w3-xlarge" href="{{route('admin.manage.fund')}}" title="Manage Payment"><i class="fa fa-money fa-fw"></i></a>
        @endcan

        @can('admin-only')
        <a class="w3-text-teal w3-xlarge" href="{{route('admin.manage.form')}}" title="Manage Form"><i class="fa fa-file-text-o fa-fw"></i></a>

        <a class=" w3-text-teal w3-xlarge" href="{{route('admin.manage.postsfeed')}}" title="Manage Post"><i class="fa fa-newspaper-o fa-fw"></i></a>
        @endcan
</div>
<hr style="border: 0.5px solid gray ">

<div class="container-fluid w3-margin-right">
  <div class="row">
    <div class="col-sm-8"></div>
    <div class="col-sm-4">
    @if($errors->any())
      @foreach ($errors->all() as $error)
      <div class="alert alert-danger alert-dismissable fade in w3-right col-sm-12" id="success-alert">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>
      {{$error}}
      </strong>
      </div>
      @endforeach
    @endif
    </div>
  </div>
</div>
<br>

@yield('middle')
@yield('other')

</div>

@endsection



