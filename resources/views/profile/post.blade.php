@extends('profile')

@section('middle')
<?php use Illuminate\Support\Facades\Auth; ?>

<style type="text/css">
  .no-border, .no-border:focus {
    border: 0;
    box-shadow: none;
    outline: none; /* You may want to include this as bootstrap applies these styles too */
  }

  .input-lg {
    font-size: 32px
  }
</style>

<script src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/js/tinymce/js/tinymce/tinymce.min.js') : asset('js/tinymce/js/tinymce/tinymce.min.js')}}"></script>
<script>tinymce.init({selector:'textarea', plugins: "autoresize"});</script>

<div class="w3-card-4 w3-white w3-container w3-padding w3-margin">
  <br>
  <form action="{{ url('member/profile/post/add') }}" method="post" enctype="multipart/form-data">

    <div class="form-group" align="right">
      <label id="post-image-file-name" for="image" style="cursor: pointer;" class="w3-center"><i class="fa fa-file-image-o fa-fw w3-text-deep-orange w3-large w3-padding" data-toggle="tooltip" title="Upload Featured Photo" data-placement="bottom"></i></label>
      <input type="file" name="image" id="image" accept="image/*" style="opacity: 0; position: absolute; z-index: -1;" onchange="showFileName1.call(this, event)" />

      <i class="fa fa-ellipsis-v fa-fw"></i>

       <button type="submit" class="btn btn-default w3-green"><i class="fa fa-pencil-square-o fa-fw w3-large"></i> Publish</button>
    </div>

    <div class="form-group postImageDiv" align="center" style="display: none;">
      <img src="" style="width: 50%;" id="postImageContainer">
    </div>

    <hr>

    <div class="row">
      <div class="form-group">
        <div class="col-lg-8 col-md-8">
          <strong>
            <input type="text" name="title" id="title" class="form-control no-border input-lg w3-text-black" placeholder="Title" required>
          </strong>
        </div>

        <div class="col-lg-4 col-md-4">
          <select class="form-control" id="post_category" name="post_category">
            <option class="w3-center"> -- Select Category -- </option>
            @foreach($Post_Categories as $category)
            <option value="{{$category->id}}">{{$category->type}}</option>
            @endforeach
          </select>
        </div>

      </div>

    </div>

    <hr>

    <div class="form-group">
      <textarea rows="2" placeholder="what's on your mind?" name="body" class="form-control"></textarea>
    </div>
    <!-- <div class="form-group">
     <input type="file" id="image" name="image" accept="image/*" >
    </div> -->
    <hr>

    <div class="form-group" align="right">

      <input type="hidden" name="_token" value="{{ csrf_token() }}">


    </div>

    <input type="hidden" name="_token" value="{{ csrf_token() }}">
  </form>

</div>

@if ($posts)
@foreach($posts as $post)
<div class="w3-card-4 w3-white w3-round w3-margin w3-padding"><br>
  <div class="w3-container">
    <div class="row w3-padding-small">
      <img src="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.auth::user()->avatar) : asset('images/'.auth::user()->avatar) }}" alt="Avatar" class="w3-left w3-circle w3-margin-right w3-hide-small" style="width: 60px; height: 60px">

      <img src="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.auth::user()->avatar) : asset('images/'.auth::user()->avatar) }}" alt="Avatar" class="w3-left w3-circle w3-margin-right w3-hide-large" style="width: 50px; height: 50px">

      <span class="w3-dropdown-hover w3-right">
        <a href="javascript:void(0)" class="w3-text-gray"><i class="fa fa-sort-down fa-fw w3-large"></i></a>


        <div class="w3-dropdown-content w3-card-4 panel panel-default w3-animate-zoom" align="left" style="right: 0">
          <a href="{{url('member/profile/post/'.$post->id.'/'.$post->slug.'/edit')}}" class="w3-text-gray w3-hover-teal"><i class="fa fa-pencil-square-o fa-fw w3-large"></i>Edit Post</a>

          <a href="javascript:void(0)" onclick="deletePost( {{$post->id}} )" class="w3-text-gray w3-hover-teal" style="outline: 0px"><i class="fa fa-trash-o fa-fw w3-large"></i>Delete Post</a>
        </div>

      </span>

      <span>
        <strong>{{auth::user()->first_name}} {{auth::user()->last_name}}</strong>
        <br>
        {{$post->created_at}}
      </span>

    </div>

    <!-- DELETE POST MODAL -->
    <form action="{{ url('member/profile/post/'.$post->id.'/delete' ) }}" id="formPost-{{$post->id}}" method="POST" enctype="multipart/form-data">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
    </form>
    <!-- END DELETE POST MODAL -->

    <hr>

    <div class="row">
      <img src="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$post->image) : asset('images/'.$post->image) }}" style="width:100%" alt="Northern Lights" class="w3-margin-bottom">

      <a href="{{url('/member/profile/post/'.$post->id.'/'.$post->slug)}}">
        <h3 class="w3-text-black"><strong>{{$post->title}}</strong></h3>
      </a>
    </div>

    <div class="row w3-margin-top" align="justify">
      <p>
        <?php
        $string = strip_tags($post->body);

        if (strlen($string) > 300) {

            // truncate string
            $stringCut = substr($string, 0, 300);

            // make sure it ends in a word so assassinate doesn't become ass...
            $string = substr($stringCut, 0, strrpos($stringCut, ' '))."... <a href=".'/member/profile/post/'.$post->id.'/'.$post->slug." class='w3-text-gray'> more</a>";
        }
        echo $string;
        ?>
      </p>
    </div>

  </div>
</div>
@endforeach
@endif

<script type="text/javascript">
function deletePost( id ) {
  swal({
  title: "Are you sure?",
  text: "Once deleted, you will not be able to undo this action!",
  icon: "warning",
  buttons: true,
  dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      $("#formPost-" + id).submit();
    }
  });
}
</script>

<script>
  function showFileName1( event ) {

    var infoArea = document.getElementById( 'post-image-file-name' );
    // the change event gives us the input it occurred in
    var input = event.srcElement;

    // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
    var fileName = input.files[0].name;

    // use fileName however fits your app best, i.e. add it into a div
    $('#post-image-file-name').html("<i class='fa fa-file-image-o fa-fw w3-text-deep-orange w3-large'></i> " + fileName);

    $(".postImageDiv").show();

    var reader = new FileReader();
    reader.onload = function (e) {
      $("#postImageContainer").attr('src', e.target.result);
    };

    reader.readAsDataURL(input.files[0]);
  };
</script>

@endsection
