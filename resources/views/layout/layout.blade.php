<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/css/w3.css') : asset('css/w3.css') }}">
    <link rel="stylesheet" href="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/css/bootstrap.min.css') : asset('css/bootstrap.min.css') }}">

    <link href="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/css/fullcalendar.css') : asset('css/fullcalendar.css') }}" rel='stylesheet' />

    <link rel="stylesheet" href="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/css/fullcalendar.min.css') : asset('css/fullcalendar.min.css')}}"/>

    <link rel="stylesheet" href="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/css/font-awesome.min.css') : asset('css/font-awesome.min.css') }}"/>

    <link rel="stylesheet" href="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/css/font-awesome.min.css') : asset('css/font-awesome.min.css') }}">

    <link rel="stylesheet" href="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/DataTables/datatables.css') : asset('DataTables/datatables.css') }}">

    <link rel="shortcut icon" href="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/logo/mtics.ico') : asset('images/logo/mtics.ico')}}" />

    <!-- RIGHT MODAL -->
    <style type="text/css">
      .modal.right .modal-dialog-right {
        position: fixed;
        margin: auto;
        width: 320px;
        height: 100%;
        overflow-y: initial;
        -webkit-transform: translate3d(0%, 0, 0);
            -ms-transform: translate3d(0%, 0, 0);
             -o-transform: translate3d(0%, 0, 0);
                transform: translate3d(0%, 0, 0);
      }

      .modal.right .modal-content-right {
        height: 100%;
      }

      .modal.right .modal-body-right {
        padding: 15px 15px 80px;
        overflow-y: auto;
      }

      @media only screen and (max-height: 320px) {
        .modal.right .modal-body-right {
          height: 60%;
        }
      }

      @media (min-height: 321px) {
        .modal.right .modal-body-right {
          height: 65%;
        }
      }

      @media (min-height: 381px) {
        .modal.right .modal-body-right {
          height: 70%;
        }
      }

      @media (min-height: 446px) {
        .modal.right .modal-body-right {
          height: 75%;
        }
      }

      @media (min-height: 531px) {
        .modal.right .modal-body-right {
          height: 80%;
        }
      }

      @media (min-height: 730px) {
        .modal.right .modal-body-right {
          height: 85%;
        }
      }

      @media (min-height: 1024px) {
        .modal.right .modal-body-right {
          height: 88%;
        }
      }

      @media (min-height: 1366px) {
        .modal.right .modal-body-right {
          height: 90%;
        }
      }

      .modal.right.fade .modal-dialog-right {
        right: -320px;
        -webkit-transition: opacity 0.3s linear, right 0.3s ease-out;
           -moz-transition: opacity 0.3s linear, right 0.3s ease-out;
             -o-transition: opacity 0.3s linear, right 0.3s ease-out;
                transition: opacity 0.3s linear, right 0.3s ease-out;
      }

      .modal.right.fade.in .modal-dialog-right {
        right: 0;
      }

      .modal-content-right {
        border-radius: 0;
        border: none;
      }
    </style>
    <!-- END RIGHT MODAL -->

    <!-- PRELOADER -->
    <style type="text/css">
      .no-js #loader { display: none;  }
      .js #loader { display: block; position: absolute; left: 100px; top: 0; }
      .se-pre-con {
        position: absolute;
        background-position: top center;
        margin: 0;
        left: 38%;
        top: 25%;
        width: 25%;
        opacity: 0.5;
        float: center;
        -webkit-animation: spin 4s linear infinite;
        animation: spin 4s linear infinite;
      }

      @-webkit-keyframes spin {
        0% { -webkit-transform: rotate(0deg); }
        100% { -webkit-transform: rotate(360deg); }
      }

      @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
      }
    </style>
    <!-- END PRELOADER -->

    <style type="text/css">
      @media only screen and (max-width: 1365px) {
        .navbar-small-medium-screen {
          display: block;
          background-color: #F8F8F8;
        }

        .navbar-large-screen {
          display: none;
        }
      }

      @media (min-width: 1366px) {
        .navbar-small-medium-screen {
          display: none;
        }

        .navbar-large-screen {
          display: block;
          background-color: #F8F8F8;
        }
      }
    </style>

    <script src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/fullcalendar/lib/moment.min.js') : asset('fullcalendar/lib/moment.min.js')}}"></script>
    <script src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/fullcalendar/lib/jquery.min.js') : asset('fullcalendar/lib/jquery.min.js')}}"></script>
    <script src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/fullcalendar/lib/jquery-ui.min.js') : asset('fullcalendar/lib/jquery-ui.min.js')}}"></script>
    <script src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/fullcalendar/fullcalendar.min.js') : asset('fullcalendar/fullcalendar.min.js')}}"></script>
    <script src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/DataTables/datatables.js') : asset('DataTables/datatables.js')}}"></script>
    <script src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/js/sweetalert.min.js') : asset('js/sweetalert.min.js')}}"></script>

    <script>
    $(window).on('load', function() {

    // Animate loader off screen
    $(".se-pre-con").fadeOut("slow");
    $('#main-body').fadeIn("slow").css("display","block");
    $('#sub-body').fadeIn("slow").css("visibility","visible");
    });
    </script>

    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

    <title>@yield('title')</title>
</head>

<body class="w3-white">

<!-- NOTIFICATIONS MODAL -->
<div class="modal right fade" id="notifications" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog-right" role="document">
    <div class="modal-content-right w3-black">

      <div class="modal-header">

        <a href="javascript:void(0)" class="w3-right w3-text-white w3-padding-small" data-dismiss="modal" aria-label="Close"><i class="fa fa-arrow-right"></i>
        </a>

        <h5 class="modal-title w3-text-white" id="myModalLabel">
          <i class="fa fa-bell w3-text-amber w3-xlarge"></i>
          <span class="w3-large">
            <strong>
              Notifications
            </strong>
          </span>
          @if (count(Auth::user()->unreadNotifications) > 0)<span class="w3-badge w3-red">{{count(Auth::user()->unreadNotifications)}}</span>@endif
        </h5>
      </div>

      <div class="modal-body-right w3-black w3-bar-block">
        <!-- UNREAD -->

        @if (count(Auth::user()->unreadNotifications) > 0)

          @foreach(Auth::user()->unreadNotifications as $notification)

            @if ($notification->data['origin'] == 'privilege_removed' || $notification->data['origin'] == 'event_cancelled')

              <a href="javascript:void(0)" class="w3-text-white w3-bar-item w3-button w3-hover-pale-green">
              <i class="fa fa-envelope fa-fw w3-text-green"></i> {{$notification->data['data']}}
              <br>
              {{$notification->created_at}}
              </a>

            @else

              <a href="{{url('read-notification/' . $notification->data['origin'] . '/' . $notification->id)}}" class="w3-text-white w3-bar-item w3-button w3-hover-pale-green">
              <i class="fa fa-envelope fa-fw w3-text-green"></i> {{$notification->data['data']}}
              <br>
              {{$notification->created_at}}
              </a>

            @endif

            <hr style="margin-top: 0px; margin-bottom: 0px">

          @endforeach

        @endif

        <!-- READ -->
        @foreach(Auth::user()->readNotifications as $notification)

        @if ($notification->data['origin'] == 'privilege_removed' || $notification->data['origin'] == 'event_cancelled')

          <a href="javascript:void(0)" class="w3-text-white w3-bar-item w3-button w3-hover-pale-red">
          <i class="fa fa-envelope-open fa-fw w3-text-deep-orange"></i> {{$notification->data['data']}}
          <br>
          {{$notification->created_at}}
          </a>

        @else

          <a href="{{url('read-notification/' . $notification->data['origin'] . '/' . $notification->id)}}" class="w3-text-white w3-bar-item w3-button w3-hover-pale-red">
          <i class="fa fa-envelope-open fa-fw w3-text-deep-orange"></i> {{$notification->data['data']}}
          <br>
          {{$notification->created_at}}
          </a>

        @endif

        <hr style="margin-top: 0px; margin-bottom: 0px">
        @endforeach
      </div>

      <div class="modal-footer w3-text-white">
        <h5>
          <a href="{{url('read-notification/all')}}" class="w3-text-green"><i class="fa fa-check-circle fa-fw"></i>Mark All As Read</a>
        </h5>
      </div>

    </div>
  </div>
</div>
<!-- END NOTIFICATIONS MODAL -->

<!-- navbar large screen -->
<div class="container-fluid w3-top navbar-fixed-top navbar-large-screen">
  <div class="row">
    <div align="left" class="col-lg-6 w3-margin-bottom">
      <a href="{{url('/')}}">
        <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/logo/mtics_logo.png') : asset('images/logo/mtics_logo.png')}}" align="center" style="width: 280px;">
      </a>
    </div>

    <div align="right" class="w3-padding-large w3-margin-top">
      @if(Gate::allows('admin-only'))
      <div class="w3-dropdown-hover">

        <div style="background-color: #F8F8F8;">
            <i class="fa fa-th fa-fw w3-text-gray w3-padding"></i>
        </div>

        <div class="w3-dropdown-content w3-card-4 w3-animate-zoom" style="width: 230px; right: 0">
          <div class="w3-row" align="center">
            <a href="{{url('member/dashboard')}}" class="w3-hover-opacity w3-text-white col-sm-6 w3-padding-large w3-red"><i class="fa fa-dashboard fa-fw w3-xlarge"></i>
              <p>Dashboard</p>
            </a>
            <a href="{{url('admin/additional-feature')}}" class="w3-hover-opacity w3-text-white col-sm-6 w3-padding-large w3-amber"><i class="fa fa-superpowers fa-fw w3-xlarge"></i>
              <p>Add-ons</p>
            </a>
          </div>

          <div class="w3-row" align="center">
            <a href="{{url('admin/')}}" class="w3-hover-opacity w3-text-white col-sm-6 w3-padding-large w3-green"><i class="fa fa-id-badge fa-fw w3-xlarge"></i>
              <p>Admin</p>
            </a>
            <a href="{{url('admin/settings')}}" class="w3-hover-opacity w3-text-white col-sm-6 w3-padding-large w3-blue"><i class="fa fa-cogs fa-fw w3-xlarge"></i>
              <p>Settings</p>
            </a>
          </div>
        </div>
      </div>
      @endif

      <a data-target="#notifications" href="javascript:void(0)" data-toggle="modal" style="outline: none"><i class="fa fa-bell-o w3-text-gray fa-fw w3-padding"></i>@if (count(Auth::user()->unreadNotifications) > 0)<span class="w3-badge w3-red">  {{count(Auth::user()->unreadNotifications)}}</span>@endif</a>

      <div class="w3-dropdown-hover">
        <a href="javascript:void(0)" class="w3-text-gray"><i class="fa fa-user-o fa-fw w3-text-gray w3-padding"></i> {{Auth::user()->username}}<i class="fa fa-sort-down fa-fw"></i></a>

        <div class="w3-dropdown-content w3-card-4 panel panel-default w3-animate-zoom" style="right: 0; width: 325px" align="left">

          <div class="panel-body">
            <div class="container-fluid">
              <a href="{{url('profile/')}}" class="w3-text-black w3-hover-opacity w3-hover-white">

                <div class="row" align="center">
                  <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.Auth::user()->avatar) : asset('images/'.Auth::user()->avatar)}}" class="img-circle" style="width: 100px; height: 100px">

                </div>
                <div class="row" align="center">
                  <h5>{{Auth::user()->first_name}} {{Auth::user()->last_name}}</h5>
                  {{Auth::user()->id_num}}<br>
                  {{Auth::user()->email}}

                </div>
              </a>

              <hr>

              <div class="row">
                <a href="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? url('public/documents/bylaws/mtics_bylaws.pdf') : url('documents/bylaws/mtics_bylaws.pdf')}}" target="_blank" class="w3-hover-teal w3-bar-item w3-hover-text-white w3-text-black"><i class="fa fa-book fa-fw w3-large"></i> MTICS Bylaws</a>

                <a href="{{url('member/android-download')}}" class="w3-hover-teal w3-bar-item w3-hover-text-white w3-text-black"><i class="fa fa-android fa-fw w3-large"></i> Download apk</a>

                <a href="{{ url('/member/logout') }}" class="w3-hover-teal w3-bar-item w3-hover-text-white w3-text-black"><i class="fa fa-sign-out fa-fw w3-large"></i> Logout</a>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>

  </div>
</div>
<!-- end navbar large screen -->

<!-- navbar small screen -->
<div class="container-fluid w3-top navbar-fixed-top navbar-small-medium-screen">

  <div class="row">
    @if(route::current()->getName() == 'profile.post')
      <a href="#edit" data-toggle="modal" style="outline: none;"><i class="fa fa-navicon w3-text-gray fa-fw w3-padding w3-left w3-hide-large" style="margin-top: 10px"></i></a>
    @endif

    <a href="{{url('/')}}" class="w3-text-gray">
      <span class="w3-left w3-padding w3-xlarge">
        <strong>MTICS</strong>
      </span>
    </a>

    <div align="right" class="w3-padding-large">

      @if(Gate::allows('admin-only'))
      <div class="w3-dropdown-hover">

        <div style="background-color: #F8F8F8;">
            <i class="fa fa-th fa-fw w3-text-gray w3-padding"></i>
        </div>

        <div class="w3-dropdown-content w3-card-4 w3-animate-zoom" style="width: 200px; ">
          <div class="w3-row" align="center">
            <a href="{{url('member/dashboard')}}" class="w3-hover-opacity w3-text-white col-sm-6 w3-padding-large w3-red"><i class="fa fa-dashboard fa-fw w3-xlarge"></i>
              <p>Dashboard</p>
            </a>
            <a href="{{url('admin/additional-feature')}}" class="w3-hover-opacity w3-text-white col-sm-6 w3-padding-large w3-amber"><i class="fa fa-superpowers fa-fw w3-xlarge"></i>
              <p>Add-ons</p>
            </a>
          </div>

          <div class="w3-row" align="center">
            <a href="{{url('admin/')}}" class="w3-hover-opacity w3-text-white col-sm-6 w3-padding-large w3-green"><i class="fa fa-id-badge fa-fw w3-xlarge"></i>
              <p>Admin</p>
            </a>
            <a href="{{url('admin/settings')}}" class="w3-hover-opacity w3-text-white col-sm-6 w3-padding-large w3-blue"><i class="fa fa-cogs fa-fw w3-xlarge"></i>
              <p>Settings</p>
            </a>
          </div>
        </div>
      </div>
      @endif

      <a data-target="#notifications" href="javascript:void(0)" data-toggle="modal" style="outline: none"><i class="fa fa-bell-o w3-text-gray fa-fw w3-padding"></i>@if (count(Auth::user()->unreadNotifications) > 0)<span class="w3-badge w3-red">  {{count(Auth::user()->unreadNotifications)}}</span>@endif</a>

      <div class="w3-dropdown-hover">
        <a href="javascript:void(0)" class="w3-text-gray"><i class="fa fa-user-o fa-fw w3-text-gray w3-padding"></i> {{Auth::user()->username}}<i class="fa fa-sort-down fa-fw"></i></a>

        <div class="w3-dropdown-content w3-card-4 panel panel-default w3-animate-zoom" style="right: 0; width: 270px;" align="left">

          <div class="panel-body">
            <div class="container-fluid">
              <a href="{{url('profile/')}}" class="w3-text-black w3-hover-opacity w3-hover-white">

                <div class="row" align="center">
                  <img src="{{url('/') == 'http://http://mtics-ma.tuptaguig.com/' ? asset('public/images/' . Auth::user()->avatar) : asset('images/' . Auth::user()->avatar)}}" class="img-circle" style="width: 100px; height: 100px">
                </div>
                <div class="row" align="center">
                  <h5>{{Auth::user()->first_name}} {{Auth::user()->last_name}}</h5>
                </div>
              </a>

              <hr>

              <div class="row">
                <a href="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? url('public/documents/bylaws/mtics_bylaws.pdf') : url('documents/bylaws/mtics_bylaws.pdf')}}" target="_blank" class="w3-hover-teal w3-bar-item w3-hover-text-white w3-text-black"><i class="fa fa-book fa-fw w3-large"></i> MTICS Bylaws</a>

                <a href="{{url('member/android-download')}}" class="w3-hover-teal w3-bar-item w3-hover-text-white w3-text-black"><i class="fa fa-android fa-fw w3-large"></i> Download apk</a>

                <a href="{{ url('/member/logout') }}" class="w3-hover-teal w3-bar-item w3-hover-text-white w3-text-black"><i class="fa fa-sign-out fa-fw w3-large"></i> Logout</a>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>

  </div>
</div>
<!-- end navbar large screen -->


<div class="content" id="main-body" style="max-width:1500px; display: none;">
    @yield('content')
</div>

<div class="back">
  <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/logo/mtics.png') : asset('images/logo/mtics.png')}}" class="se-pre-con">
</div>

@yield('script')

<script>
  $(document).ready(function(){
      $('[data-toggle="tooltip"]').tooltip();
  });
</script>

<script type="text/javascript">
  $(window).on('load', function() {
    if("{{session('success')}}" != ""){
      swal("{{session('success')}}", "Congratulations!", "success")
    }

    if("{{session('warning')}}" != ""){
      swal("{{session('warning')}}", "Oops!", "warning")
    }
  });

</script>



<script src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/js/bootstrap.min.js') : asset('js/bootstrap.min.js')}}"></script>

</body>

</html>
