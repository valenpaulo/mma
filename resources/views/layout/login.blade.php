<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/css/w3.css') : asset('css/w3.css') }}">
    <link rel="stylesheet" href="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/css/bootstrap.min.css') : asset('css/bootstrap.min.css') }}">

    <link rel="stylesheet" href="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/css/font-awesome.min.css') : asset('css/font-awesome.min.css') }}">
    <link rel="shortcut icon" href="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/logo/mtics.ico') : asset('images/logo/mtics.ico')}}" />

    <script src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/fullcalendar/lib/jquery.min.js') : asset('fullcalendar/lib/jquery.min.js')}}"></script>
    <script src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('js/bootstrap.min.js') : asset('fullcalendar/lib/jquery.min.js')}}"></script>

    <script src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/js/sweetalert.min.js') : asset('js/sweetalert.min.js')}}"></script>


    <title>@yield('title')</title>
</head>

<body class="w3-light-gray">

    <div class="content">
        @yield('content')
    </div>

</body>

</html>
