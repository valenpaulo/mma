@extends('layout/layout')

@section('title')
Control Panel
@endsection

@section('content')

<style type="text/css">

  .side {
    background-color: #F8F8F8;
    position: fixed;
    overflow-y: scroll;
    bottom: 0;
  }

  @media only screen and (max-width: 1365px) {
    .main {
      margin-top: 60px;
    }

    .side {
      top: 54px;
    }
  }

  @media (min-width: 1366px) {
    .main {
      margin-top: 105px;
    }

    .side {
      top: 105px;
    }
  }
</style>

<!-- start container -->
<div class="w3-container-fluid main">
<div class="row">
<!-- 1st column -->
<div class="col-lg-2 col-md-2">
  <div class="w3-card-4 w3-blue-gray col-lg-2 col-md-2 w3-hide-small w3-hide-medium side">
    <div class="w3-container-fluid">
      <div class="row w3-center">
        <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.Auth::user()->avatar) : asset('images/'.Auth::user()->avatar)}}" class="w3-margin-bottom " style="width: 100%;">
        <h4>{{Auth::user()->first_name}} {{Auth::user()->last_name}}</h4>
        @foreach(Auth::user()->role as $role)
          <p>{{$role->display_name}}</p>
        @endforeach
      </div>

      <hr>

      <div class="row  w3-margin">
        <a href="{{url('admin/manage-event')}}" class="w3-text-white">
          <label class="w3-hover-opacity">
          <i class="fa fa-calendar-o fa-fw w3-xlarge"></i> <strong>Event</strong>
          </label>
        </a>

      </div>

      @if(Gate::allows('president-only'))
      <div class="row  w3-margin">

        <a class="w3-text-white" href="{{route('admin.president')}}">
          <p class="w3-hover-opacity"><i class="
          @if (\Route::current()->getName() == 'admin.president')
            fa fa-circle fa-fw w3-text-green
          @else
            fa fa-circle-o fa-fw w3-text-white
          @endif
          "></i> Breakdown Request</p>
        </a>

      </div>
      @endif

      <hr>
      <div class="row  w3-margin">
        <label><i class="fa fa-superpowers fa-fw w3-xlarge"></i> <strong>Added Feature</strong></label>
      </div>

      <div class="row w3-margin">
      @if(Gate::allows('superadmin-only'))

        <a class="w3-text-white" href="{{route('admin.manage.user')}}">
          <p class="w3-hover-opacity"><i class="
          @if (\Route::current()->getName() == 'admin.manage.user')
            fa fa-circle fa-fw w3-text-green
          @else
            fa fa-circle-o fa-fw w3-text-white
          @endif
          "></i> Manage Member</p>
        </a>

        <a class="w3-text-white" href="{{route('admin.manage.privilege')}}">
          <p class="w3-hover-opacity"><i class="
          @if (\Route::current()->getName() == 'admin.manage.privilege')
            fa fa-circle fa-fw w3-text-green
          @else
            fa fa-circle-o fa-fw w3-text-white
          @endif
          "></i> Manage Privilege</p>
        </a>

        <a class="w3-text-white" href="{{route('admin.manage.section')}}">
          <p class="w3-hover-opacity"><i class="
          @if (\Route::current()->getName() == 'admin.manage.section')
            fa fa-circle fa-fw w3-text-green
          @else
            fa fa-circle-o fa-fw w3-text-white
          @endif
          "></i> Manage Section</p>
        </a>

      @endif

      @if(Gate::allows('accntreq_approval-only'))

        <a class="w3-text-white" href="{{route('admin.request.account')}}">
          <p class="w3-hover-opacity"><i class="
          @if (\Route::current()->getName() == 'admin.request.account')
            fa fa-circle fa-fw w3-text-green
          @else
            fa fa-circle-o fa-fw w3-text-white
          @endif
          "></i> Manage Request</p>
        </a>

      @endif

      @if(Gate::allows('ReceivePayment-only') or Gate::allows('superadmin-only'))

        <a class="w3-text-white" href="{{route('admin.manage.payment')}}">
          <p class="w3-hover-opacity"><i class="
          @if (\Route::current()->getName() == 'admin.manage.payment')
            fa fa-circle fa-fw w3-text-green
          @else
            fa fa-circle-o fa-fw w3-text-white
          @endif
          "></i> Collect Payment</p>
        </a>

      @endif

      @if(Gate::allows('BorrowReturn-only'))

        <a class="w3-text-white" href="{{route('admin.borrow.return')}}">
          <p class="w3-hover-opacity"><i class="
          @if (\Route::current()->getName() == 'admin.borrow.return')
            fa fa-circle fa-fw w3-text-green
          @else
            fa fa-circle-o fa-fw w3-text-white
          @endif
          "></i> Manage Borrow/Return</p>
        </a>

      @endif

      @if(Gate::allows('mticsadviser-only'))
        <a class="w3-text-white" href="{{route('admin.turn-over')}}">
          <p class="w3-hover-opacity"><i class="
          @if (\Route::current()->getName() == 'admin.turn-over')
            fa fa-circle fa-fw w3-text-green
          @else
            fa fa-circle-o fa-fw w3-text-white
          @endif
          "></i> Turn Over</p>
        </a>
      @endif

      @if(Gate::allows('superadmin-only'))
        <a class="w3-text-white" href="{{route('admin.backup')}}">
          <p class="w3-hover-opacity"><i class="
          @if (\Route::current()->getName() == 'admin.backup')
            fa fa-circle fa-fw w3-text-green
          @else
            fa fa-circle-o fa-fw w3-text-white
          @endif
          "></i> Backup & Restore</p>
        </a>
      @endif

      </div>


      <hr>

      <div class="row  w3-margin">
        <label><i class="fa fa-id-badge fa-fw w3-xlarge"></i> <strong>Admin Panel</strong></label>
      </div>

      <div class="row w3-margin">
      @if(Gate::allows('president-only'))
        <a class="w3-text-white" href="{{route('pres.manage.budget')}}">
          <p class="w3-hover-opacity"><i class="
          @if (\Route::current()->getName() == 'pres.manage.budget')
            fa fa-circle fa-fw w3-text-green
          @else
            fa fa-circle-o fa-fw w3-text-white
          @endif
          "></i> Budget Request</p>
        </a>

        <a class="w3-text-white" href="{{url('admin/president/purchased-item-reported')}}">
          <p class="w3-hover-opacity"><i class="
          @if (\Route::current()->getName() == 'purchased.item.issue')
            fa fa-circle fa-fw w3-text-green
          @else
            fa fa-circle-o fa-fw w3-text-white
          @endif
          "></i> Purchased Item Issues</p>
        </a>


        <a class="w3-text-white" href="{{url('admin/president/bank-transaction-request-reported')}}">
          <p class="w3-hover-opacity"><i class="
          @if (\Route::current()->getName() == 'bank.trans.issue')
            fa fa-circle fa-fw w3-text-green
          @else
            fa fa-circle-o fa-fw w3-text-white
          @endif
          "></i> Bank Transaction Issues</p>
        </a>

        <a class="w3-text-white" href="{{route('admin.activities')}}">
          <p class="w3-hover-opacity"><i class="
          @if (\Route::current()->getName() == 'admin.activities')
            fa fa-circle fa-fw w3-text-green
          @else
            fa fa-circle-o fa-fw w3-text-white
          @endif
          "></i> Calendar</p>
        </a>
      @endif

      @if(Gate::allows('activity-only'))

        <a class="w3-text-white" href="{{route('admin.activities')}}">
          <p class="w3-hover-opacity"><i class="
          @if (\Route::current()->getName() == 'admin.activities')
            fa fa-circle fa-fw w3-text-green
          @else
            fa fa-circle-o fa-fw w3-text-white
          @endif
          "></i> Calendar</p>
        </a>

      @endif

      @if(Gate::allows('info-only'))

        <a class="w3-text-white" href="{{route('admin.info')}}">
          <p class="w3-hover-opacity"><i class="
          @if (\Route::current()->getName() == 'admin.info')
            fa fa-circle fa-fw w3-text-green
          @else
            fa fa-circle-o fa-fw w3-text-white
          @endif
          "></i> Send SMS</p>
        </a>

      @endif

      @if(Gate::allows('finance-only'))

        <a class="w3-text-white" href="{{route('admin.manage.fund')}}">
          <p class="w3-hover-opacity"><i class="
          @if (\Route::current()->getName() == 'admin.manage.fund')
            fa fa-circle fa-fw w3-text-green
          @else
            fa fa-circle-o fa-fw w3-text-white
          @endif
          "></i> Manage Payment</p>
        </a>

        <a class="w3-text-white" href="{{url('admin/finance/manage-mtics-budget')}}">
          <p class="w3-hover-opacity"><i class="
          @if (\Route::current()->getName() == 'admin.manage.budget')
            fa fa-circle fa-fw w3-text-green
          @else
            fa fa-circle-o fa-fw w3-text-white
          @endif
          "></i> Manage Budget</p>
        </a>

        <a class="w3-text-white" href="{{url('admin/finance/manage-mtics-budget/expenses')}}">
          <p class="w3-hover-opacity"><i class="
          @if (\Route::current()->getName() == 'admin.manage.expenses')
            fa fa-circle fa-fw w3-text-green
          @else
            fa fa-circle-o fa-fw w3-text-white
          @endif
          "></i> Manage Expenses</p>
        </a>

        <a class="w3-text-white" href="{{url('admin/finance/requested-money')}}">
          <p class="w3-hover-opacity"><i class="
          @if (\Route::current()->getName() == 'finance.request.money')
            fa fa-circle fa-fw w3-text-green
          @else
            fa fa-circle-o fa-fw w3-text-white
          @endif
          "></i> Money Request</p>
        </a>

      @endif

      @if(Gate::allows('finance-only') or Gate::allows('faculty-only'))
      <a class="w3-text-white" href="{{Gate::allows('finance-only') ? url('admin/finance/manage-bank-transaction/mtics') : url('admin/adviser/mtics-bank-transaction-request')}}">
        <p class="w3-hover-opacity"><i class="
        @if (\Route::current()->getName() == 'admin.bank.transaction' or \Route::current()->getName() == 'admin.adviser.mtics.banktrans')
          fa fa-circle fa-fw w3-text-green
        @else
          fa fa-circle-o fa-fw w3-text-white
        @endif
        "></i> Bank Transaction</p>
      </a>
      @endif

      @if(Gate::allows('auditor-only') or Gate::allows('finance-only') or Gate::allows('president-only') or Gate::allows('mticsadviser-only'))

        <a class="w3-text-white" href="{{url('admin/finance/financial-reports')}}">
          <p class="w3-hover-opacity"><i class="
          @if (\Route::current()->getName() == 'view.financial.reports')
            fa fa-circle fa-fw w3-text-green
          @else
            fa fa-circle-o fa-fw w3-text-white
          @endif
          "></i> Financial Reports</p>
        </a>
      @endif

      @if(Gate::allows('docu-only'))

        <a class="w3-text-white" href="{{route('admin.document')}}">
          <p class="w3-hover-opacity"><i class="
          @if (\Route::current()->getName() == 'admin.document')
            fa fa-circle fa-fw w3-text-green
          @else
            fa fa-circle-o fa-fw w3-text-white
          @endif
          "></i> Manage Documents</p>
        </a>

        <a class="w3-text-white" href="{{route('admin.document.bylaws')}}">
          <p class="w3-hover-opacity"><i class="
          @if (\Route::current()->getName() == 'admin.document.bylaws')
            fa fa-circle fa-fw w3-text-green
          @else
            fa fa-circle-o fa-fw w3-text-white
          @endif
          "></i> MTICS By-laws</p>
        </a>

        <a class="w3-text-white" href="{{route('admin.activities')}}">
          <p class="w3-hover-opacity"><i class="
          @if (\Route::current()->getName() == 'admin.activities')
            fa fa-circle fa-fw w3-text-green
          @else
            fa fa-circle-o fa-fw w3-text-white
          @endif
          "></i> Calendar</p>
        </a>

      @endif

      @if(Gate::allows('internal-only'))

        <a class="w3-text-white" href="{{route('admin.internal')}}">
          <p class="w3-hover-opacity"><i class="
          @if (\Route::current()->getName() == 'admin.internal')
            fa fa-circle fa-fw w3-text-green
          @else
            fa fa-circle-o fa-fw w3-text-white
          @endif
          "></i> Manage Letters</p>
        </a>

      @endif

      @if(Gate::allows('logistics-only'))

        <a class="w3-text-white" href="{{route('admin.logistics.inventory')}}">
          <p class="w3-hover-opacity"><i class="
          @if (\Route::current()->getName() == 'admin.logistics.inventory')
            fa fa-circle fa-fw w3-text-green
          @else
            fa fa-circle-o fa-fw w3-text-white
          @endif
          "></i> Manage Inventory</p>
        </a>

        <a class="w3-text-white" href="{{route('admin.manage.purchases')}}">
          <p class="w3-hover-opacity"><i class="
          @if (\Route::current()->getName() == 'admin.manage.purchases')
            fa fa-circle fa-fw w3-text-green
          @else
            fa fa-circle-o fa-fw w3-text-white
          @endif
          "></i> Manage Purchases</p>
        </a>

      @endif

      @if(Gate::allows('auditor-only'))

        <a class="w3-text-white" href="{{route('reimburse.request')}}">
          <p class="w3-hover-opacity"><i class="
          @if (\Route::current()->getName() == 'reimburse.request')
            fa fa-circle fa-fw w3-text-green
          @else
            fa fa-circle-o fa-fw w3-text-white
          @endif
          "></i> Reimburse Request</p>
        </a>
      @endif

      <a class="w3-text-white" href="{{route('admin.request.to.buy')}}">
          <p class="w3-hover-opacity"><i class="
          @if (\Route::current()->getName() == 'admin.request.to.buy')
            fa fa-circle fa-fw w3-text-green
          @else
            fa fa-circle-o fa-fw w3-text-white
          @endif
          "></i> Purchase Request</p>
        </a>

      <a class="w3-text-white" href="{{route('admin.manage.postsfeed')}}">
        <p class="w3-hover-opacity"><i class="
        @if (\Route::current()->getName() == 'admin.manage.postsfeed')
          fa fa-circle fa-fw w3-text-green
        @else
          fa fa-circle-o fa-fw w3-text-white
        @endif
        "></i> Manage Post</p>
      </a>

      <a class="w3-text-white" href="{{route('admin.manage.form')}}">
        <p class="w3-hover-opacity"><i class="
        @if (\Route::current()->getName() == 'admin.manage.form')
          fa fa-circle fa-fw w3-text-green
        @else
          fa fa-circle-o fa-fw w3-text-white
        @endif
        "></i> Manage Form</p>
      </a>

      </div>

      <hr>

      <div class="row  w3-margin">
        <label><i class="fa fa-cogs fa-fw w3-xlarge"></i> <strong>Settings</strong></label>
      </div>

      <div class="row w3-margin">
        <a class="w3-text-white" href="{{route('admin.banner')}}">
          <p class="w3-hover-opacity"><i class="
            @if (\Route::current()->getName() == 'admin.banner')
              fa fa-circle fa-fw w3-text-green
            @else
              fa fa-circle-o fa-fw w3-text-white
            @endif
          "></i> Manage Banner</p>
        </a>

        <a class="w3-text-white" href="{{route('admin.filter.words')}}">
          <p class="w3-hover-opacity"><i class="
            @if (\Route::current()->getName() == 'admin.filter.words')
              fa fa-circle fa-fw w3-text-green
            @else
              fa fa-circle-o fa-fw w3-text-white
            @endif
          "></i> Manage Profanity</p>
        </a>
      </div>
    </div>
  </div>
<!-- end 1st column -->
</div>


<!-- start 2nd column -->
<div class="col-lg-10 col-md-10">

<div class="container-fluid w3-margin-right">
  <div class="row">
    <div class="col-sm-8"></div>
    <div class="col-sm-4">
    @if($errors->any())
      @foreach ($errors->all() as $error)
      <div class="alert alert-danger alert-dismissable fade in w3-right col-sm-12" id="success-alert">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>
      {{$error}}
      </strong>
      </div>
      @endforeach
    @endif
    </div>
  </div>
</div>

@yield('middle')
<!-- end 2nd column -->
</div>

<!-- end row -->
</div>
<!-- end container -->
</div>

<script type="text/javascript">
  window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove();
    });
}, 4000);
</script>

@endsection
